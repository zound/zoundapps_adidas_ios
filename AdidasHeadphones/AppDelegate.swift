//
//  AppDelegate.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 15/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSpotifyWrapper
import SwiftSpotifySDK
import RxSwift
import Reachability
import FirebaseMessaging
import UserNotifications

struct SpotifyConfig {
    static let clientId = "afa9c2011a5c4e268c6fa1f80eb6c5c9"
    static let redirectURL = URL(string: "comzoundindustriesadidasheadphones://callback")!
    static let tokenSwapURL = URL(string: "https://adidasheadphones.herokuapp.com/api/token")!
    static let tokenRefreshURL = URL(string: "https://adidasheadphones.herokuapp.com/api/refresh_token")!
    static let userDefaultsKey = "spotifySessionUserDefaultsKey"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?
    var services: ServicesType!
    var appFlowModel: AppFlowModel!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        CloudManager.setupAmazonWebServices()
        let actionButtonHandlers: [ActionButtonEventHandler] = [RxStreamingSpotifyManager.shared,
                                                                GVAEventHandler.shared]
        services = Services(actionButtonHandlers, application)
        services.rxSpotifyManager.rxAuthStatus.subscribe(onNext: { status in
            if case .connected(let accessToken, let refreshToken) = status {
                DefaultStorage.shared.spotifyAccessToken = accessToken
                DefaultStorage.shared.spotifyRefreshToken = refreshToken
            }
        })
        .disposed(by: disposeBag)
        appFlowModel = AppFlowModel(rootViewController: MainNavigationViewController(), services: services)
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController = appFlowModel.rootViewController
        if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            services.quickActionsService.registerToProcess(shortcutItem: shortcutItem, as: .launch)
        }

        return true
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        //Handle the notification
        completionHandler(
           [UNNotificationPresentationOptions.alert,
            UNNotificationPresentationOptions.sound,
            UNNotificationPresentationOptions.badge])
    }

    func applicationWillResignActive(_ application: UIApplication) {
        application.shortcutItems = services.quickActionsService.shortcutItems
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        services.quickActionsService.registerToProcess(shortcutItem: shortcutItem, as: .action)
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return self.services.rxSpotifyManager.application(app, openURL: url, options: options)
    }

    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        CloudManager.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("didRegisterForRemoteNotificationsWithDeviceToken \(deviceToken)")
    }
    
    private var disposeBag = DisposeBag()
}

extension RxStreamingSpotifyManager {
    func configure() -> RxStreamingSpotifyManager {
        let configuration = SpotifyConfiguration(clientId: SpotifyConfig.clientId,
                                                 redirectURL: SpotifyConfig.redirectURL,
                                                 tokenSwapURL: SpotifyConfig.tokenSwapURL,
                                                 tokenRefreshURL: SpotifyConfig.tokenRefreshURL,
                                                 userDefaultsKey: SpotifyConfig.userDefaultsKey)
        let scopes = [
            SPTAuthStreamingScope,
            SPTAuthUserLibraryReadScope,
            SPTAuthPlaylistReadPrivateScope,
            SPTAuthUserFollowReadScope,
            SPTAuthUserReadPrivateScope,
            SPTAuthUserLibraryReadScope
        ]
        return self.configure(configuration: configuration, requestedScopes: scopes)
    }
}


