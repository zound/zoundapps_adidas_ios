//
//  GVAEventHandler.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 08/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import ConnectionService

final class GVAEventHandler: ActionButtonEventHandler {
    static let supportedActions: [ButtonAction] =  [.googleVoiceAssistantBisto]
    static let supportedPressTypes: [PressType] = ActionType.googleAssistant.pressTypes()
    public static let shared = GVAEventHandler()
    private init() {}
    func execute(action: ButtonAction, pressType: PressType, using: ReactiveDevice?) {
        return
    }
    func set(action: ButtonAction, pressType: PressType, using: ReactiveDevice?) {
        return
    }
    func verify(remote: ActionButtonConfiguration, device: ReactiveDevice?) -> Single<ActionButtonConfiguration> {
        return Single<ActionButtonConfiguration>.create { event in
            guard remote.filter({ pressType, buttonAction in
                buttonAction == .googleVoiceAssistantBisto}).count > 0 else {
                    event(.success(remote))
                    return Disposables.create()
            }
            let corrupted = remote.map { pressType, buttonAction -> Bool in
                guard type(of: self).supportedPressTypes.contains(pressType) else {
                    guard buttonAction == .noAction else {
                        return true
                    }
                    return false
                }
                guard buttonAction == .googleVoiceAssistantBisto else {
                    return true
                }
                return false
            }
            .contains(true)
            guard corrupted == false else {
                let verified: ActionButtonConfiguration = [
                    .singlePress: .googleVoiceAssistantBisto,
                    .doublePress: .googleVoiceAssistantBisto,
                    .triplePress: .noAction,
                    .longPress: .googleVoiceAssistantBisto
                ]
                device?.write(actionButtonConfiguration: verified, completion: { _ in
                    event(.success(verified))
                })
                return Disposables.create()
            }
            event(.success(remote))
            return Disposables.create()
        }
    }
    func didSelect(type: ActionType, device: ReactiveDevice?) {}
}
