//
//  FirmwareManager.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 01/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

struct AvailableFirmware: Decodable {
    struct Data: Decodable {
        let name: String
        let size: String
        let md5: String
    }
    let version: String
    let firmware: Data
}

class FirmwareManager {
    static func check(forNew firmwareType: FirmwareType, completion: @escaping (AvailableFirmware?) -> Void) {
        let fileName = AWSDownload.currentFirmwareVersionFile(firmwareType).fileName
        AWSDownload.currentFirmwareVersionFile(firmwareType).execute(fileName) { downloadedFileURL in
            if let downloadedFileURL = downloadedFileURL {
                do {
                    let data = try Data(contentsOf: downloadedFileURL)
                    let availableFirmware = try JSONDecoder().decode(AvailableFirmware.self, from: data)
                    completion(availableFirmware)
                } catch {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    static func downloadedFirmwareURL(for type: FirmwareType, with version: String) -> URL? {
        let firmware = AWSDownload.firmware(type, version: version)
        return firmware.exists() ? firmware.downloadingFileURL : nil
    }
    static func get(availableFirmware: AvailableFirmware, type: FirmwareType, completion: @escaping (_ downloadedFirmwareURL: URL?) -> Void) {
        let downloadingFileURL = AWSDownload.currentFirmwareVersionFile(type).downloadingFileURL
        let firmware = AWSDownload.firmware(type, version: availableFirmware.version)
        if let data = try? Data(contentsOf: downloadingFileURL),
            let downloadedAvailableFirmware = try? JSONDecoder().decode(AvailableFirmware.self, from: data),
            downloadedAvailableFirmware.version == availableFirmware.version,
            firmware.exists() {
            completion(firmware.downloadingFileURL)
            return
        }
        firmware.execute(availableFirmware.firmware.name) { downloadedFileURL in
            completion(downloadedFileURL)
        }
    }
    static func purgeAllFirmwareData() {
        FirmwareType.allCases.forEach {
            AWSDownload.currentFirmwareVersionFile($0).purge()
            AWSDownload.firmware($0, version: "").purgeAll()
        }
    }
}
