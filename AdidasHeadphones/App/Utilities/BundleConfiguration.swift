//
//  BundleConfiguration.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 18/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

private struct Const {
    static let bundleConfigurationFileName = "Info"
    static let bundleConfigurationFileExtension = "plist"

}

class BundleConfiguration {
    enum Property: String {
        case firmwareReleaseType
        case appVersion = "CFBundleShortVersionString"
    }
    private let infoPlistFileDictionary: [String: AnyObject]
    init?() {
        guard let pathToInfoPlistFile = Bundle.main.path(forResource: Const.bundleConfigurationFileName, ofType: Const.bundleConfigurationFileExtension),
              let infoPlistFileDictionary = NSDictionary(contentsOfFile: pathToInfoPlistFile) as? [String: AnyObject] else {
            return nil
        }
        self.infoPlistFileDictionary = infoPlistFileDictionary
    }
    func property<T>(_ property: Property) -> T? {
        return infoPlistFileDictionary[property.rawValue.capitalizedFirst] as? T
    }
}

extension BundleConfiguration {
    static var firmwareReleaseType: FirmwareReleaseType {
        guard let bundleConfiguration = BundleConfiguration(),
            let firmwareReleaseTypeRawValue: String = bundleConfiguration.property(.firmwareReleaseType),
            let firmwareReleaseType = FirmwareReleaseType(rawValue: firmwareReleaseTypeRawValue) else {
                return .dev
        }
        return firmwareReleaseType
    }
}

extension BundleConfiguration {
    static var version: String {
         guard let bundleConfiguration = BundleConfiguration(), let version: String = bundleConfiguration.property(.appVersion) else {
            return String()
        }
        return version
    }
}
