//
//  HTTPHelper.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 14/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case patch = "PATCH"
}


extension  URLResponse {
    var code: Int?{
        get{
            return (self as? HTTPURLResponse)?.statusCode
        }
    }
    func printStatus() {
        print(code ?? 0)
    }
}
