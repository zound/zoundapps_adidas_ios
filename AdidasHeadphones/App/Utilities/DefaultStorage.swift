//
//  DefaultStorage.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 08/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService

@objc public final class DefaultStorage: NSObject {
    @objc public static let shared = DefaultStorage()
    @objc public dynamic var welcomePresented: Bool = false
    @objc public dynamic var notificationSettingsPresented: Bool = false
    @objc public dynamic var spotifyAccessToken: String?
    @objc public dynamic var spotifyRefreshToken: String?
    @objc public dynamic var newsletterSubscription: Bool = false
    @objc public dynamic var newsletterSubscriptionEmail: String?
    @objc public dynamic var newsletterAccountID: String?
    @objc public dynamic var anonymousDataPermissions: Bool = false
    @objc public dynamic var googleAssistantHelpPresented: Bool = false


    
    override init() {
        super.init()
        for c in Mirror.init(reflecting: self).children {
            guard let key = c.label else {
                continue
            }
            self.addObserver(self, forKeyPath: key, options: .new, context: nil)
            guard let value = UserDefaults.standard.value(forKey: key) else {
                continue
            }
            self.setValue(value, forKey: key)
        }
    }
    deinit {
        for c in Mirror(reflecting: self).children {
            guard let key = c.label else {
                continue
            }
            self.removeObserver(self, forKeyPath: key)
        }
    }
    @objc public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        for c in Mirror.init(reflecting: self).children {
            guard let key = c.label else {
                continue
            }
            if key == keyPath {
                UserDefaults.standard.set(change?[NSKeyValueChangeKey.newKey], forKey: key)
            }
        }
    }
}

extension DefaultStorage {
    private func set<T: Encodable>(_ value: T?, forKey key: String = #function) {
        if let value = value {
            do {
                let data = try PropertyListEncoder().encode(value)
                UserDefaults.standard.set(data, forKey: key)
            } catch {}
        } else {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
    private func get<T: Decodable>(_ type: T.Type, forKey key: String = #function) -> T? {
        if let data = UserDefaults.standard.data(forKey: key) {
            return try? PropertyListDecoder().decode(type, from: data) as T
        } else {
            return nil
        }
    }
}

extension  DeviceType: Codable {}

extension DefaultStorage {
    var guide: Set<DeviceType> {
        get { return get(Set<DeviceType>.self) ?? Set<DeviceType>() }
        set { set(newValue) }
    }
    var customDefaultUIGraphicalEqualizer: UIGraphicalEqualizer? {
        get { return get(UIGraphicalEqualizer.self) }
        set { set(newValue) }
    }
    var spotifyLocalData: StoredActionButtonData? {
        get { return get(StoredActionButtonData.self) }
        set { set(newValue) }
    }
}
