//
//  HtmlStringProviding.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 13/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

private struct Const {
    static let defaultocalization = "en"
}

private struct PathComponent {
    static let htmlExtension = "html"
    static let baseFileName = "Base"
}

private struct  HtmlTemplatePlaceholder {
    static let body = "${body}"
}

enum HtmlFile: String {
    case termsAndConditions = "TermsAndConditions"
    case freeAndOpenSourceSoftware = "FreeAndOpenSourceSoftware"
    case privacyPolicy = "PrivacyPolicy"
    case softwareUpdateReleaseNotesForFWD_01 = "software_update_release_notes_fwd_01_html"
}

protocol HtmlStringProviding {
    func htmlString(for htmlFile: HtmlFile) -> String
}

extension HtmlStringProviding {
    func htmlString(for htmlFile: HtmlFile) -> String {
        guard let basePath = Bundle.main.path(forResource: PathComponent.baseFileName, ofType: PathComponent.htmlExtension),
              var htmlBase = try? String(contentsOfFile: basePath, encoding: String.Encoding.utf8) else {
            return String()
        }
        htmlBase = htmlBase.replacingOccurrences(of: HtmlTemplatePlaceholder.body, with: htmlFile.translation)
        return htmlBase
    }
}

extension HtmlFile {
    var translation: String {
        switch self {
        case .termsAndConditions:
            return NSLocalizedString("terms_and_conditions_html", value: "HTML placeholder", comment: "HTML placeholder")
        case .freeAndOpenSourceSoftware:
            return NSLocalizedString("free_and_open_source_software_html", value: "HTML placeholder", comment: "HTML placeholder")
        case .privacyPolicy:
            return NSLocalizedString("privacy_policy_html", value: "HTML placeholder", comment: "HTML placeholder")
        case.softwareUpdateReleaseNotesForFWD_01:
            return NSLocalizedString("software_update_release_notes_fwd_01_html", value: "HTML placeholder", comment: "HTML placeholder")
        }
    }
}
