//
//  DisableDoubleTapGestureRecognizer.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 13/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let numberOfTapsRequired = 2
}

class DisableDoubleTapGestureRecognizer: UITapGestureRecognizer, UIGestureRecognizerDelegate {
    init() {
        super.init(target: nil, action: nil)
        numberOfTapsRequired = Const.numberOfTapsRequired
        delegate = self
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
