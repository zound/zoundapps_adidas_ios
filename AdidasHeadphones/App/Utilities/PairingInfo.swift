//
//  PairingInfo.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 09/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import ConnectionService

struct PairingInfo {
    let bluetoothClassicPairingStatus: BluetoothClassicPairingStatus
    let bluetoothClassicPairingMode: BluetoothClassicPairingMode
    let isAccessoryConnected: Bool
    init (_ bluetoothClassicPairingStatus: BluetoothClassicPairingStatus,
          _ bluetoothClassicPairingMode: BluetoothClassicPairingMode,
          _ isAccessoryConnected: Bool) {
        self.bluetoothClassicPairingStatus = bluetoothClassicPairingStatus
        self.bluetoothClassicPairingMode = bluetoothClassicPairingMode
        self.isAccessoryConnected = isAccessoryConnected
    }
}
