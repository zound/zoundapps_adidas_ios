//
//  Keyboard.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 22/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import UIKit

public struct KeyboardInfo {
    var frame: CGRect
    var duration: TimeInterval
    var options: UIView.AnimationOptions
    var notificationName: Notification.Name
}

public final class Keyboard {
    
    public static let shared  = Keyboard()

    public static var info: PublishSubject<KeyboardInfo> {
        return self.shared.changedInfo
    }
    
    private let changedInfo = PublishSubject<KeyboardInfo>()
    
    private init() {
        NotificationCenter.default.addObserver(
            self, selector: #selector(updateInfo(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(updateInfo(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func updateInfo(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber,
            let curveNumber = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
            let curve = UIView.AnimationCurve(rawValue: curveNumber.intValue) else {
            return
        }
        changedInfo.onNext(KeyboardInfo(frame: frame.cgRectValue,
                                        duration: duration.doubleValue,
                                        options: UIView.AnimationOptions(rawValue: UInt(curve.rawValue)),
                                        notificationName: notification.name))
    }
}

