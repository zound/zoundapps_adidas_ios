//
//  DeviceFirmwareFailureDetector.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 08/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import ConnectionService

protocol DeviceFirmwareFailureDetectorDelegate: class {
    func failureOccured()
}
class DeviceFirmwareFailureDetector {
    weak var delegate: DeviceFirmwareFailureDetectorDelegate?
    private var currentPeripheralConnectionState: PeripheralConnectionState = .disconnected {
        didSet {
            if oldValue == .connecting, currentPeripheralConnectionState == .disconnected {
                delegate?.failureOccured()
            }
        }
    }
    init(delegate: DeviceFirmwareFailureDetectorDelegate) {
        self.delegate = delegate
    }
    func register(_ peripheralConnectionState: PeripheralConnectionState) {
        currentPeripheralConnectionState = peripheralConnectionState
    }
}
