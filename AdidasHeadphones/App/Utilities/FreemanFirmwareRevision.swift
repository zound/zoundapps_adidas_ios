//
//  FreemanFirmwareRevision.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 27/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

private struct Const {
    static let firmwareRevisionComponentsSeparator = "."
    static let firmwareRevisionComponentsCount = 4
    static let firmwareRevisionStringSupportingGva = "4.2.4.3"
}

struct FreemanFirmwareRevision: Comparable {
    static let supportingGva = FreemanFirmwareRevision(Const.firmwareRevisionStringSupportingGva)!
    let major: UInt
    let minor: UInt
    let patch: UInt
    let subpatch: UInt
    static func < (lhs: FreemanFirmwareRevision, rhs: FreemanFirmwareRevision) -> Bool {
        if lhs.major != rhs.major {
            return lhs.major < rhs.major
        } else if lhs.minor != rhs.minor {
            return lhs.minor < rhs.minor
        } else if lhs.patch != rhs.patch {
            return lhs.patch < rhs.patch
        } else {
            return lhs.subpatch < rhs.subpatch
        }
    }
    init?(_ firmwareRevision: String) {
        let firmwareRevisionComponents = firmwareRevision.components(separatedBy: Const.firmwareRevisionComponentsSeparator)
        guard firmwareRevisionComponents.count == Const.firmwareRevisionComponentsCount,
            let major = UInt(firmwareRevisionComponents[0]),
            let minor = UInt(firmwareRevisionComponents[1]),
            let patch = UInt(firmwareRevisionComponents[2]),
            let subpatch = UInt(firmwareRevisionComponents[3]) else {
            return nil
        }
        self.major = major
        self.minor = minor
        self.patch = patch
        self.subpatch = subpatch
    }
}
