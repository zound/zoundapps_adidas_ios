//
//  UILocalizedIndexCollation+Extensions.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 19/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import UIKit

extension UILocalizedIndexedCollation {
    func partitionObjects(array: [AnyObject], collationStringSelector: Selector) -> ([AnyObject], [String]) {
        var unsortedSections = [[AnyObject]]()
        for _ in self.sectionTitles { unsortedSections.append([])}
        for item in array {
            let index = self.section(for: item, collationStringSelector: collationStringSelector)
            unsortedSections[index].append(item)
        }
        var sectionTitles = [String]()
        var sections = [AnyObject]()
        for index in 0..<unsortedSections.count {
            if(unsortedSections[index].count > 0) {
                sectionTitles.append(self.sectionTitles[index])
                sections.append(self.sortedArray(from: unsortedSections[index], collationStringSelector: collationStringSelector) as AnyObject)
            }
        }
        return (sections, sectionTitles)
    }
}
