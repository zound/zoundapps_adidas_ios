//
//  RxSpotifyManager+ActionButtonEventHandler.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 30/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSpotifyWrapper
import ConnectionService
import RxSwift

extension PressType: Codable { }

struct StoredSpotifyEntry: Codable {
    let pressType: PressType
    let value: SpotifyUISearchEntry
}
struct SpotifyActionButtonData: Codable, Hashable, Equatable {
    let uuid: String
    let entries: [StoredSpotifyEntry]
    static func ==(lhs: SpotifyActionButtonData, rhs: SpotifyActionButtonData) -> Bool {
        return lhs.uuid == rhs.uuid
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid)
    }
}
struct StoredActionButtonData: Codable {
    let perDeviceConfiguration: Set<SpotifyActionButtonData>
}
extension RxStreamingSpotifyManager: ActionButtonEventHandler {
    static var supportedActions: [ButtonAction] = [.playSpotifyPlaylist, .playSpotifyArtist, .playSpotifyAlbum]
    func execute(action: ButtonAction, pressType: PressType, using device: ReactiveDevice?) {
        guard type(of: self).supportedActions.contains(action) else {
            return
        }
        guard let uuid = device?.uuid, let stored = DefaultStorage.shared.spotifyLocalData?.perDeviceConfiguration.filter({ $0.uuid == uuid }).first else {
            return
        }
        guard let uri = stored.entries.first(where: { $0.value.type.action() == action && $0.pressType == pressType})?.value.uri else {
            return
        }
        var internalDisposeBag = DisposeBag()
        playSpotifyEntry(uri: uri)
            .subscribe(
                onSuccess: { isPlaying in
                    internalDisposeBag = DisposeBag()
                },
                onError: { error in
                    internalDisposeBag = DisposeBag()
                }
            )
        .disposed(by: internalDisposeBag)
    }
    func set(action: ButtonAction, pressType: PressType, using device: ReactiveDevice?) {
        guard type(of: self).supportedActions.contains(where: { $0 == action }) == false else { return }
        guard let uuid = device?.uuid else { return }
        guard let currentlyStoredConfiguration = DefaultStorage.shared.spotifyLocalData,
            let existing = currentlyStoredConfiguration.perDeviceConfiguration.first(where: { ($0.uuid == uuid) }) else {
                return
        }
        guard existing.entries.contains(where: { $0.pressType == pressType }) == true else { return }
        var perDeviceConfiguration = currentlyStoredConfiguration.perDeviceConfiguration
        perDeviceConfiguration.remove(existing)
        let filtered = existing.entries.filter { $0.pressType != pressType }
        let actionButtonData = SpotifyActionButtonData(uuid: uuid, entries: filtered)
        perDeviceConfiguration.insert(actionButtonData)
        let toBeStored = StoredActionButtonData(perDeviceConfiguration: perDeviceConfiguration)
        DefaultStorage.shared.spotifyLocalData = toBeStored
    }
    func verify(remote: ActionButtonConfiguration, device: ReactiveDevice?) -> Single<ActionButtonConfiguration> {
        guard let uuid = device?.uuid else { return Single.just(remote) }
        return Single<ActionButtonConfiguration>.create { event in
            var verified: ActionButtonConfiguration = [:]
            var needsUpdate = false
            remote.forEach { key, value in
                switch value {
                case .playSpotifyPlaylist, .playSpotifyArtist, .playSpotifyAlbum:
                    guard let currentlyStoredConfiguration = DefaultStorage.shared.spotifyLocalData,
                        let existing = currentlyStoredConfiguration.perDeviceConfiguration.first(where: { ($0.uuid == uuid) }) else {
                            verified[key] = .noAction
                            needsUpdate = true
                            return
                    }
                    guard let _ = existing.entries.filter({ storedEntry in
                        return storedEntry.pressType == key && storedEntry.value.type == value.spotifySearchType()
                    }).first else {
                        verified[key] = .noAction
                        needsUpdate = true
                        return
                    }
                    verified[key] = value
                default:
                    verified[key] = value
                }
            }
            guard needsUpdate == false else {
                device?.write(actionButtonConfiguration: verified, completion: { _ in
                    event(.success(verified))
                })
                return Disposables.create()
            }
            event(.success(remote))
            return Disposables.create()
        }
    }
    func didSelect(type: ActionType, device: ReactiveDevice?) {
        guard type != .customized else { return }
        guard let uuid = device?.uuid else { return }
        guard let currentlyStoredSpotifyConfiguration = DefaultStorage.shared.spotifyLocalData,
            let _ = currentlyStoredSpotifyConfiguration.perDeviceConfiguration.first(where: { ($0.uuid == uuid) }) else {
            return
        }
        let updated = currentlyStoredSpotifyConfiguration.perDeviceConfiguration.filter { $0.uuid != uuid }
        guard updated.count > .zero else {
            DefaultStorage.shared.spotifyLocalData = nil
            return
        }
        DefaultStorage.shared.spotifyLocalData = StoredActionButtonData(perDeviceConfiguration: updated)
    }
}

