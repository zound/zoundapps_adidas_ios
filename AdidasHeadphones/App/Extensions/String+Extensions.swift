//
//  String+Extensions.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 17/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

extension String {
    static let invisible = String(" ")
    var notEmpty: String {
        return isEmpty ? String.invisible : self
    }
}
extension String {
    var capitalizedFirst: String {
        return prefix(1).uppercased() + dropFirst()
    }
}
