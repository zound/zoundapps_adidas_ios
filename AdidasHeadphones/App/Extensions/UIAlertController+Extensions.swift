//
//  UIAlertController+Extensions.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 18/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIAlertController {

    public static func updateNotificationAlert( actionHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let title = NSLocalizedString("software_update_notification_dialog_title_uc", value: "Update in progress", comment: "")
        let message = NSLocalizedString("software_update_notification_dialog_message", value: "This may take about 15 minutes. We recommend to leave the phone and headphones alone to finish the update...", comment: "")
        let ok =  NSLocalizedString("appwide_ok_uc", comment: "OK")
        let okButton = UIAlertAction(title: ok, style: .default, handler: actionHandler)
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(okButton)
        return alert
    }
}
