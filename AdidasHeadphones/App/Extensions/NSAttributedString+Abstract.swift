//
//  NSAttributedString+Abstract.swift
//  AppLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 21/02/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import UIKit

extension NSAttributedString {
    public func font(_ font: UIFont) -> NSAttributedString {
        var attributes = self.attributes(at: Int.zero, effectiveRange: nil)
        attributes[NSAttributedString.Key.font] = font
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    public func color(_ color: UIColor = UIColor.black) -> NSAttributedString {
        var attributes = self.attributes(at: Int.zero, effectiveRange: nil)
        attributes[NSAttributedString.Key.foregroundColor] = color
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    public func letterSpacing(_ spacing: CGFloat = CGFloat.zero) -> NSAttributedString {
        var attributes = self.attributes(at: Int.zero, effectiveRange: nil)
        attributes[NSAttributedString.Key.kern] = spacing
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    public func line(spacing: CGFloat = CGFloat.zero, height: CGFloat = CGFloat.zero) -> NSAttributedString {
        var attributes = self.attributes(at: Int.zero, effectiveRange: nil)
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        
        if spacing >= CGFloat.zero {
            paragraphStyle.lineSpacing = spacing
            if height >= CGFloat.zero {
                paragraphStyle.maximumLineHeight = height
                paragraphStyle.minimumLineHeight = height
            }
        }
        
        attributes[NSAttributedString.Key.paragraphStyle] = paragraphStyle
        return NSAttributedString(string: string, attributes: attributes)
    }
}
