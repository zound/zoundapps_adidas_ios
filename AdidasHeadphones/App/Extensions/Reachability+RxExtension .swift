//
//  Reachability+RxExtension.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 12.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import Reachability
import RxCocoa
import RxSwift

extension Reachability: ReactiveCompatible { }

// MARK: - Rx Reachability to support static instances
public extension Reactive where Base: Reachability {
    
    static var reachabilityChanged: Observable<Reachability> {
        return NotificationCenter.default.rx.notification(Notification.Name.reachabilityChanged)
            .flatMap { notification -> Observable<Reachability> in
                guard let reachability = notification.object as? Reachability else { return .empty() }
                return .just(reachability) }
    }
    
    static var status: Observable<Reachability.Connection> {
        return reachabilityChanged
            .map { $0.connection }
    }
    
    static var reachable: Observable<Bool> {
        return reachabilityChanged
            .map { $0.connection != .unavailable }
    }
    
    static var connected: Observable<Void> {
        return reachable
            .filter { $0 == true }
            .map { _ in Void() }
    }
    
    static var disconnected: Observable<Void> {
        return reachable
            .filter { $0 == false }
            .map { _ in Void() }
    }
}

// MARK: - Rx Reachability to support local instances
public extension Reactive where Base: Reachability {
    
    var reachabilityChanged: Observable<Reachability> {
        return NotificationCenter.default.rx.notification(Notification.Name.reachabilityChanged, object: base)
            .flatMap { notification -> Observable<Reachability> in
                guard let reachability = notification.object as? Reachability else { return .empty() }
                return .just(reachability) }
    }
    
    var status: Observable<Reachability.Connection> {
        return reachabilityChanged
            .map { $0.connection }
    }
    
    var reachable: Observable<Bool> {
        return reachabilityChanged
            .map { $0.connection != .unavailable }
    }
    var connected: Observable<Void> {
        return reachable
            .filter { $0 }
            .map { _ in Void() }
    }
    
    var disconnected: Observable<Void> {
        return reachable
            .filter { !$0 }
            .map { _ in Void() }
    }
}


