//
//  RxSpotifyManager+ConnectedApplicationInfoProvider.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 04/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSpotifyWrapper
import RxSwift

extension RxStreamingSpotifyManager: ConnectedApplicationInfoProvider {
    private func spotifyRowHandler() -> (SpotifyConnectedAppDelegate?) -> Void {
        return { [weak self] delegate in
            guard let strongSelf = self else { return }
            switch strongSelf.rxAuthStatus.value {
            case .notConnected, .connected:
                    delegate?.openSpotifyAppInfo()
            default:
                break
            }
        }
    }
    func status() -> ConnectedApplicationStatus {
        switch rxAuthStatus.value {
        case .notConnected:
            return .notConnected
        case .connected(_, _):
            return .connected
        case .waitingForAuthorizationCallback:
            return .authorizationCallback
        default:
            return .checking
        }
    }
    func info(delegate: SpotifyConnectedAppDelegate?) -> ConnectedApplicationInfo {
        return ConnectedApplicationInfo(id: id(),
                                        icon: icon(),
                                        name: name(),
                                        status: status(),
                                        didSelectRowHandler: spotifyRowHandler())
    }
    func changed() -> Observable<ConnectedApplicationStatus> {
        return rxAuthStatus.map { [unowned self] _ in self.status() }.asObservable()
    }
    func checkStatus() {
        self.spotifyStatus()
    }
    private func id() -> ConnectedApplicationID { return .spotify }
    private func name() -> String { return "SPOTIFY" }
    private func icon() -> UIImage { return Image.Icon.Service.spotify }
}
