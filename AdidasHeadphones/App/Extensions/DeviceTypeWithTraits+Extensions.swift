//
//  DeviceTypeWithTraits+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 30/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService


extension DeviceTypeWithTraits {
    var discoveredDeviceImage: UIImage {
        switch self {
        case .arnold(let color):
            return Image.DeviceType.Arnold.discovery(with: color)
        case .freeman(let color):
            return Image.DeviceType.Freeman.discovery(with: color)
        case .desir(let color):
            return Image.DeviceType.Desir.discovery(with: color)
        }
    }
    var knownDeviceImage: UIImage {
        switch self {
        case .arnold(let color):
            return Image.DeviceType.Arnold.main(with: color)
        case .freeman(let color):
            return Image.DeviceType.Freeman.main(with: color)
        case .desir(let color):
            return Image.DeviceType.Desir.main(with: color)
        }
    }
    var thumbnailImage: UIImage {
        switch self {
        case .arnold(let color):
            return Image.DeviceType.Arnold.thumbnail(with: color)
        case .freeman(let color):
            return Image.DeviceType.Freeman.thumbnail(with: color)
        case .desir(let color):
            return Image.DeviceType.Desir.thumbnail(with: color)
        }
    }
    var hasHardwareGVA: Bool {
        switch self {
        case .arnold(_):
            return false
        case .freeman(_):
            return false
        case .desir(_):
            return false
        }
    }
}
extension DeviceTypeWithTraits {
    var pairingDescription1Text: String {
        switch self {
        case .arnold(_):
            return NSLocalizedString("enable_pairing_mode_hint_l1_rpt01", value: "Press and hold the control jog for 2 seconds,", comment: "Press and hold the control jog for 2 seconds,")
        case .freeman(_):
            return NSLocalizedString("enable_pairing_mode_hint_l1_fwd01", value: "Placeholder for fwd01 line1", comment: "Placeholder for fwd01 line1")
        case .desir(_):
            return NSLocalizedString("enable_pairing_mode_hint_l1_rpd01", value: "Placeholder for rpd01 line1", comment: "Placeholder for rpd01 line1")
        }
    }
    var pairingDescription2Text: String {
        switch self {
        case .arnold(_):
            return NSLocalizedString("enable_pairing_mode_hint_l2_rpt01", value: "then press and hold again for 4 seconds until the light turns blue.", comment: "then press and hold again for 4 seconds until the light turns blue.")
        case .freeman(_):
            return NSLocalizedString("enable_pairing_mode_hint_l2_fwd01", value: "Placeholder for fwd01 line2", comment: "Placeholder for fwd01 line2")
        case .desir(_):
            return NSLocalizedString("enable_pairing_mode_hint_l2_rpd01", value: "Placeholder for rpd01 line2", comment: "Placeholder for rpd01 line2")
        }
    }
}
extension DeviceTypeWithTraits {
    func guideImage(type: GuideItemType) -> UIImage {
        switch (self, type) {
        case (.arnold(let color), .actionButton_rpt01):
            return Image.Guide.Arnold.actionButton(with: color)
        case (.arnold(let color), .equalizer_rpt01):
            return Image.Guide.Arnold.equalizer(with: color)
        case (.arnold(let color), .stayUpdated_rpt01):
            return Image.Guide.Arnold.stayUpdated(with: color)
        case (.freeman(let color), .actionButton_fwd01):
            return Image.Guide.Freeman.actionButton(with: color)
        case (.freeman(let color), .ergo_fwd01):
            return Image.Guide.Freeman.ergo(with: color)
        case (.freeman(let color), .knob_fwd01):
            return Image.Guide.Freeman.knob(with: color)
        case (.freeman(let color), .stayUpdated_fwd01):
            return Image.Guide.Freeman.stayUpdated(with: color)
        case (.desir(let color), .actionButton_rpd01):
            return Image.Guide.Desir.actionButton(with: color)
        case (.desir(let color), .ergo_rpd01):
            return Image.Guide.Desir.ergo(with: color)
        case (.desir(let color), .knob_rpd01):
            return Image.Guide.Desir.knob(with: color)
        case (.desir(let color), .stayUpdated_rpd01):
            return Image.Guide.Desir.stayUpdated(with: color)
        default: return UIImage()
        }
    }
    func guideTitle(type: GuideItemType) -> String {
        switch (self, type) {
        case (.arnold, .actionButton_rpt01): return NSLocalizedString("guide_ab_headline_uc_rpt01", value: "ACTION BUTTON", comment: "Action button")
        case (.arnold, .equalizer_rpt01): return NSLocalizedString("guide_eq_headline_uc_rpt01", value: "EQUALIZER", comment: "Equalizer")
        case (.arnold, .stayUpdated_rpt01): return NSLocalizedString("newsletter_headline_uc", value: "STAY UPDATED", comment: "Stay updated")
        case (.freeman, .actionButton_fwd01): return NSLocalizedString("guide_ab_headline_uc_fwd01", value: "ACTION BUTTON", comment: "Action button")
        case (.freeman, .ergo_fwd01): return NSLocalizedString("guide_ergo_headline_uc_fwd01", value: "ERGO", comment: "Ergo")
        case (.freeman, .knob_fwd01): return NSLocalizedString("guide_knob_headline_uc_fwd01", value: "KNOB", comment: "Knob")
        case (.freeman, .stayUpdated_fwd01): return NSLocalizedString("newsletter_headline_uc", value: "STAY UPDATED", comment: "Stay updated")
        case (.desir(_), .actionButton_rpd01): return NSLocalizedString("guide_ab_headline_uc_rpd01", value: "ACTION BUTTON", comment: "Action button")
        case (.desir(_), .ergo_rpd01): return NSLocalizedString("guide_ergo_headline_uc_rpd01", value: "ERGO", comment: "Ergo")
        case (.desir(_), .knob_rpd01): return NSLocalizedString("guide_knob_headline_uc_rpd01", value: "KNOB", comment: "Knob")
        case (.desir(_), .stayUpdated_rpd01): return NSLocalizedString("newsletter_headline_uc", value: "STAY UPDATED", comment: "Stay updated")
        default: return String.invisible
        }
    }
    func guideDescription(type: GuideItemType) -> String {
        switch (self, type) {
        case (.arnold, .actionButton_rpt01): return NSLocalizedString("guide_ab_paragraph_rpt01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.arnold, .equalizer_rpt01): return NSLocalizedString("guide_eq_paragraph_rpt01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.arnold, .stayUpdated_rpt01): return NSLocalizedString("newsletter_paragraph", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.freeman, .actionButton_fwd01): return NSLocalizedString("guide_ab_paragraph_fwd01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.freeman, .ergo_fwd01): return NSLocalizedString("guide_ergo_paragraph_fwd01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.freeman, .knob_fwd01): return NSLocalizedString("guide_knob_paragraph_fwd01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.freeman, .stayUpdated_fwd01): return NSLocalizedString("newsletter_paragraph", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.desir, .actionButton_rpd01): return NSLocalizedString("guide_ab_paragraph_rpd01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.desir, .ergo_rpd01): return NSLocalizedString("guide_ergo_paragraph_rpd01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.desir, .knob_rpd01): return NSLocalizedString("guide_knob_paragraph_rpd01", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        case (.desir, .stayUpdated_rpd01): return NSLocalizedString("newsletter_paragraph", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        default: return String.invisible
        }
    }
    func newsletterVisible(type: GuideItemType) -> Bool {
        switch (self, type) {
            case (.arnold, .stayUpdated_rpt01),
                 (.freeman, .stayUpdated_fwd01),
                 (.desir, .stayUpdated_rpd01):
                return true
            default:
                return false
        }
    }
    var supportedTypes: [GuideItemType] {
        switch self {
        case .arnold(_): return [.actionButton_rpt01, .equalizer_rpt01, .stayUpdated_rpt01]
        case .freeman(_): return [.actionButton_fwd01, .ergo_fwd01, .knob_fwd01, .stayUpdated_fwd01]
        case .desir(_): return  [.actionButton_rpd01, .ergo_rpd01, .knob_rpd01, .stayUpdated_rpd01]
        }
    }
    var guideItems: [GuideItem] {
        return supportedTypes.map {
            GuideItem(image: guideImage(type: $0),
                      header: guideTitle(type: $0),
                      description: guideDescription(type: $0),
                      newsletterVisible: newsletterVisible(type: $0))
        }
    }
}
