//
//  CGRect+Extensions.swift
//  HeadsetApp
//
//  Created by Grzegorz Kiel on 02/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import CoreGraphics

extension CGRect {
    var center: CGPoint {
        return CGPoint(x: origin.x + size.width.half, y: origin.y + size.height.half)
    }
}
