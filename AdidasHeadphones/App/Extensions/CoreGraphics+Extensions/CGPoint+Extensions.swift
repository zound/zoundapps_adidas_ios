//
//  CGPoint+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 03/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import CoreGraphics

extension CGPoint {
    func distance(to point: CGPoint) -> CGFloat {
        return sqrt(pow((point.x - x), 2) + pow((point.y - y), 2))
    }
}
