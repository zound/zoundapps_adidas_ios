//
//  CGAffineTransform+Extensions.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 14/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import CoreGraphics

extension CGAffineTransform {
    static let headerScale = CGAffineTransform(scaleX: 0.9, y: 0.9)
}
