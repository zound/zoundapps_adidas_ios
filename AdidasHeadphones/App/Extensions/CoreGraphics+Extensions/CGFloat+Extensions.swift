//
//  CGFloat+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 02/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import CoreGraphics

extension CGFloat {
    var half: CGFloat {
        return self / 2.0
    }

    var twice: CGFloat {
        return self * 2.0
    }
}
