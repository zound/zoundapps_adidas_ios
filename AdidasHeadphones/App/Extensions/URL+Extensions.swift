//
//  URL+Extensions.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 14/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

extension URL {

    static let adidasWebpage = URL(string: "https://adidasheadphones.com")!
    static let contactPageUrl = URL(string: "https://adidasheadphones.com/contact-support.html")!
    static let spotifyAccountWebpage = URL(string: "https://www.spotify.com/account")!
}
