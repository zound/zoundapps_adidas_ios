//
//  UIScreen+Extensions.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 17/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


extension UIScreen {
    static let isSmallScreen: Bool = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height) <= 320
}
