//
//  UIView+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 03/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIView {
    func rotate(with duration: CFTimeInterval = 1) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = CGFloat()
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = Float.infinity
        layer.add(rotateAnimation, forKey: nil)
    }
    func setupBorder() {
        layer.borderColor = Colors.greyExtraLight.cgColor
        layer.borderWidth = 1
        clipsToBounds = true
    }
    func stopAnimations() {
        layer.removeAllAnimations()
        layoutIfNeeded()
    }
    func containsAnimation() -> Bool{
        return (layer.animationKeys()?.count ?? 0)  > 0
    }
}
