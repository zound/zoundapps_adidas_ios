//
//  UIImage+Extensions.swift
//  002
//
//  Created by Łukasz Wudarski on 09/04/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit

extension UIImage {
    func scaled(to size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, CGFloat())
        draw(in: CGRect(x: CGFloat(), y: CGFloat(), width: size.width, height: size.height))
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    struct RectWithColor {
        let rect: CGRect
        let color: UIColor
    }
    static func make(rectImage: RectWithColor, with innerRectImage: RectWithColor? = nil) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: rectImage.rect.size)
        return renderer.image { ctx in
            ctx.cgContext.setFillColor(rectImage.color.cgColor)
            ctx.cgContext.addRect(rectImage.rect)
            ctx.cgContext.drawPath(using: .fill)
            if let innerRectImage = innerRectImage {
                ctx.cgContext.setFillColor(innerRectImage.color.cgColor)
                ctx.cgContext.addRect(innerRectImage.rect)
                ctx.cgContext.drawPath(using: .fill)
            }
        }

    }
}
