//
//  UIColor+Hex.swift
//  AppLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 21/02/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIColor {
    static func take(from hex: String) -> UIColor {
        var colorString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (colorString.hasPrefix("#")) {
            colorString.remove(at: colorString.startIndex)
        }
        if colorString.count != 6 {
            return UIColor.clear
        }
        var rgbValue = UInt32()
        Scanner(string: colorString).scanHexInt32(&rgbValue)
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(rgbValue & 0x0000FF) / 255.0
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
