//
//  UILabel+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 09/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let blinkingTime: TimeInterval = 0.35
    static let alphaMin: CGFloat = 0.2
    static let alphaMax: CGFloat = 1
}

extension UILabel {
    func startBlinking() {
        UIView.animate(withDuration: Const.blinkingTime,
                       delay: .zero,
                       options: [.allowUserInteraction, .curveEaseOut, .autoreverse, .repeat],
                       animations: { self.alpha = Const.alphaMin })
    }
    func stopBlinking() {
        layer.removeAllAnimations()
        alpha = Const.alphaMax
    }
    func indicateAnimated(completion: ((Bool) -> Void)? = nil) {
        let fadeInAmination: () -> Void = {
            self.transform = .identity
        }
        transform = .headerScale
        UIView.animate(withDuration: .t025, delay: .zero, options: [.curveEaseInOut, .transitionCrossDissolve],
                       animations: fadeInAmination,
                       completion: completion)
    }
    struct TextTransitionParameters {
        let attributedText: NSAttributedString
        let textAligment: NSTextAlignment
        let lineBreakMode: NSLineBreakMode
    }
    func animateTextTransition(with textTransitionPatameters: TextTransitionParameters, completion: ((Bool) -> Void)? = nil) {
        let fadeOutAmination: () -> Void = { [weak self] in
            self?.attributedText = NSAttributedString()
        }
        let fadeInAmination = { [weak self] in
            self?.attributedText = textTransitionPatameters.attributedText
            self?.textAlignment = textTransitionPatameters.textAligment
            self?.lineBreakMode = textTransitionPatameters.lineBreakMode
        }
        let options: UIView.AnimationOptions = [.curveEaseInOut, .transitionCrossDissolve]
        UIView.transition(with: self, duration: .t010, options: options, animations: fadeOutAmination) { _ in
            UIView.transition(with: self, duration: .t025, options: options, animations: fadeInAmination, completion: completion)
        }
    }
}

