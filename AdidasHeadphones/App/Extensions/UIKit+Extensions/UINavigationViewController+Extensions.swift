//
//  UINavigationViewController+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 09/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

extension UINavigationController {
    func replaceTopViewController(with viewController: UIViewController, animated: Bool) {
        var currentViewControllers = viewControllers
        currentViewControllers[currentViewControllers.count - 1] = viewController
        setViewControllers(currentViewControllers, animated: animated)
    }
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
    override open var prefersStatusBarHidden: Bool {
        return topViewController?.prefersStatusBarHidden ?? false
    }
}
