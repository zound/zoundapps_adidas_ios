//
//  UIViewController+Extensions.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 07/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIViewController {
    private final class NavigationItemTitleView: UILabel {
        static func create(with attributedText: NSAttributedString) -> NavigationItemTitleView {
            let navigationItemTitleView = NavigationItemTitleView()
            navigationItemTitleView.attributedText = attributedText
            return navigationItemTitleView
        }
    }
    func setupNavigationBar(title: NSAttributedString) {
        navigationItem.titleView = NavigationItemTitleView.create(with: title)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = Image.Icon.Black.chevronLeft
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = Image.Icon.Black.chevronLeft
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = Colors.black
    }
}

