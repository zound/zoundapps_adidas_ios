//
//  UIButton+Extensions.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 16/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.

import UIKit

extension UIButton: Toggleable {
    func toggle(_ flag: Bool) {
        isEnabled = flag
        UIView.animate(withDuration: .t050, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            self?.alpha = flag ? 1.0 : 0.5
        })
    }
}
