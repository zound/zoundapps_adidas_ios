//
//  CircleSlider.swift
//  HeadsetApp
//
//  Created by Grzegorz Kiel on 02/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

fileprivate struct Const {
    static let spins = 4
    static let defaultTimeout: TimeInterval = 5.0
    static let springDamping: CGFloat = 0.8
    static let springResponse: CGFloat = 0.3
    static let circleRadius: CGFloat = 30
    static let minimalDistance: CGFloat = 44
    static let rotationStepAngle: Double = 180
}

protocol CircleSliderDelegate: class {
    func didChange(circleSliderCurrentState: CircleSlider.CurrentState)
}

public class CircleSlider: UIControl {
    public enum CurrentState {
        case idle
        case dragging
        case commingBack
        case dragged
    }
    weak var delegate: CircleSliderDelegate?
    private let impactFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
    private let blackBall = UIImageView(image: Image.CircleSlider.blackCircle)
    private let handleBall = UIImageView(image: Image.CircleSlider.handle)
    private let spinnerLight = UIImageView(image: Image.CircleSlider.spinnerLight)
    private let targetBall = UIImageView(image: Image.CircleSlider.target)
    private var originalTouchPoint = CGPoint.zero
    private var currentState: CircleSlider.CurrentState = .idle {
        didSet {
            delegate?.didChange(circleSliderCurrentState: currentState)
        }
    }
    var isUserInteractionInProgress: Bool {
        return currentState != .idle
    }
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Colors.clear
        [blackBall, spinnerLight, targetBall, handleBall].forEach { addSubview($0) }
        [blackBall, spinnerLight].forEach { $0.isHidden = true }
        layoutCircles()
    }
    private func layoutCircles() {
        let targetOrigin = CGPoint(x: intrinsicContentSize.width - Const.circleRadius.twice, y: CGFloat())
        let targetSize = CGSize(width: Const.circleRadius.twice, height: Const.circleRadius.twice)
        targetBall.frame = CGRect(origin: targetOrigin, size: targetSize)
        let handleSize = CGSize(width: Const.circleRadius.twice, height: Const.circleRadius.twice)
        handleBall.frame = CGRect(origin: CGPoint.zero, size: handleSize)
    }
    override public var intrinsicContentSize: CGSize {
        return CGSize(width: Const.circleRadius.twice.twice + Const.minimalDistance, height: Const.circleRadius.twice)
    }
}

extension CircleSlider {
    override public func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        guard currentState == .idle else {
            return false
        }
        let touchPoint = touch.location(in: self)
        let handleBallCenter = CGPoint(x: handleBall.frame.origin.x + handleBall.frame.size.width.half,
                                       y: handleBall.frame.origin.y + handleBall.frame.size.height.half)
        guard touchPoint.distance(to: handleBallCenter) <= Const.circleRadius else {
            return false
        }
        originalTouchPoint = touchPoint
        currentState = .dragging
        sendActions(for: .touchUpInside)
        return true
    }
    override public func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let touchPoint = touch.location(in: self)
        let offset = touchPoint.x - originalTouchPoint.x
        guard offset > CGFloat() else {
            return true
        }
        let handleBallTranslationX = handleBall.transform.tx
        let targetBallTranslationX = targetBall.transform.tx
        guard handleBall.center.x + handleBallTranslationX < targetBall.center.x + targetBallTranslationX else {
            currentState = .dragged
            impactFeedbackGenerator.impactOccurred()
            startSpinner()
            return false
        }
        handleBall.transform = CGAffineTransform(translationX: offset, y: CGFloat())
        targetBall.transform = CGAffineTransform(translationX: -offset, y: CGFloat())
        return true
    }
    override public func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        let timingParameters = UISpringTimingParameters(damping: Const.springDamping, response: Const.springResponse)
        let animator = UIViewPropertyAnimator(duration: TimeInterval.zero, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            self?.handleBall.transform = .identity
            self?.targetBall.transform = .identity
        }
        animator.addCompletion { [weak self] _ in
            self?.sendActions(for: .touchCancel)
            self?.currentState = .idle
        }
        animator.startAnimation()
    }
}

extension CircleSlider {
    private func startSpinner() {
        [handleBall, targetBall].forEach { $0.isHidden = true }
        [blackBall, spinnerLight].forEach { $0.isHidden = false }
        let centralPoint = CGRect(origin: CGPoint.zero, size: intrinsicContentSize).center
        let finalOrigin = CGPoint(x: centralPoint.x - targetBall.frame.size.width.half,
                                  y: centralPoint.y - targetBall.frame.size.height.half)
        blackBall.frame = CGRect(origin: finalOrigin, size: targetBall.frame.size)
        let spinnerOrigin = CGPoint(x: blackBall.center.x - spinnerLight.frame.size.width.half,
                                    y: blackBall.center.y - spinnerLight.frame.size.height.half)
        spinnerLight.frame = CGRect(origin: spinnerOrigin, size: spinnerLight.frame.size)
        spinnerLight.rotate()
    }
    func stopSpinner() {
        spinnerLight.layer.removeAllAnimations()
        [handleBall, targetBall].forEach { $0.isHidden = false }
        [blackBall, spinnerLight].forEach { $0.isHidden = true }
        let timingParameters = UISpringTimingParameters(damping: Const.springDamping, response: Const.springResponse)
        let animator = UIViewPropertyAnimator(duration: TimeInterval.zero, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            self?.handleBall.transform = .identity
            self?.targetBall.transform = .identity
        }
        animator.addCompletion { [weak self] _ in
            self?.currentState = .idle
        }
        animator.startAnimation()
        currentState = .commingBack
    }
}
