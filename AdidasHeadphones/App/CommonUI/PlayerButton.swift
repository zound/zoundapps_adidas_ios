//
//  PlayerButton.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 31/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let imageViewTransform = CGAffineTransform(scaleX: 0.7, y: 0.7)
    static let imageViewAnimationDelayFactor: CGFloat = 0.5
    static let highlightedViewBackgroundColor = Colors.greyExtraLight.withAlphaComponent(0.5)
}

enum PlayerButtonType {
    enum PlayPause {
        case play
        case pause
        case unknown
    }
    case skipBackward
    case playPause(PlayPause)
    case skipForward
}

extension PlayerButtonType {
    var image: UIImage {
        switch self {
        case .skipBackward:
            return Image.Icon.Black.arrowLeft
        case .playPause(.play):
            return Image.Icon.Black.play
        case .playPause(.pause):
            return Image.Icon.Black.pause
        case .playPause(.unknown):
            return UIImage()
        case .skipForward:
            return Image.Icon.Black.arrowRight
        }
    }
}
enum ControlState {
    case enabled
    case disabled
}

extension ControlState {
    var bool: Bool {
        return self == .enabled ? true : false
    }
}

class PlayerButton: UIButton {
    var playerButtonType: PlayerButtonType? {
        didSet {
            guard let playerButtonType = playerButtonType, let imageView = imageView else {
                return
            }
            UIView.transition(with: imageView, duration: .t025, options: .transitionFlipFromRight, animations: { [weak self] in
                self?.setImage(playerButtonType.image, for: .normal)
            })
        }
    }
    var controlState: ControlState = .enabled {
        didSet {
            isUserInteractionEnabled = controlState.bool
            tintColor = controlState.bool ? Colors.black : Colors.greyLight
        }
    }
    private lazy var hightlightView: UIView = {
        let hightlightView = UIView()
        hightlightView.isUserInteractionEnabled = false
        hightlightView.layer.cornerRadius = Dimension.playerButtonSize.half
        hightlightView.layer.masksToBounds = true
        return hightlightView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        commonSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
        commonSetup()
    }
    private func setupConstraints() {
        [hightlightView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
            sendSubviewToBack($0)
        }
        NSLayoutConstraint.activate([
            hightlightView.centerXAnchor.constraint(equalTo: centerXAnchor),
            hightlightView.centerYAnchor.constraint(equalTo: centerYAnchor),
            hightlightView.heightAnchor.constraint(equalToConstant: Dimension.playerButtonSize),
            hightlightView.widthAnchor.constraint(equalToConstant: Dimension.playerButtonSize)
        ])
    }
    private func commonSetup() {
        tintColor = Colors.greyLight
        addTarget(self, action: #selector(buttonTouchedDown), for: .touchDown)
    }
    @objc private func buttonTouchedDown() {
        hightlightView.backgroundColor = Const.highlightedViewBackgroundColor
        UIView.transition(with: self, duration: .t050, options: [.curveEaseInOut], animations: { [weak self] in
            self?.hightlightView.backgroundColor = Colors.clear
        })
        let animator = UIViewPropertyAnimator(duration: .t025, curve: .linear) { [weak self] in
            self?.imageView?.transform = Const.imageViewTransform
        }
        animator.addAnimations({ [weak self] in
            self?.imageView?.transform = .identity
        }, delayFactor: Const.imageViewAnimationDelayFactor)
        animator.startAnimation()
    }
}
