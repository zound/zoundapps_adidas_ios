//
//  LinearProgressIndicator.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 23/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let width: CGFloat = 74
    static let height: CGFloat = 2
}
class LinearProgressIndicator: UIView {
    private let lineShape = CAShapeLayer()
    private let linePath = UIBezierPath()
    private let backgroundLineShape = CAShapeLayer()
    private let backgroundLinePath = UIBezierPath()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: Const.width),
            heightAnchor.constraint(equalToConstant: Const.height)
        ])
        linePath.move(to: CGPoint(x: CGFloat(), y: Const.height.half))
        linePath.addLine(to: CGPoint(x: Const.width, y: Const.height.half))
        lineShape.strokeColor = Colors.black.cgColor
        lineShape.strokeStart = .zero
        lineShape.strokeEnd = .zero
        lineShape.fillColor = Colors.clear.cgColor
        lineShape.path = linePath.cgPath
        lineShape.lineWidth = Const.height
        backgroundLinePath.move(to: CGPoint(x: CGFloat(), y: Const.height.half))
        backgroundLinePath.addLine(to: CGPoint(x: Const.width, y: Const.height.half))
        backgroundLineShape.strokeColor = Colors.greyLight.cgColor
        backgroundLineShape.strokeStart = .zero
        backgroundLineShape.strokeEnd = Const.width
        backgroundLineShape.fillColor = Colors.clear.cgColor
        backgroundLineShape.path = backgroundLinePath.cgPath
        backgroundLineShape.lineWidth = Const.height
        layer.addSublayer(backgroundLineShape)
        layer.addSublayer(lineShape)
    }
    func progress(value: CGFloat) {
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        lineShape.strokeEnd = value
        CATransaction.commit()
    }
}

