//
//  RectangularSwitch.swift
//  AdidasHeadphones
//
//  Created by Bartosz Dolewski on 15/02/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import UIKit

final class RectangularSwitch: UIControl {
    public var onTintColor = UIColor.black {
        didSet { setupUI() }
    }

    public var offTintColor = UIColor(red: 0.78, green: 0.80, blue: 0.80, alpha:1.0) {
        didSet { setupUI() }
    }

    public var thumbTintColor = UIColor.white {
        didSet { thumbView.backgroundColor = thumbTintColor }
    }

    public var thumbSize = CGSize(width: 24, height: 24) {
        didSet { layoutSubviews() }
    }

    public var padding = CGFloat(1.0) {
        didSet { layoutSubviews() }
    }

    public var isOn = true {
        didSet {
            setNeedsLayout()
        }
    }
    public var animationDuration = Double(0.5)

    fileprivate var thumbView = UIView(frame: .zero)
    fileprivate var onPoint = CGPoint.zero
    fileprivate var offPoint = CGPoint.zero
    fileprivate var isAnimating = false

    public override func layoutSubviews() {
        super.layoutSubviews()

        if !isAnimating {
            backgroundColor = isOn ? onTintColor : offTintColor

            // thumb managment
            let size = CGSize(width: bounds.size.width.half - padding,
                              height: bounds.height - (2 * padding))
            let thumbSize = self.thumbSize != CGSize.zero ? self.thumbSize : size

            onPoint = CGPoint(x: bounds.size.width.half, y: padding)
            offPoint = CGPoint(x: padding, y: padding)

            thumbView.frame = CGRect(origin: isOn ? onPoint : offPoint, size: thumbSize)
        }
    }

    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)

        animate()
        return true
    }

    func setupUI() {
        clear()
        clipsToBounds = false

        thumbView.layer.shadowColor = UIColor.black.cgColor
        thumbView.layer.shadowRadius = 1.5
        thumbView.layer.shadowOffset = CGSize(width: 0.75, height: 2)
        thumbView.backgroundColor = thumbTintColor
        thumbView.isUserInteractionEnabled = false
        addSubview(thumbView)
    }

    private func clear() {
        subviews.forEach { $0.removeFromSuperview() }
    }

    private func animate() {
        isOn.toggle()
        isAnimating = true

        UIView.animate(withDuration: animationDuration,
                       delay: 0.0,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0.5,
                       options: [.curveEaseOut, .beginFromCurrentState],
                       animations: {
                        self.thumbView.frame.origin = self.isOn ? self.onPoint : self.offPoint
                        self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
        }) { _ in
            self.isAnimating = false
            self.sendActions(for: .valueChanged)
        }
    }
}
