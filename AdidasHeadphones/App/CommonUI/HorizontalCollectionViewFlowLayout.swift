//
//  HorizontalCollectionViewFlowLayout.swift
//  002
//
//  Created by Łukasz Wudarski on 28/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit

class HorizontalCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit() {
        scrollDirection = .horizontal
    }
    public override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return false
    }
}
