//
//  LinearIndicator.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 03/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let width: CGFloat = 74
    static let height: CGFloat = 2
}

class LinearIndicator: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        startAnimating()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        startAnimating()
    }
    private func commonInit() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: Const.width),
            heightAnchor.constraint(equalToConstant: Const.height)
        ])
    }
    func startAnimating() {
        let lineShape = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: CGFloat(), y: Const.height.half))
        linePath.addLine(to: CGPoint(x: Const.width, y: Const.height.half))
        lineShape.strokeColor = Colors.black.cgColor
        lineShape.fillColor = Colors.clear.cgColor
        lineShape.path = linePath.cgPath
        lineShape.lineWidth = Const.height
        layer.addSublayer(lineShape)
        let animationFrom = CABasicAnimation(keyPath: "strokeEnd")
        animationFrom.fromValue = 0.0
        animationFrom.toValue = 2.0
        animationFrom.fillMode = CAMediaTimingFillMode.forwards
        let animationTo = CABasicAnimation(keyPath: "strokeStart")
        animationTo.fromValue = -1.0
        animationTo.toValue = 1.0
        animationTo.fillMode = CAMediaTimingFillMode.forwards
        let group = CAAnimationGroup()
        group.animations = [animationFrom, animationTo]
        group.duration = 1.0
        group.repeatCount = .infinity
        lineShape.add(group, forKey: nil)
    }
}

