//
//  BarSlider.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 06/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let barWidth: CGFloat = ((Dimension.screenWidth - Dimension.edgeMargin.twice - Dimension.smallSpace * 4) / 5).rounded(.down)
    static let barHeight: CGFloat = 2
    static let thumbHeight: CGFloat = 40
    static let thumbOffset: CGFloat = 2
    static let thumbInnerRectWidth: CGFloat = 20
    static let thumbInnerRectHeight: CGFloat = 2
    static let thumbImage = UIImage.make(rectImage:
        UIImage.RectWithColor(rect: CGRect(origin: .zero, size: CGSize(width: Const.thumbHeight, height: Const.barWidth)), color: Colors.black), with:
        UIImage.RectWithColor(
            rect: CGRect(origin: CGPoint(x: Const.thumbHeight.half + Const.thumbOffset, y: Const.barWidth.half - Const.thumbInnerRectWidth.half),
            size: CGSize(width: Const.thumbInnerRectHeight, height: Const.thumbInnerRectWidth)), color: Colors.greyDark)
    )
    static let thumbImageHighlighted = UIImage.make(rectImage:
        UIImage.RectWithColor(rect: CGRect(origin: .zero, size: CGSize(width: Const.thumbHeight, height: Const.barWidth)), color: Colors.black), with:
        UIImage.RectWithColor(
            rect: CGRect(origin: CGPoint(x: Const.thumbHeight.half + Const.thumbOffset, y: Const.barWidth.half - Const.thumbInnerRectWidth.half),
            size: CGSize(width: Const.thumbInnerRectHeight, height: Const.thumbInnerRectWidth)), color: Colors.greyDark)
    )
    static let maxTrackImage = UIImage.make(rectImage:
        UIImage.RectWithColor(rect: CGRect(origin: .zero, size: CGSize(width: Const.barHeight, height: Const.barWidth)), color: Colors.white)
    )
    static let minTrackImage = UIImage.make(rectImage:
        UIImage.RectWithColor(rect: CGRect(origin: .zero, size: CGSize(width: Const.barHeight, height: Const.barWidth)), color: Colors.black)
    )
}

private class CustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: Const.barWidth))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        return super.thumbRect(forBounds: bounds, trackRect: rect, value: value).offsetBy(dx: -Const.thumbOffset, dy: .zero)
    }
}

class BarSlider: UIControl {
    private let slider = CustomSlider()
    func setValue(_ newValue: Float, animated: Bool) {
        slider.setValue(newValue, animated: animated)
    }
    var value: Float {
        set { slider.setValue(newValue, animated: true) }
        get { return slider.value }
    }
    var minimumValue: Float {
        set { slider.minimumValue = newValue }
        get { return slider.minimumValue }
    }
    var maximumValue: Float {
        set { slider.maximumValue = newValue }
        get { return slider.maximumValue }
    }
    var isContinuous: Bool {
        set { slider.isContinuous = newValue }
        get { return slider.isContinuous }
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    private func configure() {
        setupBorder()
        slider.setThumbImage(Const.thumbImage, for: .normal)
        slider.setThumbImage(Const.thumbImageHighlighted, for: .highlighted)
        slider.setMaximumTrackImage(Const.maxTrackImage, for: .normal)
        slider.setMinimumTrackImage(Const.minTrackImage, for: .normal)
        slider.transform = CGAffineTransform(rotationAngle: -CGFloat.pi.half)
        addSubview(slider)
        slider.addTarget(self, action: #selector(onSliderValueChanged(slider:event:)), for: .valueChanged)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        slider.bounds.size.width = bounds.height
        slider.bounds.size.height = Const.barWidth
        slider.center.x = bounds.midX
        slider.center.y = bounds.midY
    }
    override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event) {
        slider.addTarget(target, action: action, for: controlEvents)
    }
    override func removeTarget(_ target: Any?, action: Selector?, for controlEvents: UIControl.Event) {
        slider.removeTarget(target, action: action, for: controlEvents)
    }
    @objc func onSliderValueChanged(slider: UISlider, event: UIEvent) {
        if event.allTouches?.first?.phase == .began {
            setBorder(highlighted: true)
        }
        if event.allTouches?.first?.phase == .ended || event.allTouches?.first?.phase == .cancelled {
            setBorder(highlighted: false)
        }
    }
}

extension BarSlider {
    func setBorder(highlighted: Bool) {
        UIView.transition(with: self, duration: .t025, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.layer.borderColor = highlighted ? Colors.greyDark.cgColor : Colors.greyExtraLight.cgColor
        }, completion: nil)
    }
}
