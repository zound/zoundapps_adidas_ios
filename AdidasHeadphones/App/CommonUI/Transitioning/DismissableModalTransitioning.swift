//
//  DismissableModalTransitioning.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 23/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    struct DimmingView {
        static let alphaMax: CGFloat = 1
        static let alphaMin: CGFloat = .zero
        static let backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    struct ReverseAnimation {
        static let springDamping: CGFloat = 0.8
        static let springResponse: CGFloat = 0.3
    }
    static let dismissTreshold: CGFloat = 40
}

protocol DismissableModalCompliance {
    var modalHeight: CGFloat { get }
    var dismissable: Bool { get }
}

final class DismissableModalTransitioning: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return DismissableModalPresentationController(presentedViewController: presented, presenting: presenting)
    }
}

final class DismissableModalPresentationController: UIPresentationController {
    private var offset: CGFloat = .zero
    private var reversionAnimator: UIViewPropertyAnimator?
    private lazy var dimmingView: UIView = {
        let view = UIView(frame: containerView?.bounds ?? .zero)
        view.backgroundColor = Const.DimmingView.backgroundColor
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapDimmingView(_:))))
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPanDimmingView(_:))))
        return view
    }()
    var isDismissing: Bool = false
    func updateOffset() {
        if let contentHeight = (presentedViewController as? DismissableModalCompliance)?.modalHeight {
            offset = presentingViewController.view.bounds.height - contentHeight
        }
    }
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        presentedViewController.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(pan:))))
    }
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let container = containerView else {
            return .zero
        }
        return CGRect(x: .zero, y: offset, width: container.bounds.width, height: container.bounds.height - offset + Const.dismissTreshold)
    }
    override func presentationTransitionWillBegin() {
        updateOffset()
        containerView?.addSubview(dimmingView)
        dimmingView.alpha = Const.DimmingView.alphaMin
        dimmingView.addSubview(presentedViewController.view)
        presentingViewController.transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView.alpha = Const.DimmingView.alphaMax
        })
    }
    override func dismissalTransitionWillBegin() {
        presentingViewController.transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView.alpha = Const.DimmingView.alphaMin
        })
    }
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            dimmingView.removeFromSuperview()
        }
    }
    @objc func didPan(pan: UIPanGestureRecognizer) {
        guard isPresentedViewControllerDismissable else {
            return
        }
        guard let view = pan.view, let superView = view.superview, let presented = presentedView, let container = containerView else {
            return
        }
        let location = pan.translation(in: superView).y >= .zero ? pan.translation(in: superView) : .zero
        switch pan.state {
        case .began:
            presented.frame.size.height = container.frame.height
        case .changed:
            presented.frame.origin.y = location.y + offset
        case .ended where presented.frame.origin.y > offset:
            if presented.frame.origin.y > offset + Const.dismissTreshold {
                dismissPresentedViewControllerIfPossible()
            } else {
                setFrameOfPresentedViewInContainerViewAnimated()
            }
        default:
            break
        }
    }
    @objc func didTapDimmingView(_ sender: UITapGestureRecognizer) {
        dismissPresentedViewControllerIfPossible()
    }
    @objc func didPanDimmingView(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .ended:
            dismissPresentedViewControllerIfPossible()
        default:
            break
        }
    }
    private var isPresentedViewControllerDismissable: Bool {
        guard let dismissable = (presentedViewController as? DismissableModalCompliance)?.dismissable else {
            return true
        }
        return dismissable
    }
    private func dismissPresentedViewControllerIfPossible() {
        if isPresentedViewControllerDismissable {
            isDismissing = true
            presentedViewController.dismiss(animated: true) { [weak self] in
                self?.isDismissing.toggle()
            }
        }
    }
    func setFrameOfPresentedViewInContainerViewAnimated() {
        let timingParameters = UISpringTimingParameters(damping: Const.ReverseAnimation.springDamping, response: Const.ReverseAnimation.springResponse)
        reversionAnimator = UIViewPropertyAnimator(duration: .zero, timingParameters: timingParameters)
        reversionAnimator?.addAnimations { [weak self] in
            self?.presentedView?.frame = self?.frameOfPresentedViewInContainerView ?? .zero
        }
        reversionAnimator?.addCompletion { [weak self] _ in
            self?.reversionAnimator = nil
        }
        reversionAnimator?.startAnimation()
    }
}
