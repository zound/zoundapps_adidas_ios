//
//  TilesCollectionViewFlowLayout.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 21/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TilesLayout: UICollectionViewFlowLayout {
    var layoutHeight = CGFloat.zero
    override init() {
        super.init()
        self.scrollDirection = .vertical
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes  = super.layoutAttributesForElements(in: rect)
        let attributesAction = calcualateAttributesAction(screenWidth: Dimension.screenWidth - Dimension.edgeMargin.twice,
                                                          firstItemHeight: Dimension.tileSmallHeight,
                                                          itemHeight: Dimension.tileBigHeight,
                                                          firstItemIdx: .zero,
                                                          fullLengthItemsIdx: [3],
                                                          interItemSpacing: Dimension.tileSpace)
        let calculatedAttributes = attributes.map(attributesAction)
        if let calculatedHeight = calculatedAttributes?.reversed().first?.frame.maxY {
            layoutHeight = calculatedHeight
        }
        return calculatedAttributes
    }
}

private func calcualateAttributesAction(screenWidth: CGFloat,
                                        firstItemHeight: CGFloat,
                                        itemHeight: CGFloat,
                                        firstItemIdx: Int,
                                        fullLengthItemsIdx: [Int],
                                        interItemSpacing: CGFloat) -> ([UICollectionViewLayoutAttributes]) -> ([UICollectionViewLayoutAttributes]) {
        return { attributes in
            var newAttributes = [UICollectionViewLayoutAttributes]()
            var currentY = CGFloat.zero
            var currentX = CGFloat.zero
            attributes.enumerated().forEach { offset, attributes in
                guard offset != firstItemIdx else {
                    attributes.frame = CGRect(origin: CGPoint(x: .zero, y: currentY), size: CGSize(width: screenWidth, height: firstItemHeight))
                    newAttributes.append(attributes)
                    currentY = currentY.advanced(by: firstItemHeight) + interItemSpacing
                    return
                }
                guard fullLengthItemsIdx.contains(offset) == false else {
                    attributes.frame = CGRect(origin: CGPoint(x: currentX, y: currentY), size: CGSize(width: screenWidth, height: itemHeight))
                    newAttributes.append(attributes)
                    currentY = currentY.advanced(by: itemHeight) + interItemSpacing
                    return
                }
                let frameWidth = (screenWidth - interItemSpacing - interItemSpacing / 1000).half
                attributes.frame = CGRect(origin: CGPoint(x: currentX, y: currentY), size: CGSize(width: frameWidth, height: itemHeight))
                newAttributes.append(attributes)
                currentX = currentX.advanced(by: frameWidth) + interItemSpacing
                guard currentX + frameWidth < screenWidth else {
                    currentY = currentY.advanced(by: itemHeight) + interItemSpacing
                    currentX = .zero
                    return
                }
            }
            return newAttributes
        }
}
