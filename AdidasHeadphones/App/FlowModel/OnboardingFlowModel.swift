//
//  OnboardingFlowModel.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 28/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import ConnectionService

enum OnboardingType {
    case initialDeviceSetup
    case afterDeviceFailure
    case addNewDevice
}

private extension OnboardingType {
    var onboardingNavigationViewControllerPresentAnimated: Bool {
        switch self {
        case .initialDeviceSetup:
            return false
        case .afterDeviceFailure, .addNewDevice:
            return true
        }
    }
}

protocol OnboardingFlowModelDelegate: class {
    func finished()
}

class OnboardingFlowModel: BaseFlowModel {
    weak var delegate: OnboardingFlowModelDelegate?
    private let rootViewController: BaseNavigationViewController
    private var onboardingNavigationViewController: OnboardingNavigationViewController?
    private let services: ServicesType
    private var onboardingType: OnboardingType
    private let disposeBag = DisposeBag()
    init(rootViewController: BaseNavigationViewController,
         services: ServicesType,
         delegate: OnboardingFlowModelDelegate,
         onboardingType: OnboardingType) {
        self.rootViewController = rootViewController
        self.services = services
        self.delegate = delegate
        self.onboardingType = onboardingType
        super.init()
        start()
    }
}

private extension OnboardingFlowModel {
    func start() {
        onboardingNavigationViewController = OnboardingNavigationViewController()
        onboardingNavigationViewController?.modalPresentationStyle = .fullScreen
        let viewController = prepareFirstViewController()
        onboardingNavigationViewController!.setViewControllers([viewController], animated: false)
        rootViewController.present(onboardingNavigationViewController!,
                                   animated: onboardingType.onboardingNavigationViewControllerPresentAnimated,
                                   completion: { self.rootViewController.view.isHidden = false })
    }
    func prepareFirstViewController() -> UIViewController  {
        if DefaultStorage.shared.welcomePresented {
            if DefaultStorage.shared.notificationSettingsPresented {
                return prepareDiscoveryViewController()
            } else {
                return prepareNotificationsViewController()
            }
        } else {
            return prepareWelcomeViewController()
        }
    }
    func prepareWelcomeViewController() -> WelcomeViewController {
        let welcomeViewController = WelcomeViewController()
        let viewModel = WelcomeViewModel(flowOutputDelegate: self, viewInputDelegate: welcomeViewController, services: services)
        welcomeViewController.viewModel = viewModel
        return welcomeViewController
    }
    func prepareTermsAndCondViewController() -> TermsAndCondViewController {
        let termsAndCondViewController = TermsAndCondViewController()
        let viewModel = TermsAndCondViewModel(flowOutputDelegate: self, viewInputDelegate: termsAndCondViewController, services: services)
        termsAndCondViewController.viewModel = viewModel
        return termsAndCondViewController
    }
    func prepareQualityProgramViewController() -> QualityProgramViewController {
        let qualityProgramViewController = QualityProgramViewController()
        let viewModel = QualityProgramViewModel(flowOutputDelegate: self, viewInputDelegate: qualityProgramViewController, services: services)
        qualityProgramViewController.viewModel = viewModel
        return qualityProgramViewController
    }
    func prepareNotificationsViewController() -> NotificationsViewController {
        let notificationsViewController = NotificationsViewController()
        let viewModel = NotificationsViewModel(flowOutputDelegate: self, viewInputDelegate: notificationsViewController, services: services)
        notificationsViewController.viewModel = viewModel
        return notificationsViewController
    }
    func prepareDiscoveryViewController() -> DiscoveryViewController {
        let discoveryViewController = DiscoveryViewController(collectionViewLayout: HorizontalCollectionViewFlowLayout())
        let viewModel = DiscoveryViewModel(flowOutputDelegate: self, viewInputDelegate: discoveryViewController,
                                           services: services, onboardingType: onboardingType)
        discoveryViewController.viewModel = viewModel
        return discoveryViewController
    }
    func preparePairingViewController(with pairingInfo: PairingInfo) -> PairingViewController {
        let pairingViewController = PairingViewController()
        let viewModel = PairingViewModel(flowOutputDelegate: self, viewInputDelegate: pairingViewController,
                                         services: services, pairingInfo: pairingInfo)
        pairingViewController.viewModel = viewModel
        return pairingViewController
    }
    func prepareGuideViewController() -> GuideViewController {
        let guideViewController = GuideViewController(collectionViewLayout: HorizontalCollectionViewFlowLayout())
        let viewModel = GuideViewModel(flowOutputDelegate: self, viewInputDelegate: guideViewController, services: services)
        guideViewController.viewModel = viewModel
        return guideViewController
    }
    func finishOnboarding() {
        onboardingNavigationViewController?.dismiss(animated: true) {
            self.onboardingNavigationViewController = nil
            self.delegate?.finished()
        }
    }
}
extension OnboardingFlowModel: NotificationsViewModelFlowOutputDelegate {
    func notificationsDidRequestDismissal() {
        let viewController = prepareDiscoveryViewController()
        onboardingNavigationViewController!.replaceTopViewController(with: viewController, animated: false)
    }
}
extension OnboardingFlowModel: WelcomeViewModelFlowOutputDelegate {
    func didRequestStartSetup() {
        DefaultStorage.shared.welcomePresented = true
        if DefaultStorage.shared.notificationSettingsPresented {
            let viewController = prepareDiscoveryViewController()
            onboardingNavigationViewController!.replaceTopViewController(with: viewController, animated: false)
        } else {
            let viewController = prepareNotificationsViewController()
            onboardingNavigationViewController!.replaceTopViewController(with: viewController, animated: true)
        }

    }
    func didRequestTermsAndCond() {
        onboardingNavigationViewController?.pushViewController(prepareTermsAndCondViewController(), animated: true)
    }
    func didRequestQualityProgram() {
        onboardingNavigationViewController?.pushViewController(prepareQualityProgramViewController(), animated: true)
    }
}

extension OnboardingFlowModel: TermsAndCondViewModelFlowOutputDelegate {}
extension OnboardingFlowModel: QualityProgramViewModelFlowOutputDelegate {}

extension OnboardingFlowModel: DiscoveryViewModelFlowOutputDelegate {
    private func isGuideAvailable(for deviceType: DeviceType) -> Bool{
        return !DefaultStorage.shared.guide.contains(deviceType)
    }
    func didConnectDevice(reading pairingInfo: PairingInfo, deviceType: DeviceType) {
        if pairingInfo.isAccessoryConnected {
            if isGuideAvailable(for: deviceType) {
                onboardingNavigationViewController!.replaceTopViewController(with: prepareGuideViewController(), animated: false)
            } else {
                finishOnboarding()
            }
        } else {
            onboardingNavigationViewController!.replaceTopViewController(with: preparePairingViewController(with: pairingInfo), animated: false)
        }
    }
}

extension OnboardingFlowModel: PairingViewModelFlowOutputDelegate {
    func didFinishPairingAccessory() {
        guard let deviceType = services.rxConnectionService.knownDevice?.deviceTypeWithTraits.deviceType else {
            onboardingNavigationViewController!.replaceTopViewController(with: prepareGuideViewController(), animated: false)
            return
        }
        if  isGuideAvailable(for: deviceType) {
            onboardingNavigationViewController!.replaceTopViewController(with: prepareGuideViewController(), animated: false)
        } else {
            finishOnboarding()
        }
    }
    func didFailToFinishPairingAccessory() {
        onboardingType = .addNewDevice
        onboardingNavigationViewController!.replaceTopViewController(with: prepareDiscoveryViewController(), animated: false)
    }
}

extension OnboardingFlowModel: GuideViewModelFlowOutputDelegate {
    func didFinishGuide() {
        if let device = services.rxConnectionService.knownDevice?.deviceTypeWithTraits {
            var guideFinished = DefaultStorage.shared.guide
            guideFinished.insert(device.deviceType)
            DefaultStorage.shared.guide = guideFinished
        }
        finishOnboarding()
    }
    func preparePrivacyPolicyViewController() -> PrivacyPolicyViewController {
        let viewController = PrivacyPolicyViewController()
        let viewModel = PrivacyPolicyViewModel(flowOutputDelegate: self, viewInputDelegate: viewController, services: services)
        viewController.viewModel = viewModel
        return viewController
    }
    func didTapPrivacyPolicy(){
        let viewController = preparePrivacyPolicyViewController()
        onboardingNavigationViewController?.pushViewController(viewController, animated: true)
    }
}

extension OnboardingFlowModel: PrivacyPolicyViewModelFlowOutputDelegate {}

