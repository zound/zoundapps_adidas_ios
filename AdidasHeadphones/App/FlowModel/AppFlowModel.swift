//
//  AppFlowModel.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit
import ConnectionService
import RxSwift

class AppFlowModel: BaseFlowModel {
    let rootViewController: BaseNavigationViewController
    private var onboardingFlowModel: OnboardingFlowModel?
    private var settingsFlowModel: SettingsFlowModel?
    private var bluetoothOffViewController: BluetoothOffViewController?
    private let services: ServicesType
    private let disposeBag = DisposeBag()
    private var updateNotificationAlert : UIAlertController? = nil

    init(rootViewController: BaseNavigationViewController, services: ServicesType) {
        self.rootViewController = rootViewController
        self.services = services
        super.init()
        start()
    }
}

private extension AppFlowModel {
    func start() {
        
        rootViewController.view.isHidden = !DefaultStorage.shared.welcomePresented
        services.rxConnectionService.inited.subscribe(onCompleted: {
            print("AppFlowModel::inited")
            self.services.firmwareUpdateService.set(suspended: true)
            self.services.firmwareUpdateService.initialize()
            let mainViewController = MainViewController()
            let viewModel = MainViewModel(flowOutputDelegate: self, viewInputDelegate: mainViewController, services: self.services)
            let tilesViewModel = TilesViewModel(superviewOutputDelegate: mainViewController, viewInputDelegate: mainViewController.tilesView, services: self.services)
            let playerViewModel = PlayerViewModel(superviewOutputDelegate: mainViewController, viewInputDelegate: mainViewController.playerView, services: self.services)
            let firmwareUpdateMessageViewModel = FirmwareUpdateMessageViewModel(superviewOutputDelegate: mainViewController, viewInputDelegate: mainViewController.firmwareUpdateMessageView, services: self.services)
            mainViewController.viewModel = viewModel
            mainViewController.tilesView.viewModel = tilesViewModel
            mainViewController.playerView.viewModel = playerViewModel
            mainViewController.firmwareUpdateMessageView.viewModel = firmwareUpdateMessageViewModel
            self.rootViewController.setViewControllers([mainViewController], animated: false)
            self.handle(bluetoothState: self.services.rxConnectionService.bluetoothState)
        }).disposed(by: disposeBag)
        self.services.firmwareUpdateService.didChangeFirmwareUpdateState.subscribe(onNext: {[weak self] firmwareUpdateState in
            self?.handle(firmwareUpdateState: firmwareUpdateState)
         }).disposed(by: disposeBag)
        services.rxConnectionService.didUpdateBluetoothState.subscribe(onNext: { bluetoothState in
            print("AppFlowModel::didUpdateBluetoothState")
            self.handle(bluetoothState: bluetoothState)
        }).disposed(by: disposeBag)
        services.rxConnectionService.didCancelPeripheralConnection.subscribe(onNext: {
            print("AppFlowModel::didCancelPeripheralConnection")
            if !self.services.rxConnectionService.isReestablishingPeripheralConnection, self.onboardingFlowModel == nil {
                self.presentOnboardingIfNeeded(as: .afterDeviceFailure)
            }
        }).disposed(by: disposeBag)
        services.applicationModeService.appWillResignActive.subscribe(onNext: { _ in
            print("AppFlowModel::appWillResignActive")
            if let _ = self.rootViewController.presentedViewController as? DismissableModalCompliance {
                self.rootViewController.presentedViewController?.dismiss(animated: true)
            }
        }).disposed(by: disposeBag)
        print("AppFlowModel::asNew")
        _ = services.rxConnectionService.asNew() // TODO: Change to not to return value

    }
    private func present(_ viewControllerToPresent: UIViewController, completion: (() -> Void)? = nil) {
        backToRootViewController() { [weak self] in
            self?.rootViewController.present(viewControllerToPresent, animated: true, completion: completion)
        }
    }
    func handle(firmwareUpdateState: FirmwareUpdateState) {
        switch firmwareUpdateState {
        case .uploading:
            didRequestUpdateNotification()
        case .readyToInstall:
            hideUpdateNotification()
        default:
            break;
        }
    }
    func handle(bluetoothState: BluetoothState) {
        // TODO: Add modal removal handling
        func manageBluetoothOffViewController(for bluetoothState: BluetoothState) {
            if bluetoothState == .available {
                if let bluetoothOffViewController = self.bluetoothOffViewController {
                    bluetoothOffViewController.dismiss(animated: false, completion: {
                        self.bluetoothOffViewController = nil
                        self.presentOnboardingIfNeeded(as: .initialDeviceSetup)
                    })
                } else {
                    presentOnboardingIfNeeded(as: .initialDeviceSetup)
                }
            } else {
                self.bluetoothOffViewController = BluetoothOffViewController()
                self.bluetoothOffViewController?.modalPresentationStyle = .fullScreen
                self.rootViewController.present(self.bluetoothOffViewController!, animated: false)
            }
        }

        if bluetoothState == .notAvailable, let _ = rootViewController.presentedViewController {
            rootViewController.presentedViewController?.dismiss(animated: false, completion: {
                manageBluetoothOffViewController(for: bluetoothState)
            })
        } else if bluetoothState == .notAvailable, rootViewController.viewControllers.count > 1 {
            rootViewController.popToRootViewController(animated: true)
            manageBluetoothOffViewController(for: bluetoothState)
        } else {
            manageBluetoothOffViewController(for: bluetoothState)
        }
    }
    func prepareNotificationsViewController() -> NotificationsViewController {
        let notificationsViewController = NotificationsViewController()
        let viewModel = NotificationsViewModel(flowOutputDelegate: self, viewInputDelegate: notificationsViewController, services: services)
        notificationsViewController.viewModel = viewModel
        return notificationsViewController
    }
    func establishPeripheralConnection() {
        guard DefaultStorage.shared.notificationSettingsPresented else {
            let controller = prepareNotificationsViewController()
            controller.modalPresentationStyle = .fullScreen
            self.rootViewController.present(controller, animated: false)
            return
        }
        print("AppFlowModel::establishPeripheralConnection()")
        services.firmwareUpdateService.set(suspended: false)
        services.rxConnectionService.establishPeripheralConnection()
    }
    func presentOnboardingIfNeeded(as onboardingType: OnboardingType) {
        self.services.firmwareUpdateService.set(suspended: true)
        func initOnboardingFlowModel() {
            backToRootViewController {
                self.onboardingFlowModel = OnboardingFlowModel(rootViewController: self.rootViewController, services: self.services, delegate: self, onboardingType: onboardingType)
            }
        }
        switch onboardingType {
        case .initialDeviceSetup:
            guard services.rxConnectionService.knownDevice == nil else {
                establishPeripheralConnection()
                return
            }
            initOnboardingFlowModel()
        case .afterDeviceFailure, .addNewDevice:
            initOnboardingFlowModel()
        }
    }
    func backToRootViewController(completion: @escaping () -> Void) {
        if let _ = rootViewController.presentedViewController {
            rootViewController.presentedViewController?.dismiss(animated: true, completion: {
                completion()
            })
        } else if rootViewController.viewControllers.count > 1 {
            rootViewController.popToRootViewController(animated: true)
            rootViewController.transitionCoordinator?.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}

extension AppFlowModel: OnboardingFlowModelDelegate {
    func finished() {
        onboardingFlowModel = nil
        services.firmwareUpdateService.set(suspended: false)
        services.firmwareUpdateService.start()
    }
}

extension AppFlowModel: SettingsFlowModelDelegate {
    func finishedSettings() {
        settingsFlowModel = nil
    }
}

extension AppFlowModel: UpdateIndicatorViewModelFlowOutputDelegate {
    func didRequestFinishUpdate() {
        rootViewController.presentedViewController?.dismiss(animated: true)
    }
}

extension AppFlowModel: ReleaseNotesViewModelFlowOutputDelegate {}

extension AppFlowModel: UpdateViewModelFlowOutputDelegate {
    func didRequestReleaseNotes() {
        let releaseNotesViewController = ReleaseNotesViewController()
        let viewModel = ReleaseNotesViewModel(flowOutputDelegate: self, viewInputDelegate: releaseNotesViewController, services: services)
        releaseNotesViewController.viewModel = viewModel
        rootViewController.pushViewController(releaseNotesViewController, animated: true)
    }
    func didRequestInstallButton() {
        let updateViewController = UpdateIndicatorViewController()
        let viewModel = UpdateIndicatorViewModel(flowOutputDelegate: self, viewInputDelegate: updateViewController, services: services)
        updateViewController.viewModel = viewModel
        updateViewController.modalPresentationStyle = .fullScreen
        present(updateViewController) { [weak self] in
            if let count = self?.rootViewController.viewControllers.count, count > 1 {
                self?.rootViewController.popToRootViewController(animated: false)
            }
        }
    }
}

extension AppFlowModel: MainViewModelFlowOutputDelegate {
    func didRequestUpdateScreen() {
        let updateViewController = UpdateViewController()
        let viewModel = UpdateViewModel(flowOutputDelegate: self, viewInputDelegate: updateViewController, services: services)
        updateViewController.viewModel = viewModel
        rootViewController.pushViewController(updateViewController, animated: true)
    }
    func didRequestSettings() {
        let settingsFlowModel = SettingsFlowModel(rootViewController: rootViewController,
                                                  services: services,
                                                  delegate: self)
        self.settingsFlowModel = settingsFlowModel
    }
    private func privateDidRequestKnownDevices(selectingKnownDevice UUID: String? = nil) {
        let knownDevicesViewController = KnownDevicesViewController()
        let viewModel = KnownDevicesViewModel(flowOutputDelegate: self, viewInputDelegate: knownDevicesViewController, services: services)
        knownDevicesViewController.viewModel = viewModel
        knownDevicesViewController.modalPresentationStyle = .custom
        knownDevicesViewController.transitioningDelegate = rootViewController.dismissableModalTransitioning
        if let _ = UUID {
            knownDevicesViewController.viewModel?.triggerSelect(for: UUID!)
        }
        present(knownDevicesViewController)
    }
    func hideUpdateNotification() {
        guard let alert = updateNotificationAlert else {
            return
        }
        alert.dismiss(animated: true) { [weak self] in
            self?.updateNotificationAlert = nil
        }

    }
    func didRequestUpdateNotification() {
        guard updateNotificationAlert == nil else {
            return
        }
        guard rootViewController.presentedViewController == nil else {
             return
        }
        let alert = UIAlertController.updateNotificationAlert() { [weak self] _ in
            self?.updateNotificationAlert = nil
        }
        updateNotificationAlert = alert
        rootViewController.present(alert, animated: true)
    }
    func didRequestKnownDevices() {
        privateDidRequestKnownDevices()
    }
    func didRequestKnownDevices(selectingKnownDevice UUID: String) {
        backToRootViewController {
            self.privateDidRequestKnownDevices(selectingKnownDevice: UUID)
        }
    }
    func didRequestAddNewDeviceForced() {
        print("MainViewModel::cancelPeripheralConnection(), FORCED")
        services.rxConnectionService.cancelPeripheralConnection()
        services.firmwareUpdateService.set(suspended: true)
    }
    func didRequestActionButton() {
        let limitedActionButtonStyleViewController = LimitedActionButtonStyleViewController()
        let viewModel = ActionButtonStyleViewModel(services: services)
        viewModel.flowOutputDelegate = self
        viewModel.limitedFlowOutputDelegate = self
        limitedActionButtonStyleViewController.viewModel = viewModel
        limitedActionButtonStyleViewController.modalPresentationStyle = .custom
        limitedActionButtonStyleViewController.transitioningDelegate = rootViewController.dismissableModalTransitioning
        present(limitedActionButtonStyleViewController)
    }
    func didRequestEqualizer() {
        let limitedGraphicalEqualizerViewController = LimitedGraphicalEqualizerViewController()
        let viewModel = LimitedGraphicalEqualizerViewModel(flowOutputDelegate: self, viewInputDelegate: limitedGraphicalEqualizerViewController, services: services)
        limitedGraphicalEqualizerViewController.viewModel = viewModel
        limitedGraphicalEqualizerViewController.modalPresentationStyle = .custom
        limitedGraphicalEqualizerViewController.transitioningDelegate = rootViewController.dismissableModalTransitioning
        present(limitedGraphicalEqualizerViewController)
    }
    func didRequestAutoOffTimer() {
        // TODO: Implement
    }
    func didRequestStatus() {
        // TODO: Implement
    }
    func didDisconnectKnownDevice() {
        if rootViewController.viewControllers.count > 1 {
            rootViewController.popToRootViewController(animated: true)
        }
    }
}

extension AppFlowModel: KnownDevicesViewModelFlowOutputDelegate {
    func didRequestDismissal() {
        rootViewController.presentedViewController?.dismiss(animated: true)
    }
    func didRequestAddNewDevice() {
        rootViewController.presentedViewController?.dismiss(animated: true) {
            self.presentOnboardingIfNeeded(as: .addNewDevice)
        }
    }
}

extension AppFlowModel: ActionButtonStyleViewModelLimitedFlowOutputDelegate {
    func didRequestActionButtonStyleConfiuration() {
        backToRootViewController { [unowned self] in
            let settingsFlowModel = SettingsFlowModel(rootViewController: self.rootViewController,
                                                      services: self.services,
                                                      delegate: self,
                                                      requested: .actionButton)
            self.settingsFlowModel = settingsFlowModel
        }
    }
    func didRequestGoogleAssistantHelpViewController() {
        let controller = GoogleAssistantHelpViewController()
        let viewModel = GoogleAssistantHelpViewModel(flowOutputDelegate: self, viewInputDelegate: controller)
        controller.viewModel = viewModel
        rootViewController.pushViewController(controller, animated: true)
    }
}
extension AppFlowModel: GoogleAssistantHelpViewModelFlowOutputDelegate {}
extension AppFlowModel: ActionButtonStyleViewModelFlowOutputDelegate {
    func didRequestButtonConfiguration(pressType: PressType, value: ButtonAction) {
        DispatchQueue.main.async { [unowned self] in
            self.backToRootViewController { [unowned self] in
                let settingsFlowModel = SettingsFlowModel(rootViewController: self.rootViewController,
                                                          services: self.services,
                                                          delegate: self,
                                                          requested: .singleButtonConfiguration(pressType, value))
                self.settingsFlowModel = settingsFlowModel
            }
        }
    }
}

extension AppFlowModel: GraphicalEqualizerViewModelFlowOutputDelegate {}

extension AppFlowModel: LimitedGraphicalEqualizerViewModelFlowOutputDelegate {
    func didRequestGraphicalEqualizer() {
        backToRootViewController {
            let graphicalEqualizerViewController = GraphicalEqualizerViewController()
            let viewModel = GraphicalEqualizerViewModel(flowOutputDelegate: self, viewInputDelegate: graphicalEqualizerViewController, services: self.services)
            graphicalEqualizerViewController.viewModel = viewModel
            self.rootViewController.pushViewController(graphicalEqualizerViewController, animated: true)
        }
    }
}
extension AppFlowModel: NotificationsViewModelFlowOutputDelegate {
    func notificationsDidRequestDismissal() {
        rootViewController.presentedViewController?.dismiss(animated: true)
        establishPeripheralConnection()
    }
}
