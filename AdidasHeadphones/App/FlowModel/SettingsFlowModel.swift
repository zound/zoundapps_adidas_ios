//
//  SettingsFlowModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 11/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import StoreKit
import RxSwift
import ConnectionService
import SafariServices

protocol SettingsFlowModelDelegate: class {
    func finishedSettings()
}
class SettingsFlowModel: BaseFlowModel {
    weak var delegate: SettingsFlowModelDelegate?
    let rootViewController: BaseNavigationViewController
    private let services: ServicesType
    private var storeDelegate: SKStoreDelegate?
    private weak var appConnectionFlowInputDelegate: AppConnectionsViewModelFlowInputDelegate?
    init(rootViewController: BaseNavigationViewController, services: ServicesType, delegate: SettingsFlowModelDelegate, requested: SettingsEntry? = nil) {
        self.rootViewController = rootViewController
        self.services = services
        self.delegate = delegate
        super.init()
        guard let requested = requested else {
            start()
            return
        }
        didRequestSetting(entry: requested)
    }
}
private extension SettingsFlowModel {
    func start() {
        showSettings()
    }
    func showSettings() {
        let settingsViewController = SettingsViewController()
        let viewModel = SettingsViewModel(flowOutputDelegate: self, viewInputDelegate: settingsViewController, services: services)
        settingsViewController.viewModel = viewModel
        rootViewController.pushViewController(settingsViewController, animated: true)
    }
}
extension SettingsFlowModel: SettingsViewModelFlowOutputDelegate {
    func didRequestSetting(entry: SettingsEntry) {
        switch entry {
        case .aboutThisDevice:
            let aboutThisDeviceViewController = AboutThisDeviceViewController()
            let viewModel = AboutThisDeviceViewModel(flowOutputDelegate: self, viewInputDelegate: aboutThisDeviceViewController, services: services)
            aboutThisDeviceViewController.viewModel = viewModel
            rootViewController.pushViewController(aboutThisDeviceViewController, animated: true)
        case .actionButton:
            let actionStyleButtonViewController = ActionButtonStyleViewController()
            let viewModel = ActionButtonStyleViewModel(services: services)
            viewModel.flowOutputDelegate = self
            actionStyleButtonViewController.viewModel = viewModel
            rootViewController.pushViewController(actionStyleButtonViewController, animated: true)
        case .appConnections:
            let appConnectionsViewController = AppConnectionsViewController()
            let viewModel = AppConnectionsViewModel(services: services)
            viewModel.viewInputDelegate = appConnectionsViewController
            viewModel.flowOutputDelegate = self
            appConnectionFlowInputDelegate = viewModel
            appConnectionsViewController.viewModel = viewModel
            rootViewController.pushViewController(appConnectionsViewController, animated: true)
        case .graphicalEqualizer:
            let graphicalEqualizerViewController = GraphicalEqualizerViewController()
            let viewModel = GraphicalEqualizerViewModel(flowOutputDelegate: self, viewInputDelegate: graphicalEqualizerViewController, services: services)
            graphicalEqualizerViewController.viewModel = viewModel
            rootViewController.pushViewController(graphicalEqualizerViewController, animated: true)
        case .help:
            let homeViewController = HelpViewController()
            let viewModel = HelpViewModel(flowOutputDelegate: self, viewInputDelegate: homeViewController)
            homeViewController.viewModel = viewModel
            rootViewController.pushViewController(homeViewController, animated: true)
        case .aboutThisApp:
            let aboutThisAppViewController = AboutThisAppViewController()
            let viewModel = AboutThisAppViewModel(flowOutputDelegate: self, viewInputDelegate: aboutThisAppViewController)
            aboutThisAppViewController.viewModel = viewModel
            rootViewController.pushViewController(aboutThisAppViewController, animated: true)
        case .newsletter:
            let viewController = NewsletterViewController()
            let viewModel = NewsletterParentViewModel(flowOutputDelegate: self, viewInputDelegate: viewController, services: services)
            viewController.viewModel = viewModel
            rootViewController.pushViewController(viewController, animated: true)
        case .singleButtonConfiguration(let pressType, let value):
            let viewModel = ActionButtonConfigurationViewModel(pressType: pressType, value: value, services: services)
            viewModel.flowOutputDelegate = self
            let actionButtonConfigurationViewController = ActionButtonConfigurationViewController()
            actionButtonConfigurationViewController.viewModel = viewModel
            rootViewController.pushViewController(actionButtonConfigurationViewController, animated: true)
        case .qualityProgram:
            let viewController = prepareQualityProgramViewController()
            rootViewController.pushViewController(viewController, animated: true)
        }
    }
    func prepareQualityProgramViewController() -> QualityProgramViewController {
        let qualityProgramViewController = QualityProgramViewController()
        let viewModel = QualityProgramViewModel(flowOutputDelegate: self, viewInputDelegate: qualityProgramViewController, services: services)
        qualityProgramViewController.viewModel = viewModel
        return qualityProgramViewController
    }
}
extension SettingsFlowModel: QualityProgramViewModelFlowOutputDelegate {}

extension SettingsFlowModel: NewsletterParentViewModelFlowOutputDelegate {

    func preparePrivacyPolicyViewController() -> PrivacyPolicyViewController {
        let viewController = PrivacyPolicyViewController()
        let viewModel = PrivacyPolicyViewModel(flowOutputDelegate: self, viewInputDelegate: viewController, services: services)
        viewController.viewModel = viewModel
        return viewController
    }
    func didTapPrivacyPolicy(){
        let viewController = preparePrivacyPolicyViewController()
        rootViewController.pushViewController(viewController, animated: true)
    }
}

extension SettingsFlowModel: PrivacyPolicyViewModelFlowOutputDelegate {}
extension SettingsFlowModel: TermsAndCondViewModelFlowOutputDelegate {}
extension SettingsFlowModel: FOSViewModelFlowOutputDelegate {}

extension SettingsFlowModel: AboutThisAppViewModelFlowOutputDelegate {
    func didRequestAboutThisAppEntry(_ entry: AboutThisAppEntry) {
        switch entry {
        case .termsAndConditions:
            let termsAndCondViewController = prepareTermsAndCondViewController()
            rootViewController.pushViewController(termsAndCondViewController, animated: true)
        case .freeAndOpenSource:
            let viewController = prepareFOSViewController()
            rootViewController.pushViewController(viewController, animated: true)
        }
    }
}
extension SettingsFlowModel: HelpViewModelFlowOutputDelegate {

    func didRequestHelpScreenEntry(_ entry: HelpScreenEntry) {
        switch entry {
        case .selectDevice:
            let viewController = prepareSelectYourDeviceScreen()
            rootViewController.pushViewController(viewController, animated: true)
        case .contactSupport:
            UIApplication.shared.open(URL.contactPageUrl)
        case .website:
            UIApplication.shared.open(URL.adidasWebpage)
            return
        }
    }
    func prepareSelectYourDeviceScreen() -> SelectYourDeviceViewController {
        let viewController = SelectYourDeviceViewController()
        let viewModel = SelectYourDeviceViewModel(flowOutputDelegate: self, viewInputDelegate: viewController)
        viewController.viewModel = viewModel
        return viewController
    }
    func prepareTermsAndCondViewController() -> TermsAndCondViewController {
        let termsAndCondViewController = TermsAndCondViewController()
        let viewModel = TermsAndCondViewModel(flowOutputDelegate: self, viewInputDelegate: termsAndCondViewController, services: services)
        termsAndCondViewController.viewModel = viewModel
        return termsAndCondViewController
    }
    func prepareFOSViewController() -> FOSViewController {
        let viewController = FOSViewController()
        let viewModel = FOSViewModel(flowOutputDelegate: self, viewInputDelegate: viewController)
        viewController.viewModel = viewModel
        return viewController
    }
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
extension SettingsFlowModel: SelectYourDeviceViewModelFlowOutputDelegate {
    func didRequestAboutThisAppEntry(_ entry: SelectYourDeviceScreenEntry) {
        let viewController = prepareOnlineSettings(for: entry.deviceType())
        rootViewController.pushViewController(viewController, animated: true)
    }
    func prepareOnlineSettings(for device: DeviceType) -> CommonWebViewController {
        let viewController = CommonWebViewController()
        let viewModel = OnlineSettingsViewModel(flowOutputDelegate: self, viewInputDelegate: viewController, services: services, deviceType: device)
        viewController.viewModel = viewModel
        return viewController
    }
}
extension SettingsFlowModel: CommonWebViewModelFlowOutputDelegate {}
extension SettingsFlowModel: AboutThisDeviceViewModelFlowOutputDelegate {}
extension SettingsFlowModel: GraphicalEqualizerViewModelFlowOutputDelegate {}


extension SettingsFlowModel: AppConnectionsViewModelFlowOutputDelegate {
    func openSpotifyAppInfo() {
        let spotifyInfoViewController = SpotifyAppInfoViewController()
        let viewModel = SpotifyAppInfoViewModel(services: services)
        spotifyInfoViewController.viewModel = viewModel
        viewModel.viewInputDelegate = spotifyInfoViewController
        viewModel.flowOutputDelegate = self
        self.rootViewController.pushViewController(spotifyInfoViewController, animated: true)
    }
}
extension SettingsFlowModel: GoogleAssistantHelpViewModelFlowOutputDelegate {}
extension SettingsFlowModel: ActionButtonStyleViewModelFlowOutputDelegate {
    func didRequestGoogleAssistantHelpViewController() {
        let controller = GoogleAssistantHelpViewController()
        let viewModel = GoogleAssistantHelpViewModel(flowOutputDelegate: self, viewInputDelegate: controller)
        controller.viewModel = viewModel
        rootViewController.pushViewController(controller, animated: true)
    }
    func didRequestButtonConfiguration(pressType: PressType, value: ButtonAction) {
        let viewModel = ActionButtonConfigurationViewModel(pressType: pressType, value: value, services: services)
        viewModel.flowOutputDelegate = self
        let actionButtonConfigurationViewController = ActionButtonConfigurationViewController()
        actionButtonConfigurationViewController.viewModel = viewModel
        rootViewController.pushViewController(actionButtonConfigurationViewController, animated: true)
    }
}
extension SettingsFlowModel: ActionButtonConfigurationViewModelFlowOutputDelegate {
    func didRequestSpotifyButtonConfiguration(pressType: PressType) {
        let viewModel = SpotifyActionButtonConfigurationViewModel(pressType: pressType, services: services)
        viewModel.flowOutputDelegate = self
        let spotifyActionButtonConfigurationViewController = SpotifyActionButtonConfigurationViewController()
        spotifyActionButtonConfigurationViewController.viewModel = viewModel
        rootViewController.pushViewController(spotifyActionButtonConfigurationViewController, animated: true)
    }
    func didFinishButtonConfiguration() {
        rootViewController.popViewController(animated: true)
    }
    func didRequestApplicationConfiguration(id: ConnectedApplicationID) {
        didRequestSetting(entry: .appConnections)
    }
}
extension SettingsFlowModel: SpotifyActionButtonConfigurationViewModelFlowOutputDelegate {
    func didRequestSpotifySearch(type: SpotifySearchEntryType, pressType: PressType) {
        let spotifySearchViewController = SpotifySearchViewController()
        let viewModel = SpotifySearchViewModel(type: type, pressType: pressType, services: services)
        viewModel.viewInputDelegate = spotifySearchViewController
        viewModel.flowOutputDelegate = self
        spotifySearchViewController.viewModel = viewModel
        rootViewController.pushViewController(spotifySearchViewController, animated: true)
    }
}
extension SettingsFlowModel: SpotifySearchViewModelFlowOutputDelegate {
    func didSelectSpotifyAction() {
        rootViewController.popViewController(animated: true)
    }
}
class SKStoreDelegate: NSObject, SKStoreProductViewControllerDelegate {
    init(delegate: AppConnectionsViewModelFlowInputDelegate?) {
        self.delegate = delegate
    }
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
        delegate?.didFinishAppStoreRequest()
    }
    private weak var delegate: AppConnectionsViewModelFlowInputDelegate?
}
extension SettingsFlowModel: SpotifyAppInfoViewModelFlowOutputDelegate {
    func popWhileAuthorizing() {
        rootViewController.popViewController(animated: true)
    }
    func openSpotifyAccountPage() {
        UIApplication.shared.open(.spotifyAccountWebpage)
    }
}
extension SelectYourDeviceScreenEntry {
    func deviceType() -> DeviceType {
        switch self {
        case .arnold: return .arnold
        case .freeman: return .freeman
        case .desir: return .desir
        }
    }
}
