//
//  NewsletterService.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 10/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

enum NewsletterResult {
    case ok
    case error
}

protocol NewsletterServiceResult:class {
    func signUpFinishedWithResult(result: NewsletterResult)
    func signOutFinishedWithResult(result: NewsletterResult)
}

protocol NewsletterServiceType: class {
    var delegate: NewsletterServiceResult? { get set }
    var workInProgress:Bool { get }
    var newsletterSubscription:Bool { get }
    var subscriptionEmail: String { get }
    func signUp(with email: String)
    func signOut()

}
class NewsletterService: NewsletterServiceType {
    var workInProgress = false
    let api = SalesforceApiHandler()
    var newsletterSubscription: Bool {
        get {
            return DefaultStorage.shared.newsletterSubscription
        }
        set {
            DefaultStorage.shared.newsletterSubscription = newValue
        }
    }
    weak var delegate: NewsletterServiceResult?
    var subscriptionEmail: String {
        get {
            return DefaultStorage.shared.newsletterSubscriptionEmail ?? ""
        }
        set {
            DefaultStorage.shared.newsletterSubscriptionEmail = newValue
        }
    }
    var accountID: String {
        get {
            return DefaultStorage.shared.newsletterAccountID ?? ""
        }
        set {
            DefaultStorage.shared.newsletterAccountID = newValue
        }
    }
    init() {

    }
    private func subscriptionFinished(result: CreateSuccesResult) {
        if(result.success) {
            self.newsletterSubscription = true
            self.accountID = result.id
        }
        workInProgress = false
        if let delegate = delegate {
            delegate.signUpFinishedWithResult(result: result.success ? .ok : .error)
        }
    }
    func signUp(with email: String) {
        workInProgress = true
        subscriptionEmail = email
        api.signUp(email: email){ [weak self] result in
            DispatchQueue.main.async {
                self?.subscriptionFinished(result: result)
            }
        }
    }
    private func signOutFinished(result: NewsletterResult) {
        workInProgress = false
        if result == .ok {
            subscriptionEmail = ""
            newsletterSubscription = false
        }
        if let delegate = delegate {
            delegate.signOutFinishedWithResult(result: result)
        }
    }
    func signOut() {
        workInProgress = true
        guard  accountID.count > 0 else {
            self.signOutFinished(result: .error)
            return
        }
        api.signOut(account: accountID){ [weak self] result in
            DispatchQueue.main.async {
                let succes = result ? NewsletterResult.ok: NewsletterResult.error
                self?.signOutFinished(result: succes)
            }
        }
    }
}
