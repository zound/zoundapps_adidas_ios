//
//  OnlineManualUrlRequestBuilder.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 03/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//


import Foundation
import ConnectionService

private let credential = "secretKey" // TODO: Change once known

private struct HttpHeader {
    static let authField = "Authorization"
    static let authValue = "Basic \(credential)"
}

private struct PathComponents {
    static let scheme = "https"
    static let host = "d37o760fdkcpae.cloudfront.net"
    static let path = "adidas-headphones-online-output"
    static let file = "index.html"
}

extension DeviceType {
    var pathComponent: String {
        switch self {
        case .arnold:
            return "rpt-01"
        case .freeman:
            return "fwd-01"
        case .desir:
            return "rpd-01"
        }
    }
}
extension Language {
    var onlineManualLanguageComponent: String {
        return self.rawValue
    }
}
protocol OnlineManualUrlRequestBuilderType {
    var request: URLRequest? { get }
    func add(_ deviceModel: DeviceType) -> OnlineManualUrlRequestBuilderType
    func add(_ language: Language?) -> OnlineManualUrlRequestBuilderType
}
class OnlineManualUrlRequestBuilder: OnlineManualUrlRequestBuilderType {

    private var deviceModel: DeviceType = .arnold
    private var language: Language = Language.prefered()

    private var urlComponents: URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponents.scheme
        urlComponents.host = PathComponents.host
        return urlComponents
    }

    var request: URLRequest? {
        guard var url = urlComponents.url else {
            return nil
        }
        [PathComponents.path, language.onlineManualLanguageComponent, deviceModel.pathComponent, PathComponents.file].forEach {
            url.appendPathComponent($0)
        }
        var request = URLRequest(url: url)
        request.setValue(HttpHeader.authValue, forHTTPHeaderField: HttpHeader.authField)
        return request
    }

    @discardableResult
    func add(_ deviceModel: DeviceType) -> OnlineManualUrlRequestBuilderType {
        self.deviceModel = deviceModel
        return self
    }

    @discardableResult
    func add(_ language: Language?) -> OnlineManualUrlRequestBuilderType {
        guard let language = language else { return self }
        self.language = language
        return self
    }

}
