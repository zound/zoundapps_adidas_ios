//
//  FirmwareUpdateService.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 09/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import ConnectionService

private struct Const {
    static let autoPowerOnTimeout = 20
    static let autoOffTimeout = 4
    static let retryTimeout =  10
    static let minimumBaterryLevel = 50
    static let retryLimits = 1
}

enum FirmwareUpdateState {
    case off
    case started
    case restarted
    case uploading
    case readyToInstall // UI
    case installing // UI
    case waitingToPowerOn // UI
    case finalizing // UI
    case completed // UI
    case failed // UI
}

protocol FirmwareUpdateServiceType {
    func setup(dependencies: FirmwareUpdateService.Dependencies)
    func initialize()
    func start()
    func set(suspended: Bool)
    func install()
    var firmwareUpdateState: FirmwareUpdateState { get }
    var didChangeFirmwareUpdateState: PublishSubject<FirmwareUpdateState> { get }
    var didChangeProgress: PublishSubject<Float>  { get }
}

class FirmwareUpdateService: FirmwareUpdateServiceType {
    typealias Dependencies = ReactiveConnectionServiceType
    private var rxConnectionService: ReactiveConnectionServiceType!
    private var monitoredPeripheralConnectionStateSubscription: Disposable?
    private var batteryLevelSubscription: Disposable?
    private var firmwareRevisionSubscription: Disposable?
    private var monitoredOverTheAirStateSubscription: Disposable?
    private var suspended = false
    private var shouldRetry = false
    private var retryCounter = 0
    var firmwareUpdateState: FirmwareUpdateState = .off {
        didSet {
            print("OTASTATE >", firmwareUpdateState)
            guard firmwareUpdateState != oldValue else { return }
            didChangeFirmwareUpdateState.onNext(firmwareUpdateState)
            autoSwitchToOffIfNeeded()
            retryIfNeeded()
        }
    }

    var didChangeProgress = PublishSubject<Float>()

    var didChangeFirmwareUpdateState = PublishSubject<FirmwareUpdateState>()
    private let disposeBag = DisposeBag()
    func setup(dependencies: Dependencies) {
        self.rxConnectionService = dependencies
    }
    func initialize() {
        rxConnectionService.didEstablishPeripheralConnectionToReactiveDevice.subscribe(onNext: { [weak self] reactiveDevice in
                print("FirmwareUpdateService::didEstablishPeripheralConnectionToReactiveDevice")
                self?.startMonitoringPeripheralConnectionState(of: reactiveDevice)
        }).disposed(by: disposeBag)
        rxConnectionService.didCancelPeripheralConnection.subscribe(onNext: { [weak self] in
            self?.monitoredPeripheralConnectionStateSubscription?.dispose()
            self?.shouldRetry = false
        }).disposed(by: disposeBag)
    }
    func start() {
        print("FirmwareUpdateService::start")
        if let reactiveDevice = rxConnectionService.reactiveDevice {
            handle(peripheralConnectionState: reactiveDevice.peripheralConnectionState, of: reactiveDevice)
        }
    }
    func set(suspended: Bool) {
        print("FirmwareUpdateService::set \(suspended)")
        self.suspended = suspended
    }
    func install() {
        print("FirmwareUpdateService::install")
        firmwareUpdateState = .installing
        rxConnectionService.reactiveDevice?.reboot()
    }
}

private extension FirmwareUpdateService {
    func startMonitoringPeripheralConnectionState(of reactiveDevice: ReactiveDevice) {
        monitoredPeripheralConnectionStateSubscription?.dispose()
        monitoredPeripheralConnectionStateSubscription = reactiveDevice.monitoredPeripheralConnectionState.subscribe(onNext: { [weak self] peripheralConnectionState in
            self?.handle(peripheralConnectionState: peripheralConnectionState, of: reactiveDevice)
        })
    }
    func testBatteryLevel(for reactiveDevice: ReactiveDevice, completion: @escaping (Bool) -> Void) {
        batteryLevelSubscription?.dispose()
        batteryLevelSubscription = reactiveDevice.batteryLevel.take(1).subscribe(onNext: {  batteryLevelResult in
            switch batteryLevelResult {
            case .success(let batteryLevel):
                completion(batteryLevel > Const.minimumBaterryLevel)
            default:
                completion(false)
            }
        })
        reactiveDevice.read(deviceFeature: .batteryLevel)
    }
    private func startOverTheAir(on reactiveDevice: ReactiveDevice, isBatteryOK: Bool) {
        guard isBatteryOK else {
            return
        }
        print("INTERRUPTED \(reactiveDevice.overTheAirInfo.isInterrupted)")
        if reactiveDevice.overTheAirInfo.isInterrupted {
            completeFirmwareUpdate(on: reactiveDevice)
        } else {
            readFirmwareRevision(from: reactiveDevice)
        }
    }
    private func startOverTheAir(on reactiveDevice: ReactiveDevice) {
        guard reactiveDevice.isOverTheAirSupported() else {
            return
        }
        testBatteryLevel(for: reactiveDevice) { [weak self] ok in
            self?.batteryLevelSubscription?.dispose()
            self?.startOverTheAir(on: reactiveDevice, isBatteryOK: ok)
        }
    }
    func handle(peripheralConnectionState: PeripheralConnectionState, of reactiveDevice: ReactiveDevice) {
        if !suspended, peripheralConnectionState == .connected {
            startOverTheAir(on: reactiveDevice)
        } else if peripheralConnectionState == .disconnected {
            shouldRetry = false
            retryCounter = 0
            firmwareRevisionSubscription?.dispose()
            monitoredOverTheAirStateSubscription?.dispose()
            autoSwitchToWaitingToPowerOnIfNeeded()
        }
    }
}

private extension FirmwareUpdateService {
    func completeFirmwareUpdate(on reactiveDevice: ReactiveDevice) {
        let deviceType = reactiveDevice.deviceTypeWithTraits.deviceType
        let firmwareType = FirmwareType(deviceType: deviceType)
        guard let overTheAirInfo = rxConnectionService.reactiveDevice?.overTheAirInfo else {
            return
        }
        guard !overTheAirInfo.firmwareVersion.isEmpty else {
            readFirmwareRevision(from: reactiveDevice)
            return
        }
        guard let downloadedFirmwareURL = FirmwareManager.downloadedFirmwareURL(for: firmwareType, with: overTheAirInfo.firmwareVersion) else {
            readFirmwareRevision(from: reactiveDevice)
            return
        }
        guard let firmware = Firmware(contentsOf: downloadedFirmwareURL) else {
            return
        }
        monitoredOverTheAirStateSubscription?.dispose()
        monitoredOverTheAirStateSubscription = rxConnectionService.reactiveDevice?.monitoredOverTheAirState.subscribe(onNext: { [weak self] overTheAirState in
            self?.handle(overTheAirState: overTheAirState)
        })
        firmwareUpdateState = .restarted
        rxConnectionService.reactiveDevice?.completeFirmwareUpdate(with: firmware)

    }
    func autoSwitchToWaitingToPowerOnIfNeeded() {
        if firmwareUpdateState == .installing {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Const.autoPowerOnTimeout)) { [weak self] in
                if self?.firmwareUpdateState == .installing {
                    self?.firmwareUpdateState = .waitingToPowerOn
                }
            }
        } else {
            firmwareUpdateState = .off
        }
    }
    func autoSwitchToOffIfNeeded() {
        if firmwareUpdateState == .completed || firmwareUpdateState == .failed {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Const.autoOffTimeout)) { [weak self] in
                if self?.firmwareUpdateState == .completed || self?.firmwareUpdateState == .failed {
                    self?.firmwareUpdateState = .off
                }
            }
        }
    }
    func retryIfNeeded() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Const.retryTimeout)) { [weak self] in
            if self?.firmwareUpdateState == .off, self?.shouldRetry == true {
                self?.shouldRetry = false
                self?.retryCounter += 1
                self?.start()
            }
        }
    }
}

private extension FirmwareUpdateService {
    func readFirmwareRevision(from reactiveDevice: ReactiveDevice) {
        firmwareRevisionSubscription = reactiveDevice.firmwareRevision.subscribe(onNext: { [weak self] firmwareRevisionResult in
            if case .success(let firmwareRevision) = firmwareRevisionResult {
                self?.compare(currentFirmwareVersion: firmwareRevision)
            }
        })

        reactiveDevice.read(deviceFeature: .firmwareRevision)
    }
    func compare(currentFirmwareVersion: String, with availableFirmware: AvailableFirmware?, for firmwareType: FirmwareType) {
        print("FirmwareUpdateService::didCheckForFirmware \(String(describing: availableFirmware))")
        guard let availableFirmware = availableFirmware else {
            return
        }
        guard availableFirmware.version != currentFirmwareVersion else {
            print("Available firmware version is the same as current firmware version.")
            return
        }
        print("availableFirmware:\( availableFirmware.version), currentFirmwareVersion\(currentFirmwareVersion)")
        FirmwareManager.get(availableFirmware: availableFirmware, type: firmwareType) { [weak self] downloadedFirmwareURL in
            self?.firmwareUpdateState = .started
            if let downloadedFirmwareURL = downloadedFirmwareURL {
                print("FirmwareUpdateService::didGetNewFirmware \(downloadedFirmwareURL)")
                self?.startFirmwareUpdate(with: downloadedFirmwareURL, firmwareVersion: availableFirmware.version)
            } else {
                self?.firmwareUpdateState = .off
            }
        }
    }
    func compare(currentFirmwareVersion: String) {
        firmwareRevisionSubscription?.dispose()
        if let deviceType = rxConnectionService.reactiveDevice?.deviceTypeWithTraits.deviceType {
            let firmwareType = FirmwareType(deviceType: deviceType)
            FirmwareManager.check(forNew: firmwareType) { [weak self] availableFirmware in
                self?.compare(currentFirmwareVersion: currentFirmwareVersion, with: availableFirmware, for: firmwareType)
            }
        }
    }
    func startFirmwareUpdate(with downloadedFirmwareURL: URL, firmwareVersion: String) {
        if let firmware = Firmware(contentsOf: downloadedFirmwareURL) {
            monitoredOverTheAirStateSubscription?.dispose()
            monitoredOverTheAirStateSubscription = rxConnectionService.reactiveDevice?.monitoredOverTheAirState.subscribe(onNext: { [weak self] overTheAirState in
                self?.handle(overTheAirState: overTheAirState)
            })
            print("FirmwareUpdateService::speedMode(.on)")
            rxConnectionService.reactiveDevice?.write(speedMode: .on) { [weak self] result in
                if case .success(()) = result {
                    self?.rxConnectionService.reactiveDevice?.startOverTheAirUpdate(with: firmware, firmwareVersion: firmwareVersion)
                } else {
                    self?.firmwareUpdateState = .off
                }
            }
        } else {
            firmwareUpdateState = .off
        }
    }
    func handle(overTheAirState: OverTheAirState) {
        if case .sentInternal(progress: let progress) = overTheAirState {
            if Int(progress * 10000) % 100 == 0 {
                print("Internal progress: \(Int(progress * 100))%")
            }
        } else {
             print(overTheAirState)
        }
        switch overTheAirState {
        case .sent(progress: let progress):
            firmwareUpdateState = .uploading
            didChangeProgress.onNext(progress)
        case .sentInternal(progress: let progress):
            didChangeProgress.onNext(progress)
        case .initiated, .readyToInit, .waitingForRequest, .started, .rebooting:
            break
        case .resumedAfterReboot:
            firmwareUpdateState = .finalizing
        case .readyToReboot:
            firmwareUpdateState = .readyToInstall
        case .completed:
            firmwareUpdateState = .completed
            clearAfterOverTheAir()
        case .aborted(let error):
            firmwareUpdateState = .failed
            clearAfterOverTheAir()
            shouldRetry = shouldRetry(for: error)
        }
    }
    func shouldRetry( for error: OverTheAirError) -> Bool {
        switch error {
        case .writeError, .batteryTooLow:
            return false
        default:
            return retryCounter < Const.retryLimits
        }
    }
    func clearAfterOverTheAir() {
        monitoredOverTheAirStateSubscription?.dispose()
        print("FirmwareUpdateService::speedMode(.off)")
        rxConnectionService.reactiveDevice?.write(speedMode: .off, completion: { _ in })
    }
}
private extension ReactiveDevice {
    func isOverTheAirSupported() -> Bool {
        switch deviceTypeWithTraits.deviceType {
        case .freeman, .desir:
            return true
        default:
            return false
        }
    }
}
