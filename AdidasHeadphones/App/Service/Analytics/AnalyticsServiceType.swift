//
//  AnalyticsServiceType.swift
//  AdidasHeadphones
//
//  Created by Łukasz Wudarski on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import ConnectionService

public protocol AnalyticsServiceInputs {
    func log(_ event: AnalyticsEvent)
//    func log(_ event: AnalyticsEvent, associatedWith deviceModel: String)
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceType?)
    func setAnalyticsCollection(enabled: Bool)
}

public protocol AnalyticsServiceType {
    var inputs: AnalyticsServiceInputs { get }
}

public extension AnalyticsServiceInputs {
    func log(_ event: AnalyticsEvent) {
        log(event, associatedWith: nil)
    }
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceType?) {
        log(event, associatedWith: deviceModel)
    }
}
