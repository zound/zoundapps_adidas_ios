//
//  AnalyticsEventProviding.swift
//  AdidasHeadphones
//
//  Created by Łukasz Wudarski on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

protocol AnalyticsEventProviding {
    var analyticsEvent: AnalyticsEvent { get }
}
