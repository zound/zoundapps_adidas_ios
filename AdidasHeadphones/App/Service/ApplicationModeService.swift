//
//  ApplicationModeService.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 29/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol ApplicationModeServiceType {
    var appDidBecomeActive: PublishSubject<Void> { get }
    var appWillResignActive: PublishSubject<Void> { get }
    var appWillTerminate: PublishSubject<Void> { get }
}

class ApplicationModeService: ApplicationModeServiceType {
    var appDidBecomeActive = PublishSubject<Void>()
    var appWillResignActive = PublishSubject<Void>()
    var appWillTerminate = PublishSubject<Void>()
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActiveNotification),
                                               name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActiveNotification),
                                               name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willTerminateNotification),
                                               name: UIApplication.willTerminateNotification, object: nil)
    }
    @objc func appDidBecomeActiveNotification() {
        appDidBecomeActive.onNext(())
    }
    @objc func appWillResignActiveNotification() {
        appWillResignActive.onNext(())
    }
    @objc func willTerminateNotification() {
        appWillTerminate.onNext(())
    }
}
