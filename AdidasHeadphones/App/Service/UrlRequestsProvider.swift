//
//  UrlRequestBuildersProvider.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 04/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


protocol UrlRequestsProviderType {
    func onlineManualUrlRequestBuilder() -> OnlineManualUrlRequestBuilderType
    func contactPageUrlRequest() -> URLRequest?
    func homePageUrlRequest() -> URLRequest?
}
class UrlRequestsProvider: UrlRequestsProviderType {
    func onlineManualUrlRequestBuilder() -> OnlineManualUrlRequestBuilderType {
        return OnlineManualUrlRequestBuilder()
    }
    func contactPageUrlRequest() -> URLRequest? {
        return  URLRequest(url: URL.contactPageUrl)
    }
    func homePageUrlRequest() -> URLRequest? {
        return URLRequest(url: URL.adidasWebpage)
    }
}
