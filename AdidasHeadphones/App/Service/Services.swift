//
//  Services.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 28/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSpotifyWrapper
import Reachability

typealias RxSpotifyManagerType = RxStreamingSpotifyManagerInterface & ActionButtonEventHandler & ConnectedApplicationInfoProvider

protocol ServicesType {
    var rxConnectionService: ReactiveConnectionServiceType { get }
    var firmwareUpdateService: FirmwareUpdateServiceType { get }
    var quickActionsService: QuickActionServiceType { get }
    var rxSpotifyManager: RxSpotifyManagerType { get }
    var applicationModeService: ApplicationModeServiceType { get }
    var actionButtonService: ActionButtonService { get }
    var urlRequests: UrlRequestsProviderType { get }
    var reachability: Reachability { get }
    var newsletterService: NewsletterServiceType { get }
    var analyticsService: AnalyticsServiceType { get }
    var notificationsService: FirebaseNotificationsServiceType { get }
}
class Services: ServicesType {
    var rxConnectionService: ReactiveConnectionServiceType
    var firmwareUpdateService: FirmwareUpdateServiceType
    var rxSpotifyManager: RxSpotifyManagerType
    var quickActionsService: QuickActionServiceType
    var applicationModeService: ApplicationModeServiceType
    var actionButtonService: ActionButtonService
    var urlRequests: UrlRequestsProviderType
    var reachability: Reachability
    var newsletterService: NewsletterServiceType
    var analyticsService: AnalyticsServiceType
    var notificationsService: FirebaseNotificationsServiceType

    init(_ actionButtonHandlers: [ActionButtonEventHandler], _ application: UIApplication) {
        self.rxConnectionService = ReactiveConnectionService()
        self.firmwareUpdateService = FirmwareUpdateService()
        self.quickActionsService = QuickActionService()
        self.rxSpotifyManager = RxStreamingSpotifyManager.shared.configure()
        self.applicationModeService = ApplicationModeService()
        self.actionButtonService = ActionButtonService(handlers: actionButtonHandlers)
        self.urlRequests = UrlRequestsProvider()
        self.reachability = try! Reachability()
        self.newsletterService = NewsletterService()
        self.notificationsService = FirebaseNotificationsService(application: application)
        self.analyticsService = FirebaseAnalyticsService()


        setupDependencies()
    }
    private func setupDependencies() {
        quickActionsService.setup(dependencies: rxConnectionService)
        firmwareUpdateService.setup(dependencies: rxConnectionService)
    }
}
