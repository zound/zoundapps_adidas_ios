//
//  QuickActionsService.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 28/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService

enum ShortcutItemsSource {
    case launch
    case action
}

private struct Const {
    static let userInfoUUIDKey = "uuid"
    struct ShortcutItemType {
        static let headphones = "headphones"
    }
}

protocol QuickActionServiceType {
    var shortcutItems: [UIApplicationShortcutItem] { get }
    @discardableResult func getUUIDToProcess() -> String?
    func registerToProcess(shortcutItem: UIApplicationShortcutItem, as source: ShortcutItemsSource)
    func setup(dependencies: QuickActionService.Dependencies)
}

class QuickActionService: QuickActionServiceType {
    typealias Dependencies = ReactiveConnectionServiceType
    private var rxConnectionService: ReactiveConnectionServiceType?
    private let headphonesIcon = UIApplicationShortcutIcon(templateImageName: "headphonesBlackHomeAction")
     var uuidToProcess: String?
     var uuidToProcessLaunch: String?
    var shortcutItems: [UIApplicationShortcutItem] {
        guard let _ = rxConnectionService?.knownDevice else {
            return []
        }
        return rxConnectionService?.knownDevices.map({
            let userInfo: [String: NSSecureCoding] = [Const.userInfoUUIDKey: $0.peripheralUUIDString as NSSecureCoding]
            return UIApplicationShortcutItem(type: Const.ShortcutItemType.headphones,
                                             localizedTitle: $0.name,
                                             localizedSubtitle: nil,
                                             icon: headphonesIcon,
                                             userInfo: userInfo)
        }) ?? []
    }
    @discardableResult
    func getUUIDToProcess() -> String? {
        defer {
            (uuidToProcess, uuidToProcessLaunch) = (nil, nil)
        }
        return uuidToProcessLaunch == nil ? uuidToProcess : nil
    }
    func registerToProcess(shortcutItem: UIApplicationShortcutItem, as source: ShortcutItemsSource) {
        switch shortcutItem.type {
        case Const.ShortcutItemType.headphones where shortcutItem.userInfo != nil:
            handleHeadphone(shortcutItem.userInfo!, as: source)
        default:
            break
        }
    }
    func setup(dependencies: Dependencies) {
        self.rxConnectionService = dependencies
    }
}

private extension QuickActionService {
    func handleHeadphone(_ userInfo: [String: NSSecureCoding], as source: ShortcutItemsSource) {
        guard let uuid = userInfo[Const.userInfoUUIDKey] as? String else {
            return
        }
        switch source {
        case .launch:
            rxConnectionService?.replace(knownDeviceWith: uuid)
            uuidToProcessLaunch = uuid
        case .action:
            uuidToProcess = uuid
        }
    }
}
