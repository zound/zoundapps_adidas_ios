//
//  Language.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 03/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit



public enum Language: String {
    case en
    case da                  // da Danish
    case nl                  // nl Dutch
    case de                  // de German
    case fr                  // fr French
    case it                  // it Italian
    case nb = "no"           // no Norwegian
    case pt                  // pt Portuguese
    case ru                  // ru Russian
    case es                  // es Spanish
    case sv = "se"           // se Swedish
    case fi                  // fi Finnish
    case zh = "zh-cn"        // zh-cn Chinese (Simplified)
    case ja = "jp"           // jp Japanese

    public init?(languageString language: String) {
        switch language.prefix(3) {
        case "en-": self = .en
        case "da-": self = .da    // danish
        case "nl-": self = .nl    // dutch
        case "de-": self = .de    // german
        case "fr-": self = .fr    // french
        case "it-": self = .it    // italian
        case "nb-": self = .nb    // norwegian
        case "pt-": self = .pt    // portuguese
        case "ru-": self = .ru    // russian
        case "es-": self = .es    // spanish
        case "sv-": self = .sv    // swedish
        case "fi-": self = .fi    // finnish
        case "zh-": self = .zh    // chinese (simplified)
        case "ja-": self = .ja    // japanese
        default: return nil
        }
    }

    public init?(languageStrings languages: [String]) {
        guard let language = languages
            .lazy
            .map ({ String($0.prefix(7)) })
            .compactMap(Language.init(languageString:))
            .first else {
                return nil
        }
        self = language
    }

    public static func prefered()->Language{
        let language = Language(languageStrings: Locale.preferredLanguages) ?? Language.en
        return language
    }


}
