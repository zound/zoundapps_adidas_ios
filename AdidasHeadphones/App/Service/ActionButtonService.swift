//
//  ActionButtonService.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 22/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService
import RxSwift

protocol ActionButtonEventHandler {
    static var supportedActions: [ButtonAction] { get }
    func execute(action: ButtonAction, pressType: PressType, using: ReactiveDevice?)
    func set(action: ButtonAction, pressType: PressType, using: ReactiveDevice?)
    func verify(remote: ActionButtonConfiguration, device: ReactiveDevice?) -> Single<ActionButtonConfiguration>
    func didSelect(type: ActionType, device: ReactiveDevice?)
}
typealias ConfigurationInfo = (events: [ActionButtonEvent], local: Bool)

final class ActionButtonService {
    static let supportedButtonActions: [PressType] = [.singlePress, .doublePress, .triplePress, .longPress]
    private var handlers = [ActionButtonEventHandler]()
    private var device: ReactiveDevice?
    private var disposeBag = DisposeBag()
    private var slots = { supportedButtonActions.map { ActionButtonEvent(pressType: $0, buttonAction: .noAction) } }()
    var rxButtonConfiguration = BehaviorSubject<ConfigurationInfo>.init(value: ConfigurationInfo(events: [], local: true))
    init(handlers: [ActionButtonEventHandler]) {
        self.handlers = handlers
    }
    func configure(with device: ReactiveDevice?) {
        disposeBag = DisposeBag()
        device?.actionButtonConfiguration
            .subscribe(
                onNext: { [unowned self] result in
                    if case .success(let configuration) = result {
                        var localDisposeBag = DisposeBag()
                        let validators: [Observable<ActionButtonConfiguration>] = self.handlers.map { $0.verify(remote: configuration, device: device).asObservable() }
                        Observable.concat(validators)
                            .subscribe(
                                onNext: { [unowned self] verified in
                                    update(remoteConfiguration: verified,
                                           localConfiguration: &self.slots,
                                           supportedActions: type(of: self).supportedButtonActions)
                                    self.rxButtonConfiguration.onNext(ConfigurationInfo(events: self.slots, local: false))
                                },
                                onCompleted: {
                                    localDisposeBag = DisposeBag()
                                }
                            )
                            .disposed(by: localDisposeBag)
                    }
                })
            .disposed(by: disposeBag)
        device?.monitoredActionButtonConfiguration
            .subscribe(
                onNext: { [unowned self] monitoredConfiguration in
                    update(remoteConfiguration: monitoredConfiguration,
                           localConfiguration: &self.slots,
                           supportedActions: type(of: self).supportedButtonActions)
                    self.rxButtonConfiguration.onNext(ConfigurationInfo(events: self.slots, local: false))
                }
            )
            .disposed(by: disposeBag)
        device?.monitoredActionButtonEvent
            .subscribe(
                onNext: { [unowned self] (pressType, buttonAction) in
                    guard let provider = self.handlers.first(where: { type(of: $0).supportedActions.contains(buttonAction) }) else {
                        return
                    }
                    provider.execute(action: buttonAction, pressType: pressType, using: self.device)
                })
            .disposed(by: disposeBag)
        device?.read(deviceFeature: .actionButtonConfiguration)
        device?.setActionButtonConfigurationMonitoring(enabled: true)
        device?.setActionButtonEventMonitoring(enabled: true)
        self.device = device
    }
    func requestConfiguration() {
        device?.read(deviceFeature: .actionButtonConfiguration)
    }
    func write(userConfiguration: [ActionButtonEvent], skipUpdate: Bool = true) {
        var configuration: [PressType: ButtonAction] = [:]
        userConfiguration.forEach { configuration[$0.pressType] = $0.buttonAction }
        update(remoteConfiguration: configuration,
               localConfiguration: &slots,
               supportedActions: type(of: self).supportedButtonActions)
        device?.write(actionButtonConfiguration: configuration, completion: { [unowned self] _ in
            self.rxButtonConfiguration.onNext(ConfigurationInfo(events: self.slots, local: skipUpdate))
        })
    }
    func set(action: ButtonAction, for pressType: PressType) {
        guard let current = try? rxButtonConfiguration.value() else { return }
        let newConfiguration = current.events.map { ActionButtonEvent(pressType: $0.pressType,
                                                               buttonAction: $0.pressType == pressType ? action : $0.buttonAction) }
        handlers.forEach { $0.set(action: action, pressType: pressType, using: device) }
        write(userConfiguration: newConfiguration, skipUpdate: false)
    }
    func didSelect(type: ActionType) {
        handlers.forEach { $0.didSelect(type: type, device: device) }
    }
}
private func update(remoteConfiguration: ActionButtonConfiguration,
                    localConfiguration: inout [ActionButtonEvent],
                    supportedActions: [PressType]) {
    localConfiguration.removeAll()
    supportedActions.forEach {
        if let action = remoteConfiguration[$0] {
            localConfiguration.append(ActionButtonEvent(pressType: $0, buttonAction: action))
        } else {
            localConfiguration.append(ActionButtonEvent(pressType: $0, buttonAction: .noAction))
        }
    }
}
