//
//  FirebaseLoggable.swift
//  AdidasHeadphones
//
//  Created by Łukasz Wudarski on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

protocol FirebaseLoggable {
var objectValue: NSObject { get }
}

extension String: FirebaseLoggable {
    var objectValue: NSObject {
        return self as NSString
    }
}

extension Int: FirebaseLoggable {
    var objectValue: NSObject {
        return NSNumber(value: self)
    }
}

extension UInt: FirebaseLoggable {
    var objectValue: NSObject {
        return NSNumber(value: self)
    }
}
