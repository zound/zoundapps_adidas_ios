//
//  FirebaseAnalyticsParametersBuilder.swift
//  AdidasHeadphones
//
//  Created by Łukasz Wudarski on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService

class FirebaseAnalyticsParametersBuilder {

    private enum FirebaseEventParameterName: String {
        case deviceModel
    }

    private var internalParameters: [String: FirebaseLoggable] = [:]

    var parameters: [String : Any]? {
        return internalParameters.isEmpty ? nil : internalParameters.mapValues { $0.objectValue }
    }

    @discardableResult
    func add(_ parameters: [String: FirebaseLoggable]?) -> FirebaseAnalyticsParametersBuilder {
        guard let nonOptionalParameters = parameters else { return self }
        internalParameters = internalParameters.merging(nonOptionalParameters) { (_, new) in new }
        return self
    }

    @discardableResult
    func add(_ deviceModelParameter: DeviceType) -> FirebaseAnalyticsParametersBuilder {
        switch deviceModelParameter {
        case .arnold:
            internalParameters[FirebaseEventParameterName.deviceModel.rawValue] = "RPT-01"
        case .freeman:
            internalParameters[FirebaseEventParameterName.deviceModel.rawValue] = "FWD-01"
        case .desir:
            internalParameters[FirebaseEventParameterName.deviceModel.rawValue] = "RPD-01"
        }
        return self
    }

}
