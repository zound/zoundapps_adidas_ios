//
//  FirebaseAnalyticsService.swift
//  AdidasHeadphones
//
//  Created by Łukasz Wudarski on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import FirebaseCore
import FirebaseAnalytics
import ConnectionService
import FirebaseInstanceID

final class FirebaseAnalyticsService: AnalyticsServiceType, AnalyticsServiceInputs {

    public var inputs: AnalyticsServiceInputs { return self }

    public func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceType?) {
        guard let type = deviceModel else { return }
        let parameters = FirebaseAnalyticsParametersBuilder()
            .add(type)
            .add(event.parameters)
            .parameters
        Analytics.logEvent(event.name, parameters: parameters)
    }

    public func setAnalyticsCollection(enabled: Bool) {
        Analytics.setAnalyticsCollectionEnabled(enabled)
    }
    public init() {
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()

        // [START log_iid_reg_token]
        InstanceID.instanceID().instanceID { (result, error) in
          if let error = error {
            print("Error fetching remote instance ID: \(error)")
          } else if let result = result {
            print("Remote instance ID token: \(result.token)")
           
          }
        }

    }
}
