//
//  AnalyticsEvent+Extensions.swift
//  AdidasHeadphones
//
//  Created by Łukasz Wudarski on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

private struct FirebaseEvent {
    enum Name: String {
        case appGraphicalEqualizerPresetChanged
        case appSpeakerNameChanged
        case appShareDataSwitched
        case appOtaStarted
        case appOtaCompleted
        case appOtaFailed
        case appForcedOtaStarted
        case appOnboardingStayUpdatedSkipped
        case appOnboardingShareDataSkipped
        case appNewsletterSignedUp
        case appVolumeChanged
        case appSourceBluetoothActivated
        case appSourceAuxActivated
        case appSourceRcaActivated
        case appMediaButtonTouchedPlay
        case appMediaButtonTouchedPause
        case appMediaButtonTouchedNext
        case appMediaButtonTouchedPrev
        case appCouplingSwitchedAmbient
        case appCouplingSwitchedStereo
        case appLightSettingChanged
        case appErrorOccurredBluetooth
        case appErrorOccurredNetwork
        case appErrorOccurredOtaFlashing
        case appErrorOccurredOtaDownload
        case appErrorOccurredOtaUndefined
    }
    enum ParameterName: String {
        case preset
        case switchState
        case otaError
        case volume
        case intensity
    }
}

extension AnalyticsEvent {
    var name: String {
        switch self {
        case .appGraphicalEqualizerPresetChanged:
            return FirebaseEvent.Name.appGraphicalEqualizerPresetChanged.rawValue
        case .appSpeakerNameChanged:
            return FirebaseEvent.Name.appSpeakerNameChanged.rawValue
        case .appShareDataSwitched:
            return FirebaseEvent.Name.appShareDataSwitched.rawValue
        case .appOtaStarted:
            return FirebaseEvent.Name.appOtaStarted.rawValue
        case .appOtaCompleted:
            return FirebaseEvent.Name.appOtaCompleted.rawValue
        case .appOtaFailed:
            return FirebaseEvent.Name.appOtaFailed.rawValue
        case .appForcedOtaStarted:
            return FirebaseEvent.Name.appForcedOtaStarted.rawValue
        case .appOnboardingStayUpdatedSkipped:
            return FirebaseEvent.Name.appOnboardingStayUpdatedSkipped.rawValue
        case .appOnboardingShareDataSkipped:
            return FirebaseEvent.Name.appOnboardingShareDataSkipped.rawValue
        case .appNewsletterSignedUp:
            return FirebaseEvent.Name.appNewsletterSignedUp.rawValue
        case .appVolumeChanged:
            return FirebaseEvent.Name.appVolumeChanged.rawValue
        case .appSourceBluetoothActivated:
            return FirebaseEvent.Name.appSourceBluetoothActivated.rawValue
        case .appSourceAuxActivated:
            return FirebaseEvent.Name.appSourceAuxActivated.rawValue
        case .appSourceRcaActivated:
            return FirebaseEvent.Name.appSourceRcaActivated.rawValue
        case .appMediaButtonTouchedPlay:
            return FirebaseEvent.Name.appMediaButtonTouchedPlay.rawValue
        case .appMediaButtonTouchedPause:
            return FirebaseEvent.Name.appMediaButtonTouchedPause.rawValue
        case .appMediaButtonTouchedNext:
            return FirebaseEvent.Name.appMediaButtonTouchedNext.rawValue
        case .appMediaButtonTouchedPrev:
            return FirebaseEvent.Name.appMediaButtonTouchedPrev.rawValue
        case .appCouplingSwitchedAmbient:
            return FirebaseEvent.Name.appCouplingSwitchedAmbient.rawValue
        case .appCouplingSwitchedStereo:
            return FirebaseEvent.Name.appCouplingSwitchedStereo.rawValue
        case .appLightSettingChanged:
            return FirebaseEvent.Name.appLightSettingChanged.rawValue
        case .appErrorOccurredBluetooth:
            return FirebaseEvent.Name.appErrorOccurredBluetooth.rawValue
        case .appErrorOccurredNetwork:
            return FirebaseEvent.Name.appErrorOccurredNetwork.rawValue
        case .appErrorOccurredOtaFlashing:
            return FirebaseEvent.Name.appErrorOccurredOtaFlashing.rawValue
        case .appErrorOccurredOtaDownload:
            return FirebaseEvent.Name.appErrorOccurredOtaDownload.rawValue
        case .appErrorOccurredOtaUndefined:
            return FirebaseEvent.Name.appErrorOccurredOtaUndefined.rawValue
        }
    }
    var parameters: [String: FirebaseLoggable]? {
        switch self {
        case .appGraphicalEqualizerPresetChanged(let graphicalEqualizerPreset):
            return graphicalEqualizerPreset.parameters
        case .appShareDataSwitched(let switchState):
            return switchState.parameters
        case .appOtaFailed(let otaError):
            return otaError.parameters
        case .appVolumeChanged(let volume):
            return [FirebaseEvent.ParameterName.volume.rawValue: volume]
        case .appLightSettingChanged(let intensity):
            return [FirebaseEvent.ParameterName.intensity.rawValue: intensity]
        case .appSpeakerNameChanged,
             .appOtaStarted,
             .appOtaCompleted,
             .appForcedOtaStarted,
             .appOnboardingStayUpdatedSkipped,
             .appOnboardingShareDataSkipped,
             .appNewsletterSignedUp,
             .appSourceBluetoothActivated,
             .appSourceAuxActivated,
             .appSourceRcaActivated,
             .appMediaButtonTouchedPlay,
             .appMediaButtonTouchedPause,
             .appMediaButtonTouchedNext,
             .appMediaButtonTouchedPrev,
             .appCouplingSwitchedAmbient,
             .appCouplingSwitchedStereo,
             .appErrorOccurredBluetooth,
             .appErrorOccurredNetwork,
             .appErrorOccurredOtaFlashing,
             .appErrorOccurredOtaDownload,
             .appErrorOccurredOtaUndefined:
            return nil
        }
    }
}

extension AnalyticsEvent.GraphicalEqualizerPreset {
    var parameters: [String: FirebaseLoggable] {
        return [FirebaseEvent.ParameterName.preset.rawValue: rawValue]
    }
}

extension AnalyticsEvent.SwitchState {
    var parameters: [String: FirebaseLoggable] {
        return [FirebaseEvent.ParameterName.switchState.rawValue: rawValue]
    }
}

extension AnalyticsEvent.OtaError {
    var parameters: [String: FirebaseLoggable] {
        return [FirebaseEvent.ParameterName.otaError.rawValue: rawValue]
    }
}

