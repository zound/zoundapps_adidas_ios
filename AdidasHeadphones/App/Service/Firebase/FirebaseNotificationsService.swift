//
//  FirebaseNotificationsService.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 29/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import FirebaseMessaging

public protocol FirebaseNotificationsServiceType {
    func register(_ completionHandler: @escaping (Bool, Error?) -> Void)
}
private struct NotificationKeys {
    static let aps = "aps"
    static let alert = "alert"
    static let title = "title"
    static let subtitle = "subtitle"
    static let body = "body"
}
class FirebaseNotificationsService: NSObject, FirebaseNotificationsServiceType, UNUserNotificationCenterDelegate {

    let application: UIApplication

    init(application: UIApplication) {
        self.application = application
    }
    func register(_ completionHandler: @escaping (Bool, Error?) -> Void) {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {
            (granted, error) in
            print("UNUserNotificationCenter.requestAuthorization.granted: \(granted)")
            completionHandler(granted, error)
        }
        application.registerForRemoteNotifications()
    }
    private func scheduleNotification(_ title: String?, _ subtitle: String?, _ body: String?) {
        let content = UNMutableNotificationContent()
        if let title = title {
            content.title = title
        }
        if let body = body {
            content.body = body
        }
        if let subtitle = subtitle {
            content.subtitle = subtitle
        }
        content.sound = .default
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let options : UNNotificationPresentationOptions = [.alert, .sound, .badge]
        completionHandler(options)
    }
    func scheduleLocalNotification(basedOn remoteNotification: [AnyHashable : Any] ){
        guard let aps = remoteNotification[NotificationKeys.aps] as? [AnyHashable : Any] else {
            return
        }
        guard let alert = aps[NotificationKeys.alert] as? [AnyHashable : Any] else {
            return
        }
        let title = alert[NotificationKeys.title] as? String
        let subtitle = alert[NotificationKeys.subtitle] as? String
        let body = alert[NotificationKeys.body] as? String
        scheduleNotification(title, subtitle, body)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Message: \(userInfo)")
        if (application.applicationState == .active) {
            scheduleLocalNotification(basedOn: userInfo)
        }
    }
}
