//
//  SalesforceApiHandler.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 11/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct PathComponent {
    private static var isProd = BundleConfiguration.firmwareReleaseType == .prod
    static let scheme = "https"
    static let host = isProd ? "eu19.salesforce.com" : "cs85.salesforce.com"
    static let loginPath = "/services/oauth2/token"
    static let createPath = "/services/data/v44.0/sobjects/Account"
    static let lookupContactPath = "/services/data/v44.0/sobjects/Contact/customerID__c/"
    static let updatePath = "/services/data/v44.0/sobjects/Account/"
    static let customerPrefix = "AD_"
    static func lookupContactPath(for email: String)->String {
        return lookupContactPath + customerPrefix + email
    }
    static func updatePath(for account: String)->String {
        return updatePath + account
    }
}
private struct QueryItem {
    struct Name {
        static let grantType = "grant_type"
        static let clientId = "client_id"
        static let clientSecret = "client_secret"
        static let username = "username"
        static let password = "password"
    }
    struct Value {
        private static var isProd = BundleConfiguration.firmwareReleaseType == .prod
        static let grantType = "password"
        static let clientId = isProd ? "3MVG9T46ZAw5GTfX0cJhyCIHKWHl5UxX482ihrxONmvEUS9BRskbI4ZeTQ4b87VlFBcW84damp0Mj3EgtLgnL" : "3MVG9ZPHiJTk7yFyo2kgZvLTvphs0oJkBygviuHVXHaOnV7XEKWjjq7wsTrZRExf2vDvamVAgz2BYYzX6sUcn"
        static let clientSecret = isProd ? "3631653585457419922" : "2390149004901668929"
        static let username = isProd ? "zound@crmtoolbox.com" : "zound@crmtoolbox.com.SB1"
        static let password = isProd ? "A5JM5Ny6GjQjVwhZr8E1SROWoINfFmiJaNOWB7ph": "ag8TXzgja3vsbvZcfpFfGRk5bWyEr7NDdIlRIpMpL"
    }
}
struct Params {
    static let dateFormatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter }()
    static func countryCode() -> String? {
        return Locale.current.regionCode
    }
    static func date() -> String {
        return Params.dateFormatter.string(from: Date())
    }
}
struct CreateAccountParams: Codable {
    var RecordTypeId = "0121i000000bqoEAAQ"
    var PersonEmail = ""
    var customerID__pc =  ""
    var brand__pc =  "Adidas"
    var LastName = "Newsletter Signup"
    var PermissionEmail__pc = "true"
    var PermissionEmailOffers__pc = "true"
    var PermissionEmailProductUpdates__pc = "true"
    var PermissionEmailOptinDate__pc = Params.date()
    var CountryCode__pc = Params.countryCode()
    var RegistrationSourceId__pc = "adidas Headphones App"
    init(email: String) {
        self.PersonEmail = email
        self.customerID__pc = PathComponent.customerPrefix + email
    }
}
struct UpdateParams: Codable {
    var PermissionEmail__pc = ""
    var PermissionEmailOffers__pc = ""
    var PermissionEmailProductUpdates__pc = ""
    var PermissionEmailOptinDate__pc: String? = nil
    var PermissionEmailOptoutDate__pc: String? = nil
    var CountryCode__pc: String? = nil
    var Locale__pc: String? = nil
    init(permission: Bool) {
        let value = permission ? "true" : ""
        self.PermissionEmail__pc = value
        self.PermissionEmailOffers__pc = value
        self.PermissionEmailProductUpdates__pc = value
        if permission {
            self.CountryCode__pc = Params.countryCode()
            self.PermissionEmailOptinDate__pc = Params.date()
        } else {
            self.PermissionEmailOptoutDate__pc = Params.date()
        }
    }
}
struct CreateSuccesResult: Codable {
    static let failure = CreateSuccesResult(success: false, id: "")
    var success: Bool
    var id: String
}
struct CheckUserResult: Codable {
    var AccountId: String
}
class SalesforceApiHandler: NSObject {
    let loginURL: URL?  = {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponent.scheme
        urlComponents.host = PathComponent.host
        urlComponents.path = PathComponent.loginPath
        urlComponents.queryItems = [
            URLQueryItem(name: QueryItem.Name.grantType, value: QueryItem.Value.grantType),
            URLQueryItem(name: QueryItem.Name.clientId, value: QueryItem.Value.clientId),
            URLQueryItem(name: QueryItem.Name.clientSecret, value: QueryItem.Value.clientSecret),
            URLQueryItem(name: QueryItem.Name.username, value: QueryItem.Value.username),
            URLQueryItem(name: QueryItem.Name.password, value: QueryItem.Value.password),
        ]
        return urlComponents.url
    }()
    let createUser: URL? = {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponent.scheme
        urlComponents.host = PathComponent.host
        urlComponents.path = PathComponent.createPath
        return urlComponents.url
    }()
    public func login(completion:  @escaping (String?)->Void) {
        guard let url = loginURL else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        let requestCompletion : ( Data?, URLResponse?, Error?) -> Void = { [weak self] (data, response, error) -> Void in
            if let data = data, let token = self?.parseToken(data: data) {
               completion(token)
            } else {
                completion(nil)
            }
        }
        let task = URLSession.shared.dataTask(with: request, completionHandler: requestCompletion)
        task.resume()
    }
    private func parseToken(data: Data) -> String? {
        guard let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
            return nil
        }
        let token = jsonObj.value(forKey: "access_token")
        return token as? String
    }
    private func prepareCreateRequest(with token: String, for email: String) -> URLRequest? {
        guard let url = createUser else {
            return nil
        }
        var request = prepareRequest(token: token, url: url, method: .post)
        request.httpBody = try? JSONEncoder().encode(CreateAccountParams(email: email))
        return request
    }
    private func prepareRequest(token:String, url:URL, method: HTTPMethod )->URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue("Bearer " + token, forHTTPHeaderField:"Authorization")
        request.addValue("no-cache", forHTTPHeaderField:"Cache-Control")
        request.addValue("application/json", forHTTPHeaderField:"Content-Type")
        return request
    }
    public func signOutUser(with token: String, for account: String, completion:  @escaping (Bool)->Void) {
        let params = UpdateParams(permission: false)
        updateUser(with: token, for: account, params: params, completion: completion)
    }
    public func addPermissionsUser(with token: String, for account: String, completion:  @escaping (CreateSuccesResult)->Void) {
        let params = UpdateParams(permission: true)
        updateUser(with: token, for: account, params: params) { result in
            if result {
                let response = CreateSuccesResult(success: true, id: account)
                completion(response)
            } else {
                completion(CreateSuccesResult.failure)
            }
        }
    }
    public func updateUser(with token: String, for account: String, params: UpdateParams, completion:  @escaping (Bool)->Void) {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponent.scheme
        urlComponents.host = PathComponent.host
        urlComponents.path = PathComponent.updatePath(for: account)
        guard let url = urlComponents.url else {
            completion(false)
            return
        }
        var request = prepareRequest(token: token, url: url, method: .patch)
        request.httpBody = try? JSONEncoder().encode(params)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            guard error == nil else {
                completion(false)
                return
            }
            let statusCode = response?.code ?? 0
            let succes = statusCode >= 200 && statusCode < 300
            print("updateUser() statusCode: \(statusCode)")
            completion(succes)
        }
        task.resume()
    }
    public func checkUser(with token: String, for email: String, completion:  @escaping (CheckUserResult?)->Void) {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponent.scheme
        urlComponents.host = PathComponent.host
        urlComponents.path = PathComponent.lookupContactPath(for: email)
        guard let url = urlComponents.url else {
            completion(nil)
            return
        }
        let request = prepareRequest(token: token, url: url, method: .get)
        let task = URLSession.shared.dataTask(with: request){ (data, response, error) -> Void in
            guard error == nil else {
                completion(nil)
                return
            }
            print("checkUser() statusCode: \(response?.code ?? 0)")
            guard let data = data else {
                completion(nil)
                return
            }
            print("All data about user \(String(data: data, encoding: String.Encoding.utf8) ?? String())")
            let result = try? JSONDecoder().decode(CheckUserResult.self, from: data)
            completion(result)
        }
        task.resume()
    }
    private func createCustomer(with token: String, for email: String, completion:  @escaping (CreateSuccesResult)->Void) {
        guard let request = prepareCreateRequest(with: token, for: email) else {
            completion(CreateSuccesResult.failure)
            return
        }
        let requestCompletion : ( Data?, URLResponse?, Error?) -> Void = { (data, response, error) -> Void in
            guard error == nil else {
                completion(CreateSuccesResult.failure)
                return
            }
            print("createCustomer()  statusCode:\(response?.code ?? 0)")
            if let data = data {
                let decoder = JSONDecoder()
                let result = try? decoder.decode(CreateSuccesResult.self, from: data)
                completion(result ?? CreateSuccesResult.failure)
            } else {
                completion(CreateSuccesResult.failure)
            }
        }
        let task = URLSession.shared.dataTask(with: request, completionHandler: requestCompletion)
        task.resume()
    }
    public func signUp(email: String, completion:  @escaping (CreateSuccesResult)->Void) {
        login{[weak self] token in
            guard let token = token else {
                completion(CreateSuccesResult.failure)
                return
            }
            self?.checkUser(with: token, for: email) { result in
                if let result = result {
                    self?.addPermissionsUser(with: token,
                                             for: result.AccountId,
                                             completion: completion)
                } else {
                    self?.createCustomer(with: token,
                                         for: email,
                                         completion: completion)
                }
            }
        }
    }
    public func signOut(account: String, completion:  @escaping (Bool)->Void) {
        login{[weak self] token in
            guard let token = token else {
                completion(false)
                return
            }
            self?.signOutUser(with: token, for: account, completion: completion)
        }
    }
}
