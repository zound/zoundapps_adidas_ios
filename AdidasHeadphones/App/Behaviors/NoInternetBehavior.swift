//
//  NoInternetBehavior.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 14/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import Reachability
import RxSwift
import UIKit

protocol CloseableView: UIView {
    var closeButton: UIButton { get set }
}
protocol Toggleable: UIView {
    func toggle(_ flag: Bool)
}
final class NoInternetBehavior: Behavior {
    unowned let parent: UIViewController
    unowned let trigger: Reachability
    private var disposeBag = DisposeBag()
    private var closedByUser: Bool = false
    private var noInternetView: CloseableView!
    private var hiddenConstraint: NSLayoutConstraint!
    private var visibleConstraint: NSLayoutConstraint!
    private var toggleableItems: [Toggleable]
    @discardableResult init(parentViewController: UIViewController,
         trigger: Reachability,
         toggleableItems: [Toggleable] = []) {
        self.parent = parentViewController
        self.trigger = trigger
        self.toggleableItems = toggleableItems
        super.init()
        self.owner = parentViewController
        self.trigger.allowsCellularConnection = true
        try? self.trigger.startNotifier()
        self.noInternetView = NoInternetView(closeHandler: { [weak self] button in
            guard let strongSelf = self, strongSelf.closedByUser == false else { return }
            strongSelf.animateView(hide: true)
            strongSelf.closedByUser = true
        })
        setup()
    }
    deinit {
        self.trigger.stopNotifier()
    }
}
private extension NoInternetBehavior {
    func setup() {
        trigger.rx.reachabilityChanged.subscribe(onNext: { [weak self] reachability in
            guard let strongSelf = self else { return }
            strongSelf.toggleableItems.forEach { $0.toggle(reachability.connection != .unavailable ? true : false)}
            guard strongSelf.closedByUser == false else { return }
            strongSelf.animateView(hide: reachability.connection != .unavailable ? true : false)
        })
        .disposed(by: disposeBag)
        toggleableItems.forEach { $0.toggle(trigger.connection != .unavailable ? true : false)}
        setupConstraints()
    }
    func setupConstraints() {
        autolayoutStyle(noInternetView)
        parent.view.addSubview(noInternetView)
        hiddenConstraint = noInternetView.topAnchor.constraint(equalTo: parent.view.bottomAnchor)
        visibleConstraint = noInternetView.bottomAnchor.constraint(equalTo: parent.view.bottomAnchor)
        var verticalConstraint: NSLayoutConstraint
        switch trigger.connection {
        case .cellular, .wifi:
            verticalConstraint = hiddenConstraint
        default:
            verticalConstraint = visibleConstraint
        }
        NSLayoutConstraint.activate([
            noInternetView.leftAnchor.constraint(equalTo: parent.view.leftAnchor),
            noInternetView.rightAnchor.constraint(equalTo: parent.view.rightAnchor),
            verticalConstraint
        ])
    }
    func animateView(hide: Bool) {
        let activateConstraint = hide ? hiddenConstraint : visibleConstraint
        let deactivateConstraint = hide ? visibleConstraint : hiddenConstraint
        guard let toDeactivate = deactivateConstraint, let toActivate = activateConstraint else {
            return
        }
        toDeactivate.isActive = false
        toActivate.isActive = true
        UIView.animate(withDuration: .t025, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            self?.parent.view.layoutIfNeeded()
        }, completion: nil)
    }
}
