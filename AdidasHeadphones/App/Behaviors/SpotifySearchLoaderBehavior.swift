//
//  SpotifySearchLoaderBehavior.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 24/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import Reachability
import RxSwift

enum LoaderStatus {
    case loading
    case displaying(String)
    case inTransition
    case hidden
}
private struct Const {
    static let swipeInterval: CGFloat = 100.0
    static let middleDistance: CGFloat = Const.swipeInterval / 2
}
final class SpotifySearchLoaderBehavior: Behavior {
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.numberOfLines = .zero
        return headerLabel
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: NSLocalizedString("spotify_list_nothing_found_description", value: "Open Spotify app and choose something you like.", comment: "Open Spotify app and choose something you like."))
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    lazy var linearIndicator: LinearProgressIndicator = {
        let linearIndicator = LinearProgressIndicator()
        return linearIndicator
    }()
    lazy var panGestureRecognizer: UIGestureRecognizer = {
        let recognizer = UIPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(handlePan))
        recognizer.maximumNumberOfTouches = 1
        return recognizer
    }()
    var status: LoaderStatus = .loading {
        didSet {
            update(status)
        }
    }
    unowned let parent: UIViewController
    unowned let fadedView: UIView
    private let refreshHandler: (() -> ())?
    private let scrollViewOffsetTrigger: Int = .zero
    private var previous: LoaderStatus = .loading
    private var disposeBag = DisposeBag()
    private var progressTrigger: BehaviorSubject<Float>
    private var finishedLoadingTrigger: PublishSubject<Void>
    private var messageTrigger: PublishSubject<String>
    private var initialTouchPoint = CGPoint.zero
    private var offset: CGFloat = .zero
    @discardableResult init(
        parentViewController: UIViewController,
        fadedView: UIView,
        progressTrigger: BehaviorSubject<Float>,
        loadingFinishedTrigger: PublishSubject<Void>,
        messageTrigger: PublishSubject<String>,
        refreshHandler: (() -> ())?) {
        self.parent = parentViewController
        self.finishedLoadingTrigger = loadingFinishedTrigger
        self.messageTrigger = messageTrigger
        self.fadedView = fadedView
        self.progressTrigger = progressTrigger
        self.refreshHandler = refreshHandler
        super.init()
        self.owner = parentViewController
        setup()
    }
}
private extension SpotifySearchLoaderBehavior {
    func setup() {
        fadedView.isHidden = true
        headerLabel.alpha = .zero
        descriptionLabel.alpha = .zero
        linearIndicator.alpha = 1.0
        progressTrigger.subscribe(onNext: { [weak self] progress in
            self?.linearIndicator.progress(value: CGFloat(progress))
        })
        .disposed(by: disposeBag)
        Observable.combineLatest(finishedLoadingTrigger, messageTrigger.startWith(String()))
            .subscribe(onNext: { [weak self] (_, message) in
                guard message.count > .zero else {
                    self?.status = .hidden
                    return
                }
                self?.status = .displaying(message)
            })
            .disposed(by: disposeBag)
        parent.view.addGestureRecognizer(panGestureRecognizer)
        setupConstraints()
    }
    func setupConstraints() {
        [headerLabel, descriptionLabel, linearIndicator].forEach {
            autolayoutStyle($0)
            parent.view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            headerLabel.centerYAnchor.constraint(equalTo: parent.view.centerYAnchor),
            headerLabel.leadingAnchor.constraint(equalTo: parent.view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: parent.view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: parent.view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: parent.view.trailingAnchor, constant: -Dimension.edgeMargin),
            linearIndicator.centerXAnchor.constraint(equalTo: parent.view.centerXAnchor),
            linearIndicator.centerYAnchor.constraint(equalTo: parent.view.centerYAnchor)
        ])
    }
    func update(_ new: LoaderStatus) {
        switch(previous, new) {
        case (.loading, .hidden):
            fadedView.isHidden = false
            headerLabel.alpha = .zero
            descriptionLabel.alpha = .zero
            linearIndicator.alpha = .zero
        case (_, .displaying(let message)):
            display(text: message)
            fadedView.isHidden = true
            headerLabel.alpha = 1
            descriptionLabel.alpha = 1
            linearIndicator.alpha = .zero
        case (.displaying(_), .loading):
            headerLabel.alpha = .zero
            descriptionLabel.alpha = .zero
            linearIndicator.alpha = 1
            refreshHandler?()
        case (.hidden, .loading):
            fadedView.isHidden = true
            headerLabel.alpha = .zero
            descriptionLabel.alpha = .zero
            linearIndicator.alpha = 1
            refreshHandler?()
        default:
            break
        }
        previous = new
    }
    func display(text: String) {
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: text)
        headerLabel.textAlignment = .center
    }
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer) {
        switch status {
        case .displaying(_):
            break
        default: return
        }
        switch recognizer.state {
        case .began:
            linearIndicator.progress(value: .zero)
            initialTouchPoint = recognizer.location(in: parent.view)
            offset = .zero
        case .changed:
            let newPoint = recognizer.location(in: parent.view)
            offset = newPoint.y - initialTouchPoint.y
            guard .zero...Const.swipeInterval ~= offset   else {
                return
            }
            headerLabel.alpha = 1 - (offset / Const.swipeInterval)
            descriptionLabel.alpha = headerLabel.alpha
            if offset == Const.swipeInterval {
                messageTrigger.onNext(String())
                status = .loading
            }
        case .ended, .cancelled:
            guard offset >= Const.swipeInterval else {
                headerLabel.alpha = 1
                descriptionLabel.alpha = 1
                return
            }
            headerLabel.alpha = .zero
            descriptionLabel.alpha = .zero
            messageTrigger.onNext(String())
            status = .loading
        default: break
        }
    }
}


