//
//  Behavior.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 14/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import Foundation

class Behavior  {
    weak var owner: AnyObject? {
        willSet(newOwner) {
            guard let old = owner else {
                guard let new = newOwner else { return }
                bindLifetimeTo(new)
                return
            }
            guard old !== newOwner else { return }
            guard let new = newOwner else {
                releaseLifetimeFrom(old)
                return
            }
            releaseLifetimeFrom(old)
            bindLifetimeTo(new)
        }
    }
    func bindLifetimeTo(_ parent: AnyObject) {
        objc_setAssociatedObject(parent, bridge(obj: self), self, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    func releaseLifetimeFrom(_ parent: AnyObject) {
        objc_setAssociatedObject(parent, bridge(obj: self), nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}
private extension Behavior {
    func bridge<T : AnyObject>(obj : T) -> UnsafeRawPointer {
        return UnsafeRawPointer(Unmanaged.passUnretained(obj).toOpaque())
    }
}

