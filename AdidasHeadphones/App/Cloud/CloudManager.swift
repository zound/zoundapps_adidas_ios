//
//  CloudManager.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 04/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import AWSCore
import AWSS3
import ConnectionService

private struct Const {
    static let regionType: AWSRegionType = .EUWest2
    static let identityPoolId = "eu-west-2:cd6e2e64-2c41-45b8-ab53-56650ed324c3"
}

class CloudManager {
    static func setupAmazonWebServices() {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: Const.regionType, identityPoolId: Const.identityPoolId)
        let configuration = AWSServiceConfiguration(region: Const.regionType, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    static func interceptApplication(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }
}
