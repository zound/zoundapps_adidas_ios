//
//  AWSDownload.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 08/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import AWSCore
import AWSS3
import RxSwift
import ConnectionService

private struct Const {
    static let downloadBucketName = "com.zoundindustries.bt-fw-server"
    static let keyFirstComponent = "devices"
    static let currentFirmwareVersionFileName = "current-version"
    static let firmwareFileName = "firmware_"
    static let firmwareFileExtension = "bin"
}

enum FirmwareReleaseType: String {
    case prod
    case dev
}

enum FirmwareType: String, CaseIterable {
    case arnold
    case freeman
    case desir
    init(deviceType: DeviceType) {
        switch deviceType {
        case .arnold:
            self = .arnold
        case .freeman:
            self = .freeman
        case .desir:
            self = .desir
        }
    }
}

enum AWSDownload {
    static let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
    case currentFirmwareVersionFile(FirmwareType)
    case firmware(FirmwareType, version: String)
}

extension AWSDownload {
    var firmwareType: FirmwareType {
        switch self {
        case .currentFirmwareVersionFile(let firmwareType):
            return firmwareType
        case .firmware(let firmwareType, _):
            return firmwareType
        }
    }
    var fileName: String {
        switch self {
        case .currentFirmwareVersionFile:
            return Const.currentFirmwareVersionFileName
        case .firmware(_, let version):
            return Const.firmwareFileName + version
        }
    }
    var filePrefix: String {
        switch self {
        case .currentFirmwareVersionFile:
            return Const.currentFirmwareVersionFileName
        case .firmware:
            return Const.firmwareFileName
        }
    }
    private var fileExtension: String? {
        switch self {
        case .currentFirmwareVersionFile:
            return nil
        case .firmware:
            return Const.firmwareFileExtension
        }
    }
    var downloadingDirectory: URL {
        return downloadingFileURL.deletingLastPathComponent()
    }
    var downloadingFileURL: URL {
        if let fileExtension = fileExtension {
            return AWSDownload.documentDirectory
                .appendingPathComponent(firmwareType.rawValue)
                .appendingPathComponent(fileName)
                .appendingPathExtension(fileExtension)
        } else {
            return AWSDownload.documentDirectory
                .appendingPathComponent(firmwareType.rawValue)
                .appendingPathComponent(fileName)
        }
    }
    private func key(_ fileName: String) -> String {
        return URL(string: Const.keyFirstComponent)!
            .appendingPathComponent(firmwareType.rawValue.capitalizedFirst)
            .appendingPathComponent(BundleConfiguration.firmwareReleaseType.rawValue)
            .appendingPathComponent(fileName)
            .path
    }

    private func createDirectory() {
        try? FileManager.default.createDirectory(atPath: downloadingDirectory.path, withIntermediateDirectories: true, attributes: nil)
    }
    func exists() -> Bool {
        return FileManager.default.fileExists(atPath: downloadingFileURL.path)
    }
    func purge() {
        try? FileManager.default.removeItem(at: downloadingFileURL)
    }
    func purgeAll() {
        let fileNames = (try? FileManager.default.contentsOfDirectory(atPath: downloadingDirectory.path))?.filter {
            return $0.hasPrefix(filePrefix)
            }
        fileNames?.forEach { name in
            let file = downloadingDirectory.appendingPathComponent(name)
            try? FileManager.default.removeItem(at: file)
        }
    }
    func execute(_ fileName: String, completion: @escaping (_ downloadedFileURL: URL?) -> Void) {
        purgeAll()
        createDirectory()
        AWSS3TransferUtility.default()
        .download(to: downloadingFileURL, bucket: Const.downloadBucketName, key: key(fileName), expression: nil) { (_, _, _, error) in
            DispatchQueue.main.async {
                if let _ = error {
                    completion(nil)
                } else if self.exists() {
                    completion(self.downloadingFileURL)
                } else {
                    completion(nil)
                }
            }
        }
    }
}
