//
//  Fonts.swift
//  AppLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 21/02/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

public enum Headline {
    case extraLargeBlack
    case largeBlack
    case largeWhite
    case mediumBlack
    case mediumLight
    case mediumWhite
    case smallDark
    case smallLight
    case smallWhite
    case extraSmallDark
    case extraSmallLight
    case extraSmallWhite
}

extension Headline {
    public static func style(as type: Headline, text: String) -> NSAttributedString {
        switch type {
        case .extraLargeBlack:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(2.0)
                .line(height: 44.0)

        case .largeBlack:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(1.08)
                .line(height: 32.0)

        case .largeWhite:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(1.08)
                .line(height: 32.0)
            
        case .mediumBlack:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 24.0)

        case .mediumLight:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.greyMedium)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 24.0)
            
        case .mediumWhite:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 24.0)
            
        case .smallDark:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(0.5)
                .line(height: 20.0)
            
        case .smallLight:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.greyMedium)
                .font(type.font)
                .letterSpacing(0.5)
                .line(height: 20.0)
            
        case .smallWhite:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(0.5)
                .line(height: 20.0)
            
        case .extraSmallDark:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(0.5)
                .line(height: 16.0)
            
        case .extraSmallLight:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.greyMedium)
                .font(type.font)
                .letterSpacing(0.5)
                .line(height: 16.0)
            
        case .extraSmallWhite:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(0.5)
                .line(height: 16.0)
        }
    }
}

fileprivate extension Headline {
    static let largeFontSize: CGFloat = UIScreen.isSmallScreen ? 20.0 : 26.0
    var font: UIFont {
        switch self {
        case .extraLargeBlack:
            return UIFont(name: "adineuePROTT-Bold", size: 36.0)!
        case .largeBlack, .largeWhite:
            return UIFont(name: "adineuePROTT-Bold", size: Headline.largeFontSize)!
        case .mediumBlack, .mediumLight, .mediumWhite:
            return UIFont(name: "AdihausDIN-Medium", size: 18.0)!
        case .smallDark, .smallLight, .smallWhite:
            return UIFont(name: "AdihausDIN-Medium", size: 14.0)!
        case .extraSmallDark, .extraSmallLight, .extraSmallWhite:
            return UIFont(name: "AdihausDIN-Medium", size: 12.0)!
        }
    }
}
