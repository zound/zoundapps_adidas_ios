//
//  Image.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService


struct Image {
    struct DeviceType {
        struct Arnold {
            static func discovery(with color: ArnoldColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "arnoldDiscoveryDark")!
                case .light:
                    return UIImage(named: "arnoldDiscoveryLight")!
                case .coral:
                    return UIImage(named: "arnoldDiscoveryCoral")!
                }
            }
            static func main(with color: ArnoldColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "arnoldMainDark")!
                case .light:
                    return UIImage(named: "arnoldMainLight")!
                case .coral:
                    return UIImage(named: "arnoldMainCoral")!
                }
            }
            static func thumbnail(with color: ArnoldColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "arnoldThumbnailDark")!
                case .light:
                    return UIImage(named: "arnoldThumbnailLight")!
                case .coral:
                    return UIImage(named: "arnoldThumbnailCoral")!
                }
            }
        }
        struct Freeman {
            static func discovery(with color: FreemanColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "freemanDiscoveryDarkGray")!
                case .lightGray:
                    return UIImage(named: "freemanDiscoveryLightGray")!
                case .coral:
                    return UIImage(named: "freemanDiscoveryCoral")!
                }
            }
            static func main(with color: FreemanColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "freemanMainDarkGray")!
                case .lightGray:
                    return UIImage(named: "freemanMainLightGray")!
                case .coral:
                    return UIImage(named: "freemanMainCoral")!
                }
            }
            static func thumbnail(with color: FreemanColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "freemanThumbnailDarkGray")!
                case .lightGray:
                    return UIImage(named: "freemanThumbnailLightGray")!
                case .coral:
                    return UIImage(named: "freemanThumbnailCoral")!
                }
            }
        }

        struct Desir {
            static func discovery(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "desirDiscoveryDarkGray")!
                case .lightGray:
                    return UIImage(named: "desirDiscoveryLightGray")!
                case .mint:
                    return UIImage(named: "desirDiscoveryMint")!
                }
            }
            static func main(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "desirMainDarkGray")!
                case .lightGray:
                    return UIImage(named: "desirMainLightGray")!
                case .mint:
                    return UIImage(named: "desirMainMint")!
                }
            }
            static func thumbnail(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "desirThumbnailDarkGray")!
                case .lightGray:
                    return UIImage(named: "desirThumbnailLightGray")!
                case .mint:
                    return UIImage(named: "desirThumbnailMint")!
                }
            }
        }
    }
    struct Icon {
        struct Black {
            static let settings = UIImage(named: "settingsBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let headphones = UIImage(named: "headphonesBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let chevronLeft = UIImage(named: "chevronLeftBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let chevronRight = UIImage(named: "chevronRightBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let arrowLeft = UIImage(named: "arrowLeftBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let arrowRight = UIImage(named: "arrowRightBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let arrowRightSmall = UIImage(named: "arrowRightBlack")!.scaled(to: CGSize(width: 20, height: 20))
            static let play = UIImage(named: "playBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let pause = UIImage(named: "pauseBlack")!.scaled(to: CGSize(width: 24, height: 24))
            static let check = UIImage(named: "checkBlack")!.scaled(to: CGSize(width: 20, height: 20))
            static let notSet = UIImage(named: "notSetBlack")!
        }
        struct White {
            static let arrowDown = UIImage(named: "arrowDownWhite")!.scaled(to: CGSize(width: 20, height: 20))
            static let arrowRight = UIImage(named: "arrowRightWhite")!.scaled(to: CGSize(width: 20, height: 20))
            static let check = UIImage(named: "checkWhite")!.scaled(to: CGSize(width: 20, height: 20))
        }
        struct Service {
            static let googleAssistant = UIImage(named: "actionButtonGoogleAssistant")!
            static let customized = UIImage(named: "actionButtonCustomized")!
            static let spotify = UIImage(named: "actionButtonSpotify")!
            static let spotifyLarge = UIImage(named: "actionButtonSpotify")!.scaled(to: CGSize(width: 110.0, height: 110.0))
        }
    }
    struct CircleSlider {
        static let blackCircle = UIImage(named: "circleSliderBlackCircle")!
        static let handle = UIImage(named: "circleSliderHandle")!
        static let spinnerLight = UIImage(named: "circleSliderSpinnerLight")!
        static let target = UIImage(named: "circleSliderTarget")!
    }
    struct Brand {
        static let white = UIImage(named: "adidasWhite")!.scaled(to: CGSize(width: 140, height: 94))
        static let black = UIImage(named: "adidasBlack")!.scaled(to: CGSize(width: 140, height: 94))
        static let welcomeBackground = UIImage(named: "welcomeBackground")!
    }
    struct Guide {
        struct Arnold {
            static func actionButton(with color: ArnoldColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideArnoldActionButtonDark")!
                case .light:
                    return UIImage(named: "guideArnoldActionButtonLight")!
                case .coral:
                    return UIImage(named: "guideArnoldActionButtonCoral")!
                }
            }
            static func equalizer(with color: ArnoldColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideArnoldEqualizerDark")!
                case .light:
                    return UIImage(named: "guideArnoldEqualizerLight")!
                case .coral:
                    return UIImage(named: "guideArnoldEqualizerCoral")!
                }
            }
            static func stayUpdated(with color: ArnoldColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideArnoldStayUpdatedDark")!
                case .light:
                    return UIImage(named: "guideArnoldStayUpdatedLight")!
                case .coral:
                    return UIImage(named: "guideArnoldStayUpdatedCoral")!
                }
            }
        }
        struct Freeman {
                    static func actionButton(with color: FreemanColor) -> UIImage {
                        switch color {
                        case .default:
                            return UIImage(named: "guideFreemanActionButtonDarkGray")!
                        case .lightGray:
                            return UIImage(named: "guideFreemanActionButtonLightGray")!
                        case .coral:
                            return UIImage(named: "guideFreemanActionButtonCoral")!
                        }
                    }
                    static func ergo(with color: FreemanColor) -> UIImage {
                        switch color {
                        case .default:
                            return UIImage(named: "guideFreemanErgoDarkGray")!
                        case .lightGray:
                            return UIImage(named: "guideFreemanErgoLightGray")!
                        case .coral:
                            return UIImage(named: "guideFreemanErgoCoral")!
                        }
                    }
                    static func knob(with color: FreemanColor) -> UIImage {
                        switch color {
                        case .default:
                            return UIImage(named: "guideFreemanKnobDarkGray")!
                        case .lightGray:
                            return UIImage(named: "guideFreemanKnobLightGray")!
                        case .coral:
                            return UIImage(named: "guideFreemanKnobCoral")!
                        }
                    }
                    static func stayUpdated(with color: FreemanColor) -> UIImage {
                        switch color {
                        case .default:
                            return UIImage(named: "guideFreemanStayUpdatedDarkGray")!
                        case .lightGray:
                            return UIImage(named: "guideFreemanStayUpdatedLightGray")!
                        case .coral:
                            return UIImage(named: "guideFreemanStayUpdatedCoral")!
                        }
                    }
        }

        struct Desir {
            static func actionButton(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideDesirActionButtonDarkGray")!
                case .lightGray:
                    return UIImage(named: "guideDesirActionButtonLightGray")!
                case .mint:
                    return UIImage(named: "guideDesirActionButtonMint")!
                }
            }
            static func ergo(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideDesirErgoDarkGray")!
                case .lightGray:
                    return UIImage(named: "guideDesirErgoLightGray")!
                case .mint:
                    return UIImage(named: "guideDesirErgoMint")!
                }
            }
            static func knob(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideDesirKnobDarkGray")!
                case .lightGray:
                    return UIImage(named: "guideDesirKnobLightGray")!
                case .mint:
                    return UIImage(named: "guideDesirKnobMint")!
                }
            }
            static func stayUpdated(with color: DesirColor) -> UIImage {
                switch color {
                case .default:
                    return UIImage(named: "guideDesirStayUpdatedDarkGray")!
                case .lightGray:
                    return UIImage(named: "guideDesirStayUpdatedLightGray")!
                case .mint:
                    return UIImage(named: "guideDesirStayUpdatedMint")!
                }
            }
        }
    }
    struct Indicators {
        static let spinnerWhite = UIImage(named: "spinnerWhite")!.scaled(to: CGSize(width: 20, height: 20))
        static let spinnerDark = UIImage(named: "spinnerDark")!.scaled(to: CGSize(width: 28, height: 28))
        static let spinnerDarkSmall = UIImage(named: "spinnerDark")!.scaled(to: CGSize(width: 20, height: 20))
        static let error = UIImage(named: "error")!.scaled(to: CGSize(width: 28, height: 28))
    }
    struct BatteryIndicator {
        static let level10 = UIImage(named: "10")!.scaled(to: CGSize(width: 20, height: 12))
        static let level20 = UIImage(named: "20")!.scaled(to: CGSize(width: 20, height: 12))
        static let level30 = UIImage(named: "30")!.scaled(to: CGSize(width: 20, height: 12))
        static let level40 = UIImage(named: "40")!.scaled(to: CGSize(width: 20, height: 12))
        static let level50 = UIImage(named: "50")!.scaled(to: CGSize(width: 20, height: 12))
        static let level60 = UIImage(named: "60")!.scaled(to: CGSize(width: 20, height: 12))
        static let level70 = UIImage(named: "70")!.scaled(to: CGSize(width: 20, height: 12))
        static let level80 = UIImage(named: "80")!.scaled(to: CGSize(width: 20, height: 12))
        static let level90 = UIImage(named: "90")!.scaled(to: CGSize(width: 20, height: 12))
        static let level100 = UIImage(named: "100")!.scaled(to: CGSize(width: 20, height: 12))
        static let unknown = UIImage(named: "unknown")!.scaled(to: CGSize(width: 20, height: 12))
    }
    struct SmallIcon {
        struct Light {
            static let actionButton = UIImage(named: "actionButtonSmallIconLight")!.scaled(to: CGSize(width: 20, height: 20))
            static let autoOffTimer = UIImage(named: "autoOffTimerSmallIconLight")!.scaled(to: CGSize(width: 20, height: 20))
            static let equalizer = UIImage(named: "equalizerSmallIconLight")!.scaled(to: CGSize(width: 20, height: 20))
            static let status = UIImage(named: "statusSmallIconLight")!.scaled(to: CGSize(width: 20, height: 20))
        }
    }
}
