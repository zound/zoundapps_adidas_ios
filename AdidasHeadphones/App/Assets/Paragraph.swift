//
//  Paragraph.swift
//  AppLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 21/02/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

import Foundation

public enum Paragraph {
    case regularDark
    case regularLight
    case regularWhite
    case smallDark
    case smallLight
    case smallWhite
    case smallRed
}

extension Paragraph {

    static let regularHeight: CGFloat = UIScreen.isSmallScreen ? 20.0: 24.0

    public static func style(as type: Paragraph, text: String) -> NSAttributedString {
        switch type {
        case .regularDark:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.greyExtraDark)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: regularHeight)
            
        case .regularLight:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.greyDark)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height:  regularHeight)
            
        case .regularWhite:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height:  regularHeight)
            
        case .smallDark:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.greyExtraDark)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 20.0)
            
        case .smallLight:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.greyDark)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 20.0)
            
        case .smallWhite:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 24.0)

        case .smallRed:
            return NSAttributedString(string: text.notEmpty)
                .color(Colors.red)
                .font(type.font)
                .letterSpacing(0.0)
                .line(height: 20.0)
        }
    }
}

fileprivate extension Paragraph {
    var font: UIFont {
        switch self {
        case .regularDark, .regularLight, .regularWhite:
            let regularSize:CGFloat = UIScreen.isSmallScreen ? 14.0 : 16.0
            return UIFont(name: "AdihausDIN-Regular", size: regularSize)!
        case .smallDark, .smallLight, .smallWhite, .smallRed:
            return UIFont(name: "AdihausDIN-Regular", size: 14.0)!
        }
    }
}
