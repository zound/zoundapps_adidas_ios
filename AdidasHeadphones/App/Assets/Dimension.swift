//
//  Dimension.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 14/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

struct Dimension {
    static var safeAreaBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? .zero
        } else {
            return .zero
        }
    }
    static var safeAreaTopInset: CGFloat {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.keyWindow?.safeAreaInsets.top ?? .zero
        } else {
            return .zero
        }
    }
    static let screenWidth: CGFloat = UIScreen.main.bounds.size.width
    static let screenHeight: CGFloat = UIScreen.main.bounds.size.height
    static let screenHeight58: CGFloat = UIScreen.main.bounds.size.height / 8 * 5
    static let screenHeight48: CGFloat = UIScreen.main.bounds.size.height / 8 * 4
    static let screenHeight28: CGFloat = UIScreen.main.bounds.size.height / 8 * 2
    static let screenHeight18: CGFloat = UIScreen.main.bounds.size.height / 8 * 1
    static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
    static let edgeMargin: CGFloat = 16
    static let smallSpace: CGFloat = 8
    static let buttonHeight: CGFloat = 52
    static let playerButtonSize: CGFloat = 60
    static let rectangularSwitchHeight: CGFloat = 28
    static let rectangularSwitchWidth: CGFloat = 52
    static let circleSliderMargin: CGFloat = 25
    static let headerTopMargin: CGFloat = 34
    static let headerBigBottomMargin: CGFloat = 32
    static let headerSmallBottomMargin: CGFloat = 10
    static let squareButtonMargin: CGFloat = 40
    static let pageControlHeight: CGFloat = 60
    static let accessoryPickerHeight: CGFloat = 360
    static let minorAlignment: CGFloat = 2
    static let pickerViewHeight: CGFloat = 170
    static let pickerRowHeight: CGFloat = 60
    static let playerViewHeight: CGFloat = 178
    static let tileMargin: CGFloat = 12
    static let tileSmallHeight: CGFloat = 40
    //topMargin
    static let tileBigHeight: CGFloat = 100
    static let tileSpace: CGFloat = 1
    static let tileImageViewSize: CGFloat = 18
    static let whiteProgressImageViewSize: CGFloat = 20
    static let spotifySearchEntryDimension: CGFloat = 60
    static let checkboxWidth: CGFloat = 52
    static let firmwareUpdateMessaveViewHeight: CGFloat = 64
    static let firmwareUpdateActivityViewSize: CGFloat = 8
    static let checkboxHeight: CGFloat = 28
    static let checkboxThumbSize = CGSize(width: 25, height: 26)
    static let tableCellHeight: CGFloat = 59.0
    
}
