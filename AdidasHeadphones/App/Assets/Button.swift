//
//  Button.swift
//  AppLibrary
//
//  Created by Grzegorz Kiel on 10/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

public enum Button {
    case smallBlackUnderlined
    case regularWhite
    case regularBlackUnderlined
}

extension Button {
    public static func style(as type: Button, text: String) -> NSAttributedString {
        switch type {
        case .smallBlackUnderlined:
            let attributedtext = NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(2.0)
                .line(height: 16.0)
            let underlinedText = NSMutableAttributedString(attributedString: attributedtext)
            underlinedText.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0, text.count))
            return underlinedText
        case .regularWhite:
            return NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.white)
                .font(type.font)
                .letterSpacing(2.0)
                .line(height: 20.0)
        case .regularBlackUnderlined:
            let attributedtext = NSAttributedString(string: text.uppercased().notEmpty)
                .color(Colors.black)
                .font(type.font)
                .letterSpacing(2.0)
                .line(height: 20.0)
            let underlinedText = NSMutableAttributedString(attributedString: attributedtext)
            underlinedText.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0, text.count))
            return underlinedText
        }
    }
}

fileprivate extension Button {
    var font: UIFont {
        switch self {
        case .smallBlackUnderlined:
            return UIFont(name: "AdihausDIN-Bold", size: 12.0)!
        case .regularWhite, .regularBlackUnderlined:
            return UIFont(name: "AdihausDIN-Bold", size: 14.0)!
        }
    }
}
