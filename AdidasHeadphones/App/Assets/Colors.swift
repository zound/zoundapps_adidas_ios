//
//  Colors.swift
//  AppLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 21/02/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import UIKit

struct Colors {
    static let black = UIColor.take(from: "#000000")
    static let white = UIColor.take(from: "#FFFFFF")
    
    static let yellow = UIColor.take(from: "#FFB500")
    static let lightYellow = UIColor.take(from: "#FDF9C3")
    
    static let red = UIColor.take(from: "#C53622")
    static let lightRed = UIColor.take(from: "#FF6D6D")
    
    static let green = UIColor.take(from: "#92D39F")
    static let lightGreen = UIColor.take(from: "#CCFFCC")
    
    static let blue = UIColor.take(from: "#0286CD")
    static let lightBlue = UIColor.take(from: "#DEF3FA")
    
    static let greySuperLight = UIColor.take(from: "#F9F9F9")
    static let greyLight = UIColor.take(from: "#C8CBCC")
    static let greyExtraLight = UIColor.take(from: "#EBEBEB")
    
    static let greyMedium = UIColor.take(from: "#9A9B9B")
    
    static let greyDark = UIColor.take(from: "#616363")
    static let greyExtraDark = UIColor.take(from: "#363738")

    static let clear = UIColor.clear
}
