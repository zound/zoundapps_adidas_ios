//
//  Forms.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 22/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

precedencegroup SingleTypeComposition {
    associativity: left
}
infix operator <>: SingleTypeComposition
func <> <A: AnyObject>(f: @escaping (A) -> Void, g: @escaping (A) -> Void) -> (A) -> Void {
    return { a in
        f(a)
        g(a)
    }
}
func autolayoutStyle<V: UIView>(_ view: V) -> Void {
    view.translatesAutoresizingMaskIntoConstraints = false
}
func fullyContained<V: UIView>(in parent: V) -> (UIView) -> Void {
    return {
        NSLayoutConstraint.activate([
            $0.topAnchor.constraint(equalTo: parent.topAnchor),
            $0.bottomAnchor.constraint(equalTo: parent.bottomAnchor),
            $0.leftAnchor.constraint(equalTo: parent.leftAnchor),
            $0.rightAnchor.constraint(equalTo: parent.rightAnchor)
        ])
    }
}
func borderStyle(color: UIColor, width: CGFloat) -> (UIView) -> Void {
    return { view in
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = width
    }
}
func newsLetterLabelStyle(text: String) -> (UILabel) -> Void {
    return autolayoutStyle
        <> { (label: UILabel) in
            label.attributedText = Paragraph.style(as: .regularDark, text: text)
            label.textAlignment = .left }
}

func privacyPolicyLabelStyle(text: NSAttributedString) -> (UITextView) -> Void {
    return autolayoutStyle
        <> { (textView: UITextView) in
            textView.attributedText = text
            textView.textAlignment = .center
            textView.linkTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue]
            textView.backgroundColor = Colors.clear
            textView.tintColor = Colors.white
            textView.isEditable = false
            textView.isScrollEnabled = false
    }
}

func errorLabelStyle(text: String) -> (UILabel) -> Void {
    return autolayoutStyle
        <> { (label: UILabel) in
            label.attributedText = Paragraph.style(as: .smallRed, text: text)
            label.textAlignment = .left }
}

let disabledAutocorrections: (UITextField) -> Void = {
    $0.autocapitalizationType = .none
    $0.autocorrectionType = .no
    $0.spellCheckingType = .no
}
// UITextField
let baseTextFieldStyle =
    autolayoutStyle
        <> disabledAutocorrections

let newsletterEmailKeyboardStyle: (UITextField) -> Void = {
    $0.keyboardType = .emailAddress
    $0.keyboardAppearance = .default
}
let newsletterEmailDefaults: (UITextField) -> Void = {
    $0.text = nil
    let placeholderText = NSLocalizedString("newsletter_email_placeholder", value: "your.email@domain.com", comment: "Placeholder for email adress")
    $0.attributedPlaceholder = Paragraph.style(as: .regularLight, text: placeholderText)
}
let newsletterEmailTextFieldStyle =
    baseTextFieldStyle
        <> newsletterEmailKeyboardStyle
        <> newsletterEmailDefaults
let checkboxStyle =
    autolayoutStyle
        <> {
            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalToConstant: Dimension.checkboxWidth),
                $0.heightAnchor.constraint(equalToConstant: Dimension.checkboxHeight)
            ])
        }
let rootHorizontalStackViewStyle: (UIStackView) -> Void =
    autolayoutStyle
        <> {
            $0.axis = .horizontal
            $0.alignment = .center
            $0.spacing = 16.0
        }
let rootVerticalStackViewStyle: (UIStackView) -> Void =
    autolayoutStyle
        <> {
            $0.axis = .vertical
            $0.alignment = .leading
            $0.spacing = 16.0
            $0.isLayoutMarginsRelativeArrangement = true
            $0.layoutMargins = UIEdgeInsets(top: Dimension.edgeMargin, left: Dimension.edgeMargin, bottom: Dimension.edgeMargin, right: Dimension.edgeMargin)
        }
let verticalStackViewStyle: (UIStackView) -> Void =
    autolayoutStyle
        <> {
            $0.axis = .vertical
            $0.alignment = .leading
            $0.spacing = 16.0
            $0.isLayoutMarginsRelativeArrangement = true
            $0.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: Dimension.edgeMargin, right: 0)
}
