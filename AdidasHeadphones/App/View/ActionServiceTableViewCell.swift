//
//  ActionServiceTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 23/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct ServiceTraits {
    static let containerRightMargin: CGFloat = 24
    static let containerTitleDescriptionVerticalSpacing: CGFloat = 4
    static let iconViewWidth: CGFloat = 44
    static let iconViewHeight: CGFloat = 44
    static let checkmarkViewWidth: CGFloat = 20
    static let checkmarkViewHeight: CGFloat = 20
    static let edgeMargin: CGFloat = 20
    static let iconToContainerHorizontalSpacing: CGFloat = 20
}

final class ActionServiceTableViewCell: UITableViewCell {
    static let id = String(describing: ActionServiceTableViewCell.self)
    var serviceItem: ActionGroup? {
        didSet {
            guard let item = serviceItem else { return }
            titleLabel.attributedText = Headline.style(as: .smallDark, text: item.type.title())
            descriptionLabel.attributedText = Paragraph.style(as: .smallLight, text: item.type.description())
            iconView.image = item.type.icon()
            checkmarkView.image = Image.Icon.Black.check
            checkmarkView.isHidden = item.unfoldType == .always || item.unfoldType == .folded
            setupContainerConstraints()
            setupConstraints()
        }
    }
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    private let containerView: UIView = {
        let container = UIView(frame: .zero)
        return container
    }()
    private let checkmarkView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private let iconView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private func setupContainerConstraints() {
        [titleLabel, descriptionLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            containerView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor),
            titleLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -ServiceTraits.containerRightMargin),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: ServiceTraits.containerTitleDescriptionVerticalSpacing),
            descriptionLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            descriptionLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
    }
    private func setupConstraints() {
        [iconView, containerView, checkmarkView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: ServiceTraits.edgeMargin),
            iconView.heightAnchor.constraint(equalToConstant: ServiceTraits.iconViewHeight),
            iconView.widthAnchor.constraint(equalToConstant: ServiceTraits.iconViewWidth),
            containerView.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: ServiceTraits.iconToContainerHorizontalSpacing),
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: ServiceTraits.edgeMargin),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -ServiceTraits.edgeMargin),
            containerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            checkmarkView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            checkmarkView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -ServiceTraits.edgeMargin),
            checkmarkView.heightAnchor.constraint(equalToConstant: ServiceTraits.checkmarkViewHeight),
            checkmarkView.widthAnchor.constraint(equalToConstant: ServiceTraits.checkmarkViewWidth),
            containerView.rightAnchor.constraint(equalTo: checkmarkView.leftAnchor, constant: -Dimension.edgeMargin),
        ])
    }
}
