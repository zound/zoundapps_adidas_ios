//
//  PlayerView.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 30/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let emptyArtistLabelText = String.invisible
    static let emptyTitleLabelText = String.invisible
    static let unknownArtistLabelText = NSLocalizedString("home_player_unknown_artist_uc", value: "UNKNOWN ARTIST", comment: "Unknown artist")
    static let unknownTitleLabelText = NSLocalizedString("home_player_unknown_title_uc", value: "UNKNOWN TITLE", comment: "Unknown title")
    static let playingText = NSLocalizedString("home_player_status_playing_uc", value: "PLAYING", comment: "Playing")
    static let notPlayingText = NSLocalizedString("home_player_status_not_playing_uc", value: "NOT PLAYING", comment: "Not playing")
}

class PlayerView: UIView {
    var viewModel: PlayerViewModel?
    private lazy var artistLabel: UILabel = {
        let artistLabel = UILabel()
        return artistLabel
    }()
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        return titleLabel
    }()
    private lazy var statusLabel: UILabel = {
        let statusLabel = UILabel()
        return statusLabel
    }()
    private lazy var skipBackwardButton: PlayerButton = {
        let skipBackwardButton = PlayerButton(type: .system)
        skipBackwardButton.playerButtonType = .skipBackward
        skipBackwardButton.controlState = .disabled
        skipBackwardButton.addTarget(self, action: #selector(skipBackwardButtonTouchedUpInside), for: .touchUpInside)
        return skipBackwardButton
    }()
    private lazy var playPauseButton: PlayerButton = {
        let playPauseButton = PlayerButton(type: .system)
        playPauseButton.playerButtonType = .playPause(.play)
        playPauseButton.controlState = .disabled
        playPauseButton.addTarget(self, action: #selector(playPauseButtonTouchedUpInside), for: .touchUpInside)
        return playPauseButton
    }()
    private lazy var skipForwardButton: PlayerButton = {
        let skipForwardButton = PlayerButton(type: .system)
        skipForwardButton.playerButtonType = .skipForward
        playPauseButton.controlState = .disabled
        skipForwardButton.addTarget(self, action: #selector(skipForwardButtonTouchedUpInside), for: .touchUpInside)
        return skipForwardButton
    }()
    private lazy var controlsStackView: UIStackView = {
        let controlsStackView = UIStackView(arrangedSubviews: [skipBackwardButton, playPauseButton, skipForwardButton])
        controlsStackView.distribution = .fillEqually
        return controlsStackView
    }()
    var controlState: ControlState = .disabled {
        didSet {
            guard controlState != oldValue else { return }
            handle(changeOf: controlState)
        }
    }
    var uiAudioControlStatus: UIAudioControlStatus = .unknown {
        didSet {
            guard uiAudioControlStatus != oldValue else { return }
            handle(changeOf: uiAudioControlStatus)
        }
    }
    var uiAudioNowPlaying: UIAudioNowPlaying? {
        didSet {
            guard let uiAudioNowPlaying = uiAudioNowPlaying else {
                handleLackOfNowPlaying()
                return
            }
            handle(changeOf: uiAudioNowPlaying)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        setupBorder()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
        setupBorder()
    }
    private func setupConstraints() {
        [artistLabel, titleLabel, statusLabel, controlsStackView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            artistLabel.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.edgeMargin),
            artistLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            artistLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            artistLabel.heightAnchor.constraint(equalToConstant: 24),
            titleLabel.topAnchor.constraint(equalTo: artistLabel.bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            titleLabel.heightAnchor.constraint(equalToConstant: 16),
            statusLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Dimension.edgeMargin),
            statusLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            statusLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            controlsStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Dimension.edgeMargin),
            controlsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            controlsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            controlsStackView.heightAnchor.constraint(equalToConstant: Dimension.playerButtonSize)
        ])
    }
}

private extension PlayerView {
    @objc private func skipBackwardButtonTouchedUpInside() {
        viewModel?.skipBackwardButtonTouchedUpInside()
    }
    @objc private func playPauseButtonTouchedUpInside() {
        viewModel?.playPauseButtonTouchedUpInside()
    }
    @objc private func skipForwardButtonTouchedUpInside() {
        viewModel?.skipForwardButtonTouchedUpInside()
    }
    func handle(changeOf controlState: ControlState) {
        [skipBackwardButton, playPauseButton, skipForwardButton].forEach {
            $0.controlState = controlState
        }
    }
    func handle(changeOf uiAudioControlStatus: UIAudioControlStatus) {
        playPauseButton.playerButtonType = uiAudioControlStatus == .isPlaying ? .playPause(.pause) : .playPause(.play)
        let statusLabelText = uiAudioControlStatus == .isPlaying ? Const.playingText : Const.notPlayingText
        statusLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
            attributedText: Headline.style(as: .extraSmallLight, text: statusLabelText),
            textAligment: .center,
            lineBreakMode: .byTruncatingMiddle
        ))
    }
    func handle(changeOf uiAudioNowPlaying: UIAudioNowPlaying) {
        let newTitle = (uiAudioNowPlaying.title ?? Const.unknownTitleLabelText).uppercased()
        let newArtist = (uiAudioNowPlaying.artistName ?? Const.unknownArtistLabelText).uppercased()
        if let titleText = titleLabel.attributedText?.string, let artistText = artistLabel.attributedText?.string, titleText == newTitle, artistText == newArtist {
            return
        }
        titleLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
            attributedText: Headline.style(as: .mediumBlack, text: uiAudioNowPlaying.title ?? Const.unknownTitleLabelText),
            textAligment: .center,
            lineBreakMode: .byTruncatingMiddle
        ))
        artistLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
            attributedText: Headline.style(as: .extraSmallLight, text: uiAudioNowPlaying.artistName ?? Const.unknownArtistLabelText),
            textAligment: .center,
            lineBreakMode: .byTruncatingMiddle
        ))
    }
    func handleLackOfNowPlaying() {
        [titleLabel, artistLabel].forEach {
            $0.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: NSAttributedString(),
                textAligment: .center,
                lineBreakMode: .byTruncatingMiddle
            ))
        }
    }
}

extension PlayerView: PlayerViewModelViewInputDelegate {
    func update(controlState: ControlState) {
        self.controlState = controlState
    }
    func update(uiAudioControlStatus: UIAudioControlStatus) {
        self.uiAudioControlStatus = uiAudioControlStatus
    }
    func update(uiAudioNowPlaying: UIAudioNowPlaying?) {
        self.uiAudioNowPlaying = uiAudioNowPlaying
    }
}
