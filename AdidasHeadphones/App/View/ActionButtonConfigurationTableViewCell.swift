//
//  ActionButtonConfigurationTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 02/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let rowHeight: CGFloat = 67.0
    static let appIconLeftMargin: CGFloat = 23.0
    static let appIconAppNameHorizontalSegment: CGFloat = 27
    static let statusChevronRightSegment: CGFloat = Dimension.edgeMargin
    static let iconDimension: CGFloat = 22.0
    static let rightAnchorDimension: CGFloat = 20.0
    static let checkmarkViewWidth: CGFloat = 20
    static let checkmarkViewHeight: CGFloat = 20
}
final class ActionButtonConfigurationTableViewCell: UITableViewCell {
    static let id = String(describing: ActionButtonConfigurationTableViewCell.self)
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let chevronRightView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = Colors.greyLight
        return imageView
    }()
    private let iconView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = Colors.greyLight
        return imageView
    }()
    private let checkmarkView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.image = Image.Icon.Black.check
        return imageView
    }()
    var configurationInfo: ButtonConfigurationInfo? {
        didSet {
            guard let info = configurationInfo else { return }
            nameLabel.attributedText = Headline.style(as: .smallDark, text: info.entry.action.configurationTitle(for: info.pressType))
            iconView.image = info.entry.action.alternativeIcon() ?? info.entry.action.configurationIcon()
            chevronRightView.image = info.entry.active == true ? chevronRightView.image : nil
            checkmarkView.isHidden = info.entry.active == false
            setupConstraints()
        }
    }
    var collapsedEntry: GroupedActions? {
        didSet {
            guard let entry = collapsedEntry else { return }
            guard let name = entry.applicationID?.title(), let icon = entry.applicationID?.icon() else { return }
            nameLabel.attributedText = Headline.style(as: .smallDark, text: name)
            iconView.image = icon
            checkmarkView.isHidden = true
            self.accessoryType = .disclosureIndicator
            setupConstraints()
        }
    }
    private func setupConstraints() {
        [iconView, nameLabel, checkmarkView, chevronRightView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            iconView.heightAnchor.constraint(equalToConstant: Config.iconDimension),
            iconView.widthAnchor.constraint(equalToConstant: Config.iconDimension),
            nameLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: Config.appIconAppNameHorizontalSegment),
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            chevronRightView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            chevronRightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            chevronRightView.heightAnchor.constraint(equalToConstant: Config.rightAnchorDimension),
            chevronRightView.widthAnchor.constraint(equalToConstant: Config.rightAnchorDimension),
            checkmarkView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            checkmarkView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            checkmarkView.heightAnchor.constraint(equalToConstant: Config.checkmarkViewHeight),
            checkmarkView.widthAnchor.constraint(equalToConstant: Config.checkmarkViewWidth),
        ])
    }
    
}

