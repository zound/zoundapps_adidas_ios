//
//  SpotifyActionButtonConfigurationTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 05/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let appIconLeftMargin: CGFloat = 23.0
    static let appIconAppNameHorizontalSegment: CGFloat = 27
    static let statusChevronRightSegment: CGFloat = Dimension.edgeMargin
    static let iconDimension: CGFloat = 22.0
    static let rightAnchorDimension: CGFloat = 20.0
}

class SpotifyActionButtonConfigurationTableViewCell: UITableViewCell {
    static let id = String(describing: SpotifyActionButtonConfigurationTableViewCell.self)
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let spotifyEntryNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let chevronRightView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = Colors.greyLight
        return imageView
    }()
    var spotifyEntry: SpotifyEntry? {
        didSet {
            guard let entry = spotifyEntry else { return }
            nameLabel.attributedText = Headline.style(as: .smallDark, text: entry.actionName)
            spotifyEntryNameLabel.attributedText =
                Paragraph.style(as: .smallLight, text: entry.name ?? String.invisible)
            spotifyEntryNameLabel.lineBreakMode = .byTruncatingTail
            chevronRightView.image = entry.active == true ? Image.Icon.Black.check : nil
            setupConstraints()
        }
    }
    private func setupConstraints() {
        let entryStackView = UIStackView(arrangedSubviews: [nameLabel, spotifyEntryNameLabel])
        verticalStackViewStyle(entryStackView)
        entryStackView.spacing = 0
        entryStackView.layoutMargins = UIEdgeInsets.zero
        let horizontalStackView = UIStackView(arrangedSubviews: [entryStackView, chevronRightView])
        rootHorizontalStackViewStyle(horizontalStackView)
        horizontalStackView.distribution = .equalCentering
        spotifyEntryNameLabel.isHidden = spotifyEntryNameLabel.text == String.invisible ? true : false
        contentView.addSubview(horizontalStackView)
        NSLayoutConstraint.activate([
            horizontalStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            horizontalStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Dimension.edgeMargin),
            horizontalStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Dimension.edgeMargin),
            horizontalStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Dimension.edgeMargin)            
        ])
    }
}
