//
//  ConnectedAppTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 31/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSpotifyWrapper

extension SpotifyAppStatus {
    func label() -> String {
        switch self {
        case .connected:
            return NSLocalizedString("app_connections_connected", value: "Connected", comment: "Connected")
        default:
            return ""
        }
    }
}
protocol ConnectedApplicationStatusProvider {
    var status: SpotifyAppStatus { get }
}
private struct Config {
    static let rowHeight: CGFloat = 67.0
    static let appIconLeftMargin: CGFloat = 23.0
    static let appIconAppNameHorizontalSegment: CGFloat = 27
    static let statusChevronRightSegment: CGFloat = Dimension.edgeMargin
    static let iconDimension: CGFloat = 22.0
    static let rightAnchorDimension: CGFloat = 20.0
}
final class ConnectedAppTableViewCell: UITableViewCell {
    static let id = String(describing: ConnectedAppTableViewCell.self)
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    
    private let statusLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    
    private let chevronRightView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = Colors.greyLight
        return imageView
    }()
    private let waitingIndicatorImageView: UIImageView = {
        let waitingIndicatorImageView = UIImageView(image: Image.Indicators.spinnerDark)
        waitingIndicatorImageView.contentMode = .center
        waitingIndicatorImageView.isHidden = true
        return waitingIndicatorImageView
    }()
    private let iconView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    var applicationInfo: ConnectedApplicationInfo? {
        didSet {
            guard let connected = applicationInfo else { return }
            nameLabel.attributedText = Headline.style(as: .smallDark, text: connected.name)
            statusLabel.attributedText = Paragraph.style(as: .regularLight, text: connected.status.label())
            iconView.image = connected.icon
            setupConstraints()
            switch connected.status {
            case .checking, .authorizationCallback:
                chevronRightView.isHidden = true
                waitingIndicatorImageView.isHidden = false
                waitingIndicatorImageView.rotate()
                self.accessoryType = .disclosureIndicator
            default:
                chevronRightView.isHidden = false
                waitingIndicatorImageView.isHidden = true
                return
            }
        }
    }
    private func setupConstraints() {
        
        [iconView, nameLabel, statusLabel, chevronRightView, waitingIndicatorImageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            iconView.heightAnchor.constraint(equalToConstant: Config.iconDimension),
            iconView.widthAnchor.constraint(equalToConstant: Config.iconDimension),
            nameLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: Config.appIconAppNameHorizontalSegment),
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            waitingIndicatorImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            waitingIndicatorImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            chevronRightView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            chevronRightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            chevronRightView.heightAnchor.constraint(equalToConstant: Config.rightAnchorDimension),
            chevronRightView.widthAnchor.constraint(equalToConstant: Config.rightAnchorDimension),
            statusLabel.rightAnchor.constraint(equalTo: chevronRightView.leftAnchor, constant: -Dimension.edgeMargin),
            statusLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            ])
    }
    
}
