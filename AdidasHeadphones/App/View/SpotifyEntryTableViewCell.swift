//
//  SpotifyEntryTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 19/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let coverMargin: CGFloat = 12.0
    static let separatorHeight: CGFloat = 1.0
}

class SpotifyEntryTableViewCell: UITableViewCell {
    static let id = String(describing: SpotifyEntryTableViewCell.self)
    private var id = String()
    private let entryNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let entryInfoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    let coverImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    var spotifyEntry: SpotifyUISearchEntry? {
        didSet {
            guard let entry = spotifyEntry else { return }
            id = entry.id
            entryNameLabel.attributedText = Headline.style(as: .smallDark, text: entry.name)
            var detailedDescription: String
            let songsLabel = entry.numberOfSongs != 1 ?
                NSLocalizedString("spotify_search_songs_plural", value: "songs", comment: "songs") :
                NSLocalizedString("spotify_search_songs_singular", value: "song", comment: "song")
            detailedDescription = String(entry.numberOfSongs) + " " + songsLabel
            if entry.numberOfSongs > 0 {
                detailedDescription.append(contentsOf: entry.duration.count > 0 ? ", " + entry.duration : " ")
                entryInfoLabel.attributedText = Paragraph.style(as: .regularLight, text: detailedDescription)
                entryInfoLabel.isHidden = false
            } else {
                entryInfoLabel.isHidden = true
            }
            coverImageView.isHidden = true
            setupConstraints()
        }
    }
    func set(entryId: String, image: UIImage) {
        guard entryId == id else { return }
        coverImageView.isHidden = false
        coverImageView.image = image
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.coverImageView.image = nil
    }
    private func setupConstraints() {
        [coverImageView, entryNameLabel, entryInfoLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            coverImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            coverImageView.widthAnchor.constraint(equalToConstant: Dimension.spotifySearchEntryDimension),
            coverImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            coverImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Const.coverMargin),
            coverImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Const.coverMargin),
            entryNameLabel.leadingAnchor.constraint(equalTo: coverImageView.trailingAnchor, constant: Dimension.edgeMargin),
            entryNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            entryNameLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -Const.separatorHeight),
            entryInfoLabel.leadingAnchor.constraint(equalTo: coverImageView.trailingAnchor, constant: Dimension.edgeMargin),
            entryInfoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            entryInfoLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -Const.separatorHeight)
        ])
    }
}
