//
//  KnownDeviceTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 23/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

enum KnownDeviceCellState {
    case normalBluetoothClassicConnected
    case normalBluetoothClassicNotConnected
    case connecting
    case unableToConnect
}

extension KnownDeviceCellState {
    var statusLabelAttributedText: NSAttributedString {
        switch self {
        case .normalBluetoothClassicConnected:
            return Paragraph.style(as: .regularLight, text: NSLocalizedString("home_modal_connected", value: "Connected", comment: "Connected"))
        case .normalBluetoothClassicNotConnected:
            return Paragraph.style(as: .regularLight, text: NSLocalizedString("home_modal_not_connected", value: "Not connected", comment: "Not connected"))
        case .connecting:
            return Paragraph.style(as: .regularLight, text: NSLocalizedString("home_modal_switching", value: "Switching...", comment: "Switching..."))
        case .unableToConnect:
            return Paragraph.style(as: .regularLight, text: NSLocalizedString("home_modal_unable_to_switch", value: "Unable to switch", comment: "Unable to switch")).color(Colors.red)
        }
    }
    var statusIndicatorImageViewImage: UIImage? {
        switch self {
        case .normalBluetoothClassicConnected, .normalBluetoothClassicNotConnected:
            return nil
        case .connecting:
            return Image.Indicators.spinnerDark
        case .unableToConnect:
            return Image.Indicators.error
        }
    }
}

class KnownDeviceTableViewCell: UITableViewCell {
    static let id: String = String(describing: KnownDeviceTableViewCell.self)
    var uiKnownDevice: UIKnownDevice?
    var showCheckMark = false {
        didSet {
            update(state)
        }
    }
    var state: KnownDeviceCellState = .normalBluetoothClassicConnected {
        didSet {
            update(state)
        }
    }
    private let deviceImageView: UIImageView = {
        let deviceImageView = UIImageView()
        deviceImageView.contentMode = .center
        return deviceImageView
    }()
    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        return nameLabel
    }()
    private let statusLabel: UILabel = {
        let statusLabel = UILabel()
        return statusLabel
    }()
    private let statusIndicatorImageView: UIImageView = {
        let statusIndicatorImageView = UIImageView()
        statusIndicatorImageView.contentMode = .center
        return statusIndicatorImageView
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
        configure()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
        configure()
    }
    private func configure() {
        selectionStyle = .none
        preservesSuperviewLayoutMargins = false
        separatorInset = .zero
        layoutMargins = .zero
    }
    private func setupConstraints() {
        [deviceImageView, nameLabel, statusLabel, statusIndicatorImageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            deviceImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            deviceImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            deviceImageView.heightAnchor.constraint(equalToConstant: 60),
            deviceImageView.widthAnchor.constraint(equalTo: deviceImageView.heightAnchor),
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18),
            nameLabel.leadingAnchor.constraint(equalTo: deviceImageView.trailingAnchor, constant: Dimension.edgeMargin),
            statusLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor),
            statusLabel.leadingAnchor.constraint(equalTo: deviceImageView.trailingAnchor, constant: Dimension.edgeMargin),
            statusIndicatorImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            statusIndicatorImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            statusIndicatorImageView.heightAnchor.constraint(equalToConstant: 28),
            statusIndicatorImageView.widthAnchor.constraint(equalTo: statusIndicatorImageView.heightAnchor),
            statusIndicatorImageView.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor, constant: Dimension.edgeMargin),
            statusIndicatorImageView.leadingAnchor.constraint(equalTo: statusLabel.trailingAnchor, constant: Dimension.edgeMargin),
        ])
    }
    private func update(_ state: KnownDeviceCellState) {
        guard let uiKnownDevice = uiKnownDevice else { return }
        deviceImageView.image = uiKnownDevice.image
        nameLabel.attributedText = Headline.style(as: .smallLight, text: uiKnownDevice.name)
        nameLabel.textAlignment = .left
        animate(state)
    }
    func animate(_ state: KnownDeviceCellState) {
        statusLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
            attributedText: state.statusLabelAttributedText,
            textAligment: .left,
            lineBreakMode: .byTruncatingTail
        ))
        let statusIndicatorImageViewFadeOutAmination: () -> Void = { [weak self] in
            self?.statusIndicatorImageView.image = UIImage()
        }
        let statusIndicatorImageViewFadeInAmination: () -> Void = { [weak self] in
            guard let self = self else {
                return
            }
            if self.showCheckMark  {
                self.statusIndicatorImageView.image =  Image.Icon.Black.check
            } else {
                self.statusIndicatorImageView.image = state.statusIndicatorImageViewImage
            }
        }
        statusIndicatorImageView.layer.removeAllAnimations()
        let options: UIView.AnimationOptions = [.curveEaseInOut, .transitionCrossDissolve]
        UIView.transition(with: statusIndicatorImageView, duration: .t010, options: options, animations: statusIndicatorImageViewFadeOutAmination) { _ in
            if state == .connecting {
                self.statusIndicatorImageView.rotate()
            }
            UIView.transition(with: self.statusIndicatorImageView, duration: .t025, options: options, animations: statusIndicatorImageViewFadeInAmination)
        }
    }
}
