//
//  ActionButtonTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 23/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct ButtonTraits {
    static let containerRightMargin: CGFloat = 24
    static let containerTitleDescriptionVerticalSpacing: CGFloat = 1
    static let iconViewWidth: CGFloat = 22
    static let iconViewHeight: CGFloat = 22
    static let chevronRightViewWidth: CGFloat = 20
    static let chevronRightViewHeight: CGFloat = 20
    static let edgeMargin: CGFloat = 20
    static let iconToContainerHorizontalSpacing: CGFloat = 20
}

final class ActionButtonTableViewCell: UITableViewCell {
    static let id = String(describing: ActionButtonTableViewCell.self)
    var simplified = false
    var buttonActionItem: ButtonActionInfo? {
        didSet {
            guard let item = buttonActionItem else { return }
            titleLabel.attributedText = Headline.style(as: .smallDark, text: item.type.label())
            descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: item.info.action.title(for: item.type))
            chevronRightView.image = Image.Icon.Black.chevronRight
            iconView.image = item.info.action.configurationIcon().scaled(to: CGSize(width: ButtonTraits.iconViewWidth, height: ButtonTraits.iconViewHeight))
            if simplified == true { chevronRightView.isHidden = true }
            setupConstraints()
        }
    }
    var extendedActionItem: ExtendedButtonActionInfo? {
        didSet {
            valueLabel.isHidden = true
            guard let item = extendedActionItem else { return }
            buttonActionItem = item.info
            guard let uuid = item.uuid, let stored = DefaultStorage.shared.spotifyLocalData?.perDeviceConfiguration.filter({ $0.uuid == uuid }).first else {
                return
            }
            guard let toUpdate = stored.entries.filter({ $0.pressType == item.info.type }).first else { return }
            guard item.info.info.action == toUpdate.value.type.action() else { return }
            valueLabel.isHidden = false
            valueLabel.attributedText = Paragraph.style(as: .regularLight, text: toUpdate.value.name)
            valueLabel.lineBreakMode = .byTruncatingTail
        }
    }
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let chevronRightView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private let iconView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private func setupConstraints() {
        let infoStackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel, valueLabel])
        verticalStackViewStyle(infoStackView)
        infoStackView.spacing = 0
        infoStackView.layoutMargins = UIEdgeInsets.zero
        let horizontalStackView = UIStackView(arrangedSubviews: [iconView, infoStackView, chevronRightView])
        rootHorizontalStackViewStyle(horizontalStackView)
        horizontalStackView.distribution = .fill
        contentView.addSubview(horizontalStackView)
        NSLayoutConstraint.activate([
            horizontalStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            horizontalStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Dimension.edgeMargin),
            horizontalStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Dimension.edgeMargin),
            horizontalStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Dimension.edgeMargin)
        ])
    }
}
extension NSLayoutConstraint {
    func with(priority: Float) -> NSLayoutConstraint {
        self.priority = UILayoutPriority(priority)
        return self
    }
}

