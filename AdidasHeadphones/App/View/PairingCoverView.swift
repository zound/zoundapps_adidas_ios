//
//  PairingCoverView.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 10/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class PairingCoverView: UIView {
    var headerLabelAttributedText: NSAttributedString = NSAttributedString() {
        didSet {
            headerLabel.attributedText = headerLabelAttributedText
            headerLabel.textAlignment = .center
        }
    }
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.numberOfLines = 2
        return headerLabel
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularWhite, text: NSLocalizedString("ios_select_pairing_device_paragraph", value: "It can take a few minutes before your headphones appear in the list.", comment: "It can take a few minutes before your headphones appear in the list."))
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        [headerLabel, descriptionLabel].forEach {
            containerView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: Dimension.headerBigBottomMargin),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            headerLabel.topAnchor.constraint(equalTo: containerView.topAnchor),
            headerLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            headerLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.accessoryPickerHeight),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
}
