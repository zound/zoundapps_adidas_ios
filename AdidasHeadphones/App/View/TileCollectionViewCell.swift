//
//  TileCollectionViewCell.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 24/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private extension Tile {
    var text: String {
        switch self {
        case .batteryLevel:
            return String.invisible
        case .actionButton:
            return NSLocalizedString("home_tile_action_button", value: "ACTION BUTTON", comment: "ACTION BUTTON")
        case .equalizer:
            return NSLocalizedString("home_tile_equalizer", value: "EQUALIZER", comment: "EQUALIZER")
//        case .autoOffTimer:
//            return NSLocalizedString("home_tile_auto_off_timer", value: "AUTO OFF TIMER", comment: "AUTO OFF TIMER")
        case .status:
            return NSLocalizedString("home_tile_status", value: "STATUS", comment: "STATUS")
        }
    }
    var image: UIImage {
        switch self {
        case .batteryLevel:
            return UIImage()
        case .actionButton:
            return Image.SmallIcon.Light.actionButton
        case .equalizer:
            return Image.SmallIcon.Light.equalizer
//        case .autoOffTimer:
//            return Image.SmallIcon.Light.autoOffTimer
        case .status:
            return Image.SmallIcon.Light.status
        }
    }
}

enum TileValue: Equatable {
    enum Color {
        case light, white
    }
    case notSet
    case setting
    case set(String, color: Color)
}

private extension TileValue {
    var attributedText: NSAttributedString {
        switch self {
        case .notSet, .setting:
            return Headline.style(as: .mediumLight, text: String.invisible)
        case .set(let value, color: let color):
            return Headline.style(as: color == .light ? .mediumLight : .mediumWhite, text: value)
        }
    }
    var isProgressIndicatorHidden: Bool {
        switch self {
        case .notSet, .set:
            return true
        case .setting:
            return false
        }
    }
}

class TileCollectionViewCell: UICollectionViewCell {
    static let id: String = String(describing: TileCollectionViewCell.self)
    var tile: Tile = .actionButton {
        didSet {
            headerLabel.attributedText = Headline.style(as: .extraSmallLight, text: tile.text)
            headerLabel.textAlignment = .left
            tileImageView.image = tile.image.withRenderingMode(.alwaysTemplate)
        }
    }
    var tileValue: TileValue = .notSet {
        didSet {
            valueLabel.isHidden = !tileValue.isProgressIndicatorHidden
            valueLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: tileValue.attributedText,
                textAligment: .left,
                lineBreakMode: .byTruncatingTail
            ))
            progressIndicatorImageView.isHidden = tileValue.isProgressIndicatorHidden
            if !tileValue.isProgressIndicatorHidden {
                progressIndicatorImageView.rotate()
            } else {
                progressIndicatorImageView.layer.removeAllAnimations()
            }
        }
    }
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.numberOfLines = 3
        return headerLabel
    }()
    private lazy var tileImageView: UIImageView = {
        let tileImageView = UIImageView()
        tileImageView.contentMode = .scaleAspectFit
        tileImageView.tintColor = Colors.greyMedium
        return tileImageView
    }()
    private lazy var valueLabel: UILabel = {
        let valueLabel = UILabel()
        return valueLabel
    }()
    private lazy var progressIndicatorImageView: UIImageView = {
        let progressIndicatorImageView = UIImageView(image: Image.Indicators.spinnerWhite)
        progressIndicatorImageView.contentMode = .center
        progressIndicatorImageView.isHidden = tileValue.isProgressIndicatorHidden
        return progressIndicatorImageView
    }()
    private var highlightPropertyAnimator = UIViewPropertyAnimator()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        contentView.backgroundColor = Colors.black
        [headerLabel, tileImageView, valueLabel, progressIndicatorImageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.tileMargin),
            headerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.tileMargin),
            tileImageView.centerYAnchor.constraint(equalTo: headerLabel.centerYAnchor),
            tileImageView.leadingAnchor.constraint(equalTo: headerLabel.trailingAnchor, constant: Dimension.tileMargin),
            tileImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.tileMargin),
            tileImageView.heightAnchor.constraint(equalToConstant: Dimension.tileImageViewSize),
            tileImageView.widthAnchor.constraint(equalToConstant: Dimension.tileImageViewSize),
            valueLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Dimension.tileMargin),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.tileMargin),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.tileMargin),
            progressIndicatorImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Dimension.tileMargin),
            progressIndicatorImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.tileMargin),
            progressIndicatorImageView.heightAnchor.constraint(equalToConstant: Dimension.whiteProgressImageViewSize),
            progressIndicatorImageView.widthAnchor.constraint(equalToConstant: Dimension.whiteProgressImageViewSize)
        ])
    }
}

extension TileCollectionViewCell {
    func highlight() {
        highlightPropertyAnimator.stopAnimation(true)
        highlightPropertyAnimator = UIViewPropertyAnimator(duration: .t010, curve: .easeOut, animations: { [weak self] in
            self?.contentView.backgroundColor = Colors.greyExtraDark
            self?.tileImageView.transform = Const.tileImageViewTransform
        })
        highlightPropertyAnimator.startAnimation()
    }
    func unhighlight() {
        highlightPropertyAnimator.stopAnimation(true)
        highlightPropertyAnimator = UIViewPropertyAnimator(duration: .t010, curve: .easeOut, animations: { [weak self] in
            self?.contentView.backgroundColor = Colors.black
            self?.tileImageView.transform = .identity
        })
        highlightPropertyAnimator.startAnimation()
    }
}

private struct Const {
    static let tileImageViewTransform = CGAffineTransform(scaleX: 0.9, y: 0.9)
}
