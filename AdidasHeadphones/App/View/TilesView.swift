//
//  TilesView.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 21/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

enum Tile: CaseIterable {
    case batteryLevel
    case actionButton
    case equalizer
    //case autoOffTimer
    case status
}

class TilesView: UIView {
    var viewModel: TilesViewModel?
    private var isActive = false
    private let collectionViewModel = Tile.allCases
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: TilesLayout())
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(BatteryCollectionViewCell.self, forCellWithReuseIdentifier: BatteryCollectionViewCell.id)
        collectionView.register(TileCollectionViewCell.self, forCellWithReuseIdentifier: TileCollectionViewCell.id)
        return collectionView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        collectionView.backgroundColor = Colors.greyDark
        [collectionView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

extension TilesView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewModel.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = indexPath.item == .zero ? BatteryCollectionViewCell.id : TileCollectionViewCell.id
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        if indexPath.item > .zero, let tileCollectionViewCell = cell as? TileCollectionViewCell {
            tileCollectionViewCell.tile = collectionViewModel[indexPath.item]
            tileCollectionViewCell.tileValue = .notSet
        }
        return cell
    }
}

extension TilesView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionViewModel[indexPath.item] == .status {
            viewModel?.collectionViewWillDisplayStatusTile()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = cell(for: collectionViewModel[indexPath.item]), isActive {
            cell.highlight()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = cell(for: collectionViewModel[indexPath.item]) {
            cell.unhighlight()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionViewModel[indexPath.item] != .batteryLevel, isActive {
            viewModel?.didSelect(tile: collectionViewModel[indexPath.item])
        }
    }
}

private extension TilesView {
    func cell(for tile: Tile) -> TileCollectionViewCell? {
        guard let index = collectionViewModel.firstIndex(of: tile),
              let cell = collectionView.cellForItem(at: IndexPath(item: Int(index), section: .zero)) as? TileCollectionViewCell else {
            return nil
        }
        return cell
    }
    func update(tileValue: TileValue, for tile: Tile) {
        if let cell = cell(for: tile) {
            cell.tileValue = tileValue
        }
    }
}

extension TilesView: TilesViewModelViewInputDelegate {
    func update(uiBatteryLevel: UIBatteryLevel) {
        if let batteryLevelCell = collectionView.cellForItem(at: IndexPath(item: .zero, section: .zero)) as? BatteryCollectionViewCell {
            batteryLevelCell.uiBatteryLevel = uiBatteryLevel
        }
    }
    func update(actionButton: TileValue) {
        update(tileValue: actionButton, for: .actionButton)
    }
    func update(equalizer: TileValue) {
        update(tileValue: equalizer, for: .equalizer)
    }
//    func update(autoOffTimer: TileValue) {
//        update(tileValue: autoOffTimer, for: .autoOffTimer)
//    }
    func update(status: TileValue) {
        update(tileValue: status, for: .status)
    }
    func update(isActive: Bool) {
        self.isActive = isActive
    }
}
