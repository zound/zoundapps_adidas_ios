//
//  GuideCollectionViewCell.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 12/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

struct GuideItem {
    let image: UIImage
    let header: String
    let description: String
    let newsletterVisible: Bool
}
private struct Config {
    static var descriptionNewsletterBlockMargin: CGFloat = 27.0
    static var newsLetterViewBorderWidth: CGFloat = 1.0
}
class GuideCollectionViewCell: UICollectionViewCell {
    static let id: String = String(describing: GuideCollectionViewCell.self)
    var guideItem: GuideItem? {
        didSet {
            guard let _ = guideItem else { return }
            imageView.image = guideItem!.image
            headerLabel.attributedText = Headline.style(as: .largeBlack, text: guideItem!.header)
            descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: guideItem!.description)
            [headerLabel, descriptionLabel].forEach {
                $0.textAlignment = .center
            }
            guard guideItem!.newsletterVisible == true else {
                newsletterView.isHidden = true
                return
            }
            newsletterView.isHidden = false
        }
    }
    var newsletterView: NewsletterView = {
        let newsletterView = NewsletterView()
        return newsletterView
    }()
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private let headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private let descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private var disposeBag = DisposeBag()
    private lazy var keyboardConstraint = newsletterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Dimension.screenHeight18)
    func configure(viewModel: NewsletterViewModel) {
        newsletterView.newsletterViewModel = viewModel
        viewModel.viewInputDelegate = newsletterView
        viewModel.setup()
    }
    private func setupConstraints() {
        [imageView, headerLabel, descriptionLabel, newsletterView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        let imageTop = imageView.topAnchor.constraint(equalTo: contentView.topAnchor)
        imageTop.priority = UILayoutPriority.defaultLow

        let imageBottom = imageView.bottomAnchor.constraint(equalTo: headerLabel.topAnchor)

        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Dimension.screenWidth),
            imageTop, imageBottom,
            newsletterView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            newsletterView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            keyboardConstraint,
            descriptionLabel.bottomAnchor.constraint(equalTo: newsletterView.topAnchor, constant: -Config.descriptionNewsletterBlockMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            headerLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin)
        ])
        Keyboard.info
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] info in
                self?.keyboardEvent(info: info)
            })
            .disposed(by: disposeBag)
    }
    private func keyboardEvent(info: KeyboardInfo) {
        switch info.notificationName {
        case UIResponder.keyboardWillShowNotification:
            NSLayoutConstraint.deactivate([
                keyboardConstraint
            ])
            keyboardConstraint = newsletterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Dimension.edgeMargin - info.frame.height)
            NSLayoutConstraint.activate([
                keyboardConstraint
            ])
            newsletterView.newsletterViewModel?.keyboard(shown: true)
        case UIResponder.keyboardWillHideNotification:
            NSLayoutConstraint.deactivate([
                keyboardConstraint
            ])
            keyboardConstraint = newsletterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Dimension.screenHeight18)
            NSLayoutConstraint.activate([
                keyboardConstraint
            ])
            newsletterView.newsletterViewModel?.keyboard(shown: false)
        default:
            break
        }
    }
}
