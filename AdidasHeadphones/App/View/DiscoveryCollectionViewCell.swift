//
//  DiscoveryCollectionViewCell.swift
//  002
//
//  Created by Łukasz Wudarski on 28/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit

struct DiscoveryItem {
    let image: UIImage
    let name: String
}

class DiscoveryCollectionViewCell: UICollectionViewCell {
    static let id: String = String(describing: DiscoveryCollectionViewCell.self)
    var discoveryItem: DiscoveryItem? {
        didSet {
            guard let _ = discoveryItem else { return }
            imageView.image = discoveryItem!.image
        }
    }
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Dimension.screenWidth),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.statusBarHeight)
        ])
    }
}
