//
//  RectangularSwitchComponentView.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 15/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class RectangularSwitchComponentView: UIView {
    var descriptionLabelAttributedText: NSAttributedString = NSAttributedString() {
        didSet {
            descriptionLabel.attributedText = descriptionLabelAttributedText
            descriptionLabel.lineBreakMode = .byTruncatingTail
        }
    }
    private lazy var rectangularSwitch: RectangularSwitch = {
        let rectangularSwitch = RectangularSwitch()
        rectangularSwitch.onTintColor = Colors.black
        rectangularSwitch.thumbTintColor = Colors.white
        return rectangularSwitch
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        return descriptionLabel
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        setupBorder()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
        setupBorder()
    }
    private func setupConstraints() {
        [rectangularSwitch, descriptionLabel].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            rectangularSwitch.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.edgeMargin),
            rectangularSwitch.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Dimension.edgeMargin),
            rectangularSwitch.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            rectangularSwitch.widthAnchor.constraint(equalToConstant: Dimension.rectangularSwitchWidth),
            rectangularSwitch.heightAnchor.constraint(equalToConstant: Dimension.rectangularSwitchHeight),
            descriptionLabel.leadingAnchor.constraint(equalTo: rectangularSwitch.trailingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.centerYAnchor.constraint(equalTo: rectangularSwitch.centerYAnchor, constant: -Dimension.minorAlignment)
        ])
    }
}
