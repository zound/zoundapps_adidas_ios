//
//  DisconnectedMessageView.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 28/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let shownTransitionTimeout: TimeInterval = 2
    static let headerText = NSLocalizedString("home_disconnected_header", value: "OUT OF RANGE", comment: "OUT OF RANGE")
    static let descriptionText = NSLocalizedString("home_disconnected_description", value: "Please make sure that headphones are powered on and in range.", comment: "Please make sure that headphones are powered on and in range.")
}

enum DisconnectedMessageState {
    case waiting
    case hidden
    case shown
}

private extension DisconnectedMessageState {
    var isHeaderLabelHidden: Bool {
        return self != .shown
    }
    var isDescriptionLabelHidden: Bool {
        return self != .shown
    }
    var isWaitingIndicatorHidden: Bool {
        return self != .waiting
    }
}

class DisconnectedMessageView: UIView {
    var disconnectedMessageState: DisconnectedMessageState = .waiting {
        didSet {
            headerLabel.isHidden = disconnectedMessageState.isHeaderLabelHidden
            descriptionLabel.isHidden = disconnectedMessageState.isDescriptionLabelHidden
            waitingIndicatorImageView.isHidden = disconnectedMessageState.isWaitingIndicatorHidden
            if disconnectedMessageState == .shown {
                headerLabel.indicateAnimated()
            }
            shownTransitionTimer?.invalidate()
            if disconnectedMessageState == .waiting {
                shownTransitionTimer = Timer.scheduledTimer(withTimeInterval: Const.shownTransitionTimeout, repeats: false) { [weak self] _ in
                    self?.disconnectedMessageState = .shown
                }
            }
        }
    }
    private var shownTransitionTimer: Timer?
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: Const.headerText)
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = .zero
        headerLabel.isHidden = true
        return headerLabel
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: Const.descriptionText)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        descriptionLabel.isHidden = true
        return descriptionLabel
    }()
    private let waitingIndicatorImageView: UIImageView = {
        let waitingIndicatorImageView = UIImageView(image: Image.Indicators.spinnerDark)
        waitingIndicatorImageView.contentMode = .center
        waitingIndicatorImageView.isHidden = true
        return waitingIndicatorImageView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        waitingIndicatorImageView.rotate()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
        waitingIndicatorImageView.rotate()
    }
    private func setupConstraints() {
        [headerLabel, descriptionLabel, waitingIndicatorImageView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            waitingIndicatorImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            waitingIndicatorImageView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: -Dimension.edgeMargin.twice),
        ])
    }
}
