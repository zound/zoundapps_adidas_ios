//
//  NoInternetView.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 14/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

final class NoInternetView: UIView, CloseableView {
    private var closeHandler: (UIButton) -> Void
    internal lazy var closeButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.setImage(Image.Icon.Black.notSet.scaled(to: CGSize(width: 21, height: 21)), for: .normal)
        return button
    }()
    private lazy var noInternetHeader: UILabel = {
        let label = UILabel(frame: .zero)
        label.attributedText = Headline.style(as: .smallDark, text: NSLocalizedString("appwide_no_internet_header_uc", comment: "No internet header"))
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    private lazy var noInternetDescription: UILabel = {
        let label = UILabel(frame: .zero)
        label.attributedText = Paragraph.style(as: .smallDark, text: NSLocalizedString("appwide_no_internet_description", comment: "Please make sure that your internet connection is working properly."))
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    init(closeHandler: @escaping (UIButton) -> Void) {
        self.closeHandler = closeHandler
        super.init(frame: .zero)
        closeButton.addTarget(self, action: #selector(hideView), for: .touchUpInside)
        self.backgroundColor = Colors.lightYellow
        setupConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
private extension NoInternetView {
    func setupConstraints() {
        [closeButton, noInternetHeader, noInternetDescription]
            .forEach {
                autolayoutStyle($0)
                addSubview($0)
            }
        NSLayoutConstraint.activate([
            noInternetHeader.leftAnchor.constraint(equalTo: leftAnchor, constant: Dimension.edgeMargin),
            noInternetHeader.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.edgeMargin),
            noInternetHeader.rightAnchor.constraint(equalTo: rightAnchor, constant: 3 * Dimension.edgeMargin),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -Dimension.edgeMargin),
            closeButton.centerYAnchor.constraint(equalTo: noInternetHeader.centerYAnchor),
            noInternetDescription.leftAnchor.constraint(equalTo: leftAnchor, constant: Dimension.edgeMargin),
            noInternetDescription.topAnchor.constraint(equalTo: noInternetHeader.bottomAnchor),
            noInternetDescription.leftAnchor.constraint(equalTo: leftAnchor, constant: Dimension.edgeMargin),
            noInternetDescription.rightAnchor.constraint(equalTo: rightAnchor, constant: Dimension.edgeMargin),
            noInternetDescription.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset)
        ])
    }
    @objc func hideView(_ target: UIButton) {
        closeHandler(target)
    }
}

