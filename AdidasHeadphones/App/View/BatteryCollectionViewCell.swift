//
//  BatteryCollectionViewCell.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 24/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

enum UIBatteryLevel: Equatable {
    case unknown
    case level(Int)
    init(_ batteryLevel: Int) {
        if 0...100 ~= batteryLevel {
            self = .level(batteryLevel)
        } else {
            self = .unknown
        }
    }
}

private extension UIBatteryLevel {
    var image: UIImage {
        if case .level(let batteryLevel) = self {
            switch batteryLevel {
            case 0...19:
                return Image.BatteryIndicator.level10
            case 20...29:
                return Image.BatteryIndicator.level20
            case 30...39:
                return Image.BatteryIndicator.level30
            case 40...49:
                return Image.BatteryIndicator.level40
            case 50...59:
                return Image.BatteryIndicator.level50
            case 60...69:
                return Image.BatteryIndicator.level60
            case 70...79:
                return Image.BatteryIndicator.level70
            case 80...89:
                return Image.BatteryIndicator.level80
            case 90...99:
                return Image.BatteryIndicator.level90
            case 100:
                return Image.BatteryIndicator.level100
            default:
                fatalError()
            }
        } else {
            return Image.BatteryIndicator.unknown
        }
    }
    var value: String {
        switch self {
        case .unknown:
            return String.invisible
        case .level(let batteryLevel):
            return String(batteryLevel) + Const.percentageCharacter
        }
    }
    var color: UIColor {
        switch self {
        case .unknown:
            return Colors.lightGreen
        case .level(let batteryLevel):
            if 0...19 ~= batteryLevel {
                return Colors.lightRed
            } else if 20...39 ~= batteryLevel {
                return Colors.yellow
            } else {
                return Colors.green
            }
        }
    }
}

private struct Const {
    static let valueLabelWidth: CGFloat = 40.0
    static let batteryImageViewWidth: CGFloat = 20.0
    static let percentageCharacter: String = "%"
}

class BatteryCollectionViewCell: UICollectionViewCell {
    static let id: String = String(describing: BatteryCollectionViewCell.self)
    var uiBatteryLevel: UIBatteryLevel = .unknown {
        didSet {
            guard oldValue != uiBatteryLevel else { return }
            valueLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: Headline.style(as: .extraSmallWhite, text: uiBatteryLevel.value).color(uiBatteryLevel.color),
                textAligment: .right,
                lineBreakMode: .byTruncatingTail
            ))
            DispatchQueue.main.asyncAfter(deadline: .now() + .t010) { [weak self] in
                guard let strongSelf = self else { return }
                UIView.transition(with: strongSelf.batteryImageView, duration: .t025, options: [.transitionCrossDissolve, .curveEaseInOut], animations: { [weak self] in
                    self?.batteryImageView.image = self?.uiBatteryLevel.image
                }, completion: nil)
            }
        }
    }
    private let headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .extraSmallLight, text: NSLocalizedString("appwide_battery_level", value: "BATTERY LEVEL", comment: "BATTERY LEVEL"))
        headerLabel.textAlignment = .left
        return headerLabel
    }()
    private let valueLabel: UILabel = {
        let valueLabel = UILabel()
        valueLabel.attributedText = Headline.style(as: .extraSmallWhite, text: UIBatteryLevel.unknown.value).color(UIBatteryLevel.unknown.color)
        valueLabel.textAlignment = .right
        return valueLabel
    }()
    private let batteryImageView: UIImageView = {
        let batteryImageView = UIImageView(image: Image.BatteryIndicator.unknown)
        batteryImageView.contentMode = .center
        return batteryImageView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        contentView.backgroundColor = Colors.black
        [headerLabel, valueLabel, batteryImageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
        NSLayoutConstraint.activate([
            headerLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            headerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.tileMargin),
            headerLabel.trailingAnchor.constraint(equalTo: valueLabel.leadingAnchor, constant: Dimension.tileMargin),
            valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: batteryImageView.leadingAnchor, constant: -Dimension.smallSpace),
            valueLabel.widthAnchor.constraint(equalToConstant: Const.valueLabelWidth),
            batteryImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            batteryImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.tileMargin),
            batteryImageView.widthAnchor.constraint(equalToConstant: Const.batteryImageViewWidth)
        ])
    }
}
