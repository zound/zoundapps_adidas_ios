//
//  ResizableTableView.swift
//  AppLibrary
//
//  Created by Grzegorz Kiel on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import UIKit

class ResizeableTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
