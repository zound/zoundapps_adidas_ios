//
//  NewsletterView.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 22/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct PrivacyPolicy{
    private static let part1 = NSLocalizedString("newsletter_privacy_policy_part1", value: "Your email address is safe with us:\nBy clicking the button bellow you agree to our ", comment: "Your email address is safe with us:\nBy clicking the button bellow you agree to our ")
    private static let part2 = NSLocalizedString("newsletter_privacy_policy_part2", value: ".", comment: ".")
    private static let link = NSLocalizedString("newsletter_privacy_policy_link", value: "PrivacyPolicy", comment: "PrivacyPolicy")
    public static let linkRarange = NSRange(location: PrivacyPolicy.part1.count,
                                            length: PrivacyPolicy.link.count)
    public static func getAttributedText(style: Paragraph)->NSMutableAttributedString{
        let text: String
        if( PrivacyPolicy.part2 == String.invisible){
            text = PrivacyPolicy.part1 + PrivacyPolicy.link
        } else {
            text = PrivacyPolicy.part1 + PrivacyPolicy.link + PrivacyPolicy.part2
        }
        let attributedText = NSMutableAttributedString(attributedString: Paragraph.style(as: style, text: text))
        attributedText.addAttribute(.link, value: String(), range: PrivacyPolicy.linkRarange)
        return attributedText
    }
}
final class NewsletterView: UIView {
    let newsLetterViewBorderWidth: CGFloat = 1.0
    let borderView = UIView()
    var newsletterViewModel: NewsletterViewModel?
    init() {
        super.init(frame: .zero)
        setupConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private lazy var spinnerView: UIImageView = {
        let waitingIndicatorImageView = UIImageView(image: Image.Indicators.spinnerDark)
        waitingIndicatorImageView.contentMode = .center
        waitingIndicatorImageView.isHidden = true
        return waitingIndicatorImageView
    }()
    private let subscriptionState = false
    private var subscriptionEmail: String? = nil
    private lazy var emailInputTextField: UITextField = {
        let textField = UITextField.init(frame: .zero)
        newsletterEmailTextFieldStyle(textField)
        textField.addTarget(self,
                            action: #selector(emailChanged(_:)),
                            for: [.editingDidEndOnExit, .editingChanged])
        textField.isHidden = subscriptionState == false
        return textField
    }()
    private lazy var privacyPolicyLabel: UITextView = {
        let label = UITextView.init(frame: .zero)
        let text = PrivacyPolicy.getAttributedText(style: .smallDark)
        privacyPolicyLabelStyle(text: text)(label)
        label.delegate = self
        label.isHidden = true
        return label
    }()
    private let errorLabel: UILabel = {
        let label = UILabel.init(frame: .zero)
        return label
    }()
    private let checkBox = RectangularSwitch()
    private lazy var subscriptionStateStackView: UIStackView = {
        checkboxStyle(checkBox)
        checkBox.thumbSize = Dimension.checkboxThumbSize
        checkBox.setupUI()
        checkBox.addTarget(self,
                           action: #selector(toggleNewsletter),
                           for: [.valueChanged])
        checkBox.isOn = subscriptionState
        let description = UILabel(frame: .zero)
        let descriptionText = NSLocalizedString("newsletter_accept", value: "Sign me up for the newsletter", comment: "Sign me up for the newsletter")
        newsLetterLabelStyle(text: descriptionText)(description)
        description.numberOfLines = 0
        description.setContentCompressionResistancePriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.horizontal)
        let stackView = UIStackView(arrangedSubviews: [checkBox, description])
        rootHorizontalStackViewStyle(stackView)
        return stackView
    }()
    func setBorder(status: NewsletterErrorStatus){
        switch status {
        case .noError:
            borderStyle(color: Colors.greyExtraLight, width: newsLetterViewBorderWidth)(borderView)
        case .emailInvalid,.somethingWentWrong:
            borderStyle(color: Colors.red, width: newsLetterViewBorderWidth)(borderView)
        }
    }
    func setupConstraints() {
        addSubview(borderView)
        borderView.backgroundColor = UIColor.clear
        borderView.translatesAutoresizingMaskIntoConstraints = false
        let stackView = UIStackView(arrangedSubviews: [subscriptionStateStackView, emailInputTextField,
                                                 spinnerView, errorLabel])
        verticalStackViewStyle(stackView)
        let allViews = UIStackView(arrangedSubviews: [  stackView, privacyPolicyLabel])
        rootVerticalStackViewStyle(allViews)
        allViews.spacing = 0
        addSubview(allViews)
        fullyContained(in: self)(allViews)
        NSLayoutConstraint.activate([
            spinnerView.widthAnchor.constraint(equalTo: stackView.widthAnchor,constant: stackView.spacing * -2),
            privacyPolicyLabel.widthAnchor.constraint(equalTo: stackView.widthAnchor),
            borderView.topAnchor.constraint(equalTo: self.topAnchor),
            borderView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor),
            borderView.leftAnchor.constraint(equalTo: self.leftAnchor),
            borderView.rightAnchor.constraint(equalTo: self.rightAnchor),
            ])
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let hitFound = [checkBox, emailInputTextField]
            .map({
                $0.hitTest($0.convert(point, from: self), with: event)
            })
            .filter({ $0 != nil })
            .first else {
                return super.hitTest(point, with: event)
        }
        return hitFound
    }
    @objc internal func emailChanged(_ textField: UITextField) {
        newsletterViewModel?.changed(email: textField.text)
    }
    @objc internal func toggleNewsletter() {
        newsletterViewModel?.toggleNewsletter(email: emailInputTextField.text)
    }
}
extension NewsletterView: NewsletterViewModelViewInputDelegate {
    func updateCheckBox(isEnabled: Bool){
        checkBox.isEnabled = isEnabled
        checkBox.alpha = isEnabled ? 1 : 0.5
    }
    func setup(subscribed: Bool, email: String?, error: NewsletterErrorStatus, indicator: Bool) {
        checkBox.isOn = subscribed
        emailInputTextField.text = email
        toggle(subscriptionStatus: subscribed, showKeyboard: false)
        newsletterViewModel?.changed(email: email)
        updateErrorStatus(status: error)
        updateIndicator(animating: indicator)
        setNeedsLayout()
    }
    private func updateErrorlabel(status: NewsletterErrorStatus){
        switch status {
        case .noError:
            errorLabel.isHidden = true
            errorLabelStyle(text: String.invisible)(errorLabel)
        case .emailInvalid:
            let text = NSLocalizedString("newsletter_email_invalid", value: "Please enter a correct email adress", comment: "Please enter a correct email adress")
            errorLabel.isHidden = false
            errorLabelStyle(text: text)(errorLabel)
        case .somethingWentWrong:
            let text = NSLocalizedString("newsletter_common_error", value: "Something went wrong", comment: "Something went wrong")
            errorLabel.isHidden = false
            errorLabelStyle(text: text)(errorLabel)
        }
    }
    func updateIndicator(animating: Bool) {
        if animating {
            if !spinnerView.containsAnimation(){
                spinnerView.rotate()
            }
        } else {
            if spinnerView.containsAnimation() {
                spinnerView.stopAnimating()
            }
        }
        spinnerView.isHidden = !animating
        checkBox.isEnabled = !animating
    }
    func updateErrorStatus(status: NewsletterErrorStatus) {
        updateErrorlabel(status: status)
        setBorder(status: status)
    }
    private func animateHidden(hidden: Bool, for view:UIView, completion: ((Bool) -> Void)? = nil){
        let fadeInAmination = {
            view.isHidden = hidden
        }
        let options: UIView.AnimationOptions = [.curveEaseInOut, .transitionCrossDissolve]
        UIView.transition(with: view,
                          duration: .t010,
                          options: options,
                          animations: fadeInAmination,
                          completion: completion)
    }
    func toggle(subscriptionStatus: Bool, showKeyboard: Bool) {
        let hidden = subscriptionStatus == false
        if emailInputTextField.isHidden != hidden {
            animateHidden(hidden: hidden, for: emailInputTextField)
        }
        if privacyPolicyLabel.isHidden != hidden {
            animateHidden(hidden: hidden, for: privacyPolicyLabel)
        }
        if checkBox.isOn != subscriptionStatus{
            checkBox.isOn = subscriptionStatus
        }
        if emailInputTextField.isHidden {
            emailInputTextField.endEditing(false)
        } else {
            if showKeyboard {
                emailInputTextField.becomeFirstResponder()
            }
        }
    }
}
extension NewsletterView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if(characterRange == PrivacyPolicy.linkRarange){
            newsletterViewModel?.didTapPrivacyPolicy()
        }
        return false
    }
}
