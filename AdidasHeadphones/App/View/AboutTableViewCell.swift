//
//  AboutTableViewCell.swift
//  AppLibrary
//
//  Created by Grzegorz Kiel on 16/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

enum AboutItemState {
    case loading
    case value(String)
}

struct AboutItem {
    let title: String
    let state: AboutItemState
}

final class AboutTableViewCell: UITableViewCell {
    static let id = String(describing: AboutTableViewCell.self)
    var aboutItem: AboutItem? {
        didSet {
            guard let _ = aboutItem else { return }
            titleLabel.attributedText = Headline.style(as: .smallDark, text: aboutItem!.title)
            if case .value(let itemValue) = aboutItem!.state {
                spinnerView.stopAnimating()
                spinnerView.isHidden = true
                valueLabel.attributedText = Paragraph.style(as: .regularLight, text: itemValue)
                valueLabel.isHidden = false
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    
    private let valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.isHidden = true
        return label
    }()
    
    private let spinnerView: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        return spinner
    }()
    
    private func setupConstraints() {
        [titleLabel, valueLabel, spinnerView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            valueLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin),
            spinnerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            spinnerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin)
        ])
    }
}
