//
//  SearchingView.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 03/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class SearchingView: UIView {
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("search_headphones_headline_uc", value: "SEARCHING FOR HEADPHONES...", comment: "Searching for headphones..."))
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = Int.zero
        return headerLabel
    }()
    lazy var linearIndicator: LinearIndicator = {
        let linearIndicator = LinearIndicator()
        return linearIndicator
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: NSLocalizedString("search_headphones_paragraph", value: "Please make sure that your Adidas headphones are compatible, powered on, and in range.", comment: "Please make sure that your Adidas headphones are compatible, powered on, and in range."))
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = Int.zero
        return descriptionLabel
    }()
    lazy var visitWebsiteButton: UIButton = {
        let visitWebsiteButton = UIButton()
        visitWebsiteButton.setAttributedTitle(Button.style(as: .regularBlackUnderlined, text: NSLocalizedString("search_headphones_link_uc", value: "VISIT ADIDASHEADPHONES.COM.", comment: "Visit adidasheadphones.com")), for: .normal)
        return visitWebsiteButton
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        [headerLabel, linearIndicator, descriptionLabel, visitWebsiteButton].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.screenHeight28),
            headerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            linearIndicator.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.headerBigBottomMargin),
            linearIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: linearIndicator.bottomAnchor, constant: Dimension.headerBigBottomMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            visitWebsiteButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            visitWebsiteButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            visitWebsiteButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            visitWebsiteButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset)
        ])
    }
    func setAll(hidden: Bool) {
        if hidden {
            [headerLabel, linearIndicator, descriptionLabel, visitWebsiteButton].forEach {
                $0?.isHidden = hidden
            }
        } else {
            [headerLabel, descriptionLabel, visitWebsiteButton].forEach {
                $0.isHidden = hidden
            }
            headerLabel.indicateAnimated { [weak self] _ in
                self?.linearIndicator.isHidden = hidden
            }
        }
    }
}
