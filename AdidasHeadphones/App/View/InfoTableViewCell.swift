//
//  InfoTableViewCell.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 30/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

struct InfoItem {
    let title: String
    let value: String
}

class InfoTableViewCell:UITableViewCell {
    static let id = String(describing: InfoTableViewCell.self)
    var infoItem: InfoItem? {
        didSet {
            if let infoItem = infoItem {
                let title = Headline.style(as: .smallDark, text: infoItem.title)
                titleLabel.attributedText = title
                let value = Paragraph.style(as: .regularLight, text: infoItem.value)
                valueLabel.attributedText = value
            }
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()

    private let valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()


    private func setupConstraints() {
        [titleLabel, valueLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Dimension.edgeMargin),
            valueLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Dimension.edgeMargin)])
    }

}
