//
//  FirmwareUpdateMessageView.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 31/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private extension FirmwareUpdateState {
    var color: UIColor {
        switch self {
        case .off, .started, .restarted, .uploading:
            return Colors.clear
        case .readyToInstall:
            return Colors.lightBlue
        case .installing:
            return Colors.lightGreen
        case .waitingToPowerOn:
            return Colors.lightBlue
        case .finalizing:
            return Colors.lightGreen
        case .completed:
            return Colors.lightGreen
        case .failed:
            return Colors.lightBlue
        }
    }
    var text: String {
        switch self {
        case .off:
            return String.invisible
        case .started, .restarted, .uploading:
            return String.invisible
        case .readyToInstall:
            return NSLocalizedString("firmware_update_status_ready_to_install", value: "Install new firmware", comment: "Install new firmware")
        case .installing:
            return NSLocalizedString("firmware_update_status_installing", value: "Instaling...", comment: "Instaling...")
        case .waitingToPowerOn:
            return NSLocalizedString("firmware_update_status_waiting_to_power_on", value: "Power on your headphones", comment: "Power on your device")
        case .finalizing:
            return NSLocalizedString("firmware_update_status_finalyzing", value: "Finalizing...", comment: "Finalizing...")
        case .completed:
            return NSLocalizedString("firmware_update_status_completed", value: "Installation completed", comment: "Installation completed")
        case .failed:
            return NSLocalizedString("firmware_update_status_failed", value: "Failed", comment: "Failed")
        }
    }
    var image: UIImage? {
        switch self {
        case .off:
            return nil
        case .started, .restarted, .uploading:
            return nil
        case .readyToInstall:
            return Image.Icon.Black.arrowRightSmall
        case .installing:
            return Image.Indicators.spinnerDarkSmall
        case .waitingToPowerOn:
            return nil
        case .finalizing:
            return Image.Indicators.spinnerDarkSmall
        case .completed:
            return Image.Icon.Black.check
        case .failed:
            return Image.Indicators.error
        }
    }
    var isImageSpinning: Bool {
        switch self {
        case .installing, .finalizing:
            return true
        default:
            return false
        }
    }
}

private struct Const {
    struct ShowHideAnimation {
        static let springDamping: CGFloat = 0.8
        static let springResponse: CGFloat = 0.3
    }
}

class FirmwareUpdateMessageView: UIView {
    var viewModel: FirmwareUpdateMessageViewModel?
    var bottomConstraint: NSLayoutConstraint?
    private lazy var messageLabel: UILabel = {
        let messageLabel = UILabel()
        return messageLabel
    }()
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        return imageView
    }()
    private lazy var firmwareUpdateActivityView: UIView = {
        let firmwareUpdateActivityView = UIView()
        firmwareUpdateActivityView.layer.cornerRadius = Dimension.firmwareUpdateActivityViewSize.half
        firmwareUpdateActivityView.backgroundColor = Colors.red
        firmwareUpdateActivityView.isHidden = true
        return firmwareUpdateActivityView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        addGestureRecognizer()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
        addGestureRecognizer()
    }
    private func setupConstraints() {
        [messageLabel, imageView, firmwareUpdateActivityView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            firmwareUpdateActivityView.widthAnchor.constraint(equalToConstant: Dimension.firmwareUpdateActivityViewSize),
            firmwareUpdateActivityView.heightAnchor.constraint(equalToConstant: Dimension.firmwareUpdateActivityViewSize),
            firmwareUpdateActivityView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin.half.half),
            firmwareUpdateActivityView.bottomAnchor.constraint(equalTo: topAnchor, constant: -Dimension.edgeMargin.half.half),

            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.edgeMargin + 4),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            messageLabel.trailingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: -Dimension.edgeMargin),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: Dimension.edgeMargin + 4),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 24),
            heightAnchor.constraint(equalToConstant: Dimension.firmwareUpdateMessaveViewHeight)
        ])
    }
    private func addGestureRecognizer() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(_:))))
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        viewModel?.touchedUpInside()
    }
}

extension FirmwareUpdateMessageView: FirmwareUpdateMessageViewModelViewInputDelegate {
    func updateMessage(with firmwareUpdateState: FirmwareUpdateState) {
        UIView.animate(withDuration: .t025, delay: .t010, options: .curveEaseInOut, animations: { [weak self] in
            self?.backgroundColor = firmwareUpdateState.color
        }, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) { [weak self] in
            if firmwareUpdateState.isImageSpinning {
                self?.imageView.rotate()
            } else {
                self?.imageView.layer.removeAllAnimations()
            }
            guard let imageView = self?.imageView else { return }
            UIView.transition(with: imageView, duration: .t025, options: [.curveEaseInOut, .transitionCrossDissolve], animations: { [weak self] in
                self?.imageView.image = firmwareUpdateState.image
            }, completion: nil)
        }
        messageLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
            attributedText: Paragraph.style(as: .smallDark, text: firmwareUpdateState.text),
            textAligment: .left,
            lineBreakMode: .byTruncatingTail
        ))
    }
    func showAndUpdateMessage(with firmwareUpdateState: FirmwareUpdateState) {
        backgroundColor = firmwareUpdateState.color
        imageView.image = firmwareUpdateState.image
        if firmwareUpdateState.isImageSpinning {
            imageView.rotate()
        } else {
            imageView.layer.removeAllAnimations()
        }
        messageLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
            attributedText: Paragraph.style(as: .smallDark, text: firmwareUpdateState.text),
            textAligment: .left,
            lineBreakMode: .byTruncatingTail
        ))
        let timingParameters = UISpringTimingParameters(damping: Const.ShowHideAnimation.springDamping, response: Const.ShowHideAnimation.springResponse)
        let propertyAnimator = UIViewPropertyAnimator(duration: .zero, timingParameters: timingParameters)
        propertyAnimator.addAnimations { [weak self] in
            self?.bottomConstraint?.constant = .zero
            self?.superview?.layoutIfNeeded()
        }
        propertyAnimator.startAnimation()
    }
    func hideMessage() {
        let timingParameters = UISpringTimingParameters(damping: Const.ShowHideAnimation.springDamping, response: Const.ShowHideAnimation.springResponse)
        let propertyAnimator = UIViewPropertyAnimator(duration: .zero, timingParameters: timingParameters)
        propertyAnimator.addAnimations { [weak self] in
            self?.bottomConstraint?.constant = Dimension.firmwareUpdateMessaveViewHeight
            self?.superview?.layoutIfNeeded()
        }
        propertyAnimator.addCompletion { [weak self] _ in
            self?.backgroundColor = Colors.clear
            self?.imageView.image = nil
            self?.messageLabel.attributedText = nil
        }
        propertyAnimator.startAnimation()
    }
    func showFirmwareUpdateActivityView() {
        if BundleConfiguration.firmwareReleaseType != .prod {
            firmwareUpdateActivityView.isHidden = false
        }
    }
    func hideFirmwareUpdateActivityView() {
        firmwareUpdateActivityView.isHidden = true
    }
}
