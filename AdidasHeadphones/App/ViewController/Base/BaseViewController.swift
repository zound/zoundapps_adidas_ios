//
//  BaseViewController.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.white
    }
    deinit {
        print(#function, String(describing: self))
    }
}

class BaseNavigationViewController: UINavigationController {
    var dismissableModalTransitioning = DismissableModalTransitioning()
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor()
    }
    private func setBackgroundColor() {
        view.backgroundColor = DefaultStorage.shared.welcomePresented ? Colors.white : Colors.black
    }
    deinit {
        print(#function, String(describing: self))
    }
}

class BaseCollectionViewController: UICollectionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.white
        collectionView.backgroundColor = Colors.white
    }
    deinit {
        print(#function, String(describing: self))
    }
}
