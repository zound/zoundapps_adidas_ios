//
//  MainViewController.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit

enum UIPeripheralConnectionState {
    case connected
    case connecting
    case disconnected
}

class MainViewController: BaseViewController {
    var viewModel: MainViewModel?
    private var tilesViewHeight: CGFloat {
        return Dimension.tileSmallHeight + Dimension.tileBigHeight.twice + Dimension.tileSpace.twice
    }
    private var scrollViewContentSize: CGSize {
        let height = 3 * Dimension.edgeMargin + Dimension.screenWidth + tilesViewHeight + Dimension.playerViewHeight
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = scrollViewContentSize
        scrollView.alpha = .zero
        return scrollView
    }()
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var progressBar: KDCircularProgress = {
        let progressView = KDCircularProgress()
        progressView.trackColor = Colors.greyExtraLight
        progressView.progressColors = [Colors.black, Colors.greyDark]
        progressView.glowMode = .noGlow
        progressView.isHidden = true
        progressView.startAngle = 270
        return progressView
    }()
    private lazy var progressLabel: UILabel = {
        return  UILabel()
    }()
    lazy var tilesView: TilesView = {
        let tilesView = TilesView()
        return tilesView
    }()
    lazy var disconnectedMessageView: DisconnectedMessageView = {
        let disconnectedMessageView = DisconnectedMessageView()
        return disconnectedMessageView
    }()
    lazy var playerView: PlayerView = {
        let playerView = PlayerView()
        return playerView
    }()
    lazy var firmwareUpdateMessageView: FirmwareUpdateMessageView = {
        let firmwareUpdateMessageView = FirmwareUpdateMessageView()
        return firmwareUpdateMessageView
    }()
    private func titleViewLabel(with text: String, with headline: Headline) -> UILabel {
        let titleViewLabel = UILabel()
        titleViewLabel.attributedText = Headline.style(as: headline, text: text)
        return titleViewLabel
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: NSAttributedString())
        navigationController?.navigationBar.topItem?.leftBarButtonItem =
            UIBarButtonItem(image: Image.Icon.Black.headphones, style: .done, target: self, action: #selector(didTapLeftBarButtonItem))
        navigationController?.navigationBar.topItem?.rightBarButtonItem =
            UIBarButtonItem(image: Image.Icon.Black.settings, style: .plain, target: self, action: #selector(didTapRightBarButtonItem))
        navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = Colors.black
        navigationController?.navigationBar.topItem?.rightBarButtonItem?.tintColor = Colors.black
        setupScrollView()
        setupImageView()
        setupProgress()
        setupDisconnectedMessageView()
        setupTilesView()
        setupPlayerView()
        setupFirmwareUpdateMessageView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.viewDidAppear()
    }
    @objc func didTapLeftBarButtonItem() {
        viewModel?.didTapLeftBarButtonItem()
    }
    @objc func didTapRightBarButtonItem() {
        viewModel?.didTapRightBarButtonItem()
    }
}

private extension MainViewController {
    func setupScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    func setupProgress() {
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(progressBar)
        scrollView.addSubview(progressLabel)
        NSLayoutConstraint.activate([
            progressBar.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: Dimension.headerTopMargin),
            progressBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.headerTopMargin),
            progressBar.heightAnchor.constraint(equalToConstant: Dimension.tileBigHeight),
            progressBar.widthAnchor.constraint(equalToConstant: Dimension.tileBigHeight),
            progressLabel.centerXAnchor.constraint(equalTo: progressBar.centerXAnchor),
            progressLabel.centerYAnchor.constraint(equalTo: progressBar.centerYAnchor),
        ])
    }

    func setupImageView() {
        imageView.image = viewModel?.deviceImage()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor)
        ])
    }
    func setupDisconnectedMessageView() {
        disconnectedMessageView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(disconnectedMessageView)
        NSLayoutConstraint.activate([
            disconnectedMessageView.topAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: Dimension.screenHeight58),
            disconnectedMessageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            disconnectedMessageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin)
        ])
    }
    func setupTilesView() {
        tilesView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(tilesView)
        NSLayoutConstraint.activate([
            tilesView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Dimension.edgeMargin),
            tilesView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            tilesView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            tilesView.heightAnchor.constraint(equalToConstant: tilesViewHeight)
        ])
    }
    func setupPlayerView() {
        playerView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(playerView)
        NSLayoutConstraint.activate([
            playerView.topAnchor.constraint(equalTo: tilesView.bottomAnchor, constant: Dimension.edgeMargin),
            playerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            playerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            playerView.heightAnchor.constraint(equalToConstant: Dimension.playerViewHeight)
        ])
    }

    func setupFirmwareUpdateMessageView() {
        firmwareUpdateMessageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(firmwareUpdateMessageView)
        firmwareUpdateMessageView.bottomConstraint = firmwareUpdateMessageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: Dimension.firmwareUpdateMessaveViewHeight)
        NSLayoutConstraint.activate([
            firmwareUpdateMessageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            firmwareUpdateMessageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            firmwareUpdateMessageView.bottomConstraint!
        ])
    }
}

private extension MainViewController {
    func hideControls() {
        tilesView.transform = CGAffineTransform(translationX: .zero, y: 100)
        playerView.transform = CGAffineTransform(translationX: .zero, y: 100)
        tilesView.alpha = .zero
        playerView.alpha = .zero
        navigationController?.navigationBar.topItem?.rightBarButtonItem?.isEnabled = false
    }
    func showControls() {
        tilesView.transform = .identity
        playerView.transform = .identity
        tilesView.alpha = 1
        playerView.alpha = 1
        navigationController?.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
    }
}

extension MainViewController: MainViewModelViewInputDelegate {
    func setProgress(hidden: Bool) {
        UIView.transition(with: imageView, duration: .t050, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.progressBar.isHidden = hidden
            self?.progressLabel.isHidden = hidden
            }, completion: nil)
    }
    func setProgress(label: String) {
        UIView.transition(with: imageView, duration: .t050, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.progressLabel.attributedText = Headline.style(as: Headline.mediumBlack, text: label)
            }, completion: nil)
    }
    func setProgress(value: Float) {
        progressBar.progress = Double(value)
    }
    func set(deviceImage: UIImage) {
        UIView.transition(with: imageView, duration: .t050, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.imageView.image = deviceImage
        }, completion: nil)
    }
    func update(deviceName: String, for uiPeripheralConnectionState: UIPeripheralConnectionState) {
        let headline: Headline = uiPeripheralConnectionState == .disconnected || uiPeripheralConnectionState == .connecting ?
            .smallLight : .smallDark
        navigationController?.navigationBar.topItem?.titleView = titleViewLabel(with: deviceName, with: headline)
    }
    func initialize(uiPeripheralConnectionState: UIPeripheralConnectionState) {
        UIView.animate(withDuration: .t025, delay: .t025, options: .curveEaseOut, animations: { [weak self] in
            self?.scrollView.alpha = 1
        }, completion: nil)
        if uiPeripheralConnectionState == .disconnected {
            if disconnectedMessageView.disconnectedMessageState != .shown {
                disconnectedMessageView.disconnectedMessageState = .waiting
            }
            hideControls()
            scrollView.isScrollEnabled = false
        }
    }
    func update(uiPeripheralConnectionState: UIPeripheralConnectionState) {
        if uiPeripheralConnectionState == .disconnected {
            let topDistance = (navigationController?.navigationBar.bounds.height ?? .zero) + Dimension.statusBarHeight
            scrollView.setContentOffset(CGPoint(x: .zero, y: -topDistance), animated: true)
            scrollView.isScrollEnabled = false
            UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseIn, animations: { [weak self] in
                self?.hideControls()
            }, completion: { [weak self] _ in
                self?.disconnectedMessageView.disconnectedMessageState = .shown
            })
        }
        if uiPeripheralConnectionState ==  .connected {
            scrollView.isScrollEnabled = true
            disconnectedMessageView.disconnectedMessageState = .hidden
            UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseOut, animations: { [weak self] in
                self?.showControls()
            }, completion: nil)
        }
    }
}

extension MainViewController: TilesViewModelSuperviewOutputDelegate {
    func didSelect(tile: Tile) {
        viewModel?.didSelect(tile: tile)
    }
}
extension MainViewController: PlayerViewModelSuperviewOutputDelegate {}
extension MainViewController: FirmwareUpdateMessageViewModelSuperviewOutputDelegate {
    func didTapShowUpdateScreen(){
        viewModel?.didTapShowUpdateScreen()
    }
}
