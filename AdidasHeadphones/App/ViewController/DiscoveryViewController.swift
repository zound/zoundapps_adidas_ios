//
//  DiscoveryViewController.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit

class DiscoveryViewController: BaseCollectionViewController {
    var viewModel: DiscoveryViewModel?
    var discoveryItems: [DiscoveryItem] = [] {
        didSet {
            handleBottomPannelVisibility()
        }
    }
    private var pageControlCurrentPage = Int.zero {
        didSet {
            guard pageControlCurrentPage != oldValue else { return }
            pageControl.currentPage = pageControlCurrentPage
            handleNameLabelTextUpdate()
        }
    }
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPage = Int.zero
        pageControl.numberOfPages = discoveryItems.count
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Headline.style(as: .extraSmallLight, text: NSLocalizedString("headphones_found_instruction_uc", value: "DRAG THE CIRCLE TO CONNECT", comment: "Drag the circle to connect"))
        descriptionLabel.textAlignment = .center
        return descriptionLabel
    }()
    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel()
        return nameLabel
    }()
    private lazy var circleSlider: CircleSlider = {
        let circleSlider = CircleSlider()
        circleSlider.delegate = self
        return circleSlider
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(DiscoveryCollectionViewCell.self, forCellWithReuseIdentifier: DiscoveryCollectionViewCell.id)
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        automaticallyAdjustsScrollViewInsets = false
        restartLinearIndicator()
        setupConstraints()
        handleBottomPannelVisibility()
        viewModel?.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.viewDidAppear()
    }
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        circleSlider.isUserInteractionEnabled = false
    }
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        circleSlider.isUserInteractionEnabled = true
        pageControlCurrentPage = Int(targetContentOffset.pointee.x / view.frame.width)
        
    }
    private func setupConstraints() {
        [pageControl, descriptionLabel, circleSlider, nameLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.safeAreaBottomInset),
            pageControl.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pageControl.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pageControl.heightAnchor.constraint(equalToConstant: Dimension.pageControlHeight),
            descriptionLabel.topAnchor.constraint(equalTo: circleSlider.bottomAnchor, constant: Dimension.circleSliderMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            circleSlider.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: Dimension.circleSliderMargin),
            circleSlider.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.screenHeight58),
            nameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            nameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
        ])
    }
    @objc func visitWebsiteButtonTouchedUpIndide() {
        UIApplication.shared.open(.adidasWebpage)
    }
}

extension DiscoveryViewController: CircleSliderDelegate {
    func didChange(circleSliderCurrentState: CircleSlider.CurrentState) {
        handleDescriptionLabelText(basedOn: circleSliderCurrentState)
        if circleSliderCurrentState == .dragged {
            viewModel?.circleSliderDragged(for: pageControl.currentPage)
        }
    }
}

private extension DiscoveryViewController {
    func handleDescriptionLabelText(basedOn circleSliderCurrentState: CircleSlider.CurrentState) {
        func animate(text: String, color: UIColor = Colors.greyMedium, completion: ((Bool) -> Void)? = nil) {
            descriptionLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: Headline.style(as: .extraSmallLight, text: text).color(color),
                textAligment: .center,
                lineBreakMode: .byTruncatingTail
            ), completion: completion)
        }
        switch circleSliderCurrentState {
        case .commingBack:
            collectionView.isUserInteractionEnabled = false
            animate(text: NSLocalizedString("headphones_found_unable_to_connect_uc", value: "UNABLE TO CONNECT", comment: "Unable to connect"), color: Colors.red) { [weak self] _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) { [weak self] in
                    self?.clearDiscoveryItemsInternally()
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
                        self?.viewModel?.discoveryItemsClearedAutomaically()
                    }
                }
            }
        case .dragged:
            circleSlider.isUserInteractionEnabled = false
            collectionView.isUserInteractionEnabled = false
            animate(text: NSLocalizedString("headphones_found_connecting_uc", value: "CONNECTING", comment: "Connecting"))
        default:
            break
        }
    }
    func handleBottomPannelVisibility() {
        [nameLabel, circleSlider, descriptionLabel].forEach { $0.isHidden = discoveryItems.isEmpty }
    }
    func updateNameLabel(with text: String) {
        nameLabel.attributedText = Headline.style(as: .largeBlack, text: text)
        nameLabel.textAlignment = .center
        nameLabel.lineBreakMode = .byTruncatingTail
    }
    func handleNameLabelTextUpdate() {
        let name = discoveryItems[pageControl.currentPage].name
        let fadeOutAmination: () -> Void = { [weak self] in
            self?.updateNameLabel(with: String.invisible)
        }
        let fadeInAmination = { [weak self] in
            self?.nameLabel.transform = .identity
            self?.updateNameLabel(with: name)
        }
        let options: UIView.AnimationOptions = [.curveEaseInOut, .transitionCrossDissolve]
        UIView.transition(with: nameLabel, duration: .t010, options: options, animations: fadeOutAmination) { _ in
            self.nameLabel.transform = .headerScale
            UIView.transition(with: self.nameLabel, duration: .t025, options: options, animations: fadeInAmination)
        }
    }
    func clearDiscoveryItemsInternally() {
        discoveryItems.removeAll()
        collectionView.reloadData()
        (collectionView.backgroundView as? SearchingView)?.setAll(hidden: false)
        pageControl.numberOfPages = Int.zero
        descriptionLabel.attributedText = Headline.style(as: .extraSmallLight, text: NSLocalizedString("headphones_found_instruction_uc", value: "DRAG THE CIRCLE TO CONNECT", comment: "Drag the circle to connect"))
        descriptionLabel.textAlignment = .center
        circleSlider.isUserInteractionEnabled = true
        collectionView.isUserInteractionEnabled = true
    }
}

extension DiscoveryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat()
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return discoveryItems.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DiscoveryCollectionViewCell.id, for: indexPath) as! DiscoveryCollectionViewCell
        let discoveryItem = discoveryItems[indexPath.item]
        cell.discoveryItem = discoveryItem
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
}

extension DiscoveryViewController: DiscoveryViewModelViewInputDelegate {
    func clearDiscoveryItems() {
        clearDiscoveryItemsInternally()
    }
    func showUnableToConnect() {
        circleSlider.stopSpinner()
    }
    func add(discoveryItem: DiscoveryItem, at index: Int) {
        guard discoveryItems.count == index else {
            print(discoveryItems.count, index)
            fatalError()
        }
        discoveryItems.append(discoveryItem)
        collectionView.reloadData()
        if index == Int.zero {
            (collectionView.backgroundView as? SearchingView)?.setAll(hidden: true)
            handleNameLabelTextUpdate()
        }
        pageControl.numberOfPages = discoveryItems.count
    }
    func restartLinearIndicator() {
        if discoveryItems.isEmpty {
            collectionView.backgroundView = SearchingView()
            (collectionView.backgroundView as? SearchingView)?
                .visitWebsiteButton.addTarget(self, action: #selector(visitWebsiteButtonTouchedUpIndide), for: .touchUpInside)
        }
    }
    func indicateSearchingViewHeader() {
        (collectionView.backgroundView as? SearchingView)?.setAll(hidden: false)
    }
}
