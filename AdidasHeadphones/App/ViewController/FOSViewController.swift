//
//  FOSViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 02/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit

class FOSViewController: BaseViewController, HtmlStringProviding {
    var viewModel: FOSViewModel?
    private var webView: WKWebView?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("free_and_open_source_software_title_uc", value: "FREE AND OPEN SOURCE SOFTWARE", comment: "FREE AND OPEN SOURCE SOFTWARE")))
        setupWebView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadHtml()
    }
    func setupWebView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.frame, configuration: webViewConfiguration)
        view.addSubview(webView!)
        webView!.scrollView.delegate = self
        webView!.isOpaque = false
        webView!.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        webView!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView!.topAnchor.constraint(equalTo: view.topAnchor, constant: (navigationController!.navigationBar.frame.height + Dimension.statusBarHeight)),
            webView!.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView!.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView!.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            ])
    }
    func loadHtml() {
        webView?.loadHTMLString(htmlString(for: .freeAndOpenSourceSoftware), baseURL: URL(fileURLWithPath: Bundle.main.bundlePath))
    }
}
extension FOSViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
extension FOSViewController: FOSViewModelViewInputDelegate {
}

