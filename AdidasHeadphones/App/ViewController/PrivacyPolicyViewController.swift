//
//  PrivacyPolicyViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 06/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController:  BaseViewController, HtmlStringProviding {
    var viewModel: PrivacyPolicyViewModel?
    private var webView: WKWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("privacy_policy_title_uc", value: "PRIVACY POLICY", comment: "PRIVACY POLICY")))
        setupWebView()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadHtml()
    }
    func setupWebView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.frame, configuration: webViewConfiguration)
        view.addSubview(webView!)
        webView!.scrollView.delegate = self
        webView!.isOpaque = false
        webView!.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        webView!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView!.topAnchor.constraint(equalTo: view.topAnchor, constant: (navigationController!.navigationBar.frame.height + Dimension.statusBarHeight)),
            webView!.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView!.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView!.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            ])
    }
    func loadHtml() {
        webView?.loadHTMLString(htmlString(for: .privacyPolicy), baseURL: URL(fileURLWithPath: Bundle.main.bundlePath))
    }
}

extension PrivacyPolicyViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

extension PrivacyPolicyViewController: PrivacyPolicyViewModelViewInputDelegate {
}
