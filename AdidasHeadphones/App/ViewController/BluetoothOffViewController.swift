//
//  BluetoothOffViewController.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 28/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class BluetoothOffViewController: BaseViewController {
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("bluetooth_off_headline_uc", value: "BLUETOOTH IS OFF", comment: "Bluetooth is off"))
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = Int.zero
        return headerLabel
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: NSLocalizedString("bluetooth_off_paragraph", value: "Please go to Settings on this device, turn on Bluetooth and try again.", comment: "Please go to Settings on this device, turn on Bluetooth and try again."))
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = Int.zero
        return descriptionLabel
    }()
    lazy var goToSettingsButton: UIButton = {
        let goToSettingsButton = UIButton()
        goToSettingsButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("bluetooth_off_button_title_uc", value: "GO TO SETTINGS", comment: "Go to Settings")), for: .normal)
        goToSettingsButton.backgroundColor = Colors.black
        goToSettingsButton.addTarget(self, action:#selector(goToSettingsButtonTouchedUpIndide), for: .touchUpInside)
        return goToSettingsButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
    }
    @objc private func goToSettingsButtonTouchedUpIndide() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
    private func setupConstraints() {
        view.backgroundColor = Colors.white
        [headerLabel, descriptionLabel, goToSettingsButton].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.screenHeight28),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.headerBigBottomMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            goToSettingsButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            goToSettingsButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            goToSettingsButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            goToSettingsButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset)
        ])
    }
}
