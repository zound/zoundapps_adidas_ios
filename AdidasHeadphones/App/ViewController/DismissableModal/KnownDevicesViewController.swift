//
//  KnownDevicesViewController.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 22/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

struct UIKnownDevice {
    let name: String
    let uuid: String
    let image: UIImage
}

private enum Sections: Int {
    case currentDevice = 0
    case allDevices = 1
}

private struct Const {
    static let tableViewCellHeight: CGFloat = 84
    static let maxTableViewItems: Int = 4
    static let missingSelectionTriggerTimeout: TimeInterval = 0.2
    static let removeTitle = "x"
    struct TableViewHeightAnimation {
        static let springDamping: CGFloat = 0.8
        static let springResponse: CGFloat = 0.3
    }
}

class KnownDevicesViewController: BaseViewController, DismissableModalCompliance {
    var viewModel: KnownDevicesViewModel?
    var dismissable: Bool = true
    var modalHeight: CGFloat {
        return 20 + Dimension.buttonHeight + Dimension.edgeMargin * 4 + tableViewHeight + Dimension.safeAreaBottomInset
    }
    private var missingSelectionTmier: Timer?
    private var heightConstraint: NSLayoutConstraint?
    private var knownDevices: [(uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)] = []
    private var currentDevice: [(uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)] = []
    private var tableViewHeight: CGFloat {
        let totalCount = knownDevices.count + currentDevice.count
        return totalCount <= Const.maxTableViewItems ?
            Const.tableViewCellHeight * CGFloat(totalCount) :
            Const.tableViewCellHeight * CGFloat(Const.maxTableViewItems + 1)
    }
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .smallDark, text: NSLocalizedString("home_modal_headphones_headline_uc", value: "HEADPHONES", comment: "HEADPHONES"))
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = .zero
        tableView.rowHeight = Const.tableViewCellHeight
        tableView.tableFooterView = UIView()
        tableView.register(KnownDeviceTableViewCell.self, forCellReuseIdentifier: KnownDeviceTableViewCell.id)
        tableView.backgroundColor = UIColor.white
        return tableView
    }()
    lazy var addNewHeadphonesButton: UIButton = {
        let addNewHeadphonesButton = UIButton()
        addNewHeadphonesButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("home_modal_add_new_headphones_uc", value: "ADD NEW HEADPHONES", comment: "Add new headphones")), for: .normal)
        addNewHeadphonesButton.backgroundColor = Colors.black
        addNewHeadphonesButton.addTarget(self, action:#selector(addNewHeadphonesButtonTouchedUpIndide), for: .touchUpInside)
        return addNewHeadphonesButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        heightConstraint = tableView.heightAnchor.constraint(equalToConstant: tableViewHeight)
        NSLayoutConstraint.activate([heightConstraint!])
        tableView.isScrollEnabled = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (view.gestureRecognizers?.first)?.delegate = self // NOTE: Interacting with pan gesture recognizer
        viewModel?.viewDidAppear()
    }
    @objc private func addNewHeadphonesButtonTouchedUpIndide() {
        viewModel?.didTapAddNewHeadphones()
    }
    private func setupConstraints() {
        [headerLabel, tableView, addNewHeadphonesButton].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            tableView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.edgeMargin),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            addNewHeadphonesButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: Dimension.edgeMargin),
            addNewHeadphonesButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            addNewHeadphonesButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            addNewHeadphonesButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin)
        ])
    }
    private func animateTableViewHeightConstraint() {
        let timingParameters = UISpringTimingParameters(damping: Const.TableViewHeightAnimation.springDamping,
                                                        response: Const.TableViewHeightAnimation.springResponse)
        let  animator = UIViewPropertyAnimator(duration: .zero, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            self?.heightConstraint?.constant = self?.tableViewHeight ?? .zero
            self?.view.layoutIfNeeded()
        }
        animator.startAnimation()
    }
    private func setSelection(enabled: Bool) {
        tableView.allowsSelection = enabled
        addNewHeadphonesButton.isEnabled = enabled
        dismissable = enabled
    }
    private func didSelectRow(at indexPath: IndexPath, from tableView: UITableView, considerPanGesturerecognizer: Bool) {
        if considerPanGesturerecognizer,
            let panGestureRecognizer = view.gestureRecognizers?.first as? UIPanGestureRecognizer,
            panGestureRecognizer.state != .possible {
            return
        }
        if let cell = tableView.cellForRow(at: indexPath) as? KnownDeviceTableViewCell {
            if cell.state == .normalBluetoothClassicConnected || cell.state == .normalBluetoothClassicNotConnected {
                cell.state = .connecting
                setSelection(enabled: false)
                viewModel?.didSelect(uiKnownDevice: knownDevices[indexPath.row].uiKnownDevice)
            }
        }
    }
}

extension KnownDevicesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.currentDevice.rawValue:
            return currentDevice.count
        case Sections.allDevices.rawValue:
            return knownDevices.count
        default:
            return 0
        }

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: KnownDeviceTableViewCell.id, for: indexPath) as? KnownDeviceTableViewCell {
            if indexPath.section == Sections.currentDevice.rawValue {
                cell.uiKnownDevice = currentDevice[indexPath.row].uiKnownDevice
                cell.state = currentDevice[indexPath.row].state
                cell.showCheckMark = true
            } else {
                cell.uiKnownDevice = knownDevices[indexPath.row].uiKnownDevice
                cell.state = knownDevices[indexPath.row].state
            }

            cell.backgroundColor = Colors.white
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == Sections.currentDevice.rawValue {
            viewModel?.didSelectCurrentDevice()
        } else{
             didSelectRow(at: indexPath, from: tableView, considerPanGesturerecognizer: false)
        }
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == Sections.currentDevice.rawValue {
            return indexPath
        } else {
            missingSelectionTmier?.invalidate()
            return indexPath
        }
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        missingSelectionTmier = Timer.scheduledTimer(withTimeInterval: Const.missingSelectionTriggerTimeout, repeats: false) { [weak self] _ in
            if indexPath.section == Sections.currentDevice.rawValue {

            } else{
                self?.didSelectRow(at: indexPath, from: tableView, considerPanGesturerecognizer: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let remove = UITableViewRowAction(style: .normal, title: Const.removeTitle) { [weak self] (_, indexPath) in
            if let presentationController = self?.presentationController as? DismissableModalPresentationController,
                !presentationController.isDismissing {
                if let knownDevice = self?.knownDevices.remove(at: indexPath.row) {
                    tableView.beginUpdates()
                    tableView.deleteRows(at: [indexPath], with: .left)
                    tableView.endUpdates()
                    self?.viewModel?.didRemove(uiKnownDevice: knownDevice.uiKnownDevice)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250)) { [weak self] in
                        presentationController.updateOffset()
                        presentationController.setFrameOfPresentedViewInContainerViewAnimated()
                        self?.animateTableViewHeightConstraint()
                    }
                }
            }
        }
        remove.backgroundColor = Colors.red
        return [remove]
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if Sections.init(rawValue: indexPath.section) == Sections.currentDevice {
            return false
        } else {
            return dismissable
        }
    }
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        missingSelectionTmier?.invalidate()
    }
}

extension KnownDevicesViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension KnownDevicesViewController: KnownDevicesViewModelViewInputDelegate {
    func add(knownDevices: [(uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)]) {
        self.knownDevices = knownDevices
        tableView.reloadData()
    }
    func setCurent(knownDevice: (uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)) {
        self.currentDevice = [knownDevice]
        tableView.reloadData()
    }
    func markAsUnableToConnect(uiKnownDevice: UIKnownDevice) {
        if let index = knownDevices.firstIndex (where: { $0.uiKnownDevice.uuid == uiKnownDevice.uuid }),
           let cell = tableView.cellForRow(at: IndexPath(item: index, section: Sections.allDevices.rawValue)) as? KnownDeviceTableViewCell {
            cell.state = .unableToConnect
            viewModel?.didMarkAsUnableToConnect(uiKnownDevice: uiKnownDevice)
            setSelection(enabled: true)
        }
    }
    func update(knownDevice: (uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)) {
        if let index = knownDevices.firstIndex (where: { $0.uiKnownDevice.uuid == knownDevice.uiKnownDevice.uuid }),
           let cell = tableView.cellForRow(at: IndexPath(item: index, section: Sections.allDevices.rawValue)) as? KnownDeviceTableViewCell {
            knownDevices[index] = knownDevice
            cell.state = knownDevice.state
        }
    }
    func updateConditionally(knownDevice: (uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)) {
        if let index = knownDevices.firstIndex (where: { $0.uiKnownDevice.uuid == knownDevice.uiKnownDevice.uuid }),
            let cell = tableView.cellForRow(at: IndexPath(item: index, section: Sections.allDevices.rawValue)) as? KnownDeviceTableViewCell,
            cell.state == .normalBluetoothClassicConnected || cell.state == .normalBluetoothClassicNotConnected {
            knownDevices[index] = knownDevice
            cell.state = knownDevice.state
        }
    }
    func triggerSelect(for uiKnownDevice: UIKnownDevice) {
        if let index = knownDevices.firstIndex (where: { $0.uiKnownDevice.uuid == uiKnownDevice.uuid }) {
            didSelectRow(at: IndexPath(row: index, section: Sections.allDevices.rawValue), from: tableView, considerPanGesturerecognizer: false)
        }
    }
}
