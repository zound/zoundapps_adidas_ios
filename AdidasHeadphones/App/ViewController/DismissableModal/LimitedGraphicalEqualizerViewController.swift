//
//  LimitedGraphicalEqualizerViewController.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 27/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class LimitedGraphicalEqualizerViewController: BaseViewController, DismissableModalCompliance {
    var viewModel: LimitedGraphicalEqualizerViewModel?
    var dismissable: Bool = true
    var modalHeight: CGFloat {
        return 20 + Dimension.edgeMargin.twice.twice + Dimension.pickerViewHeight + Dimension.buttonHeight + Dimension.safeAreaBottomInset
    }
    private let presets: [UIGraphicalEqualizerPreset] = UIGraphicalEqualizerPreset.all
    private var customUIGraphicalEqualizer: UIGraphicalEqualizer = UIGraphicalEqualizer.unknown
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .smallDark, text: NSLocalizedString("settings_equalizer_uc", value: "EQUALIZER", comment: "EQUALIZER"))
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private lazy var presetPickerView: UIPickerView = {
        let presetPickerView = UIPickerView()
        presetPickerView.delegate = self
        presetPickerView.dataSource = self
        return presetPickerView
    }()
    lazy var editButton: UIButton = {
        let editButton = UIButton()
        editButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("appwide_edit_uc", value: "EDIT", comment: "EDIT")), for: .normal)
        editButton.backgroundColor = Colors.black
        editButton.addTarget(self, action:#selector(editButtonTouchedUpIndide), for: .touchUpInside)
        return editButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    private func setupConstraints() {
        [headerLabel, presetPickerView, editButton].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            presetPickerView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.edgeMargin),
            presetPickerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            presetPickerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            presetPickerView.heightAnchor.constraint(equalToConstant: Dimension.pickerViewHeight),
            editButton.topAnchor.constraint(equalTo: presetPickerView.bottomAnchor, constant: Dimension.edgeMargin),
            editButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            editButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            editButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin)
        ])
    }
    @objc private func editButtonTouchedUpIndide() {
        viewModel?.didTapEdit()
    }
}

extension LimitedGraphicalEqualizerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presets.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        pickerView.subviews[1].backgroundColor = Colors.greyExtraLight
        pickerView.subviews[2].backgroundColor = Colors.greyExtraLight
        let pickerLabel = UILabel()
        pickerLabel.attributedText = Headline.style(as: .extraLargeBlack, text: presets[row].text.capitalized)
        pickerLabel.textAlignment = .center
        return pickerLabel
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return Dimension.pickerRowHeight
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if case .custom = presets[row] {
            viewModel?.didSelect(uiGraphicalEqualizerPreset: .custom(customUIGraphicalEqualizer))
        } else {
            viewModel?.didSelect(uiGraphicalEqualizerPreset: presets[row])
        }
    }
}

private extension LimitedGraphicalEqualizerViewController {
    func updatePresetPicker(to preset: UIGraphicalEqualizerPreset, animated: Bool) {
        if let index = presets.firstIndex(of: preset) {
            presetPickerView.selectRow(index, inComponent: .zero, animated: animated)
        }
    }
}

extension LimitedGraphicalEqualizerViewController: LimitedGraphicalEqualizerViewModelViewInputDelegate {
    func initiate(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset, uiGraphicalEqualizerCustom: UIGraphicalEqualizer) {
        customUIGraphicalEqualizer = uiGraphicalEqualizerCustom
        updatePresetPicker(to: uiGraphicalEqualizerPreset, animated: false)
    }
    func update(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset) {
        updatePresetPicker(to: uiGraphicalEqualizerPreset, animated: true)
    }
}
