//
//  LimitedActionButtonStyleViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 04/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let tableViewCellHeight: CGFloat = 100.0
}
final class LimitedActionButtonStyleViewController: BaseViewController, DismissableModalCompliance {
    var viewModel: ActionButtonStyleViewModel?
    var dismissable: Bool = true
    var modalHeight: CGFloat {
        return 20 + Dimension.edgeMargin.twice.twice + tableViewHeight + Dimension.buttonHeight + Dimension.safeAreaBottomInset
    }
    private var tableViewHeight: CGFloat {
        guard let viewModel = viewModel else { return .zero }
        guard let currentlyUnfolded = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else { return .zero }
        return CGFloat(currentlyUnfolded.buttonActions.count) * Config.tableViewCellHeight
    }
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .smallDark, text: NSLocalizedString("settings_action_button_uc", value: "ACTION BUTTON", comment: "ACTION BUTTON"))
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(ActionButtonTableViewCell.self, forCellReuseIdentifier: String(describing: ActionButtonTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Config.tableViewCellHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    private lazy var entriesContainerView: UIView = {
        let view = UIView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        return view
    }()
    lazy var editButton: UIButton = {
        let editButton = UIButton()
        editButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("appwide_edit_uc", value: "EDIT", comment: "EDIT")), for: .normal)
        editButton.backgroundColor = Colors.black
        editButton.addTarget(self, action:#selector(editButtonTouchedUpIndide), for: .touchUpInside)
        return editButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    private func setupConstraints() {
        [headerLabel, entriesContainerView, editButton].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            entriesContainerView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.edgeMargin),
            entriesContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            entriesContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            entriesContainerView.bottomAnchor.constraint(equalTo: editButton.topAnchor, constant: -Dimension.edgeMargin),
            entriesContainerView.heightAnchor.constraint(equalToConstant: tableViewHeight),
            editButton.topAnchor.constraint(equalTo: entriesContainerView.bottomAnchor, constant: Dimension.edgeMargin),
            editButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            editButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            editButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin)
        ])
    }
    @objc private func editButtonTouchedUpIndide() {
        viewModel?.didTapEdit()
    }
}
extension LimitedActionButtonStyleViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        guard let currentlyUnfolded = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else {
            return 0
        }
        return currentlyUnfolded.buttonActions.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        guard let currentlyUnfolded = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else {
            return tableView.dequeueReusableCell(withIdentifier: String(describing: UITableViewCell.self), for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier:String(describing: ActionButtonTableViewCell.self), for: indexPath) as! ActionButtonTableViewCell
        cell.simplified = currentlyUnfolded.configurable == false
        if let uuid = viewModel.currentDeviceUUID() {
            let extendedInfo = ExtendedButtonActionInfo(uuid: uuid, info: currentlyUnfolded.buttonActions[indexPath.row])
            cell.extendedActionItem = extendedInfo
        } else {
            cell.buttonActionItem = currentlyUnfolded.buttonActions[indexPath.row]
        }
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.selectionStyle = .none
        cell.backgroundColor = Colors.white
        return cell
    }
}
extension LimitedActionButtonStyleViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        guard let unfoldedGroup = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else {
            return
        }
        let selectedButton = unfoldedGroup.buttonActions[indexPath.row]
        viewModel.didSelect(styleType: unfoldedGroup.type, pressType: selectedButton.type, value: selectedButton.info.action)
    }
}
