//
//  GraphicalEqualizerViewController.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 06/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class GraphicalEqualizerViewController: BaseViewController {
    var viewModel: GraphicalEqualizerViewModel?
    private let presets: [UIGraphicalEqualizerPreset] = UIGraphicalEqualizerPreset.all
    var customUIGraphicalEqualizer: UIGraphicalEqualizer = UIGraphicalEqualizer.unknown
    var currentUIGraphicalEqualizerPreset: UIGraphicalEqualizerPreset? {
        didSet {
            if let oldValueCurrentUIGraphicalEqualizerPreset = oldValue,
               let currentUIGraphicalEqualizerPreset = currentUIGraphicalEqualizerPreset,
               currentUIGraphicalEqualizerPreset.uiGraphicalEqualizer != oldValueCurrentUIGraphicalEqualizerPreset.uiGraphicalEqualizer {
                if case .custom = currentUIGraphicalEqualizerPreset {
                    customUIGraphicalEqualizer = currentUIGraphicalEqualizerPreset.uiGraphicalEqualizer
                }
                UISelectionFeedbackGenerator().selectionChanged()
                viewModel?.didUpdate(uiGraphicalEqualizer: currentUIGraphicalEqualizerPreset.uiGraphicalEqualizer)
            }
            updateSliders(to: currentUIGraphicalEqualizerPreset!, duration: .zero)
            updateValues(to: currentUIGraphicalEqualizerPreset!, animated: false)
        }
    }
    private lazy var bassFreqLabel: UILabel = {
        let bassFreqLabel = UILabel()
        bassFreqLabel.attributedText = Headline.style(as: .extraSmallDark, text: NSLocalizedString("sound_profile_bass", value: "160 HZ", comment: "160 HZ"))
        bassFreqLabel.textAlignment = .center
        return bassFreqLabel
    }()
    private lazy var lowFreqLabel: UILabel = {
        let lowFreqLabel = UILabel()
        lowFreqLabel.attributedText = Headline.style(as: .extraSmallDark, text: NSLocalizedString("sound_profile_low", value: "400 HZ", comment: "400 HZ"))
        lowFreqLabel.textAlignment = .center
        return lowFreqLabel
    }()
    private lazy var midFreqLabel: UILabel = {
        let midFreqLabel = UILabel()
        midFreqLabel.attributedText = Headline.style(as: .extraSmallDark, text: NSLocalizedString("sound_profile_mid", value: "1 KHZ", comment: "1 KHZ"))
        midFreqLabel.textAlignment = .center
        return midFreqLabel
    }()
    private lazy var upperFreqLabel: UILabel = {
        let upperFreqLabel = UILabel()
        upperFreqLabel.attributedText = Headline.style(as: .extraSmallDark, text: NSLocalizedString("sound_profile_upper", value: "2.5 KHZ", comment: "2.5 KHZ"))
        upperFreqLabel.textAlignment = .center
        return upperFreqLabel
    }()
    private lazy var highFreqLabel: UILabel = {
        let highFreqLabel = UILabel()
        highFreqLabel.attributedText = Headline.style(as: .extraSmallDark, text: NSLocalizedString("sound_profile_treble", value: "6.25 KHZ", comment: "6.25 KHZ"))
        highFreqLabel.textAlignment = .center
        return highFreqLabel
    }()
    private lazy var frequenciesStackView: UIStackView = {
        let frequenciesStackView = UIStackView(arrangedSubviews: [bassFreqLabel, lowFreqLabel, midFreqLabel, upperFreqLabel, highFreqLabel])
        frequenciesStackView.distribution = .fillEqually
        frequenciesStackView.spacing = Dimension.smallSpace
        return frequenciesStackView
    }()
    private lazy var bassBarSlider: BarSlider = {
        let bassBarSliderLabel = BarSlider()
        return bassBarSliderLabel
    }()
    private lazy var lowBarSlider: BarSlider = {
        let lowBarSliderLabel = BarSlider()
        return lowBarSliderLabel
    }()
    private lazy var midBarSlider: BarSlider = {
        let midBarSliderLabel = BarSlider()
        return midBarSliderLabel
    }()
    private lazy var upperBarSlider: BarSlider = {
        let upperBarSliderLabel = BarSlider()
        return upperBarSliderLabel
    }()
    private lazy var highBarSlider: BarSlider = {
        let highBarSliderLabel = BarSlider()
        return highBarSliderLabel
    }()
    private lazy var barSlidersStackView: UIStackView = {
        let barSlidersStackView = UIStackView(arrangedSubviews: barSliders)
        barSlidersStackView.distribution = .fillEqually
        barSlidersStackView.spacing = Dimension.smallSpace
        return barSlidersStackView
    }()
    private lazy var bassValueLabel: UILabel = {
        let bassValueLabel = UILabel()
        bassValueLabel.attributedText = Headline.style(as: .extraSmallDark, text: GraphicalEqualizerUIValue.n0.rawValue)
        bassValueLabel.textAlignment = .center
        return bassValueLabel
    }()
    private lazy var lowValueLabel: UILabel = {
        let lowValueLabel = UILabel()
        lowValueLabel.attributedText = Headline.style(as: .extraSmallDark, text: GraphicalEqualizerUIValue.n0.rawValue)
        lowValueLabel.textAlignment = .center
        return lowValueLabel
    }()
    private lazy var midValueLabel: UILabel = {
        let midValueLabel = UILabel()
        midValueLabel.attributedText = Headline.style(as: .extraSmallDark, text: GraphicalEqualizerUIValue.n0.rawValue)
        midValueLabel.textAlignment = .center
        return midValueLabel
    }()
    private lazy var upperValueLabel: UILabel = {
        let upperValueLabel = UILabel()
        upperValueLabel.attributedText = Headline.style(as: .extraSmallDark, text: GraphicalEqualizerUIValue.n0.rawValue)
        upperValueLabel.textAlignment = .center
        return upperValueLabel
    }()
    private lazy var highValueLabel: UILabel = {
        let highValueLabel = UILabel()
        highValueLabel.attributedText = Headline.style(as: .extraSmallDark, text: GraphicalEqualizerUIValue.n0.rawValue)
        highValueLabel.textAlignment = .center
        return highValueLabel
    }()
    private lazy var valuesStackView: UIStackView = {
        let valuesStackView = UIStackView(arrangedSubviews: [bassValueLabel, lowValueLabel, midValueLabel, upperValueLabel, highValueLabel])
        valuesStackView.distribution = .fillEqually
        valuesStackView.spacing = Dimension.smallSpace
        return valuesStackView
    }()
    private lazy var presetPickerView: UIPickerView = {
        let presetPickerView = UIPickerView()
        presetPickerView.delegate = self
        presetPickerView.dataSource = self
        return presetPickerView
    }()
    private var barSliders: [BarSlider] {
        return [bassBarSlider, lowBarSlider, midBarSlider, upperBarSlider, highBarSlider]
    }
    private var valueLabelsWithCorrespondingBands: [(UILabel, UIGraphicalEqualizer.Band)] {
        return [(bassValueLabel, .bass), (lowValueLabel, .low), (midValueLabel, .mid), (upperValueLabel, .upper), (highValueLabel, .high)]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("settings_equalizer_uc", value: "EQUALIZER", comment: "EQUALIZER")))
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.viewWillDisappear()
    }
    private func setupConstraints() {
        [frequenciesStackView, barSlidersStackView, valuesStackView, presetPickerView].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            frequenciesStackView.topAnchor.constraint(equalTo: view.topAnchor, constant:  Dimension.screenHeight18),
            frequenciesStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin.twice),
            frequenciesStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin.twice),
            barSlidersStackView.topAnchor.constraint(equalTo: frequenciesStackView.bottomAnchor, constant:  Dimension.edgeMargin),
            barSlidersStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin.twice),
            barSlidersStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin.twice),
            valuesStackView.topAnchor.constraint(equalTo: barSlidersStackView.bottomAnchor, constant:  Dimension.edgeMargin),
            valuesStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin.twice),
            valuesStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin.twice),
            presetPickerView.topAnchor.constraint(equalTo: valuesStackView.bottomAnchor, constant:  Dimension.edgeMargin),
            presetPickerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin.twice),
            presetPickerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin.twice),
            presetPickerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin.twice - Dimension.safeAreaBottomInset),
            presetPickerView.heightAnchor.constraint(equalToConstant: Dimension.pickerViewHeight),
            bassValueLabel.heightAnchor.constraint(equalToConstant: 18),
            lowValueLabel.heightAnchor.constraint(equalToConstant: 18),
            midValueLabel.heightAnchor.constraint(equalToConstant: 18),
            upperValueLabel.heightAnchor.constraint(equalToConstant: 18),
            highValueLabel.heightAnchor.constraint(equalToConstant: 18),
        ])
    }
}

extension GraphicalEqualizerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presets.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        pickerView.subviews[1].backgroundColor = Colors.greyExtraLight
        pickerView.subviews[2].backgroundColor = Colors.greyExtraLight
        let pickerLabel = UILabel()
        pickerLabel.attributedText = Headline.style(as: .extraLargeBlack, text: presets[row].text.capitalized)
        pickerLabel.textAlignment = .center
        return pickerLabel
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return Dimension.pickerRowHeight
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if case .custom = presets[row] {
            viewModel?.didSelect(uiGraphicalEqualizerPreset: .custom(customUIGraphicalEqualizer))
        } else {
            viewModel?.didSelect(uiGraphicalEqualizerPreset: presets[row])
        }
    }
}

private extension GraphicalEqualizerViewController {
    func configureBarSliders(maxValue: UInt8) {
        barSliders.forEach {
            $0.maximumValue = Float(maxValue)
            $0.value = .zero
            $0.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        }
    }
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        let uiGraphicalEqualizer = UIGraphicalEqualizer(bass: UInt8(bassBarSlider.value.rounded(.up)),
                                                      low: UInt8(lowBarSlider.value.rounded(.up)),
                                                      mid: UInt8(midBarSlider.value.rounded(.up)),
                                                      upper: UInt8(upperBarSlider.value.rounded(.up)),
                                                      high: UInt8(highBarSlider.value.rounded(.up)))
        currentUIGraphicalEqualizerPreset = UIGraphicalEqualizerPreset(uiGraphicalEqualizer)
        if event.allTouches?.first?.phase == .began {
            presetPickerView.isUserInteractionEnabled = false
        }
        if event.allTouches?.first?.phase == .ended || event.allTouches?.first?.phase == .cancelled {
            presetPickerView.isUserInteractionEnabled = true
            updatePresetPicker(to: currentUIGraphicalEqualizerPreset!, animated: true)
        }
    }
    func updateInterface(to preset: UIGraphicalEqualizerPreset, animated: Bool) {
        updateSliders(to: preset, duration: .t025)
        updatePresetPicker(to: preset, animated: animated)
        updateValues(to: preset, animated: animated)
    }
    func updateSliders(to preset: UIGraphicalEqualizerPreset, duration: TimeInterval, completion: (() -> Void)? = nil) {
        setUserInteractionForSliders(enabled: false)
        let animations = { [weak self] in
            self?.bassBarSlider.value = Float(preset.uiGraphicalEqualizer.bass)
            self?.lowBarSlider.value = Float(preset.uiGraphicalEqualizer.low)
            self?.midBarSlider.value = Float(preset.uiGraphicalEqualizer.mid)
            self?.upperBarSlider.value = Float(preset.uiGraphicalEqualizer.upper)
            self?.highBarSlider.value = Float(preset.uiGraphicalEqualizer.high)
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.setUserInteractionForSliders(enabled: true)
            completion?()
        }
        UIView.animate(withDuration: duration, delay: .zero, options: .curveEaseOut, animations: animations, completion: completionBlock)
    }
    func setUserInteractionForSliders(enabled: Bool) {
        barSliders.forEach {
            $0.isUserInteractionEnabled = enabled
        }
    }
    func updatePresetPicker(to preset: UIGraphicalEqualizerPreset, animated: Bool) {
        if let index = presets.firstIndex(of: preset) {
            presetPickerView.selectRow(index, inComponent: .zero, animated: animated)
        }
    }
    func updateValues(to preset: UIGraphicalEqualizerPreset, animated: Bool) {
        valueLabelsWithCorrespondingBands.forEach {
            guard let value = GraphicalEqualizerUIValue(uiGraphicalEgualizerBand: preset.uiGraphicalEqualizer.value(for: $0.1))?.rawValue else {
                return
            }
            if $0.0.attributedText?.string != value {
                if animated {
                    $0.0.animateTextTransition(with: UILabel.TextTransitionParameters(
                        attributedText: Headline.style(as: .extraSmallDark, text: value),
                        textAligment: .center,
                        lineBreakMode: .byTruncatingHead
                    ))
                } else  {
                    $0.0.attributedText = Headline.style(as: .extraSmallDark, text: value)
                    $0.0.textAlignment = .center
                    $0.0.lineBreakMode = .byTruncatingHead
                }
            }
        }
    }
}

extension GraphicalEqualizerViewController: GraphicalEqualizerViewModelViewInputDelegate {
    func initiate(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset, uiGraphicalEqualizerCustom: UIGraphicalEqualizer, uiGraphicalEqualizerMax: UInt8) {
        currentUIGraphicalEqualizerPreset = uiGraphicalEqualizerPreset
        customUIGraphicalEqualizer = uiGraphicalEqualizerCustom
        configureBarSliders(maxValue: uiGraphicalEqualizerMax)
        updateInterface(to: uiGraphicalEqualizerPreset, animated: false)
    }
    func update(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset) {
        updateInterface(to: uiGraphicalEqualizerPreset, animated: true)
    }
}

enum GraphicalEqualizerUIValue: String {
    case p1 = "+1", p2 = "+2", p3 = "+3", p4 = "+4", p5 = "+5"
    case n0 = " 0"
    case m1 = "-1", m2 = "-2", m3 = "-3", m4 = "-4", m5 = "-5"
    init?(uiGraphicalEgualizerBand: UInt8) {
        switch uiGraphicalEgualizerBand {
        case 10: self = .p5
        case 9: self = .p4
        case 8: self = .p3
        case 7: self = .p2
        case 6: self = .p1
        case 5: self = .n0
        case 4: self = .m1
        case 3: self = .m2
        case 2: self = .m3
        case 1: self = .m4
        case 0: self = .m5
        default:
            return nil
        }
    }
}

