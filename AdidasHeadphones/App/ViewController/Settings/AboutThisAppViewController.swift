//
//  AboutThisAppViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 29/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

enum AboutThisAppSections: Int, CaseIterable {
    case general
    case version
}

class AboutThisAppViewController: BaseViewController {

    private var versionLabel :String {
        get {
            return NSLocalizedString("about_this_app_option_3_uc", value: "APP VERSION", comment: "APP VERSION")
        }
    }

    var viewModel: AboutThisAppViewModelViewOutputDelegate?

    private var version = String.invisible

    private let entrys: [AboutThisAppEntry] = [.termsAndConditions, .freeAndOpenSource]

    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        for section in AboutThisAppSections.allCases {
            tableView.register(section.cellType, forCellReuseIdentifier: section.reusableId)
        }
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = Dimension.tableCellHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("about_this_app_title_uc", value: "ABOUT THIS APP", comment: "ABOUT THIS APP")))

        view.addSubview(tableView)
        setupConstraints()
        viewModel?.viewDidLoad()
        tableView.reloadData()
    }

    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}

extension AboutThisAppViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return AboutThisAppSections.allCases.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let current = AboutThisAppSections.init(rawValue: section) else{
            return 0
        }
        switch current {
        case .general:
            return entrys.count
        case .version:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let current = AboutThisAppSections.init(rawValue: indexPath.section) else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: current.reusableId, for: indexPath)

        switch current {
        case .general:
            update(cell, at: indexPath.row)
        case .version:
            update(cell, with: version)
        }
        return cell
    }

    private func update(_ cell: UITableViewCell, at  index: Int) {
        let entry = entrys[index]
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.textLabel?.attributedText = Headline.style(as: .smallDark, text: entry.title)
        cell.selectionStyle = .none
        cell.accessoryType =  .disclosureIndicator
        cell.backgroundColor = Colors.white
    }

    private func update(_ cell: UITableViewCell, with version:String) {
        if let cell = cell as? InfoTableViewCell {
            cell.infoItem = InfoItem(title: versionLabel, value: version)
            cell.selectionStyle = .none
            cell.backgroundColor = Colors.white
        }
    }
}

extension AboutThisAppViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == AboutThisAppSections.general.rawValue{
            viewModel?.selected(entry: entrys[indexPath.row])
        }
    }
}


extension AboutThisAppViewController: AboutThisAppViewModelViewInputDelegate {
    func update(_ version: String){
        self.version = version
        tableView.reloadData()
    }
}

extension AboutThisAppEntry {
    var title: String {
        switch self {
        case .termsAndConditions:
            return NSLocalizedString("about_this_app_option_1_uc", value: "TERMS & CONDITIONS", comment: "TERMS & CONDITIONS")
        case .freeAndOpenSource:
            return NSLocalizedString("about_this_app_option_2_uc", value: "FREE & OPEN SOURCE SOFTWARE", comment: "FREE & OPEN SOURCE SOFTWARE")
        }
    }
}

extension AboutThisAppSections {
    var reusableId: String {
        switch self {
        case .general:
            return "generalCell"
        case .version:
            return "versionCell"
        }
    }

    var cellType: AnyClass {
        switch self {
        case .general:
            return UITableViewCell.self
        case .version:
            return InfoTableViewCell.self
        }
    }
}

