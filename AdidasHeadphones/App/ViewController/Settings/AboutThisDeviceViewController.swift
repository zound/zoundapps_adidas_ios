//
//  AboutThisDeviceViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 17/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService
import RxSwift

private struct Config {
    static let rowHeight: CGFloat = 59.0
    static let topMargin: CGFloat = 100.0
}

enum AboutFeature: CaseIterable {
    case name
    case serial
    case model
    case firmware
    
    func title() -> String {
        switch self {
        case .name: return NSLocalizedString("about_this_device_name", value: "Device name", comment: "Device name")
        case .serial: return NSLocalizedString("about_this_device_serial", value: "Serial number", comment: "Serial number")
        case .model: return NSLocalizedString("about_this_device_model", value: "Model", comment: "Model")
        case .firmware: return NSLocalizedString("about_this_device_firmware", value: "Firmware version", comment: "Model")
        }
    }
}

final class AboutThisDeviceViewController: BaseViewController {
    
    var viewModel: AboutThisDeviceViewModel?
    
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(AboutTableViewCell.self, forCellReuseIdentifier: AboutTableViewCell.id)
        tableView.dataSource = self
        tableView.backgroundColor = Colors.white
        
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        
        tableView.rowHeight = Config.rowHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    private var content: [AboutItem] = AboutFeature.allCases.map { item -> AboutItem in AboutItem(title: item.title(), state: AboutItemState.loading) }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("settings_about_this_device_uc", value: "ABOUT THIS DEVICE", comment: "About this device")))
        
        view.addSubview(tableView)
        setupConstraints()
        tableView.reloadData()
        viewModel?.viewDidLoad()
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: Config.topMargin),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension AboutThisDeviceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AboutFeature.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AboutTableViewCell.id, for: indexPath) as! AboutTableViewCell
        
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        cell.aboutItem = content[indexPath.row]
        cell.selectionStyle = .none
        cell.backgroundColor = Colors.white
        return cell
    }
}

extension AboutThisDeviceViewController: AboutThisDeviceViewModelViewInputDelegate {
    func update(withDeviceData data: [AboutItem]) {
        content = data
        tableView.reloadData()
    }
}
