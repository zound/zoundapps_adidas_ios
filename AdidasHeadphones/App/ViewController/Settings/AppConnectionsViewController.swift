//
//  AppConnectionsViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 31/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let rowHeight: CGFloat = 67.0
    static let topMargin: CGFloat = 41.0
    static let bottomMargin: CGFloat = 31.0
    static let titleDescriptionVerticalSegment: CGFloat = 10.0
}

final class AppConnectionsViewController: BaseViewController {
    
    public var viewModel: AppConnectionsViewModel?
    
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(ConnectedAppTableViewCell.self, forCellReuseIdentifier: ConnectedAppTableViewCell.id)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Config.rowHeight
        
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = Config.rowHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    func mainSectionHeader() -> UIView {
        let headerView = UIView()
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("app_connections_headline", value: "CONNECT YOUR APPS", comment: "Connect your apps"))
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        
        let descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: NSLocalizedString("app_connections_paragraph", value: "A text explaining that you can enable more features by connecting your apps lorem ipsum dolor sit amet, consectetur adisplicing elit.", comment: "Enable more features by connecting apps"))
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        headerView.backgroundColor = Colors.white
        
        headerView.addSubview(titleLabel)
        headerView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: Dimension.edgeMargin),
            titleLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -Dimension.edgeMargin),
            titleLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: Config.topMargin),
            titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Config.titleDescriptionVerticalSegment),
            descriptionLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -Config.bottomMargin),
            ])
        return headerView
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("settings_app_connections_uc", value: "APP CONNECTIONS", comment: "app connections")))
        setupConstraints()
        guard let viewModel = viewModel else { return }
        NoInternetBehavior(parentViewController: self,
                           trigger: viewModel.reachabilityMonitor(),
                           toggleableItems: [tableView])
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.updateAppStates()
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}

extension AppConnectionsViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.connectedAppsInfo.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ConnectedAppTableViewCell.id, for: indexPath) as! ConnectedAppTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        let applicationInfo = viewModel.connectedAppsInfo[indexPath.row]
        cell.applicationInfo = applicationInfo
        cell.selectionStyle = .none
        cell.backgroundColor = Colors.white
        return cell
    }
}

extension AppConnectionsViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return mainSectionHeader()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        
        let applicationInfo = viewModel.connectedAppsInfo[indexPath.row]
        viewModel.didSelectApp(id: applicationInfo.id)
    }
}

extension AppConnectionsViewController: AppConnectionsViewModelViewInputDelegate {
    func refresh() {
        tableView.reloadData()
    }
}
