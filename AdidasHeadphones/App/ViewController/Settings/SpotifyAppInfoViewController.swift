//
//  SpotifyAppInfoViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 04/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let topMargin: CGFloat = 82.0
    static let logoTitleVerticalSegment: CGFloat = 34.0
    static let titleDescriptionVerticalSegment: CGFloat = 10.0
    static let titleConnectedVerticalSegment: CGFloat = 25.0
    static let descriptionFeatureVerticalSegment: CGFloat = 35.0
    static let featureSeparatorSegment: CGFloat = 12.0
    static let logoDimension: CGFloat = 110.0
    static let connectedHeight: CGFloat = 59.0
    static let iconDimension: CGFloat = 22.0
}

final class SpotifyAppInfoViewController: BaseViewController, SpotifyAppInfoViewModelViewInputDelegate {

    public var viewModel: SpotifyAppInfoViewModel?

    static let spotifyDisconnectStaticNote = NSLocalizedString("app_connections_spotify_disconnect_static", value: "If you wish to disconnect your Spotify account from the adidas headphones app, please visit ", comment: "If you wish to disconnect your Spotify account from the adidas headphones app, please visit ")
    static let spotifyDisconnectLink = NSLocalizedString("app_connections_spotify_disconnect_link", value: "spotify.com/account", comment: "spotify.com/account")
    static let spotifyDisconnectLinkRange = NSRange(location: spotifyDisconnectStaticNote.count, length: spotifyDisconnectLink.count)
    private var unregisterLinkRange = NSRange()
    private let logoView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.image = Image.Icon.Service.spotifyLarge
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        let title = NSLocalizedString("connect_spotify_headline", value: "Connect to Spotify", comment: "Connect to Spotify title")
        label.attributedText = Headline.style(as: .largeBlack, text: title)
        label.textAlignment = .center
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        let description = NSLocalizedString("connect_spotify_paragraph", value: "Add your Spotify account and enable even more features for your headphones", comment: "Add your Spotify account and enable even more features for your headphones")
        label.attributedText = Paragraph.style(as: .regularLight, text: description)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private let featuresHeadlineLabel: UILabel = {
        let label = UILabel()
        let featuresHeadline = NSLocalizedString("connect_spotify_features_headline", value: "With Spotify You’ll be able to:", comment: "With Spotify You’ll be able to:")
        label.attributedText = Headline.style(as: .smallDark, text: featuresHeadline)
        label.textAlignment = .center
        return label
    }()
    
    private let feature1Label: UILabel = {
        let label = UILabel()
        let feature1 = NSLocalizedString("connect_spotify_features_feature_1", value: "· Control Spotify with your Action Button", comment: "Control Spotify with your Action Button")
        label.attributedText = Paragraph.style(as: .smallDark, text: feature1)
        label.textAlignment = .center
        return label
    }()
    
    private let feature2Label: UILabel = {
        let label = UILabel()
        let feature1 = NSLocalizedString("connect_spotify_features_feature_2", value: "· Listen to exclusive Adidas playlists", comment: "Listen to exclusive Adidas playlists")
        label.attributedText = Paragraph.style(as: .smallDark, text: feature1)
        label.textAlignment = .center
        return label
    }()
    
    private let feature3Label: UILabel = {
        let label = UILabel()
        let feature1 = NSLocalizedString("connect_spotify_features_feature_3", value: "· Lorem ipsum dolor sit amet", comment: "Lorem ipsum dolor sit amet")
        label.attributedText = Paragraph.style(as: .smallDark, text: feature1)
        label.textAlignment = .center
        return label
    }()
    
    private let userNameValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    private let buttonIndicatorImageView: UIImageView = {
        let indicatorImageView = UIImageView(image: Image.Indicators.spinnerWhite)
        indicatorImageView.contentMode = .center
        indicatorImageView.isHidden = true
        return indicatorImageView
    }()
    private let indicatorImageView: UIImageView = {
        let indicatorImageView = UIImageView()
        indicatorImageView.contentMode = .center
        indicatorImageView.image = Image.Indicators.spinnerDark
        return indicatorImageView
    }()
    
    private lazy var footDisconnectNote: UITextView = {
        let textView = UITextView()
        unregisterLinkRange = NSRange(location: type(of: self).spotifyDisconnectStaticNote.count, length: type(of: self).spotifyDisconnectLink.count)
        let text = type(of: self).spotifyDisconnectStaticNote + type(of: self).spotifyDisconnectLink
        let disconnectInfoAttributedString = NSMutableAttributedString(attributedString: Paragraph.style(as: .smallLight, text: text))
        disconnectInfoAttributedString.addAttribute(.link, value: String(), range: unregisterLinkRange)
        textView.attributedText = disconnectInfoAttributedString
        textView.textAlignment = .center
        textView.linkTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue]
        textView.backgroundColor = Colors.clear
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    private let connectedView: UIView = {
        let connected = UIView(frame: .zero)
        let usernameLabel = UILabel()
        usernameLabel.textAlignment = .left
        let usernameLabelString = NSLocalizedString("app_connections_spotify_username_label", value: "Username", comment: "Username")
        usernameLabel.attributedText = Headline.style(as: .smallDark, text: usernameLabelString)
        
        let topSeparatorView = UIView()
        topSeparatorView.backgroundColor = Colors.greyExtraLight
        
        let bottomSeparatorView = UIView()
        bottomSeparatorView.backgroundColor = Colors.greyExtraLight
        
        [topSeparatorView, usernameLabel, bottomSeparatorView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            connected.addSubview($0)
        }
        
        NSLayoutConstraint.activate([
            topSeparatorView.leadingAnchor.constraint(equalTo: connected.leadingAnchor),
            topSeparatorView.trailingAnchor.constraint(equalTo: connected.trailingAnchor),
            topSeparatorView.topAnchor.constraint(equalTo: connected.topAnchor),
            topSeparatorView.heightAnchor.constraint(equalToConstant: 1/UIScreen.main.scale),
            usernameLabel.leadingAnchor.constraint(equalTo: connected.leadingAnchor, constant: Dimension.edgeMargin),
            usernameLabel.trailingAnchor.constraint(equalTo: connected.trailingAnchor, constant: -Dimension.edgeMargin),
            usernameLabel.centerYAnchor.constraint(equalTo: connected.centerYAnchor),
            bottomSeparatorView.leadingAnchor.constraint(equalTo: connected.leadingAnchor),
            bottomSeparatorView.trailingAnchor.constraint(equalTo: connected.trailingAnchor),
            bottomSeparatorView.bottomAnchor.constraint(equalTo: connected.bottomAnchor, constant: -1),
            bottomSeparatorView.heightAnchor.constraint(equalToConstant: 1/UIScreen.main.scale)
            ])
        return connected
    }()
    
    var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = Colors.black
        button.addTarget(self, action:#selector(buttonTouchedUpInside), for: .touchUpInside)
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("appwide_spotify_uc", value: "SPOTIFY", comment: "Spotify")))
        let buttonTitle = NSLocalizedString("connect_spotify_connect_button_title_uc", value: "CONNECT TO SPOTIFY", comment: "Connect to Spotify")
        button.setAttributedTitle(Button.style(as: .regularWhite, text: buttonTitle), for: .normal)
        setupConstraints()
        footDisconnectNote.delegate = self
        buttonIndicatorImageView.rotate()
        guard let viewModel = viewModel else { return }
        NoInternetBehavior(parentViewController: self,
                           trigger: viewModel.reachabilityMonitor(),
                           toggleableItems: [button])

    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.viewWillAppear()
    }
    
    public func set(state: SpotifyAppState, for userName: String?) {
        
        let configuredViews = [connectedView, userNameValueLabel, indicatorImageView, footDisconnectNote]
        let notConfiguredViews = [descriptionLabel, featuresHeadlineLabel, feature1Label, feature2Label, feature3Label]

        configuredViews.forEach { $0.isHidden = state != .connected }
        notConfiguredViews.forEach { $0.isHidden = state == .connected }

        let title = state == .connected ?
            NSLocalizedString("app_connections_spotify_title_connected", value: "Connected to Spotify", comment: "Connected to Spotify") :
            NSLocalizedString("app_connections_spotify_title_connect", value: "Connect to Spotify", comment: "Connect to Spotify")
        titleLabel.attributedText = Headline.style(as: .largeBlack, text: title)
        titleLabel.textAlignment = .center

        button.isHidden = state == .connected ? true: false
        footDisconnectNote.isHidden = state == .connected ? false: true
        
        
        guard case .connected = state else {
            if case .connecting = state {
                button.setAttributedTitle(nil, for: .normal)
                buttonIndicatorImageView.isHidden = false
            } else {
                let buttonTitle = NSLocalizedString("connect_spotify_connect_button_title_uc", value: "CONNECT TO SPOTIFY", comment: "Connect to Spotify")
                button.setAttributedTitle(Button.style(as: .regularWhite, text: buttonTitle), for: .normal)
                buttonIndicatorImageView.isHidden = true
            }
            return
        }
        
        indicatorImageView.layer.removeAllAnimations()
        
        guard let name = userName else {
            userNameValueLabel.isHidden = true
            indicatorImageView.isHidden = false
            indicatorImageView.rotate()
            return
        }
        indicatorImageView.isHidden = true
        userNameValueLabel.isHidden = false
        userNameValueLabel.attributedText = Paragraph.style(as: .regularLight, text: name)
    }
    
    private func setupConstraints() {
        [logoView, titleLabel, descriptionLabel, connectedView, userNameValueLabel, indicatorImageView, featuresHeadlineLabel, feature1Label, feature2Label, feature3Label, button, buttonIndicatorImageView, footDisconnectNote].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            logoView.topAnchor.constraint(equalTo: view.topAnchor, constant: Config.topMargin),
            logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoView.widthAnchor.constraint(equalToConstant: Config.logoDimension),
            logoView.heightAnchor.constraint(equalToConstant: Config.logoDimension),
            titleLabel.topAnchor.constraint(equalTo: logoView.bottomAnchor, constant: Config.logoTitleVerticalSegment),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Config.titleDescriptionVerticalSegment),
            descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            connectedView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Config.titleConnectedVerticalSegment),
            connectedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            connectedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            connectedView.heightAnchor.constraint(equalToConstant: Config.connectedHeight),
            userNameValueLabel.centerYAnchor.constraint(equalTo: connectedView.centerYAnchor),
            userNameValueLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            indicatorImageView.centerYAnchor.constraint(equalTo: connectedView.centerYAnchor),
            indicatorImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            indicatorImageView.heightAnchor.constraint(equalToConstant: Config.iconDimension),
            indicatorImageView.widthAnchor.constraint(equalToConstant: Config.iconDimension),
            featuresHeadlineLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Config.descriptionFeatureVerticalSegment),
            featuresHeadlineLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            featuresHeadlineLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            featuresHeadlineLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            feature1Label.topAnchor.constraint(equalTo: featuresHeadlineLabel.bottomAnchor, constant: Config.featureSeparatorSegment),
            feature1Label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            feature1Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            feature1Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            feature2Label.topAnchor.constraint(equalTo: feature1Label.bottomAnchor, constant: Config.featureSeparatorSegment),
            feature2Label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            feature2Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            feature2Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            feature3Label.topAnchor.constraint(equalTo: feature2Label.bottomAnchor, constant: Config.featureSeparatorSegment),
            feature3Label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            feature3Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            feature3Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            button.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            buttonIndicatorImageView.centerXAnchor.constraint(equalTo: button.centerXAnchor),
            buttonIndicatorImageView.centerYAnchor.constraint(equalTo: button.centerYAnchor),
            footDisconnectNote.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            footDisconnectNote.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            footDisconnectNote.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            footDisconnectNote.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin)
            ])
    }
    
    @objc private func buttonTouchedUpInside(_ sender: UIButton) {
        viewModel?.connectToSpotify()
    }
    public func present(authorizationViewController: UIViewController) {
        self.definesPresentationContext = true
        present(authorizationViewController, animated: true, completion: nil)
    }
    public func dismissAuthorization() {
        guard let _ = presentedViewController else { return }
        dismiss(animated: true) { [weak self] in
            self?.viewModel?.authorizationWebViewDismissed()
        }
    }
}
extension SpotifyAppInfoViewController: UITextViewDelegate {
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard characterRange == unregisterLinkRange else {
            return false
        }
        viewModel?.didRequestDeauthorizeAppWebPage()
        return false
    }
}
