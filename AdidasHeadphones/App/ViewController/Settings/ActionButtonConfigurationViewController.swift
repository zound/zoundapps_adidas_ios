//
//  ActionButtonConfigurationViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 02/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService
import RxSwift

private struct Const {
    static let rowHeight: CGFloat = 59.0
    static let applicationActionHeaderHeight: CGFloat = 1
    static let staticActionHeaderHeight: CGFloat = 33
}
extension GroupType {
    func headerHeight() -> CGFloat {
        switch self {
        case .applicationActions:
            return Const.applicationActionHeaderHeight
        case .staticActions:
            return Const.staticActionHeaderHeight
        }
    }
}
typealias ButtonConfigurationInfo = (pressType: PressType, entry: ActionButtonEntry)

final class ActionButtonConfigurationViewController: BaseViewController {
    var viewModel: ActionButtonConfigurationViewModel?
    private var disposeBag = DisposeBag()
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(ActionButtonConfigurationTableViewCell.self, forCellReuseIdentifier: ActionButtonConfigurationTableViewCell.id)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = Const.rowHeight
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = Const.rowHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    func separatorHeaderView(type: GroupType?) -> UIView {
        let containerView = UIView(frame: .zero)
        guard let type = type else {
            return containerView
        }
        let separatorView = UIView(frame: .zero)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        let bottomView = UIView(frame: .zero)
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.backgroundColor = Colors.greyExtraLight
        [separatorView, bottomView].forEach {
            containerView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            separatorView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: type.headerHeight()),
            bottomView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            bottomView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            bottomView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 2),
            bottomView.heightAnchor.constraint(equalToConstant: 1),
        ])
        return containerView
    }
}
extension ActionButtonConfigurationViewController: UITableViewDataSource {
    override public func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = viewModel else { return }
        setupNavigationBar(title: Headline.style(as: .smallDark, text: viewModel.pressType.label()))
        setupConstraints()
        viewModel.viewDidLoad()
        viewModel.modelUpdated
            .subscribe(
                onNext: { [weak self] in
                    self?.tableView.reloadData()
                }
            )
            .disposed(by: disposeBag)
    }
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.model.count
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.model[section].folded == true ? 1: viewModel.model[section].entries.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: ActionButtonConfigurationTableViewCell.id, for: indexPath) as! ActionButtonConfigurationTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.backgroundColor = Colors.white
        guard let type = GroupType(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        switch type {
        case .applicationActions:
            let entryInfo = viewModel.model[indexPath.row]
            cell.collapsedEntry = entryInfo
        case .staticActions:
            let configurationInfo = ButtonConfigurationInfo(pressType: viewModel.pressType, entry: viewModel.model[indexPath.section].entries[indexPath.row])
            cell.configurationInfo = configurationInfo
        }
        cell.selectionStyle = .none
        return cell
    }
}
extension ActionButtonConfigurationViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return separatorHeaderView(type: GroupType(rawValue: section))
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        guard let type = GroupType(rawValue: indexPath.section) else { return }
        switch type {
        case .applicationActions:
            guard let applicationID = viewModel.model[indexPath.section].applicationID else {
                return
            }
            viewModel.didSelect(application: applicationID)
        case .staticActions:
            let selectedAction = viewModel.model[indexPath.section].entries[indexPath.row].action
            viewModel.didSelect(action: selectedAction)
        }
    }
}
