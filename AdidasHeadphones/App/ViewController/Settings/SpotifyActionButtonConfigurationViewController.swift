//
//  SpotifyActionButtonConfigurationViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 05/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService
import RxSwift

private struct Const {
    static let rowHeight: CGFloat = 59.0
    static let topMargin: CGFloat = Dimension.screenHeight18
}

final class SpotifyActionButtonConfigurationViewController: BaseViewController {
    var viewModel: SpotifyActionButtonConfigurationViewModel?
    private var disposeBag = DisposeBag()
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(SpotifyActionButtonConfigurationTableViewCell.self, forCellReuseIdentifier: SpotifyActionButtonConfigurationTableViewCell.id)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.estimatedRowHeight = Const.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        return tableView
    }()
}
extension SpotifyActionButtonConfigurationViewController: UITableViewDataSource {
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("appwide_spotify_uc", value: "SPOTIFY", comment: "SPOTIFY")))
        viewModel?.reloadTrigger.subscribe(onNext: { [weak self] in
            self?.tableView.reloadData()
        })
        .disposed(by: disposeBag)
        setupConstraints()
        guard let viewModel = viewModel else { return }
        NoInternetBehavior(parentViewController: self,
                           trigger: viewModel.reachabilityMonitor(),
                           toggleableItems: [tableView])
        
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.viewWillAppear()
    }
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: Const.topMargin),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.model.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: SpotifyActionButtonConfigurationTableViewCell.id, for: indexPath) as! SpotifyActionButtonConfigurationTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        let actionEntry = viewModel.model[indexPath.row]
        let spotifyEntry = SpotifyEntry(actionName: actionEntry.action.title(for: viewModel.pressType),
                                        type: actionEntry.action.spotifySearchType(),
                                        name: actionEntry.subtitleDescription,
                                        active: actionEntry.active)
        cell.spotifyEntry = spotifyEntry
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        return cell
    }
}
extension SpotifyActionButtonConfigurationViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        viewModel.didSelect(action: viewModel.model[indexPath.row].action)
    }
}

