//
//  NewsletterViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 09/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static var descriptionNewsletterBlockMargin: CGFloat = 27.0
    static var newsLetterViewBorderWidth: CGFloat = 1.0
}
class NewsletterViewController: BaseViewController, NewsletterParentViewModelViewInputDelegate {
    var viewModel: NewsletterParentViewModel?
    private lazy var newsletterView: NewsletterView = {
        let newsletterView = NewsletterView()
        return newsletterView
    }()
    private let headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private let descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    private let okButton: UIButton = {
        let okButton = UIButton(type: .system)
        okButton.setImage(Image.Icon.White.check, for: .normal)
        okButton.backgroundColor = Colors.black
        okButton.tintColor = Colors.white
        okButton.addTarget(self, action: #selector(okButtonTouchedUpInside), for: .touchUpInside)
        return okButton
    }()
    private func configure(viewModel: NewsletterViewModel) {
        newsletterView.newsletterViewModel = viewModel
        viewModel.viewInputDelegate = newsletterView
        viewModel.setup()
    }
    func concludeOKButton(hidden: Bool){
        okButton.isHidden = hidden
    }
    @objc private func okButtonTouchedUpInside() {
        viewModel?.okButtonTouched()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("newsletter_title_uc", value: "NEWSLETTER", comment: "NEWSLETTER")))
        let headerText = NSLocalizedString("newsletter_headline_uc", value: "STAY UPDATED", comment: "Stay updated")
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: headerText)
        let descriptionText = NSLocalizedString("newsletter_paragraph", value: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. Duis aute irure dolor in velit.", comment: "#MISSING#")
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: descriptionText)
        [headerLabel, descriptionLabel].forEach {
            $0.textAlignment = .center
        }
        if let viewModel = viewModel?.newsletterViewModel{
            configure(viewModel: viewModel)
        }
        setupConstraints()
        setupInternetConnectionHandling()
        viewModel?.viewDidLoad()
    }
    private func setupInternetConnectionHandling(){
        guard let viewModel = viewModel else {
            return
        }
        NoInternetBehavior(parentViewController: self,
                           trigger: viewModel.reachabilityMonitor(),
                           toggleableItems: [])
    }
    private func setupConstraints() {
        [headerLabel, descriptionLabel, newsletterView, okButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            newsletterView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            newsletterView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.bottomAnchor.constraint(equalTo: newsletterView.topAnchor, constant: -Config.descriptionNewsletterBlockMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            headerLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            okButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            okButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            okButton.widthAnchor.constraint(equalToConstant: Dimension.squareButtonMargin),
            okButton.heightAnchor.constraint(equalToConstant: Dimension.squareButtonMargin),
            ])
    }
}
