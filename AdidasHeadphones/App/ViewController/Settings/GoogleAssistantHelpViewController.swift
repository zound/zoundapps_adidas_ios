//
//  GoogleAssistantHelpViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 21/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit



private struct Config {
    static let logoDimension: CGFloat = UIScreen.isSmallScreen ? 80.0 : 120.0
    static let featuresHeadlineTopSpace: CGFloat = UIScreen.isSmallScreen ? 2 * Dimension.headerSmallBottomMargin : Dimension.headerTopMargin
    static let featureBottomSpace: CGFloat = UIScreen.isSmallScreen ? 0 : Dimension.headerSmallBottomMargin
    static let logoTopMargin: CGFloat = UIScreen.isSmallScreen ? 60 : Dimension.tileBigHeight
    static let logoBottomMargin: CGFloat = UIScreen.isSmallScreen ? 0 : Dimension.headerTopMargin
}
class GoogleAssistantHelpViewController: BaseViewController, GoogleAssistantHelpViewModelViewInputDelegate {
    public var viewModel: GoogleAssistantHelpViewModel?
    private let logoView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.image = Image.Icon.Service.googleAssistant
        return imageView
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        let title = NSLocalizedString("enable_google_assistant_headline", value: "ENABLE GOOGLE ASSISTANT", comment: "")
        label.attributedText = Headline.style(as: .largeBlack, text: title)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        let description = NSLocalizedString("enable_google_assistant_paragraph", value: "Enable Google Assistant to be able to interact with it by pressing your Action Button.", comment: "")
        label.attributedText = Paragraph.style(as: .regularLight, text: description)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    private let featuresHeadlineLabel: UILabel = {
        let label = UILabel()
        let featuresHeadline = NSLocalizedString("enable_google_features_headline", value: "How to enable Google Assistant", comment: "")
        label.attributedText = Headline.style(as: .smallDark, text: featuresHeadline)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    private let feature1Label: UILabel = {
        let label = UILabel()
        let text = NSLocalizedString("enable_google_features_feature_1_iOS", value: "1. Download and launch the Google Assistant app.", comment: "")
        label.attributedText = Paragraph.style(as: .smallDark, text: text)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    private let feature2Label: UILabel = {
        let label = UILabel()
        let text = NSLocalizedString("enable_google_features_feature_2", value: "2. Follow the instructions in the app.", comment: "")
        label.attributedText = Paragraph.style(as: .smallDark, text: text)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    private let feature3Label: UILabel = {
        let label = UILabel()
        let text = NSLocalizedString("enable_google_features_feature_3", value: "3. Close this screen and start using the Assistant", comment: "")
        label.attributedText = Paragraph.style(as: .smallDark, text: text)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = Colors.clear
        button.addTarget(self, action:#selector(buttonTouchedUpInside), for: .touchUpInside)
        return button
    }()
    @objc private func buttonTouchedUpInside(_ sender: UIButton) {
        viewModel?.didRequestDownloadGoogleAssistant()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("appwide_google_assistant_uc", value: "GOOGLE ASSISTANT", comment: "")))
        let buttonTitle = NSLocalizedString("enable_google_download_button_title_uc", value: "DOWNLOAD GOOGLE ASSISTANT", comment: "")
        button.setAttributedTitle(Button.style(as: .regularBlackUnderlined, text: buttonTitle), for: .normal)
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    private func setupConstraints() {
        [logoView, titleLabel, descriptionLabel, featuresHeadlineLabel, feature1Label, feature2Label, feature3Label, button].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            logoView.topAnchor.constraint(equalTo: view.topAnchor, constant: Config.logoTopMargin),
            logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoView.widthAnchor.constraint(equalToConstant: Config.logoDimension),
            logoView.heightAnchor.constraint(equalToConstant: Config.logoDimension),
            titleLabel.topAnchor.constraint(equalTo: logoView.bottomAnchor, constant: Config.logoBottomMargin),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Dimension.headerSmallBottomMargin),
            descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            featuresHeadlineLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Config.featuresHeadlineTopSpace),
            featuresHeadlineLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            featuresHeadlineLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            featuresHeadlineLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            feature1Label.topAnchor.constraint(equalTo: featuresHeadlineLabel.bottomAnchor, constant: Dimension.headerSmallBottomMargin),
            feature1Label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            feature1Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            feature1Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            feature2Label.topAnchor.constraint(equalTo: feature1Label.bottomAnchor, constant: Config.featureBottomSpace),
            feature2Label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            feature2Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            feature2Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            feature3Label.topAnchor.constraint(equalTo: feature2Label.bottomAnchor, constant:  Config.featureBottomSpace),
            feature3Label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            feature3Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            feature3Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            button.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
        ])
    }



}
