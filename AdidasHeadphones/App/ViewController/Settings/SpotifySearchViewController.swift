//
//  SpotifySearchViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 10/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

private struct Const {
    static let sectionHeaderHeight: CGFloat = 48.0
    static let rowHeight: CGFloat = 84.0
    static let searchContainerViewHeight: CGFloat = 39.0
    static let refreshControlAlpha: CGFloat = .zero
}
final class SpotifySearchViewController: BaseViewController {
    var viewModel: SpotifySearchViewModel?
    var searchController: UISearchController!
    let refreshControl = UIRefreshControl()
    private let disposeBag = DisposeBag()
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(SpotifyEntryTableViewCell.self, forCellReuseIdentifier: SpotifyEntryTableViewCell.id)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.rowHeight = Const.rowHeight
        tableView.sectionHeaderHeight = Const.sectionHeaderHeight
        tableView.sectionIndexColor = Colors.black
        tableView.sectionIndexBackgroundColor = Colors.clear
        return tableView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.delegate = self
        setupConstraints()
        viewModel?.refreshFinished
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] in
                    self?.refreshControl.endRefreshing()
                }
            )
            .disposed(by: disposeBag)
        viewModel?.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = Colors.clear
        refreshControl.subviews.first?.alpha = Const.refreshControlAlpha
        tableView.addSubview(refreshControl)
        guard let searchType = viewModel?.type,
            let pressType = viewModel?.pressType else {
            return
        }
        setupNavigationBar(title: Headline.style(as: .smallDark, text: searchType.action().headerTitle(for: pressType)))
        guard let viewModel = viewModel else { return }
        NoInternetBehavior(parentViewController: self,
                           trigger: viewModel.reachabilityMonitor(),
                           toggleableItems: [tableView])
        SpotifySearchLoaderBehavior(parentViewController: self,
                                    fadedView: tableView,
                                    progressTrigger: viewModel.progressTrigger,
                                    loadingFinishedTrigger: viewModel.refreshFinished,
                                    messageTrigger: viewModel.messageTrigger,
                                    refreshHandler: viewModel.refresh)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.viewWillDisappear()
    }
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        let topMargin = Dimension.statusBarHeight + (navigationController?.navigationBar.frame.height ?? .zero)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor,constant: topMargin),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    @objc private func refresh(_ sender: UIControl) {
        guard let viewModel = viewModel else { return }
        viewModel.refresh()
    }
}
extension SpotifySearchViewController: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else {
            return .zero
            
        }
        return viewModel.filteredEntries.count
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return .zero
        }
        return viewModel.filteredEntries[section].count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var asyncCoverDisposeBag = DisposeBag()
        guard let viewModel = viewModel else {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: SpotifyEntryTableViewCell.id) as! SpotifyEntryTableViewCell
        let spotifyEntry = viewModel.filteredEntries[indexPath.section][indexPath.row]
        cell.spotifyEntry = spotifyEntry
        viewModel.remoteImage(for: spotifyEntry.id)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (id, image) in
                    cell.set(entryId: id, image: image)
                }
                ,onCompleted: {
                    asyncCoverDisposeBag = DisposeBag()
                }
            )
            .disposed(by: asyncCoverDisposeBag)
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        return cell
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = viewModel else {
            return nil
        }
        return viewModel.sectionTitles[section]
    }
}
extension SpotifySearchViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        viewModel.didSelect(indexPath: indexPath)
    }
    public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        guard let viewModel = viewModel else { return nil }
        return viewModel.sectionTitles
    }
    public func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.sectionTitles.firstIndex(where: { $0 == title }) ?? .zero
    }
}
extension SpotifySearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let viewModel = viewModel else { return }
        viewModel.update(searchQuery: searchController.searchBar.text)
    }
}
extension SpotifySearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let viewModel = viewModel else { return }
        viewModel.searchButtonTapped()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        guard let viewModel = viewModel else { return }
        viewModel.cancelButtonTapped()
    }
}
extension SpotifySearchViewController: SpotifySearchViewModelViewInputDelegate {
    func reloadData() {
        tableView.reloadData()
    }
}
