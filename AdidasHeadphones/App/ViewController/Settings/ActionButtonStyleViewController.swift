//
//  ActionButtonStyleViewController.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 23/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService
import RxSwift

private struct Config {
    static let rowHeight: CGFloat = 10.0
    static let topMargin: CGFloat = Dimension.screenHeight18.half
    static let actionsHeaderVerticalEdgeMargin: CGFloat = 12.0
    static let sectionsIfFolded = 1
    static let sectionsIfUnfolded = 2
    static let servicesSectionIdx = 0
    static let buttonConfigurationSectionIdx = 1
    static let headerSeparatorHeight: CGFloat = 1
}
struct ActionInfo {
    let parentIcon: UIImage
    let action: ButtonAction
}
struct ButtonActionInfo {
    let type: PressType
    let info: ActionInfo
}
struct ExtendedButtonActionInfo {
    let uuid: String?
    let info: ButtonActionInfo
}
final class ActionButtonStyleViewController: BaseViewController {
    var viewModel: ActionButtonStyleViewModel?
    private var disposeBag = DisposeBag()
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        return scrollView
    }()
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(ActionServiceTableViewCell.self, forCellReuseIdentifier: String(describing: ActionServiceTableViewCell.self))
        tableView.register(ActionButtonTableViewCell.self, forCellReuseIdentifier: String(describing: ActionButtonTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Config.rowHeight
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = Config.rowHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    func mainSectionHeader() -> UIView {
        let headerView = UIView()
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("action_button_apps_headline_uc", value: "SELECT YOUR ACTION BUTTON STYLE", comment: "Select your action button style"))
        label.numberOfLines = 0
        label.textAlignment = .center
        headerView.backgroundColor = Colors.white
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = tableView.separatorColor
        
        headerView.addSubview(label)
        headerView.addSubview(separatorView)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: Dimension.edgeMargin),
            label.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -Dimension.edgeMargin),
            label.topAnchor.constraint(equalTo: headerView.topAnchor, constant: Config.topMargin),
            label.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -Config.topMargin),
            label.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            separatorView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -Config.headerSeparatorHeight),
            separatorView.heightAnchor.constraint(equalToConstant: Config.headerSeparatorHeight/UIScreen.main.scale)
            ])
        return headerView
    }
    func sectionHeaderView(title: String) -> UIView {
        let headerView = UIView()
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.attributedText = Headline.style(as: .smallDark, text: title)
        label.numberOfLines = 1
        label.textAlignment = .left
        headerView.backgroundColor = Colors.greyExtraLight
        
        headerView.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: headerView.topAnchor, constant: Config.actionsHeaderVerticalEdgeMargin),
            label.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -Config.actionsHeaderVerticalEdgeMargin),
            label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: Dimension.edgeMargin),
            label.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: Dimension.edgeMargin),
            label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
            ])
        return headerView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("settings_action_button_uc", value: "ACTION BUTTON", comment: "Action Button")))
        setupScrollView()
        viewModel?.unfoldGroup.subscribe(
            onNext: { [weak self] info in
                self?.unfold(info.groupIdx, type: info.type)
            }
        )
        .disposed(by: disposeBag)
        viewModel?.viewDidLoad()
    }
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = tableView.topAnchor.constraint(equalTo: view.topAnchor)
        topConstraint.priority = UILayoutPriority.required - 1
        NSLayoutConstraint.activate([
            topConstraint,
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
extension ActionButtonStyleViewController: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).count > 0 ? Config.sectionsIfUnfolded : Config.sectionsIfFolded
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        guard section == Config.buttonConfigurationSectionIdx else {
            return viewModel.actionGroups.count
        }
        guard let currentlyUnfolded = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else {
            return 0
        }
        return currentlyUnfolded.buttonActions.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        guard indexPath.section == Config.buttonConfigurationSectionIdx else {
            let cell = ActionServiceTableViewCell(style: .default, reuseIdentifier: String(describing: ActionServiceTableViewCell.self))
            cell.serviceItem = viewModel.actionGroups[indexPath.row]
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            cell.selectionStyle = .none
            cell.backgroundColor = Colors.white
            return cell
        }
        guard let currentlyUnfolded = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else {
            return tableView.dequeueReusableCell(withIdentifier: String(describing: UITableViewCell.self), for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier:String(describing: ActionButtonTableViewCell.self), for: indexPath) as! ActionButtonTableViewCell
        cell.simplified = currentlyUnfolded.configurable == false
        if let uuid = viewModel.currentDeviceUUID() {
            let extendedInfo = ExtendedButtonActionInfo(uuid: uuid, info: currentlyUnfolded.buttonActions[indexPath.row])
            cell.extendedActionItem = extendedInfo
        } else {
            cell.buttonActionItem = currentlyUnfolded.buttonActions[indexPath.row]
        }
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.selectionStyle = .none
        cell.backgroundColor = Colors.white
        return cell
    }
}
extension ActionButtonStyleViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        guard indexPath.section == Config.buttonConfigurationSectionIdx else {
            switch (indexPath.section, viewModel.actionGroups[indexPath.row].unfoldType) {
            case (Config.servicesSectionIdx, .folded):
                viewModel.didSelect(styleType: viewModel.actionGroups[indexPath.row].type)
            default: break
            }
            return
        }
        guard let unfoldedGroup = viewModel.actionGroups.filter({ $0.unfoldType == .always || $0.unfoldType == .unfolded }).first else {
            return
        }
        let selectedButton = unfoldedGroup.buttonActions[indexPath.row]
        viewModel.didSelect(styleType: unfoldedGroup.type, pressType: selectedButton.type, value: selectedButton.info.action)
    }
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == Config.buttonConfigurationSectionIdx else {
            return mainSectionHeader()
        }
        let actionsTitle = NSLocalizedString("action_button_actions_section_title_uc", value: "ACTIONS", comment: "Actions")
        return sectionHeaderView(title: actionsTitle)
    }
}
private extension ActionButtonStyleViewController {
    private func unfold(_ groupId: Int, type: UnfoldType) {
        guard let viewModel = viewModel else { return }
        switch type {
        case .folded, .always:
            tableView.reloadData()
            return
        case .unfolded:
            tableView.beginUpdates()
            let servicesIndexes = viewModel.actionGroups.enumerated().map {
                IndexPath(item: $0.offset, section: Config.servicesSectionIdx)
            }
            let unfoldedIndexes = viewModel.actionGroups[groupId].buttonActions.enumerated().map {
                IndexPath(item: $0.offset, section: Config.buttonConfigurationSectionIdx)
            }
            tableView.reloadRows(at: servicesIndexes, with: .none)
            tableView.reloadRows(at: unfoldedIndexes, with: .left)
            tableView.endUpdates()
        }
    }
    func setupScrollView() {
        [scrollView, tableView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        scrollView.addSubview(tableView)
        view.addSubview(scrollView)
        let topConstraint = scrollView.topAnchor.constraint(equalTo: view.topAnchor)
        topConstraint.priority = UILayoutPriority.required - 1
        NSLayoutConstraint.activate([
            topConstraint,
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            tableView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            tableView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        ])
    }
}
