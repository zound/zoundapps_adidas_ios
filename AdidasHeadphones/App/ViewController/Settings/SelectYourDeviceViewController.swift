//
//  SelectYourDeviceViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 03/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let reusableId = String(describing: UITableViewCell.self)
    static let cellType = UITableViewCell.self
}
class SelectYourDeviceViewController: BaseViewController {
    var viewModel: SelectYourDeviceViewModelViewOutputDelegate?
    private let entrys: [SelectYourDeviceScreenEntry] = [.arnold, .freeman, .desir]
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(Config.cellType, forCellReuseIdentifier: Config.reusableId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = Dimension.tableCellHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("help_select_device_title_uc", value: "SELECT YOUR DEVICE", comment: "SELECT YOUR DEVICE")))
        view.addSubview(tableView)
        setupConstraints()
        tableView.reloadData()
    }
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
extension SelectYourDeviceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entrys.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Config.reusableId, for: indexPath)
        let entry = entrys[indexPath.row]
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.textLabel?.attributedText = Headline.style(as: .smallDark, text: entry.title)
        cell.selectionStyle = .none
        cell.accessoryType =  .disclosureIndicator
        cell.backgroundColor = Colors.white
        return cell
    }
}
extension SelectYourDeviceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selected(entry: entrys[indexPath.row])
    }
}
extension SelectYourDeviceViewController: SelectYourDeviceViewModelViewInputDelegate {
}
extension SelectYourDeviceScreenEntry {
    var title: String {
        switch self {
        case .arnold:
            return NSLocalizedString("help_select_device_option_1_uc", value: "ADIDAS RBT-001", comment: "ADIDAS RBT-001")
        case .freeman:
            return NSLocalizedString("help_select_device_option_2_uc", value: "ADIDAS-RBT-002", comment: "ADIDAS-RBT-002")
        case .desir:
            return NSLocalizedString("help_select_device_option_3_uc", value: "ADIDAS-RBT-003", comment: "ADIDAS-RBT-003")
        }
    }
}



