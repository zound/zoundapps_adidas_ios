//
//  SettingsViewController.swift
//  002
//
//  Created by Łukasz Wudarski on 09/04/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import UIKit
import ConnectionService

private struct Config {
    static let sectionHeaderHeight: CGFloat = 48.0
    static let rowHeight: CGFloat = 59.0
    static let reusableId = String(describing: UITableViewCell.self)
    static let bigHeaderTableViewVerticalSpacing: CGFloat = 23.0
}

enum SettingsEntry {
    case aboutThisDevice
    case actionButton
    case appConnections
    case graphicalEqualizer
    case aboutThisApp
    case singleButtonConfiguration(PressType, ButtonAction)
    /*
    case deviceName
    case soundsAndPrompts
    case autoOffTimer
    case bluetoothDevices
    case wearDetection
    case softwareUpdate
*/
    case help
    case newsletter
    case qualityProgram

}

enum SettingsSections: Int {
    case deviceName
    case general
}

extension SettingsEntry {
    var text: String {
        switch self {
        case .aboutThisDevice:
            return NSLocalizedString("settings_about_this_device_uc", value: "ABOUT THIS DEVICE", comment: "About this device")
        case .actionButton:
            return NSLocalizedString("settings_action_button_uc", value: "ACTION BUTTON", comment: "Action button")
        case .appConnections:
            return NSLocalizedString("settings_app_connections_uc", value: "APP CONNECTIONS", comment: "App connections")
        case .graphicalEqualizer:
            return NSLocalizedString("settings_equalizer_uc", value: "EQUALIZER", comment: "Equalizer")
        case .aboutThisApp:
            return NSLocalizedString("settings_about_this_app_uc", value: "ABOUT THIS APP", comment: "ABOUT THIS APP")
/*
        case .deviceName:
            return NSLocalizedString("settings_device_name_uc", value: "DEVICE NAME", comment: "DEVICE NAME")
        case .soundsAndPrompts:
            return NSLocalizedString("settings_sounds_and_prompts_uc", value: "SOUNDS & PROMPTS", comment: "SOUNDS & PROMPTS")
        case .autoOffTimer:
            return NSLocalizedString("settings_auto_off_timer_uc", value: "AUTO OFF TIMER", comment: "AUTO OFF TIMER")
        case .bluetoothDevices:
            return NSLocalizedString("settings_bluetooth_devices_uc", value: "BLUETOOTH DEVICES", comment: "BLUETOOTH DEVICES")
        case .wearDetection:
            return NSLocalizedString("settings_wear_detection_uc", value: "WEAR DETECTION", comment: "WEAR DETECTION")
        case .softwareUpdate:
            return NSLocalizedString("settings_software_update_uc", value: "SOFTWARE UPDATE", comment: "SOFTWARE UPDATE")
*/
        case .help:
            return NSLocalizedString("settings_help_uc", value: "HELP", comment: "HELP")
        case .newsletter:
            return NSLocalizedString("settings_newsletter_uc", value: "NEWSLETTER", comment: "NEWSLETTER")
        case .qualityProgram:
            return NSLocalizedString("settings_quality_program_uc", value: "QUALITY PROGRAM", comment: "QUALITY PROGRAM")

        case .singleButtonConfiguration:
            return String.invisible
        }
    }

}

final class SettingsViewController: BaseViewController {
    var viewModel: SettingsViewModel?
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        return scrollView
    }()
    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Config.reusableId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = Config.rowHeight
        tableView.sectionHeaderHeight = Config.sectionHeaderHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()
    private lazy var bigHeaderLabel: UILabel = {
        let label = UILabel()
        let text = NSLocalizedString("settings_title_uc", value: "SETTINGS", comment: "Settings")
        label.attributedText = Headline.style(as: .extraLargeBlack, text: text)
        label.textAlignment = .left
        return label
    }()
    func sectionHeaderView(title: String) -> UIView {
        let headerView = UIView()
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.attributedText = Headline.style(as: .smallDark, text: title)
        label.numberOfLines = 1
        label.textAlignment = .left
        headerView.backgroundColor = Colors.greyExtraLight
        headerView.addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: Dimension.edgeMargin),
            label.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: Dimension.edgeMargin),
            label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
            ])
        return headerView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: String.invisible))
        viewModel?.viewDidLoad()
    }
    private let entries: [[SettingsEntry]] = [
        [.graphicalEqualizer, .actionButton, .aboutThisDevice],
        [.appConnections, .help, .aboutThisApp, .newsletter, .qualityProgram]
    ]
    private var deviceName = String.invisible
}

extension SettingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return entries.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Config.reusableId, for: indexPath)
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.textLabel?.attributedText = Headline.style(as: .smallDark, text: entries[indexPath.section][indexPath.row].text)
        cell.selectionStyle = .none
        cell.accessoryType =  .disclosureIndicator
        cell.backgroundColor = .white
        return cell
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == SettingsSections.deviceName.rawValue else {
            let generalSectionName = NSLocalizedString("settings_general_section_name_uc", value: "GENERAL", comment: "General section")
            return sectionHeaderView(title: generalSectionName)
        }
        return sectionHeaderView(title: deviceName)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selected(entry: entries[indexPath.section][indexPath.row])
    }
}

private extension SettingsViewController {
    func setupScrollView() {
        [scrollView, bigHeaderLabel, tableView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        scrollView.addSubview(bigHeaderLabel)
        scrollView.addSubview(tableView)
        view.addSubview(scrollView)
        let topMargin = Dimension.statusBarHeight + (navigationController?.navigationBar.frame.height ?? 0)
        let topConstraint = scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: topMargin)
        topConstraint.priority = UILayoutPriority.required - 1
        NSLayoutConstraint.activate([
            topConstraint,
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            bigHeaderLabel.topAnchor.constraint(equalTo: scrollView.topAnchor),
            bigHeaderLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: Dimension.edgeMargin),
            bigHeaderLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -Dimension.edgeMargin),
            tableView.topAnchor.constraint(equalTo: bigHeaderLabel.bottomAnchor, constant: Config.bigHeaderTableViewVerticalSpacing),
            tableView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            tableView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            tableView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        ])
    }
}

extension SettingsViewController: SettingsViewModelViewInputDelegate {
    func update(deviceName: String) {
        self.deviceName = deviceName
        tableView.reloadData()
    }
}
