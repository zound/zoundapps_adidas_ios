//
//  OnlineManualViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 03/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit


class CommonWebViewController: BaseViewController, CommonWebViewModelViewInputDelegate {
    var viewModel: CommonWebViewModel?
    var request: URLRequest?
    private var webView: WKWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        guard let viewModel = viewModel else {
            return
        }
        viewModel.viewDidLoad()
        NoInternetBehavior(parentViewController: self,
                           trigger: viewModel.reachabilityMonitor(),
                           toggleableItems: [])
    }
    private lazy var spinnerView: UIImageView = {
        let waitingIndicatorImageView = UIImageView(image: Image.Indicators.spinnerDark)
        waitingIndicatorImageView.contentMode = .center
        waitingIndicatorImageView.isHidden = true
        return waitingIndicatorImageView

    }()
    func setupWebView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.frame, configuration: webViewConfiguration)
        view.addSubview(webView!)
        view.addSubview(spinnerView)
        webView!.scrollView.delegate = self
        webView!.isOpaque = false
        webView!.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        webView!.translatesAutoresizingMaskIntoConstraints = false
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = webView!.topAnchor.constraint(equalTo: view.topAnchor, constant: (navigationController!.navigationBar.frame.height + Dimension.statusBarHeight))
        topConstraint.priority = UILayoutPriority.required - 1
        NSLayoutConstraint.activate([
            topConstraint,
            webView!.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView!.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView!.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            spinnerView.centerXAnchor.constraint(equalTo: webView!.centerXAnchor),
            spinnerView.centerYAnchor.constraint(equalTo: webView!.centerYAnchor)
            ])
        webView!.navigationDelegate = self
    }
    func loadHtml() {
        if let request = request {
            webView?.load(request)
        }
    }
    func refreshRequest(_ request: URLRequest?) {
        self.request = request
        loadHtml()
    }
    func refreshTitle(_ title: String) {
        setupNavigationBar(title: Headline.style(as: .smallDark, text: title))
    }
    func setIndicator(hidden: Bool) {
        spinnerView.isHidden = hidden
        if hidden {
            spinnerView.stopAnimations()
        } else {
            spinnerView.rotate()
        }
    }
}
extension CommonWebViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
extension CommonWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        viewModel?.webViewDidStartLoading()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        viewModel?.webViewDidFinish()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        viewModel?.webViewDidFail()
    }
}


