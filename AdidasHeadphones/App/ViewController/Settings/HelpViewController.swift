//
//  HelpViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 02/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


private struct Config {
    static let reusableId = String(describing: UITableViewCell.self)
    static let cellType = UITableViewCell.self
}


class HelpViewController: BaseViewController {

    var viewModel: HelpViewModelViewOutputDelegate?

    private let entrys: [HelpScreenEntry] = [.selectDevice, .website, .contactSupport]

    private lazy var tableView: ResizeableTableView = {
        let tableView = ResizeableTableView(frame: .zero, style: .plain)
        tableView.register(Config.cellType, forCellReuseIdentifier: Config.reusableId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = Colors.white
        tableView.contentInset = UIEdgeInsets.zero
        tableView.isScrollEnabled = false
        tableView.rowHeight = Dimension.tableCellHeight
        tableView.tableFooterView = UIView()
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("help_title_uc", value: "HELP", comment: "HELP")))
        view.addSubview(tableView)
        setupConstraints()
        tableView.reloadData()
    }

    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension HelpViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entrys.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Config.reusableId, for: indexPath)
        let entry = entrys[indexPath.row]
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.textLabel?.attributedText = Headline.style(as: .smallDark, text: entry.title)
        cell.selectionStyle = .none
        cell.accessoryType =  .disclosureIndicator
        cell.backgroundColor = Colors.white
        return cell
    }
}

extension HelpViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selected(entry: entrys[indexPath.row])
    }
}

extension HelpViewController: HelpViewModelViewInputDelegate {

}

extension HelpScreenEntry {
    var title: String {
        switch self {
        case .selectDevice:
            return NSLocalizedString("help_option_1_uc ", value: "USER MANUAL", comment: "USER MANUAL")
        case .website:
            return NSLocalizedString("help_option_2_uc", value: "GO TO WEBSITE", comment: "GO TO WEBSITE")
       case .contactSupport:
            return NSLocalizedString("help_option_3_uc", value: "CONTACT SUPPORT", comment: "CONTACT SUPPORT")
        }
    }
}


