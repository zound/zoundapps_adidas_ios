//
//  PairingViewController.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 08/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let alphaMin: CGFloat = 0.2
    static let alphaMax: CGFloat = 1.0
    static let pairingCoverViewAlpha: CGFloat = 0.8
}

class PairingViewController: BaseViewController {
    var viewModel: PairingViewModel?
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("enable_pairing_mode_headline_uc", value: "ENABLE PAIRING MODE", comment: "ENABLE PAIRING MODE"))
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private lazy var description1Label: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: viewModel?.description1() ?? String.invisible)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = Int.zero
        return descriptionLabel
    }()
    private lazy var description2Label: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: viewModel?.description2() ?? String.invisible)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    private let waitingIndicatorImageView: UIImageView = {
        let waitingIndicatorImageView = UIImageView(image: Image.Indicators.spinnerDark)
        waitingIndicatorImageView.contentMode = .center
        return waitingIndicatorImageView
    }()
    private let pairingCoverView: PairingCoverView = {
        let pairingCoverView = PairingCoverView()
        pairingCoverView.backgroundColor = UIColor.withAlphaComponent(Colors.black)(Const.pairingCoverViewAlpha)
        pairingCoverView.isHidden = true
        return pairingCoverView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.white
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.viewDidAppear()
    }
    private func setupConstraints() {
        [imageView, headerLabel, description1Label, description2Label, waitingIndicatorImageView, pairingCoverView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        view.bringSubviewToFront(pairingCoverView)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.statusBarHeight),
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Dimension.screenWidth),
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.screenHeight58),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            description1Label.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.headerSmallBottomMargin),
            description1Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            description1Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            description2Label.topAnchor.constraint(equalTo: description1Label.bottomAnchor),
            description2Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            description2Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            waitingIndicatorImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30 - Dimension.safeAreaBottomInset),
            waitingIndicatorImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pairingCoverView.topAnchor.constraint(equalTo: view.topAnchor, constant: -(navigationController!.navigationBar.frame.height + Dimension.statusBarHeight)),
            pairingCoverView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            pairingCoverView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pairingCoverView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    private func update(description1Label alpha1: CGFloat,  description2Label alpha2: CGFloat) {
        UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
            self?.description1Label.alpha = alpha1
        })
        UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
            self?.description2Label.alpha = alpha2
        })
    }
}

extension PairingViewController: PairingViewModelViewInputDelegate {
    func update(deviceTypeImage: UIImage) {
        imageView.image = deviceTypeImage
    }
    func indicateDisconnectWill() {
        update(description1Label: Const.alphaMax, description2Label: Const.alphaMin)
    }
    func indicateLongPressConnectWill() {
        update(description1Label: Const.alphaMin, description2Label: Const.alphaMax)
    }
    func stopIndications() {
        update(description1Label: Const.alphaMax, description2Label: Const.alphaMax)
    }
    func showPairingCoverView(with name: String) {
        pairingCoverView.headerLabelAttributedText = Headline.style(as: .largeWhite, text: NSLocalizedString("ios_select_pairing_device_uc", value: "SELECT", comment: "SELECT") + "\n".appending(name))
        pairingCoverView.isHidden = false
    }
    func hidePairingCoverView() {
        let animations: () -> Void = { [weak self] in
            self?.pairingCoverView.alpha = .zero
        }
        UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseOut, animations: animations) { [weak self] _ in
            self?.pairingCoverView.isHidden = true
            self?.viewModel?.pairingCoverViewDidHide()
        }
    }
    func animateHeaderView() {
        headerLabel.indicateAnimated()
    }
    func animateSpinner() {
        waitingIndicatorImageView.rotate()
    }
    func hideAll() {
        [imageView, headerLabel, description1Label, description2Label, waitingIndicatorImageView].forEach {
            $0.isHidden = true
        }
    }
}
