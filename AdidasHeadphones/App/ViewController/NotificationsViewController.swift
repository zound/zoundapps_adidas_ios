//
//  NotificationsViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 13/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController, NotificationsViewModelViewInputDelegate {
    var viewModel: NotificationsViewModel?
    private let headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private let descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    private let cancelButton: UIButton = {
        let cancelButton = UIButton(type: .system)
        cancelButton.setAttributedTitle(Button.style(as: .regularBlackUnderlined, text: NSLocalizedString("appwide_cancel_uc", value: "CANCEL", comment: "CANCEL")), for: .normal)
        cancelButton.backgroundColor = Colors.clear
        cancelButton.addTarget(self, action: #selector(cancelButtonTouchedUpInside), for: .touchUpInside)
        return cancelButton
    }()
    private let registerButton: UIButton = {
        let registerButton = UIButton(type: .system)
        registerButton.backgroundColor = Colors.black
        registerButton.tintColor = Colors.white
        registerButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("notifications_screen_register_uc", value: "REGISTER", comment: "REGISTER")), for: .normal)
        registerButton.backgroundColor = Colors.black
        registerButton.addTarget(self, action: #selector(registerButtonTouchedUpInside), for: .touchUpInside)
        return registerButton
    }()
    @objc private func cancelButtonTouchedUpInside() {
        viewModel?.didTapCancelButton()
    }
    @objc private func registerButtonTouchedUpInside() {
        viewModel?.didTapRegisterButton()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let headerText = NSLocalizedString("notifications_headline_uc", value: "Notifications", comment: "Notifications")
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: headerText)
        let descriptionText = NSLocalizedString("notifications_paragraph", value: "FIRMWARE UPDATE\n\n\nYour headphones are always getting better.\nWe can send you notification when new features are available for you.\nDo you want to register for new updates?", comment: "#MISSING#")
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: descriptionText)
        [headerLabel, descriptionLabel].forEach {
            $0.textAlignment = .center
        }
        setupConstraints()
    }

    private func setupConstraints() {
        [headerLabel, descriptionLabel, cancelButton, registerButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            headerLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            cancelButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            cancelButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            cancelButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            cancelButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            registerButton.bottomAnchor.constraint(equalTo: cancelButton.topAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            registerButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            registerButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            registerButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            ])
    }

}
