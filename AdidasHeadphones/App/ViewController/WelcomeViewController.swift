//
//  WelcomeViewController.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class WelcomeViewController: BaseViewController {
    var viewModel: WelcomeViewModel?
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Image.Brand.welcomeBackground)
        imageView.contentMode = .center
        return imageView
    }()
    lazy var startSetupButton: UIButton = {
        let startSetupButton = UIButton()
        startSetupButton.setAttributedTitle(Button.style(as: .regularBlackUnderlined, text: NSLocalizedString("welcome_agree_button_start_setup_uc", value: "START SETUP", comment: "START SETUP")), for: .normal)
        startSetupButton.backgroundColor = Colors.white
        startSetupButton.addTarget(self, action:#selector(startSetupButtonTouchedUpIndide), for: .touchUpInside)
        return startSetupButton
    }()
    private static let firstTextComponent = NSLocalizedString("welcome_agree_confirmation_1", value: "By pressing \"Start setup\" I confirm that I agree to ", comment: "By pressing \"Start setup\" I confirm that I agree to ")
    private static let firstLinkTextComponent = NSLocalizedString("welcome_agree_confirmation_2", value: "Adidas Terms & Conditions", comment: "Adidas Terms & Conditions")
    private static let secondTextComponent = NSLocalizedString("welcome_agree_confirmation_3", value: " and that I have read the ", comment: " and that I have read the ")
    private static let secondLinkTextComponent = NSLocalizedString("welcome_agree_confirmation_4", value: "Adidas Quality Program", comment: "Adidas Quality Program")
    private static let firstRarange = NSRange(location: WelcomeViewController.firstTextComponent.count,
                                              length: WelcomeViewController.firstLinkTextComponent.count)
    private static let secondRarange = NSRange(location: WelcomeViewController.firstTextComponent.count +
                                                         WelcomeViewController.firstLinkTextComponent.count +
                                                         WelcomeViewController.secondTextComponent.count,
                                               length: WelcomeViewController.secondLinkTextComponent.count)
    lazy var clickableDescriptionTextView: UITextView = {
        let clickableDescriptionTextView = UITextView()
        let text = WelcomeViewController.firstTextComponent +
                   WelcomeViewController.firstLinkTextComponent +
                   WelcomeViewController.secondTextComponent +
                   WelcomeViewController.secondLinkTextComponent
        let termsAndCondAttributedString = NSMutableAttributedString(attributedString: Paragraph.style(as: .smallWhite, text: text))
        termsAndCondAttributedString.addAttribute(.link, value: String(), range: WelcomeViewController.firstRarange)
        termsAndCondAttributedString.addAttribute(.link, value: String(), range: WelcomeViewController.secondRarange)
        clickableDescriptionTextView.attributedText = termsAndCondAttributedString
        clickableDescriptionTextView.textAlignment = .center
        clickableDescriptionTextView.linkTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue]
        clickableDescriptionTextView.backgroundColor = Colors.clear
        clickableDescriptionTextView.tintColor = Colors.white
        clickableDescriptionTextView.isEditable = false
        clickableDescriptionTextView.isScrollEnabled = false
        clickableDescriptionTextView.delegate = self
        return clickableDescriptionTextView
    }()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.black
        setupNavigationBar(title: NSAttributedString())
        setupConstraints()
    }
    private func setupConstraints() {
        [imageView, clickableDescriptionTextView, startSetupButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        let clickableDescriptionHeightConstraint = clickableDescriptionTextView.heightAnchor.constraint(equalToConstant: 100)
        clickableDescriptionHeightConstraint.priority = .defaultHigh
        clickableDescriptionTextView.setContentCompressionResistancePriority(.required, for: .vertical)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            clickableDescriptionTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin.twice - Dimension.safeAreaBottomInset),
            clickableDescriptionTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            clickableDescriptionTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            clickableDescriptionHeightConstraint,
            startSetupButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            startSetupButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            startSetupButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            startSetupButton.bottomAnchor.constraint(equalTo: clickableDescriptionTextView.topAnchor, constant: -Dimension.edgeMargin.twice)
        ])
    }
    @objc private func startSetupButtonTouchedUpIndide() {
        viewModel?.didTapStartSetup()
    }
}

extension WelcomeViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        switch characterRange {
        case WelcomeViewController.firstRarange:
            viewModel?.didTapTermsAndCond()
        case WelcomeViewController.secondRarange:
            viewModel?.didTapQualityProgram()
        default:
            break
        }
        return false
    }
}

extension WelcomeViewController: WelcomeViewModelViewInputDelegate {

}
