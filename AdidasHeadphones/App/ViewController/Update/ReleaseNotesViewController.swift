//
//  ReleaseNotesViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 09/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit

class ReleaseNotesViewController: BaseViewController, HtmlStringProviding {

    var viewModel: ReleaseNotesViewModel?
    private var webView: WKWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("software_update_release_notes_title_uc", value: "RELEASE NOTES", comment: "RELEASE NOTES")))
        setupWebView()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadHtml()
    }
    func setupWebView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.frame, configuration: webViewConfiguration)
        view.addSubview(webView!)
        webView!.scrollView.delegate = self
        webView!.isOpaque = false
        webView!.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        webView!.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = webView!.topAnchor.constraint(equalTo: view.topAnchor, constant: (navigationController!.navigationBar.frame.height + Dimension.statusBarHeight))
        topConstraint.priority = UILayoutPriority.required - 1
        NSLayoutConstraint.activate([
            topConstraint,
            webView!.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView!.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView!.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    func loadHtml() {
        webView?.loadHTMLString(htmlString(for: .softwareUpdateReleaseNotesForFWD_01), baseURL: URL(fileURLWithPath: Bundle.main.bundlePath))
    }
}
extension ReleaseNotesViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
extension ReleaseNotesViewController: ReleaseNotesViewModelViewInputDelegate {}

