//
//  UpdateViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 02/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

class UpdateViewController: BaseViewController {

    var viewModel: UpdateViewModel?

    private let headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.numberOfLines = .zero
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private let descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    private let batteryInfoLabel: UILabel = {
        let batteryLabel = UILabel()
        batteryLabel.textAlignment = .center
        batteryLabel.numberOfLines = .zero
        return batteryLabel
    }()
    private let installButton: UIButton = {
        let installButton = UIButton()
        installButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("software_update_available_button_1_title_uc", value: "INSTALL NOW", comment: "INSTALL NOW")), for: .normal)
        installButton.backgroundColor = Colors.black
        installButton.addTarget(self, action:#selector(installButtonTouchedUpInside), for: .touchUpInside)
        return installButton
    }()
    private let releaseNotesButton: UIButton = {
        let releaseNotesButton = UIButton()
        releaseNotesButton.setAttributedTitle(Button.style(as: .regularBlackUnderlined, text: NSLocalizedString("software_update_available_button_2_title_uc", value: "RELEASE NOTES", comment: "RELEASE NOTES")), for: .normal)
        releaseNotesButton.addTarget(self, action:#selector(releaseNotesButtonButtonTouchedUpInside), for: .touchUpInside)
        return releaseNotesButton
    }()
    @objc private func releaseNotesButtonButtonTouchedUpInside() {
        viewModel?.releaseNotesTouched()
    }
    @objc private func installButtonTouchedUpInside() {
        viewModel?.installButtonTouched()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("software_update_title_uc", value: "FIRMWARE UPDATE", comment: "FIRMWARE UPDATE")))
        let headerText = NSLocalizedString("software_update_available_headline_uc", value: "THERE IS A FIRMWARE UPDATE AVAILABLE", comment: "THERE IS A FIRMWARE UPDATE AVAILABLE")
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: headerText)
        let descriptionText = NSLocalizedString("software_update_available_paragraph", value: " ", comment: "#MISSING#")
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: descriptionText)
        let batteryText = NSLocalizedString("software_update_available_battery_warning", value: " ", comment: "#MISSING#")
        batteryInfoLabel.attributedText = Paragraph.style(as: .smallLight, text: batteryText)
        [headerLabel, descriptionLabel, batteryInfoLabel].forEach {
            $0.textAlignment = .center
        }
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    private func setupConstraints() {
        [headerLabel, descriptionLabel, batteryInfoLabel, installButton, releaseNotesButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.headerBigBottomMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            batteryInfoLabel.bottomAnchor.constraint(equalTo: installButton.topAnchor, constant: -Dimension.headerBigBottomMargin),
            batteryInfoLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            batteryInfoLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            releaseNotesButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            releaseNotesButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            releaseNotesButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            releaseNotesButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            installButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            installButton.bottomAnchor.constraint(equalTo: releaseNotesButton.topAnchor, constant: -Dimension.edgeMargin),
            installButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            installButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
        ])
    }
}
extension UpdateViewController: UpdateViewModelViewInputDelegate {
    func updateInstallButtonState(enable: Bool) {
        installButton.isEnabled = enable
        let color = enable ? Colors.black : Colors.greyDark
        installButton.backgroundColor = color
    }
    func updateInfoLabelState(isHidden: Bool) {
        batteryInfoLabel.isHidden = isHidden
    }
}
