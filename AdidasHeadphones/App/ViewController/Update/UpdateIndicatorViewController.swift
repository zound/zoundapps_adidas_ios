//
//  UpdateIndicatorViewController.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 07/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

class UpdateIndicatorViewController: BaseViewController {
    var viewModel: UpdateIndicatorViewModel?

    private let headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.numberOfLines = .zero
        headerLabel.textAlignment = .center
        return headerLabel
    }()
    private let descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    private let indicatorLabel: UILabel = {
        let indicatorLabel = UILabel()
        indicatorLabel.textAlignment = .center
        indicatorLabel.numberOfLines = .zero
        return indicatorLabel
    }()
    private let spinnerView: UIImageView = {
        let waitingIndicatorImageView = UIImageView(image: Image.Indicators.spinnerDark)
        waitingIndicatorImageView.contentMode = .center
        waitingIndicatorImageView.isHidden = true
        return waitingIndicatorImageView
    }()
    private let doneButton: UIButton = {
        let doneButton = UIButton()
        doneButton.setAttributedTitle(Button.style(as: .regularWhite, text: NSLocalizedString("appwide_done_uc", value: "INSTALL NOW", comment: "INSTALL NOW")), for: .normal)
        doneButton.backgroundColor = Colors.black
        doneButton.addTarget(self, action:#selector(doneButtonTouchedUpInside), for: .touchUpInside)
        return doneButton
    }()
    @objc private func doneButtonTouchedUpInside() {
        viewModel?.doneButtonTouched()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("software_update_title_uc", value: "FIRMWARE UPDATE", comment: "FIRMWARE UPDATE")))
        let indicatorText = NSLocalizedString("software_update_installing_description", value: "Installing update…", comment: "#MISSING#")
        indicatorLabel.attributedText = Paragraph.style(as: .smallLight, text: indicatorText)
        [headerLabel, descriptionLabel, indicatorLabel].forEach {
            $0.textAlignment = .center
        }
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    func updateIndicator(animating: Bool) {
        if animating {
            if !spinnerView.containsAnimation(){
                spinnerView.rotate()
            }
        } else {
            if spinnerView.containsAnimation() {
                spinnerView.stopAnimating()
            }
        }
        spinnerView.isHidden = !animating
    }
    private func setupConstraints() {
        [headerLabel, descriptionLabel, indicatorLabel, spinnerView, doneButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.tileBigHeight),
            headerLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -Dimension.edgeMargin),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            descriptionLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: -Dimension.headerBigBottomMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            indicatorLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.screenHeight28),
            indicatorLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            indicatorLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            doneButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            doneButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            doneButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            doneButton.heightAnchor.constraint(equalToConstant: Dimension.buttonHeight),
            spinnerView.topAnchor.constraint(equalTo: indicatorLabel.bottomAnchor, constant: Dimension.headerBigBottomMargin),
            spinnerView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
        ])
    }
}
extension UpdateIndicatorViewController: UpdateIndicatorViewModelViewInputDelegate {
    func updateHeaderText(isFinished: Bool) {
        let headerText : String
        if isFinished {
            headerText = NSLocalizedString("software_update_complete_headline_uc", value: "UPDATE COMPLETE", comment: "UPDATE COMPLETE")
        } else {
            headerText = NSLocalizedString("software_update_installing_headline_uc", value: "UPDATING YOUR HEADPHONES…", comment: "UPDATING YOUR HEADPHONES…")
        }
        headerLabel.attributedText = Headline.style(as: .largeBlack, text: headerText)
        headerLabel.textAlignment = .center
    }
    func updateDescriptionText(isFinished: Bool) {
        let descriptionText : String
        if isFinished {
            descriptionText = NSLocalizedString("software_update_complete_paragraph", value: " ", comment: "#MISSING#")
        } else {
            descriptionText = NSLocalizedString("software_update_available_paragraph", value: " ", comment: "#MISSING#")
        }
        descriptionLabel.attributedText = Paragraph.style(as: .regularLight, text: descriptionText)
        descriptionLabel.textAlignment = .center
    }
    func updateDoneButtonState(isHidden: Bool) {
        doneButton.isHidden = isHidden
    }
    func updateIndicatorState(isHidden: Bool) {
        updateIndicator(animating: !isHidden)
        indicatorLabel.isHidden = isHidden
    }
}

