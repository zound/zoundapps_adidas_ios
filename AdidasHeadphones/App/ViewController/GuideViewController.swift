//
//  GuideViewController.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

final class GuideViewController: BaseCollectionViewController {
    public var viewModel: GuideViewModel?
    private var disposeBag = DisposeBag()
    private var dynamicItemSize = CGSize.zero
    private var pageControlCurrentPage = Int.zero {
        didSet {
            guard pageControlCurrentPage != oldValue else { return }
            pageControl.currentPage = pageControlCurrentPage
            handleButtonsUpdate()
        }
    }
    var guideItems: [GuideItem] = []
    private let previousButton: UIButton = {
        let previousButton = UIButton(type: .system)
        previousButton.setImage(Image.Icon.Black.arrowLeft, for: .normal)
        previousButton.tintColor = Colors.black
        previousButton.isHidden = true
        previousButton.addTarget(self, action: #selector(previousButtonTouchedUpInside), for: .touchUpInside)
        return previousButton
    }()
    private let nextButton: UIButton = {
        let nextButton = UIButton(type: .system)
        nextButton.setImage(Image.Icon.White.arrowRight, for: .normal)
        nextButton.backgroundColor = Colors.black
        nextButton.tintColor = Colors.white
        nextButton.isHidden = true
        nextButton.addTarget(self, action: #selector(nextButtonTouchedUpInside), for: .touchUpInside)
        return nextButton
    }()
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPage = Int.zero
        pageControl.numberOfPages = guideItems.count
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.isUserInteractionEnabled = false
        pageControl.isHidden = true
        return pageControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(GuideCollectionViewCell.self, forCellWithReuseIdentifier: GuideCollectionViewCell.id)
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        automaticallyAdjustsScrollViewInsets = false
        setupConstraints()
        dynamicItemSize = CGSize(width: view.frame.width, height: view.frame.height)
        setupInternetConnectionHandling()
    }
    private func setupInternetConnectionHandling(){
        guard let viewModel = viewModel else {
            return
        }
        if !viewModel.isNewsletterScreenDisable {
            NoInternetBehavior(parentViewController: self,
                               trigger: viewModel.reachabilityMonitor(),
                               toggleableItems: [])
        }
    }
    override func viewWillAppear(_ animated: Bool) { }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.viewDidAppear()
    }
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        pageControlCurrentPage = Int(targetContentOffset.pointee.x / view.frame.width)
    }
    private func setupConstraints() {
        [pageControl, previousButton, nextButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.safeAreaBottomInset),
            pageControl.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pageControl.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pageControl.heightAnchor.constraint(equalToConstant: Dimension.pageControlHeight),
            previousButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            previousButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            previousButton.widthAnchor.constraint(equalToConstant: Dimension.squareButtonMargin),
            previousButton.heightAnchor.constraint(equalToConstant: Dimension.squareButtonMargin),
            nextButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            nextButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Dimension.edgeMargin - Dimension.safeAreaBottomInset),
            nextButton.widthAnchor.constraint(equalToConstant: Dimension.squareButtonMargin),
            nextButton.heightAnchor.constraint(equalToConstant: Dimension.squareButtonMargin),
        ])
    }
    private func handleButtonsUpdate() {
        let nextButtonImage = pageControlCurrentPage == guideItems.count - 1 ? Image.Icon.White.check : Image.Icon.White.arrowRight
        if pageControlCurrentPage != guideItems.count - 1 {
            nextButton.isEnabled = true
        } else {
            if let viewModel = viewModel {
                nextButton.isEnabled = viewModel.isNewsletterScreenDisable || viewModel.checkNewsletterStatus()
            }
        }
        UIView.transition(with: nextButton, duration: .t025, options: .transitionCrossDissolve, animations: { [weak self] in self?.nextButton.setImage(nextButtonImage, for: .normal)
        })
        let previousButtonIsHidden = pageControlCurrentPage == Int.zero
        UIView.transition(with: previousButton, duration: .t025, options: .transitionCrossDissolve, animations: { [weak self] in self?.previousButton.isHidden = previousButtonIsHidden
        })
    }
    @objc private func previousButtonTouchedUpInside() {
        let nextIndex = max(pageControl.currentPage - 1, Int.zero)
        pageControlCurrentPage = nextIndex
        collectionView?.scrollToItem(at: IndexPath(item: nextIndex, section: Int.zero), at: .centeredHorizontally, animated: true)
    }
    func removeAll() {
        [pageControl, previousButton, nextButton].forEach {
            $0.isHidden = true
        }
        collectionView.performBatchUpdates({
            let indexPaths = guideItems.enumerated().map {
                IndexPath(row: $0.offset, section: Int.zero)
            }
            guideItems.removeAll()
            collectionView.deleteItems(at: indexPaths)
        })
    }
    @objc private func nextButtonTouchedUpInside() {
        if pageControlCurrentPage == guideItems.count - 1 {
            viewModel?.didTapNextButtonAtLastCell()
        } else {
            let nextIndex = min(pageControl.currentPage + 1, guideItems.count - 1)
            pageControlCurrentPage = nextIndex
            collectionView?.scrollToItem(at: IndexPath(item: nextIndex, section: Int.zero), at: .centeredHorizontally, animated: true)
        }
    }
}

extension GuideViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat()
    }
    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return guideItems.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let viewModel = viewModel else { return UICollectionViewCell() }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GuideCollectionViewCell.id, for: indexPath) as! GuideCollectionViewCell
        let guideItem = guideItems[indexPath.item]
        cell.guideItem = guideItem
        guard guideItem.newsletterVisible == true else {
            return cell
        }
        cell.configure(viewModel: viewModel.newsletterViewModel)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: dynamicItemSize.width, height: dynamicItemSize.height)
    }
}

extension GuideViewController: GuideViewModelViewInputDelegate {
    func scrolling(enabled: Bool) {
        collectionView.isScrollEnabled = enabled
    }
    func concludeGuideButton(enabled: Bool) {
        guard pageControlCurrentPage == guideItems.count - 1 else {
            return
        }
        nextButton.isEnabled = enabled
    }
    func add(guideItems: [GuideItem]) {
        self.guideItems = guideItems
        pageControl.numberOfPages = guideItems.count
        collectionView.performBatchUpdates({
            collectionView.insertItems(at: guideItems.enumerated().map { IndexPath(row: $0.offset, section: Int.zero)})
        }, completion: { _ in
            [self.pageControl, self.nextButton].forEach { view in
                UIView.transition(with: view, duration: .t025, options: .transitionCrossDissolve, animations: {
                    view.isHidden = false
                })
            }
        })
    }
}
