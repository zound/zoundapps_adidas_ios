//
//  QualityProgramViewController.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

import UIKit

class QualityProgramViewController: BaseViewController {
    var viewModel: QualityProgramViewModel?
    private lazy var firstHeaderLabel: UILabel = {
        let firstHeaderLabel = UILabel()
        firstHeaderLabel.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("anonymous_data_headline1_uc", value: "DESCRIPTION", comment: "Description"))
        firstHeaderLabel.textAlignment = .center
        firstHeaderLabel.numberOfLines = .zero
        firstHeaderLabel.setContentCompressionResistancePriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.vertical)

        return firstHeaderLabel
    }()
    private lazy var firstParagraphLabel: UILabel = {
        let firstParagraphLabel = UILabel()
        firstParagraphLabel.numberOfLines = .zero
        firstParagraphLabel.attributedText = Paragraph.style(as: .regularLight, text: NSLocalizedString("anonymous_data_paragraph1", value: "Partake in the quality program. Anonymous usage statistics will be sent to help Adidas to improve this product.", comment: "Partake in the quality program. Anonymous usage statistics will be sent to help Adidas to improve this product."))
        return firstParagraphLabel
    }()
    private lazy var secondHeaderLabel: UILabel = {
        let secondHeaderLabel = UILabel()
        secondHeaderLabel.attributedText = Headline.style(as: .largeBlack, text: NSLocalizedString("anonymous_data_headline2_uc", value: "EXAMPLES OF STATISTICS:", comment: "Example of statistics"))
        secondHeaderLabel.textAlignment = .center
        secondHeaderLabel.numberOfLines = .zero
        secondHeaderLabel.setContentCompressionResistancePriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.vertical)
        secondHeaderLabel.setContentCompressionResistancePriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.horizontal)
        return secondHeaderLabel
    }()
    private lazy var secondParagraphLabel: UILabel = {
        let secondParagraphLabel = UILabel()
        secondParagraphLabel.numberOfLines = .zero
        secondParagraphLabel.attributedText = Paragraph.style(as: .regularLight, text: NSLocalizedString("anonymous_data_paragraph2", value: "Feature intaractions to make the most used features easier and better.\n\nConnection to other devices to better understand what products to support.\n\nSystem stability to prevent future crashes and system failure.", comment: "Feature intaractions to make the most used features easier and better.\n\nConnection to other devices to better understand what products to support.\n\nSystem stability to prevent future crashes and system failure."))
        return secondParagraphLabel
    }()
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.backgroundColor = UIColor.clear
        return scrollView
    }()
    private let checkBox = RectangularSwitch()
    private lazy var rectangularSwitchComponentView: UIStackView = {
        checkboxStyle(checkBox)
        checkBox.thumbSize = Dimension.checkboxThumbSize
        checkBox.setupUI()
        checkBox.addTarget(self,
                           action: #selector(toggleCheckBox),
                           for: [.valueChanged])
        checkBox.isOn = true
        let description = UILabel(frame: .zero)
        let descriptionText = NSLocalizedString("anonymous_data_accept", value: "I partake in the Quality program", comment: "I partake in the Quality program")

        newsLetterLabelStyle(text: descriptionText)(description)
        description.numberOfLines = 0
        description.setContentCompressionResistancePriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.horizontal)
        let stackView = UIStackView(arrangedSubviews: [checkBox, description])
        rootHorizontalStackViewStyle(stackView)
        return stackView

    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: Headline.style(as: .smallDark, text: NSLocalizedString("anonymous_data_title_uc", value: "QUALITY PROGRAM", comment: "Quality program")))
        setupConstraints()
        viewModel?.viewDidLoad()
    }
    func setup(permissions: Bool) {
        checkBox.isOn = permissions
    }
    @objc func toggleCheckBox(){
        viewModel?.setAnonymousDataPermissions(accepted: checkBox.isOn)
    }
    private func setupConstraints() {
        [firstHeaderLabel, firstParagraphLabel, secondHeaderLabel, secondParagraphLabel, rectangularSwitchComponentView].forEach {
            scrollView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        fullyContained(in: view)(scrollView)

        NSLayoutConstraint.activate([
            firstHeaderLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            firstHeaderLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            firstParagraphLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            firstParagraphLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            secondHeaderLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            secondHeaderLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            secondParagraphLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            secondParagraphLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            rectangularSwitchComponentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            rectangularSwitchComponentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            firstHeaderLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: Dimension.headerBigBottomMargin),
            firstParagraphLabel.topAnchor.constraint(equalTo: firstHeaderLabel.bottomAnchor, constant: Dimension.headerSmallBottomMargin),
            secondHeaderLabel.topAnchor.constraint(equalTo: firstParagraphLabel.bottomAnchor, constant: Dimension.headerTopMargin),
            secondParagraphLabel.topAnchor.constraint(equalTo: secondHeaderLabel.bottomAnchor, constant: Dimension.headerSmallBottomMargin),
            rectangularSwitchComponentView.topAnchor.constraint(equalTo: secondParagraphLabel.bottomAnchor, constant: Dimension.headerTopMargin),
            rectangularSwitchComponentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
            ])
    }
}

extension QualityProgramViewController: QualityProgramViewModelViewInputDelegate {

}
