//
//  SettingsViewModel.swift
//  002
//
//  Created by Łukasz Wudarski on 09/04/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import Foundation

protocol SettingsViewModelViewInputDelegate: class {
    func update(deviceName: String)
}
protocol SettingsViewModelViewOutputDelegate {
    func viewDidLoad()
    func selected(entry: SettingsEntry)
}
protocol SettingsViewModelFlowInputDelegate {
}
protocol SettingsViewModelFlowOutputDelegate: class {
    func didRequestSetting(entry: SettingsEntry)
}

class SettingsViewModel: BaseViewModel, SettingsViewModelFlowInputDelegate, SettingsViewModelViewOutputDelegate {
    weak var flowOutputDelegate: SettingsViewModelFlowOutputDelegate?
    weak var viewInputDelegate: SettingsViewModelViewInputDelegate?
    private let services: ServicesType
    init(flowOutputDelegate: SettingsViewModelFlowOutputDelegate, viewInputDelegate: SettingsViewModelViewInputDelegate, services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }

    func viewDidLoad() {
        viewInputDelegate?.update(deviceName: self.services.rxConnectionService.knownDevice?.name ?? String.invisible)
    }
    func selected(entry: SettingsEntry) {
        flowOutputDelegate?.didRequestSetting(entry: entry)
    }
}

