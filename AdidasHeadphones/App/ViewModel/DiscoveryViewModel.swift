//
//  DiscoveryViewModel.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import Foundation
import RxSwift
import UIKit // TODO: remove
import ConnectionService

private struct Const {
    static let dispatchTime: TimeInterval = 1.0
}

protocol DiscoveryViewModelViewInputDelegate: class {
    func add(discoveryItem: DiscoveryItem, at index: Int)
    func showUnableToConnect()
    func clearDiscoveryItems()
    func restartLinearIndicator()
    func indicateSearchingViewHeader()

}
protocol DiscoveryViewModelViewOutputDelegate {
    func circleSliderDragged(for index: Int)
    func discoveryItemsClearedAutomaically()
    func viewDidLoad()
    func viewDidAppear()
    func appWillResignActive()
    func appDidBecomeActive()

}
protocol DiscoveryViewModelFlowInputDelegate {
}
protocol DiscoveryViewModelFlowOutputDelegate: class {
    func didConnectDevice(reading pairingInfo: PairingInfo, deviceType: DeviceType)
}

class DiscoveryViewModel: BaseViewModel, DiscoveryViewModelFlowInputDelegate, DiscoveryViewModelViewOutputDelegate {
    weak var flowOutputDelegate: DiscoveryViewModelFlowOutputDelegate?
    weak var viewInputDelegate: DiscoveryViewModelViewInputDelegate?
    private let services: ServicesType
    private let onboardingType: OnboardingType
    private var unexpectedDisconnectionHappened: Bool = false
    private let disposeBag = DisposeBag()
    init(flowOutputDelegate: DiscoveryViewModelFlowOutputDelegate,
         viewInputDelegate: DiscoveryViewModelViewInputDelegate,
         services: ServicesType,
         onboardingType: OnboardingType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        self.onboardingType = onboardingType
        super.init()
        self.services.rxConnectionService.didDiscoverPeripheralAtIndex.subscribe(onNext: { [weak self] index in
            print("DiscoveryViewModel::didDiscoverPeripheralAtIndex")
            let image = services.rxConnectionService.discoveredDevices[index].deviceTypeWithTraits.discoveredDeviceImage
            let name = services.rxConnectionService.discoveredDevices[index].name
            self?.viewInputDelegate?.add(discoveryItem: DiscoveryItem(image: image, name: name), at: index)
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didEstablishPeripheralConnectionToReactiveDevice.subscribe(onNext: { [weak self] reactiveDevice in
            print("DiscoveryViewModel::didEstablishPeripheralConnectionToReactiveDevice")
            self?.didEstablishPeripheralConnection(to: reactiveDevice)
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didFailToEstablishPeripheralConnection.subscribe(onNext: { [weak self] in
            print("DiscoveryViewModel::didFailToEstablishPeripheralConnection")
            self?.viewInputDelegate?.showUnableToConnect()
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didCancelPeripheralConnection.subscribe(onNext: { [weak self] in
            print("DiscoveryViewModel::didCancelPeripheralConnection")
            DispatchQueue.main.asyncAfter(deadline: .now() + Const.dispatchTime) { [weak self] in
                if self?.unexpectedDisconnectionHappened == false {
                    self?.startDiscoveringIfNeeded()
                }
            }
        }).disposed(by: disposeBag)
    }
    private func didEstablishPeripheralConnection(to reactiveDevice: ReactiveDevice) {
        reactiveDevice.monitoredPeripheralConnectionState.subscribe(onNext: { [weak self] peripheralConnectionState in
            if peripheralConnectionState == .connected {
                self?.checkPairing(using: reactiveDevice)
            } else if peripheralConnectionState == .disconnected {
                self?.unexpectedDisconnectionHappened = true
                print("DiscoveryViewModel::cancelPeripheralConnection(), DISCONNECTED")
                self?.services.rxConnectionService.cancelPeripheralConnection()
                self?.viewInputDelegate?.showUnableToConnect()
            }
        }).disposed(by: self.disposeBag)
    }
    private func handleDidConnectDevice(for  reactiveDevice: ReactiveDevice, with pairingInfo:PairingInfo){
        flowOutputDelegate?.didConnectDevice(reading: pairingInfo, deviceType: reactiveDevice.deviceTypeWithTraits.deviceType)
    }
    private func checkPairing(using reactiveDevice: ReactiveDevice) {
        Observable.combineLatest(reactiveDevice.bluetoothClassicPairingStatus, reactiveDevice.bluetoothClassicPairingMode,
            resultSelector: { [weak self] bluetoothClassicPairingStatusResult, bluetoothClassicPairingModeResult in
                switch (bluetoothClassicPairingStatusResult, bluetoothClassicPairingModeResult) {
                case (.success(let bluetoothClassicPairingStatus), .success(let bluetoothClassicPairingMode)):
                    let isAccessoryConnected = self?.services.rxConnectionService.isConnected(accessoryWith: reactiveDevice.mac) ?? false
                    let pairingInfo = PairingInfo(bluetoothClassicPairingStatus, bluetoothClassicPairingMode, isAccessoryConnected)
                    self?.handleDidConnectDevice(for: reactiveDevice, with: pairingInfo)
                default:
                    fatalError() // TODO: Handle .error
                }
            }
        ).observeOn(MainScheduler.instance)
            .subscribe()
            .disposed(by: disposeBag)
        reactiveDevice.read(deviceFeature: .bluetoothClassicPairingStatus)
        reactiveDevice.read(deviceFeature: .bluetoothClassicPairingMode)
    }
    private func startDiscoveringIfNeeded() {
        if services.rxConnectionService.canStartDiscovering {
            print("DiscoveryViewModel::startDiscovering()")
            services.rxConnectionService.startDiscovering()
        }
    }
    private func stopDiscoveringIfNeeded() {
        if services.rxConnectionService.canStopDiscovering {
            print("DiscoveryViewModel::stopDiscovering()")
            services.rxConnectionService.stopDiscovering()
        }
    }
}

extension DiscoveryViewModel {
    func circleSliderDragged(for index: Int) {
        print("DiscoveryViewModel::stopDiscovering(selecting:)")
        services.rxConnectionService.stopDiscovering(selecting: services.rxConnectionService.discoveredDevices[index])
        print("DiscoveryViewModel::establishPeripheralConnection()")
        services.rxConnectionService.establishPeripheralConnection()
    }
    func discoveryItemsClearedAutomaically() {
        unexpectedDisconnectionHappened = false
        startDiscoveringIfNeeded()
    }
    func viewDidLoad() {
        services.applicationModeService.appDidBecomeActive.subscribe(onNext: { [weak self] _ in
            self?.appDidBecomeActive()
        }).disposed(by: disposeBag)
        services.applicationModeService.appWillResignActive.subscribe(onNext: { [weak self]_ in
            self?.appWillResignActive()
        }).disposed(by: disposeBag)
    }
    func viewDidAppear() {
        if onboardingType == .initialDeviceSetup {
            viewInputDelegate?.indicateSearchingViewHeader()
        }
        if onboardingType == .addNewDevice {
            print("DiscoveryViewModel::cancelPeripheralConnection(), ONBOARDING ADD NEW DEVICE")
            services.rxConnectionService.cancelPeripheralConnection()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + Const.dispatchTime) { [weak self] in
                self?.startDiscoveringIfNeeded()
            }
        }
    }
    @objc func appWillResignActive() {
        stopDiscoveringIfNeeded()
        viewInputDelegate?.clearDiscoveryItems()
    }
    @objc func appDidBecomeActive() {
        startDiscoveringIfNeeded()
        viewInputDelegate?.restartLinearIndicator()
    }
}
