//
//  TermsAndCondViewModel.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

protocol TermsAndCondViewModelViewInputDelegate: class {
}
protocol TermsAndCondViewModelViewOutputDelegate {
}
protocol TermsAndCondViewModelFlowInputDelegate {
}
protocol TermsAndCondViewModelFlowOutputDelegate: class {
}

class TermsAndCondViewModel: BaseViewModel, TermsAndCondViewModelFlowInputDelegate, TermsAndCondViewModelViewOutputDelegate {
    weak var flowOutputDelegate: TermsAndCondViewModelFlowOutputDelegate?
    weak var viewInputDelegate: TermsAndCondViewModelViewInputDelegate?
    private let services: ServicesType
    init(flowOutputDelegate: TermsAndCondViewModelFlowOutputDelegate,
         viewInputDelegate: TermsAndCondViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
}

extension TermsAndCondViewModel {

}
