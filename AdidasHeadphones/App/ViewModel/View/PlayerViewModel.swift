//
//  PlayerViewModel.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 30/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import ConnectionService

enum UIAudioControlStatus {
    case isPaused
    case isPlaying
    case unknown
    init(_ audioControlStatus: AudioControlStatus) {
        switch audioControlStatus {
        case .isPaused:
            self = .isPaused
        case .isPlaying:
            self = .isPlaying
        }
    }
}

struct UIAudioNowPlaying {
    let title: String?
    let artistName: String?
    init?(_ audioNowPlaying: AudioNowPlaying) {
        if case .audioNowPlaying(info: let info) = audioNowPlaying,
           let title = info[.title],
           let artistName = info[.artistName],
           !(title.isEmpty && artistName.isEmpty) {
            self.title = title.isEmpty ? nil : info[.title]
            self.artistName = artistName.isEmpty ? nil : info[.artistName]
        } else {
            return nil
        }
    }
}

protocol PlayerViewModelViewInputDelegate: class {
    func update(controlState: ControlState)
    func update(uiAudioControlStatus: UIAudioControlStatus)
    func update(uiAudioNowPlaying: UIAudioNowPlaying?)
}
protocol PlayerViewModelViewOutputDelegate {
    func skipBackwardButtonTouchedUpInside()
    func playPauseButtonTouchedUpInside()
    func skipForwardButtonTouchedUpInside()
}
protocol PlayerViewModelSuperviewInputDelegate {
}
protocol PlayerViewModelSuperviewOutputDelegate: class {
}

class PlayerViewModel: BaseViewModel, PlayerViewModelSuperviewInputDelegate, PlayerViewModelViewOutputDelegate {
    weak var superviewOutputDelegate: PlayerViewModelSuperviewOutputDelegate?
    weak var viewInputDelegate: PlayerViewModelViewInputDelegate?
    private let services: ServicesType
    private var currentAudioControlStatus: AudioControlStatus = .isPaused {
        didSet {
            viewInputDelegate?.update(uiAudioControlStatus: UIAudioControlStatus(currentAudioControlStatus))
        }
    }
    private var currentAudioNowPlaying: AudioNowPlaying = .noAudioPlaying {
        didSet {
            viewInputDelegate?.update(uiAudioNowPlaying: UIAudioNowPlaying(currentAudioNowPlaying))
        }
    }
    private var monitoredPeripheralConnectionStateSubscription: Disposable?
    private var audioControlStatusSubscription: Disposable?
    private var monitoredAudioControlStatusSubscription: Disposable?
    private var audioNowPlayingSubscription: Disposable?
    private var monitoredAudioNowPlayingSubscription: Disposable?
    private let disposeBag = DisposeBag()
    init(superviewOutputDelegate: PlayerViewModelSuperviewOutputDelegate,
         viewInputDelegate: PlayerViewModelViewInputDelegate,
         services: ServicesType) {
        self.superviewOutputDelegate = superviewOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.rxConnectionService.didEstablishPeripheralConnectionToReactiveDevice.subscribe(onNext: { reactiveDevice in
            print("PlayerViewModel::didEstablishPeripheralConnectionToReactiveDevice")
            self.disposeAllSubscriptions()
            self.didEstablishPeripheralConnection(to: reactiveDevice)
        }).disposed(by: disposeBag)
    }
}

private extension PlayerViewModel {
    func didEstablishPeripheralConnection(to reactiveDevice: ReactiveDevice) {
        monitoredPeripheralConnectionStateSubscription?.dispose()
        monitoredPeripheralConnectionStateSubscription = reactiveDevice.monitoredPeripheralConnectionState.subscribe(onNext: { [weak self] peripheralConnectionState in
            if peripheralConnectionState == .connected {
                self?.viewInputDelegate?.update(controlState: .enabled)
                self?.didConnect(to: reactiveDevice)
            } else if peripheralConnectionState == .disconnected {
                self?.currentAudioNowPlaying = .noAudioPlaying
                self?.viewInputDelegate?.update(controlState: .disabled)
                self?.disposeAllSubscriptions()
            }
        })
    }
    func didConnect(to reactiveDevice: ReactiveDevice) {
        audioControlStatusSubscription = reactiveDevice.audioControlStatus.subscribe(onNext: { [weak self] audioControlStatusResult in
            switch audioControlStatusResult {
            case .success(let audioControlStatus):
                self?.currentAudioControlStatus = audioControlStatus
                self?.monitoredAudioControlStatusSubscription = reactiveDevice.monitoredAudioControlStatus.subscribe(onNext: { [weak self] audioControlStatus in
                    self?.currentAudioControlStatus = audioControlStatus
                })
                self?.services.rxConnectionService.reactiveDevice?.setAudioControlStatusMonitoring(enabled: true)
            default:
                break
            }
            self?.didReadAudioControlStatus(from: reactiveDevice)
        })
        reactiveDevice.read(deviceFeature: .audioControl)
    }
    func didReadAudioControlStatus(from reactiveDevice: ReactiveDevice) {
        audioNowPlayingSubscription = reactiveDevice.audioNowPlaying.subscribe(onNext: { [weak self] audioNowPlayingResult in
            switch audioNowPlayingResult {
            case .success(let audioNowPlaying):
                self?.currentAudioNowPlaying = audioNowPlaying
                self?.monitoredAudioNowPlayingSubscription = reactiveDevice.monitoredAudioNowPlaying.subscribe(onNext: { [weak self] audioNowPlaying in
                    self?.currentAudioNowPlaying = audioNowPlaying
                })
                self?.services.rxConnectionService.reactiveDevice?.setAudioNowPlayingMonitoring(enabled: true)
            default:
                break
            }
        })
        reactiveDevice.read(deviceFeature: .audioNowPlaying)
    }
    func disposeAllSubscriptions() {
        audioControlStatusSubscription?.dispose()
        monitoredAudioControlStatusSubscription?.dispose()
        audioNowPlayingSubscription?.dispose()
        monitoredAudioNowPlayingSubscription?.dispose()
    }
}


extension PlayerViewModel {
    func skipBackwardButtonTouchedUpInside() {
        print("PlayerViewModel::skipBackward()")
        services.rxConnectionService.reactiveDevice?.write(audioControlEvent: .skipBackward, completion: { _ in })
    }
    func playPauseButtonTouchedUpInside() {
        switch currentAudioControlStatus {
        case .isPaused:
            print("PlayerViewModel::play()")
            services.rxConnectionService.reactiveDevice?.write(audioControlEvent: .play, completion: { [weak self] _ in
                self?.currentAudioControlStatus = .isPlaying
            })
        case .isPlaying:
            print("PlayerViewModel::pause()")
            services.rxConnectionService.reactiveDevice?.write(audioControlEvent: .pause, completion: { [weak self] _ in
                self?.currentAudioControlStatus = .isPaused
            })
        }
    }
    func skipForwardButtonTouchedUpInside() {
        print("PlayerViewModel::skipForward()")
        services.rxConnectionService.reactiveDevice?.write(audioControlEvent: .skipForward, completion: { _ in })
    }

}
