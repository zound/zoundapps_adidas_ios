//
//  TilesViewModel.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 21/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

import RxSwift
import ConnectionService

private struct Const {
    static let actionButtonConfigurationNotSetText = NSLocalizedString("home_tile_action_button_not_set_uc", value: "NOT SET", comment: "NOT SET")
    static let actionButtonConfigurationGoogleText = NSLocalizedString("home_tile_action_button_google_uc", value: "GOOGLE", comment: "GOOGLE")
    static let actionButtonConfigurationCustomText = NSLocalizedString("home_tile_action_button_custom_uc", value: "CUSTOM", comment: "CUSTOM")
    static let statusConnectedText = NSLocalizedString("home_tile_status_connected_uc", value: "CONNECTED", comment: "CONNECTED")
    static let statusNotConnectedText = NSLocalizedString("home_tile_status_not_connected_uc", value: "NOT CONNECTED", comment: "NOT CONNECTED")
    static let statusWaitDispatchTime: DispatchTimeInterval = .milliseconds(100)
    static let statusInitDispatchTime: DispatchTimeInterval = .milliseconds(600)
}

protocol TilesViewModelViewInputDelegate: class {
    func update(uiBatteryLevel: UIBatteryLevel)
    func update(actionButton: TileValue)
    func update(equalizer: TileValue)
//    func update(autoOffTimer: TileValue)
    func update(status: TileValue)
    func update(isActive: Bool)

}
protocol TilesViewModelViewOutputDelegate {
    func collectionViewWillDisplayStatusTile()
    func didSelect(tile: Tile)
}
protocol TilesViewModelSuperviewInputDelegate {
}
protocol TilesViewModelSuperviewOutputDelegate: class {
    func didSelect(tile: Tile)
}

class TilesViewModel: BaseViewModel, TilesViewModelSuperviewInputDelegate, TilesViewModelViewOutputDelegate {
    weak var superviewOutputDelegate: TilesViewModelSuperviewOutputDelegate?
    weak var viewInputDelegate: TilesViewModelViewInputDelegate?
    private let services: ServicesType
    private var monitoredPeripheralConnectionStateSubscription: Disposable?
    private var monitoredBatteryLevelSubscription: Disposable?
    private var monitoredEqualizerSubscription: Disposable?
    private var monitoredActionButtonConfigurationSubscription: Disposable?
    private var batteryLevelSubscription: Disposable?
    private var equalizerSubscription: Disposable?
    private var actionButtonConfigurationSubscription: Disposable?
    private let disposeBag = DisposeBag()
    private var currentUIBatteryLevel: UIBatteryLevel = .unknown {
        didSet {
            guard currentUIBatteryLevel != oldValue else { return }
            viewInputDelegate?.update(uiBatteryLevel: currentUIBatteryLevel)
        }
    }
    private var currentEqualizerTileValue: TileValue = .notSet {
        didSet {
            guard currentEqualizerTileValue != oldValue else { return }
            viewInputDelegate?.update(equalizer: currentEqualizerTileValue)
        }
    }
    private var currentActionButtonTileValue: TileValue = .notSet {
        didSet {
            guard currentActionButtonTileValue != oldValue else { return }
            viewInputDelegate?.update(actionButton: currentActionButtonTileValue)
        }
    }
    private var currentStatusTileValue: TileValue = .notSet {
        didSet {
            guard currentStatusTileValue != oldValue else { return }
            viewInputDelegate?.update(status: currentStatusTileValue)
        }
    }
    private var isCurrentStatusTileValueUpdateable = false
    init(superviewOutputDelegate: TilesViewModelSuperviewOutputDelegate,
         viewInputDelegate: TilesViewModelViewInputDelegate,
         services: ServicesType) {
        self.superviewOutputDelegate = superviewOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.rxConnectionService.didEstablishPeripheralConnectionToReactiveDevice.subscribe(onNext: { reactiveDevice in
            print("TilesViewModel::didEstablishPeripheralConnectionToReactiveDevice")
            self.disposeAllSubscriptions()
            self.didEstablishPeripheralConnection(to: reactiveDevice)
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didChangeAccessoryState.subscribe(onNext: { [weak self] accessoryInfo in
            guard self?.isCurrentStatusTileValueUpdateable == true else {
                return
            }
            if accessoryInfo.mac == self?.services.rxConnectionService.knownDevice?.mac {
                self?.setStatusTileValue(connected: accessoryInfo.connected)
            }
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.setAccessoriesMonitoring(enabled: true) // NOTE: The only place when it should be called
        self.services.applicationModeService.appDidBecomeActive.subscribe(onNext: { [weak self] _ in
            self?.initStatusTileValue()
        }).disposed(by: disposeBag)
    }
}

private extension TilesViewModel {
    func didEstablishPeripheralConnection(to reactiveDevice: ReactiveDevice) {
        monitoredPeripheralConnectionStateSubscription?.dispose()
        monitoredPeripheralConnectionStateSubscription = reactiveDevice.monitoredPeripheralConnectionState.subscribe(onNext: { [weak self] peripheralConnectionState in
            switch peripheralConnectionState {
            case .disconnected:
                self?.viewInputDelegate?.update(isActive: false)
                self?.currentUIBatteryLevel = .unknown
                self?.currentActionButtonTileValue = .notSet
                self?.currentEqualizerTileValue = .notSet
                self?.disposeAllSubscriptions()
            case .connecting:
                self?.currentActionButtonTileValue = .setting
                self?.currentEqualizerTileValue = .setting
            case .connected:
                self?.viewInputDelegate?.update(isActive: true)
                self?.didConnect(to: reactiveDevice)
            }
        })
    }
    func didConnect(to reactiveDevice: ReactiveDevice) {
        handleBatteryLevel(for: reactiveDevice)
        handleActionButtonTileValue(for: reactiveDevice)
        handleEqualizerTileValue(for: reactiveDevice)

    }
    func handleBatteryLevel(for reactiveDevice: ReactiveDevice) {
        batteryLevelSubscription = reactiveDevice.batteryLevel.subscribe(onNext: { [weak self] batteryLevelResult in
            switch batteryLevelResult {
            case .success(let batteryLevel):
                self?.currentUIBatteryLevel = UIBatteryLevel(batteryLevel)
                self?.monitoredBatteryLevelSubscription?.dispose()
                self?.monitoredBatteryLevelSubscription = reactiveDevice.monitoredBatteryLevel.subscribe(onNext: { [weak self] batteryLevel in
                    self?.currentUIBatteryLevel = UIBatteryLevel(batteryLevel)
                })
                self?.services.rxConnectionService.reactiveDevice?.setBatteryLevelMonitoring(enabled: true)
            default:
                break
            }
        })
        reactiveDevice.read(deviceFeature: .batteryLevel)
    }
    func handleEqualizerTileValue(for reactiveDevice: ReactiveDevice) {
        equalizerSubscription = reactiveDevice.graphicalEqualizer.subscribe(onNext: { [weak self] graphicalEqualizerResult in
            switch graphicalEqualizerResult {
            case .success(let graphicalEqualizer):
                self?.currentEqualizerTileValue = .set(UIGraphicalEqualizerPreset(graphicalEqualizer).text, color: .white)
                self?.monitoredEqualizerSubscription?.dispose()
                self?.monitoredEqualizerSubscription = reactiveDevice.monitoredGraphicalEqualizer.subscribe(onNext: { [weak self] graphicalEqualizer in
                    self?.currentEqualizerTileValue = .set(UIGraphicalEqualizerPreset(graphicalEqualizer).text, color: .white)
                })
//                self?.services.rxConnectionService.reactiveDevice?.setGraphicalEqualizerMonitoring(enabled: true) // NOTE: Uncomment once needed
            default:
                break
            }
        })
        reactiveDevice.read(deviceFeature: .graphicalEqualizer)
    }
    func handleActionButtonTileValue(for reactiveDevice: ReactiveDevice) {
        func actionButtonTextWithColor(for actionButtonConfiguration: ActionButtonConfiguration) -> (text: String, color: TileValue.Color) {
            switch actionButtonConfiguration {
            case [.singlePress: .noAction, .doublePress: .noAction, .triplePress: .noAction, .longPress: .noAction]:
                return (Const.actionButtonConfigurationNotSetText, .light)
            case [.singlePress: .googleVoiceAssistantBisto, .doublePress: .googleVoiceAssistantBisto, .triplePress: .noAction, .longPress: .googleVoiceAssistantBisto]:
                return (Const.actionButtonConfigurationGoogleText, .white)
            default:
                return (Const.actionButtonConfigurationCustomText, .white)
            }
        }
        actionButtonConfigurationSubscription = reactiveDevice.actionButtonConfiguration.subscribe(onNext: { [weak self] actionButtonConfigurationResult in
            switch actionButtonConfigurationResult {
            case .success(let actionButtonConfiguration):
                let actionButtonTileValue = actionButtonTextWithColor(for: actionButtonConfiguration)
                self?.currentActionButtonTileValue = .set(actionButtonTileValue.text, color: actionButtonTileValue.color)
                self?.monitoredActionButtonConfigurationSubscription?.dispose()
                self?.monitoredActionButtonConfigurationSubscription = reactiveDevice.monitoredActionButtonConfiguration.subscribe(onNext: { [weak self] actionButtonConfiguration in
                    let actionButtonTileValue = actionButtonTextWithColor(for: actionButtonConfiguration)
                    self?.currentActionButtonTileValue = .set(actionButtonTileValue.text, color: actionButtonTileValue.color)
                })
                self?.services.rxConnectionService.reactiveDevice?.setActionButtonConfigurationMonitoring(enabled: true)
            default:
                break
            }
        })
        reactiveDevice.read(deviceFeature: .actionButtonConfiguration)
    }
    func disposeAllSubscriptions() {
        batteryLevelSubscription?.dispose()
        equalizerSubscription?.dispose()
        actionButtonConfigurationSubscription?.dispose()
        monitoredBatteryLevelSubscription?.dispose()
        monitoredEqualizerSubscription?.dispose()
        monitoredActionButtonConfigurationSubscription?.dispose()
    }
    func initStatusTileValue() {
        if let knownDevice = services.rxConnectionService.knownDevice, services.rxConnectionService.isConnected(accessoryWith: knownDevice.mac) {
            setStatusTileValue(connected: true)
        } else {
            setStatusTileValue(connected: false)
        }
        isCurrentStatusTileValueUpdateable = true
    }
    func setStatusTileValue(connected: Bool) {
        currentStatusTileValue = .set(connected ? Const.statusConnectedText : Const.statusNotConnectedText, color: connected ? .white : .light)
    }
}

extension TilesViewModel {
    func collectionViewWillDisplayStatusTile() {
        DispatchQueue.main.asyncAfter(deadline: .now() + Const.statusWaitDispatchTime) { [weak self] in
            self?.currentStatusTileValue = .setting
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + Const.statusInitDispatchTime) { [weak self] in
            self?.initStatusTileValue()
        }
    }
    func didSelect(tile: Tile) {
        superviewOutputDelegate?.didSelect(tile: tile)
    }
}
