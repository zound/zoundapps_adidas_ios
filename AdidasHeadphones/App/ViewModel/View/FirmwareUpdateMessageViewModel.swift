//
//  FirmwareUpdateMessageViewModel.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 31/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol FirmwareUpdateMessageViewModelViewInputDelegate: class {
    func updateMessage(with firmwareUpdateState: FirmwareUpdateState)
    func showAndUpdateMessage(with firmwareUpdateState: FirmwareUpdateState)
    func hideMessage()
    func showFirmwareUpdateActivityView()
    func hideFirmwareUpdateActivityView()
}
protocol FirmwareUpdateMessageViewModelViewOutputDelegate {
    func touchedUpInside()
}
protocol FirmwareUpdateMessageViewModelSuperviewInputDelegate {
}
protocol FirmwareUpdateMessageViewModelSuperviewOutputDelegate: class {
    func didTapShowUpdateScreen()
}

class FirmwareUpdateMessageViewModel: BaseViewModel, FirmwareUpdateMessageViewModelSuperviewInputDelegate, FirmwareUpdateMessageViewModelViewOutputDelegate {
    weak var superviewOutputDelegate: FirmwareUpdateMessageViewModelSuperviewOutputDelegate?
    weak var viewInputDelegate: FirmwareUpdateMessageViewModelViewInputDelegate?
    private let services: ServicesType
    private let disposeBag = DisposeBag()
    private var isMessageVisible = false
    private var firmwareUpdateState: FirmwareUpdateState = .off {
        didSet {
            switch (oldValue, firmwareUpdateState) {
            case (_, .failed):
                if let isOverTheAirReadyToFinalize = services.rxConnectionService.reactiveDevice?.overTheAirInfo.isReadyToFinalize, isOverTheAirReadyToFinalize {
                    viewInputDelegate?.showAndUpdateMessage(with: firmwareUpdateState)
                } else if isMessageVisible {
                    viewInputDelegate?.updateMessage(with: firmwareUpdateState)
                }
            case (_, .started):
                viewInputDelegate?.showFirmwareUpdateActivityView()
            case (_, .off), (_, .completed):
                isMessageVisible = false
                viewInputDelegate?.hideMessage()
                viewInputDelegate?.hideFirmwareUpdateActivityView()
            case (.off, _),
                 (.started, _):
                isMessageVisible = true
                viewInputDelegate?.showAndUpdateMessage(with: firmwareUpdateState)
            case (_, _):
                viewInputDelegate?.updateMessage(with: firmwareUpdateState)
            }
        }
    }
    init(superviewOutputDelegate: FirmwareUpdateMessageViewModelSuperviewOutputDelegate,
         viewInputDelegate: FirmwareUpdateMessageViewModelViewInputDelegate,
         services: ServicesType) {
        self.superviewOutputDelegate = superviewOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.firmwareUpdateService.didChangeFirmwareUpdateState.subscribe(onNext: {[weak self] firmwareUpdateState in
            self?.firmwareUpdateState = firmwareUpdateState
        }).disposed(by: disposeBag)
    }

}

extension FirmwareUpdateMessageViewModel {
    func touchedUpInside() {
        switch firmwareUpdateState {
        case .readyToInstall:
            superviewOutputDelegate?.didTapShowUpdateScreen()
        default:
            break
        }
    }
}
