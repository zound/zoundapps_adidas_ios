//
//  WelcomeViewModel.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

protocol WelcomeViewModelViewInputDelegate: class {
}
protocol WelcomeViewModelViewOutputDelegate {
    func didTapStartSetup()
    func didTapTermsAndCond()
    func didTapQualityProgram()
}
protocol WelcomeViewModelFlowInputDelegate {
}
protocol WelcomeViewModelFlowOutputDelegate: class {
    func didRequestStartSetup()
    func didRequestTermsAndCond()
    func didRequestQualityProgram()
}

class WelcomeViewModel: BaseViewModel, WelcomeViewModelFlowInputDelegate, WelcomeViewModelViewOutputDelegate {
    weak var flowOutputDelegate: WelcomeViewModelFlowOutputDelegate?
    weak var viewInputDelegate: WelcomeViewModelViewInputDelegate?
    private let services: ServicesType
    init(flowOutputDelegate: WelcomeViewModelFlowOutputDelegate,
         viewInputDelegate: WelcomeViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
}

extension WelcomeViewModel {
    func didTapStartSetup() {
        flowOutputDelegate?.didRequestStartSetup()
    }
    func didTapTermsAndCond() {
        flowOutputDelegate?.didRequestTermsAndCond()
    }
    func didTapQualityProgram() {
        flowOutputDelegate?.didRequestQualityProgram()
    }
}
