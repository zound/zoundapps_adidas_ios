//
//  MainViewModel.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import RxSwift
import ConnectionService

enum SessionEvent {
    case start, stop
}

protocol MainViewModelViewInputDelegate: class {
    func initialize(uiPeripheralConnectionState: UIPeripheralConnectionState)
    func update(uiPeripheralConnectionState: UIPeripheralConnectionState)
    func update(deviceName: String, for uiPeripheralConnectionState: UIPeripheralConnectionState)
    func set(deviceImage: UIImage)
    func setProgress(hidden: Bool)
    func setProgress(value: Float)
    func setProgress(label: String)
}
protocol MainViewModelViewOutputDelegate {
    func didTapLeftBarButtonItem()
    func didTapRightBarButtonItem()
    func didTapShowUpdateScreen()
    func viewDidAppear()
    func didSelect(tile: Tile)
}
protocol MainViewModelFlowInputDelegate {
    
}
protocol MainViewModelFlowOutputDelegate: class {
    func didRequestSettings()
    func didRequestKnownDevices()
    func didRequestKnownDevices(selectingKnownDevice UUID: String)
    func didRequestAddNewDeviceForced()
    func didRequestActionButton()
    func didRequestEqualizer()
    func didRequestAutoOffTimer()
    func didRequestStatus()
    func didDisconnectKnownDevice()
    func didRequestUpdateScreen()
}

class MainViewModel: BaseViewModel, MainViewModelFlowInputDelegate, MainViewModelViewOutputDelegate {
    weak var flowOutputDelegate: MainViewModelFlowOutputDelegate?
    weak var viewInputDelegate: MainViewModelViewInputDelegate?
    private let services: ServicesType
    private var deviceFirmwareFailureDetector: DeviceFirmwareFailureDetector? // NOTE: Temporary solution
    private let disposeBag = DisposeBag()
    init(flowOutputDelegate: MainViewModelFlowOutputDelegate,
         viewInputDelegate: MainViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.rxConnectionService.didEstablishPeripheralConnectionToReactiveDevice.subscribe(onNext: { reactiveDevice in
            print("MainViewModel::didEstablishPeripheralConnectionToReactiveDevice")
            self.didEstablishPeripheralConnection(to: reactiveDevice)
        }).disposed(by: disposeBag)
        self.services.applicationModeService.appDidBecomeActive.subscribe(onNext: { _ in
            print("MainViewModel::didBecomeActive")
            if let requestedUUID = self.services.quickActionsService.getUUIDToProcess() {
                self.flowOutputDelegate?.didRequestKnownDevices(selectingKnownDevice: requestedUUID)
            }
        }).disposed(by: disposeBag)
        self.services.firmwareUpdateService.didChangeFirmwareUpdateState.subscribe(onNext: {[weak self] firmwareUpdateState in
            self?.handle(firmwareUpdateState: firmwareUpdateState)
        }).disposed(by: disposeBag)
        self.services.firmwareUpdateService.didChangeProgress.subscribe(onNext: {[weak self] progress in
            self?.handle(progress: progress)
        }).disposed(by: disposeBag)

    }
    func handle(progress: Float) {
        let currentProgress = min(0.99, progress)
        self.viewInputDelegate?.setProgress(value: currentProgress)
        let text = "\(Int(currentProgress * 100))%"
        self.viewInputDelegate?.setProgress(label: text)
    }
    func handle(firmwareUpdateState: FirmwareUpdateState) {
        switch firmwareUpdateState {
        case .uploading:
            self.viewInputDelegate?.setProgress(hidden: false)
        default:
            self.viewInputDelegate?.setProgress(hidden: true)
        }
    }
    private func didEstablishPeripheralConnection(to reactiveDevice: ReactiveDevice) {
        deviceFirmwareFailureDetector = DeviceFirmwareFailureDetector(delegate: self)
        reactiveDevice.monitoredPeripheralConnectionState.debug().subscribe(onNext: { [unowned self] peripheralConnectionState in
            if peripheralConnectionState == .disconnected { // NOTE: Temporary solution
                self.flowOutputDelegate?.didDisconnectKnownDevice()
            }
            self.viewInputDelegate?.update(uiPeripheralConnectionState: UIPeripheralConnectionState(peripheralConnectionState))
            switch peripheralConnectionState {
            case .disconnected, .connecting:
                let deviceName = self.services.rxConnectionService.knownDevice?.name ?? String.invisible
                self.viewInputDelegate?.update(deviceName: deviceName, for: UIPeripheralConnectionState(peripheralConnectionState))
            case .connected:
                let deviceName = self.services.rxConnectionService.reactiveDevice!.name
                self.viewInputDelegate?.update(deviceName: deviceName, for: UIPeripheralConnectionState(peripheralConnectionState))
                self.services.actionButtonService.configure(with: self.services.rxConnectionService.reactiveDevice)
            }
            self.deviceFirmwareFailureDetector?.register(peripheralConnectionState)
            guard let knownDeviceTraits = self.services.rxConnectionService.knownDevice?.deviceTypeWithTraits else { return }
            self.viewInputDelegate?.set(deviceImage: knownDeviceTraits.knownDeviceImage)
        }).disposed(by: self.disposeBag)
    }
}

extension MainViewModel {
    func didTapLeftBarButtonItem() {
        flowOutputDelegate?.didRequestKnownDevices()
    }
    func didTapRightBarButtonItem() {
        flowOutputDelegate?.didRequestSettings()
    }
    func didTapShowUpdateScreen() {
        flowOutputDelegate?.didRequestUpdateScreen()
    }
    func viewDidAppear() {
        let uiPeripheralConnectionState = UIPeripheralConnectionState(services.rxConnectionService.device?.peripheralConnectionState ?? .disconnected)
        self.viewInputDelegate?.initialize(uiPeripheralConnectionState: uiPeripheralConnectionState)
        let deviceName = self.services.rxConnectionService.knownDevice?.name ?? String.invisible
        self.viewInputDelegate?.update(deviceName: deviceName, for: uiPeripheralConnectionState)
    }
    func didSelect(tile: Tile) {
        switch tile {
        case .batteryLevel:
            break
        case .actionButton:
            flowOutputDelegate?.didRequestActionButton()
        case .equalizer:
            flowOutputDelegate?.didRequestEqualizer()
//        case .autoOffTimer:
//            flowOutputDelegate?.didRequestAutoOffTimer()
        case .status:
            flowOutputDelegate?.didRequestStatus()
        }
    }
    func deviceImage() -> UIImage? {
        return services.rxConnectionService.knownDevice?.deviceTypeWithTraits.knownDeviceImage
    }
}

extension MainViewModel: DeviceFirmwareFailureDetectorDelegate {
    func failureOccured() {
        flowOutputDelegate?.didRequestAddNewDeviceForced()
    }
}

private extension UIPeripheralConnectionState {
    init(_ peripheralConnectionState: PeripheralConnectionState) {
        switch peripheralConnectionState {
        case .connected:
            self = .connected
        case .connecting:
            self = .connecting
        case .disconnected:
            self = .disconnected
        }
    }
}


