//
//  KnownDevicesViewModel.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 23/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import ConnectionService

private struct Const {
    static let maxNumberOfKnownDevices = 4
}

protocol KnownDevicesViewModelViewInputDelegate: class {
    func add(knownDevices: [(uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState)])
    func setCurent(knownDevice: (uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState))
    func markAsUnableToConnect(uiKnownDevice: UIKnownDevice)
    func update(knownDevice: (uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState))
    func updateConditionally(knownDevice: (uiKnownDevice: UIKnownDevice, state: KnownDeviceCellState))
    func triggerSelect(for uiKnownDevice: UIKnownDevice)
}
protocol KnownDevicesViewModelViewOutputDelegate {
    func viewDidLoad()
    func viewDidAppear()
    func didTapAddNewHeadphones()
    func didSelect(uiKnownDevice: UIKnownDevice)
    func didMarkAsUnableToConnect(uiKnownDevice: UIKnownDevice)
    func didRemove(uiKnownDevice: UIKnownDevice)
    func didSelectCurrentDevice()
}
protocol KnownDevicesViewModelFlowInputDelegate {
    func triggerSelect(for UUID: String)
}
protocol KnownDevicesViewModelFlowOutputDelegate: class {
    func didRequestAddNewDevice()
    func didRequestDismissal()
}

class KnownDevicesViewModel: BaseViewModel, KnownDevicesViewModelFlowInputDelegate, KnownDevicesViewModelViewOutputDelegate {
    weak var flowOutputDelegate: KnownDevicesViewModelFlowOutputDelegate?
    weak var viewInputDelegate: KnownDevicesViewModelViewInputDelegate?
    private let services: ServicesType
    private let disposeBag = DisposeBag()
    private var selectedKnownDeviceToReconnect: UIKnownDevice?
    private var selectionTriggerForKnownDevice: UIKnownDevice?
    init(flowOutputDelegate: KnownDevicesViewModelFlowOutputDelegate,
         viewInputDelegate: KnownDevicesViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.rxConnectionService.didEstablishPeripheralConnectionToReactiveDevice.subscribe(onNext: { [weak self] _ in
            print("KnownDevicesViewModel::didEstablishPeripheralConnectionToReactiveDevice")
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) { [weak self] in
                UIImpactFeedbackGenerator(style: .medium).impactOccurred()
                self?.flowOutputDelegate?.didRequestDismissal()
            }
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didFailToEstablishPeripheralConnection.subscribe(onNext: { [weak self] in
            print("KnownDevicesViewModel::didFailToEstablishPeripheralConnection")
            if let selectedKnownDeviceToReconnect = self?.selectedKnownDeviceToReconnect {
                UIImpactFeedbackGenerator(style: .medium).impactOccurred()
                self?.viewInputDelegate?.markAsUnableToConnect(uiKnownDevice: selectedKnownDeviceToReconnect)
                self?.services.rxConnectionService.establishPeripheralConnection()
            }
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didChangeAccessoryState.subscribe(onNext: { [weak self] accessoryInfo in
            self?.handle(accessoryInfo: accessoryInfo)
        }).disposed(by: disposeBag)
    }
    private var limitedKnownDevices: [KnownDevice] {
        return services.rxConnectionService.knownDevices.suffix(Const.maxNumberOfKnownDevices)
    }
    private var currentDevice: KnownDevice? {
        return services.rxConnectionService.knownDevice
    }
    private func handle(accessoryInfo: AccessoryInfo) {
        if let currentDevice = currentDevice {
            if accessoryInfo.mac == currentDevice.mac {
                viewInputDelegate?.setCurent(knownDevice: (uiKnownDevice: currentDevice.uiModel(), state: accessoryInfo.connected ? .normalBluetoothClassicConnected : .normalBluetoothClassicNotConnected))
                return
            }
        }
        if let knownDevice = limitedKnownDevices.first(where: { $0.mac == accessoryInfo.mac }) {
            let uiKnownDevice = UIKnownDevice(name: knownDevice.name,
                                              uuid: knownDevice.peripheralUUIDString,
                                              image: knownDevice.deviceTypeWithTraits.thumbnailImage)
            viewInputDelegate?.updateConditionally(knownDevice: (
                uiKnownDevice: uiKnownDevice,
                state: accessoryInfo.connected ? .normalBluetoothClassicConnected : .normalBluetoothClassicNotConnected
            ))
        }
    }
}

extension KnownDevicesViewModel {
    func triggerSelect(for UUID: String) {
        if let knownDevice = services.rxConnectionService.knownDevices.first(where: { $0.peripheralUUIDString == UUID }) {
            selectionTriggerForKnownDevice = UIKnownDevice(name: knownDevice.name,
                                                           uuid: knownDevice.peripheralUUIDString,
                                                           image: knownDevice.deviceTypeWithTraits.thumbnailImage)
        }
    }
}

extension KnownDevicesViewModel {
    func didTapAddNewHeadphones() {
        flowOutputDelegate?.didRequestAddNewDevice()
    }
    func viewDidLoad() {
        self.viewInputDelegate?.add(knownDevices: limitedKnownDevices.map {(
                uiKnownDevices: $0.uiModel(),
                state: services.rxConnectionService.isConnected(accessoryWith: $0.mac) ?
                    .normalBluetoothClassicConnected :
                    .normalBluetoothClassicNotConnected
            )
        })
        if let current = currentDevice {
            let state: KnownDeviceCellState =  services.rxConnectionService.isConnected(accessoryWith: current.mac) ?
                .normalBluetoothClassicConnected :
                .normalBluetoothClassicNotConnected
            let device = (uiKnownDevice: current.uiModel(), state: state)
            self.viewInputDelegate?.setCurent(knownDevice: device)
        }
    }
    func viewDidAppear() {
        if let selectionTriggerForKnownDevice = selectionTriggerForKnownDevice {
            viewInputDelegate?.triggerSelect(for: selectionTriggerForKnownDevice)
            self.selectionTriggerForKnownDevice = nil
        }
    }
    func didSelectCurrentDevice() {
        flowOutputDelegate?.didRequestDismissal()
    }
    func didSelect(uiKnownDevice: UIKnownDevice) {
        UISelectionFeedbackGenerator().selectionChanged()
        if let knownDevice = limitedKnownDevices.first(where: { $0.peripheralUUIDString == uiKnownDevice.uuid }) {
            print("MainViewModel::reestablishPeripheralConnection(to:)")
            selectedKnownDeviceToReconnect = uiKnownDevice
            services.rxConnectionService.reestablishPeripheralConnection(to: knownDevice)
        }
    }
    func didMarkAsUnableToConnect(uiKnownDevice: UIKnownDevice) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) { [weak self] in
            if let knownDevice = self?.limitedKnownDevices.first(where: { $0.peripheralUUIDString == uiKnownDevice.uuid }) {
                self?.viewInputDelegate?.update(knownDevice: (
                    uiKnownDevice: uiKnownDevice,
                    state: self?.services.rxConnectionService.isConnected(accessoryWith: knownDevice.mac) == true ?
                        .normalBluetoothClassicConnected :
                        .normalBluetoothClassicNotConnected
                ))
            }
        }
    }
    func didRemove(uiKnownDevice: UIKnownDevice) {
        services.rxConnectionService.remove(knownDeviceWith: uiKnownDevice.uuid)
    }
}
private extension KnownDevice {
    func uiModel()-> UIKnownDevice {
        let model = UIKnownDevice(name: name, uuid: peripheralUUIDString, image: deviceTypeWithTraits.thumbnailImage)
        return model
    }
}
