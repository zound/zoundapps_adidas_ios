//
//  LimitedGraphicalEqualizerViewModel.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 27/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService
import RxSwift

protocol LimitedGraphicalEqualizerViewModelViewInputDelegate: class {
    func initiate(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset, uiGraphicalEqualizerCustom: UIGraphicalEqualizer)
    func update(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset)
}
protocol LimitedGraphicalEqualizerViewModelViewOutputDelegate {
    func viewDidLoad()
    func didSelect(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset)
    func didTapEdit()
}
protocol LimitedGraphicalEqualizerViewModelFlowInputDelegate {
}
protocol LimitedGraphicalEqualizerViewModelFlowOutputDelegate: class {
    func didRequestGraphicalEqualizer()
    func didRequestDismissal()
}

class LimitedGraphicalEqualizerViewModel: BaseViewModel, LimitedGraphicalEqualizerViewModelFlowInputDelegate, LimitedGraphicalEqualizerViewModelViewOutputDelegate {
    weak var flowOutputDelegate: LimitedGraphicalEqualizerViewModelFlowOutputDelegate?
    weak var viewInputDelegate: LimitedGraphicalEqualizerViewModelViewInputDelegate?
    private let services: ServicesType
    private var lastWrittenGraphicalEqualizer: GraphicalEqualizer?
    private let disposeBag = DisposeBag()
    init(flowOutputDelegate: LimitedGraphicalEqualizerViewModelFlowOutputDelegate, viewInputDelegate: LimitedGraphicalEqualizerViewModelViewInputDelegate, services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.rxConnectionService.reactiveDevice?.monitoredPeripheralConnectionState.subscribe(onNext: { [weak self] monitoredPeripheralConnectionState in
            if monitoredPeripheralConnectionState == .disconnected {
                self?.flowOutputDelegate?.didRequestDismissal()
            }
        }).disposed(by: disposeBag)
    }
}

extension LimitedGraphicalEqualizerViewModel {
    func viewDidLoad() {
        services.rxConnectionService.reactiveDevice?.graphicalEqualizer.subscribe(onNext: { [weak self] resultGraphicalEqualizer in
            if case .success(let graphicalEqualizer) = resultGraphicalEqualizer {
                self?.viewInputDelegate?.initiate(
                    uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset(graphicalEqualizer),
                    uiGraphicalEqualizerCustom: DefaultStorage.shared.customDefaultUIGraphicalEqualizer ?? UIGraphicalEqualizer.customDefault
                )
            }
        }).disposed(by: disposeBag)
        services.rxConnectionService.reactiveDevice?.read(deviceFeature: .graphicalEqualizer)
        services.rxConnectionService.reactiveDevice?.monitoredGraphicalEqualizer.subscribe(onNext: { [weak self] monitoredGraphicalEqualizer in
            self?.viewInputDelegate?.update(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset(monitoredGraphicalEqualizer))
        }).disposed(by: disposeBag)
    }
    func didSelect(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset) {
        services.rxConnectionService.reactiveDevice?.write(graphicalEqualizer: uiGraphicalEqualizerPreset.uiGraphicalEqualizer.graphicalEqualizer, completion: { [weak self] _ in
            self?.services.rxConnectionService.reactiveDevice?.monitoredGraphicalEqualizer.onNext(uiGraphicalEqualizerPreset.uiGraphicalEqualizer.graphicalEqualizer)
        })
    }
    func didTapEdit() {
        flowOutputDelegate?.didRequestGraphicalEqualizer()
    }
}
