//
//  OnlineSettingsViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 04/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService

class OnlineSettingsViewModel: CommonWebViewModel {
    init(flowOutputDelegate: CommonWebViewModelFlowOutputDelegate, viewInputDelegate: CommonWebViewModelViewInputDelegate, services: ServicesType, deviceType : DeviceType) {
        let title = NSLocalizedString("help_user_manual_title_uc", value: "USER MANUAL", comment: "USER MANUAL")
        let request = services
            .urlRequests
            .onlineManualUrlRequestBuilder()
            .add(deviceType)
            .request
        super.init(flowOutputDelegate: flowOutputDelegate, viewInputDelegate: viewInputDelegate, request: request, title: title, services: services)
    }
}
