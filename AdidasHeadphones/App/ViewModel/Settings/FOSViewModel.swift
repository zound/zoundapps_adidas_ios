//
//  FOSViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 02/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


protocol FOSViewModelViewInputDelegate: class {
}
protocol FOSViewModelViewOutputDelegate {
}
protocol FOSViewModelFlowInputDelegate {
}
protocol FOSViewModelFlowOutputDelegate: class {
}

class FOSViewModel: BaseViewModel, FOSViewModelFlowInputDelegate, FOSViewModelViewOutputDelegate {
    weak var flowOutputDelegate: FOSViewModelFlowOutputDelegate?
    weak var viewInputDelegate: FOSViewModelViewInputDelegate?
    init(flowOutputDelegate: FOSViewModelFlowOutputDelegate,
         viewInputDelegate: FOSViewModelViewInputDelegate) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        super.init()
    }
}
extension TermsAndCondViewModel {

}

