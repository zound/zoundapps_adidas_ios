//
//  ActionButtonStyleViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 28/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import ConnectionService
import RxSwift

protocol ActionButtonStyleViewModelViewOutputDelegate {
    func didSelect(styleType: ActionType)
    func didSelect(styleType:ActionType, pressType: PressType, value: ButtonAction)
    func viewDidLoad()
    func didTapEdit()
    func currentDeviceUUID() -> String?
}
protocol ActionButtonStyleViewModelViewInputDelegate: class {
    func refresh()
}
protocol ActionButtonStyleViewModelFlowOutputDelegate: class {
    func didRequestButtonConfiguration(pressType: PressType, value: ButtonAction)
    func didRequestGoogleAssistantHelpViewController()
}
protocol ActionButtonStyleViewModelLimitedFlowOutputDelegate: class {
    func didRequestActionButtonStyleConfiuration()
    func didRequestDismissal()
}
struct ActionGroup {
    var unfoldType: UnfoldType
    let configurable: Bool
    let type: ActionType
    let buttonActions: [ButtonActionInfo]
}
enum UnfoldType {
    case always
    case folded
    case unfolded
}
enum ActionType {
    case googleAssistant
    case customized
    func icon() -> UIImage {
        switch self {
        case .googleAssistant:
            return Image.Icon.Service.googleAssistant
        case .customized:
            return Image.Icon.Service.customized
        }
    }
    func title() -> String {
        switch self {
        case .googleAssistant:
            return NSLocalizedString("appwide_google_assistant_uc", value: "GOOGLE ASSISTANT", comment: "Google Assistant")
        case .customized:
            return NSLocalizedString("action_button_customized_uc", value: "CUSTOMIZED", comment: "Customized action")
        }
    }
    func description() -> String {
        switch self {
        case .googleAssistant:
            return NSLocalizedString("action_button_google_description", value: "Use the Action Button to interact with Google Assistant", comment: "Use the Action Button to interact with Google Assistant")
        case .customized:
            return NSLocalizedString("action_button_customized_description_ios", value: "Start your favorite Spotify playlist? Talk to Siri? You decide!", comment: "Start your favorite Spotify playlist? Talk to Siri? You decide!")
        }
    }
    func pressTypes() -> [PressType] {
        switch self {
        case .googleAssistant: return [.singlePress, .doublePress, .longPress]
        case .customized: return [.singlePress, .doublePress, .triplePress]
        }
    }
}
extension ButtonAction {
    func headerTitle(for pressType: PressType) -> String {
        switch (self, pressType) {
        case (.googleVoiceAssistantBisto, .singlePress):
            return NSLocalizedString("action_button_gva_single_press", value: "Start Assistant", comment: "Start Assistant")
        case (.googleVoiceAssistantBisto, .doublePress):
            return NSLocalizedString("action_button_gva_double_press", value: "Stop Assistant", comment: "Stop Assistant")
        case (.googleVoiceAssistantBisto, .longPress):
            return NSLocalizedString("action_button_gva_long_press", value: "Talk to Assistant", comment: "Talk to Assistant")
        case (.noAction,_):
            return NSLocalizedString("action_button_do_nothing", value: "Configure", comment: "Configure")
        case (.defaultVoiceAssistant,_):
            return NSLocalizedString("action_button_default_voice_assistant", value: "Defaul Voice Assistant", comment: "Activate default voice assistant")
        case (.equalizer,_):
            return NSLocalizedString("action_button_change_equalizer_preset", value: "Change equalizer preset", comment: "Change equalizer preset")
        case (.playSpotifyPlaylist,_):
            return NSLocalizedString("action_button_single_press_options_title_1_uc", value: "Play playlist", comment: "Play playlist")
        case (.playSpotifyArtist,_):
            return NSLocalizedString("action_button_single_press_options_title_2_uc", value: "Play artist", comment: "Play artist")
        case (.playSpotifyAlbum,_):
            return NSLocalizedString("action_button_single_press_options_title_3_uc", value: "Play album", comment: "Play album")
        default: return String.invisible
        }
    }
    func title(for pressType: PressType) -> String {
        switch (self, pressType) {
        case (.googleVoiceAssistantBisto, .singlePress):
            return NSLocalizedString("action_button_gva_single_press", value: "Start Assistant", comment: "Start Assistant")
        case (.googleVoiceAssistantBisto, .doublePress):
            return NSLocalizedString("action_button_gva_double_press", value: "Stop Assistant", comment: "Stop Assistant")
        case (.googleVoiceAssistantBisto, .longPress):
            return NSLocalizedString("action_button_gva_long_press", value: "Talk to Assistant", comment: "Talk to Assistant")
        case (.noAction,_):
            return NSLocalizedString("action_button_do_nothing", value: "Configure", comment: "Configure")
        case (.defaultVoiceAssistant,_):
            return NSLocalizedString("action_button_default_voice_assistant", value: "Defaul Voice Assistant", comment: "Activate default voice assistant")
        case (.equalizer,_):
            return NSLocalizedString("action_button_change_equalizer_preset", value: "Change equalizer preset", comment: "Change equalizer preset")
        case (.playSpotifyPlaylist,_):
            return NSLocalizedString("action_button_play_spotify_playlist", value: "Play playlist", comment: "Play playlist")
        case (.playSpotifyArtist,_):
            return NSLocalizedString("action_button_play_spotify_artist", value: "Play artist", comment: "Play artist")
        case (.playSpotifyAlbum,_):
            return NSLocalizedString("action_button_play_spotify_album", value: "Play album", comment: "Play album")
        default: return String.invisible
        }
    }
}
private let actionGroupStaticData: [ActionGroup] = [
    ActionGroup(unfoldType: .folded,
                configurable: false,
                type: .googleAssistant,
                buttonActions: [
                    ButtonActionInfo(type: .singlePress, info: ActionInfo(parentIcon: Image.Icon.Service.googleAssistant, action: .googleVoiceAssistantBisto)),
                    ButtonActionInfo(type: .doublePress, info: ActionInfo(parentIcon: Image.Icon.Service.googleAssistant, action: .googleVoiceAssistantBisto)),
                    ButtonActionInfo(type: .triplePress, info: ActionInfo(parentIcon: Image.Icon.Service.googleAssistant, action: .noAction)),
                    ButtonActionInfo(type: .longPress,   info: ActionInfo(parentIcon: Image.Icon.Service.googleAssistant, action: .googleVoiceAssistantBisto))
                ]),
    ActionGroup(unfoldType: .folded,
                configurable: true,
                type: .customized,
                buttonActions: [
                    ButtonActionInfo(type: .singlePress, info: ActionInfo(parentIcon: Image.Icon.Service.customized, action: .noAction)),
                    ButtonActionInfo(type: .doublePress, info: ActionInfo(parentIcon: Image.Icon.Service.customized, action: .noAction)),
                    ButtonActionInfo(type: .triplePress, info: ActionInfo(parentIcon: Image.Icon.Service.customized, action: .noAction)),
                    ButtonActionInfo(type: .longPress,   info: ActionInfo(parentIcon: Image.Icon.Service.customized, action: .noAction)),
                ])
]
extension PressType {
    func label() -> String {
        switch self {
        case .singlePress:
            return NSLocalizedString("appwide_single_press_label_uc", value: "SINGLE PRESS", comment: "Single press")
        case .doublePress:
            return NSLocalizedString("appwide_double_press_label_uc", value: "DOUBLE PRESS", comment: "Double press")
        case .triplePress:
            return NSLocalizedString("appwide_tripple_press_label_uc", value: "TRIPPLE PRESS", comment: "Tripple press")
        case .longPress:
            return NSLocalizedString("appwide_long_press_label_uc", value: "LONG PRESS", comment: "Long press")
        }
    }
}
typealias UnfoldInfo = (type: UnfoldType, groupIdx: Int)

final class ActionButtonStyleViewModel: BaseViewModel {
    weak var flowOutputDelegate: ActionButtonStyleViewModelFlowOutputDelegate?
    weak var limitedFlowOutputDelegate: ActionButtonStyleViewModelLimitedFlowOutputDelegate?
    var actionGroups: [ActionGroup] = []
    let unfoldGroup = PublishSubject<UnfoldInfo>()
    private let actionButtonService: ActionButtonService
    private let connectionService: ReactiveConnectionServiceType
    private let disposeBag = DisposeBag()
    init(services: ServicesType) {
        self.actionButtonService = services.actionButtonService
        self.connectionService = services.rxConnectionService
        super.init()
        self.connectionService.reactiveDevice?.monitoredPeripheralConnectionState
            .subscribe(
                onNext: { [weak self] monitoredPeripheralConnectionState in
                    if monitoredPeripheralConnectionState == .disconnected {
                        self?.limitedFlowOutputDelegate?.didRequestDismissal()
                    }
                })
            .disposed(by: disposeBag)
        let hardwareGVA = self.connectionService.reactiveDevice?.isActionButtonGvaSupported ?? false
        self.actionGroups = hardwareGVA ? actionGroupStaticData : actionGroupStaticData.filter { $0.type != .googleAssistant }
        guard let currentConfiguration = try? actionButtonService.rxButtonConfiguration.value().events else {
            return
        }
        self.actionButtonService.rxButtonConfiguration
            .skip(1)
            .distinctUntilChanged(eventsComparator)
            .subscribe(
                onNext: { [weak self] configuration in
                    guard configuration.local == false else {
                        return
                    }
                    self?.configure(with: configuration.events, animate: true)
                }
            )
            .disposed(by: disposeBag)
        self.configure(with: currentConfiguration, animate: false)
    }
    private func configure(with slots: [ActionButtonEvent], animate: Bool) {
        guard actionGroups.count > 1 else {
            let updateGroup = updatedGroupAction(unfoldType: .always, buttonsConfig: slots)
            guard let group = actionGroups.first else { return }
            let typed = ActionGroup(unfoldType: group.unfoldType,
                                    configurable: group.configurable,
                                    type: group.type,
                                    buttonActions: group.buttonActions.filter { group.type.pressTypes().contains($0.type) })
            actionGroups = [typed].map(updateGroup)
            guard animate == true else { return }
            unfoldGroup.onNext((type: .always, groupIdx: .zero))
            return
        }
        let foldedLocal = actionGroups.filter({ $0.unfoldType == .unfolded }).count == 0
        let gvaBistoEnabled = slots.filter({ $0.buttonAction == .googleVoiceAssistantBisto }).count > 0
        guard gvaBistoEnabled == false else {
            let updateGroupAction = unfoldGroupFor(.googleAssistant,  buttonsConfig: slots)
            actionGroups = actionGroups.map(updateGroupAction)
            guard animate == true else { return }
            guard let idx = actionGroups.firstIndex(where: { $0.type == .googleAssistant }) else { return }
            unfoldGroup.onNext((type: foldedLocal ? .folded: .unfolded, groupIdx: idx))
            return
        }
        let foldedRemote = slots.filter({ $0.buttonAction != .noAction }).count == 0
        let updateGroup = foldedRemote == true ?
            updatedGroupAction(unfoldType: .folded, buttonsConfig: slots) :
            unfoldGroupFor(.customized, buttonsConfig: slots)
        actionGroups = actionGroups.map(updateGroup)
        guard animate == true else { return }
        guard foldedRemote == false else {
            unfoldGroup.onNext((type: .folded, groupIdx: .zero))
            return
        }
        guard let idx = actionGroups.firstIndex(where: { $0.type == .customized }) else { return }
        unfoldGroup.onNext((type: foldedLocal ? .folded : .unfolded, groupIdx: idx))
    }
}
extension ActionButtonStyleViewModel: ActionButtonStyleViewModelViewOutputDelegate {
    func didSelect(styleType: ActionType) {
        guard let selected = actionGroupStaticData.filter({ $0.type == styleType }).first else { return }
       if selected.type == .googleAssistant, !DefaultStorage.shared.googleAssistantHelpPresented {
            self.flowOutputDelegate?.didRequestGoogleAssistantHelpViewController()
        }
        guard selected.unfoldType == .folded else { return }
        let localConfiguration = selected.buttonActions.map { action -> ActionButtonEvent in
            return ActionButtonEvent(pressType: action.type,
                                     buttonAction: action.info.action)
        }
        actionButtonService.didSelect(type: styleType)
        actionButtonService.write(userConfiguration: localConfiguration)
        let folded = actionGroups.filter({ $0.unfoldType == .unfolded }).count == 0
        let changed = actionGroupStaticData.map { actionGroup -> ActionGroup in
            return ActionGroup(unfoldType: actionGroup.type == styleType ? .unfolded: .folded,
                               configurable: actionGroup.configurable,
                               type: actionGroup.type,
                               buttonActions: actionGroup.buttonActions.filter { styleType.pressTypes().contains($0.type) })
        }
        actionGroups = changed
        guard let idx = actionGroups.firstIndex(where: { $0.type == styleType }) else { return }
        unfoldGroup.onNext((type: folded ? .folded: .unfolded, groupIdx: idx))
    }
    func didSelect(styleType: ActionType, pressType: PressType, value: ButtonAction) {
        guard styleType == .customized else { return }
        flowOutputDelegate?.didRequestButtonConfiguration(pressType: pressType, value: value)
    }
    func viewDidLoad() {
        self.actionButtonService.requestConfiguration()
    }
    func didTapEdit() {
        limitedFlowOutputDelegate?.didRequestActionButtonStyleConfiuration()
    }
    func currentDeviceUUID() -> String? {
        return connectionService.reactiveDevice?.uuid
    }
}
private func updatedGroupAction(unfoldType: UnfoldType,
                                buttonsConfig: [ActionButtonEvent]) -> (ActionGroup) -> ActionGroup {
    return { group in
        let updatedButtonActions = group.buttonActions.map { action -> ButtonActionInfo in
            guard let updatedActionInfo = buttonsConfig.filter ({ $0.pressType == action.type }).first else {
                return action
            }
            return ButtonActionInfo(type: updatedActionInfo.pressType, info: ActionInfo(parentIcon: group.type.icon(), action: updatedActionInfo.buttonAction))
        }
        guard unfoldType == .unfolded || unfoldType == .always else {
            return ActionGroup(unfoldType: .folded,
                               configurable: group.configurable,
                               type: group.type,
                               buttonActions: updatedButtonActions)
        }
        return ActionGroup(unfoldType: unfoldType,
                           configurable: group.configurable,
                           type: group.type,
                           buttonActions: updatedButtonActions)
    }
}
func unfoldGroupFor(_ type: ActionType,
                    buttonsConfig: [ActionButtonEvent]) -> (ActionGroup) -> ActionGroup {
    return { group in
        let typed = ActionGroup(unfoldType: group.unfoldType,
                                configurable: group.configurable,
                                type: group.type,
                                buttonActions: group.buttonActions.filter { type.pressTypes().contains($0.type) })
        guard type == typed.type else {
            var toBeFolded = typed
            toBeFolded.unfoldType = .folded
            return toBeFolded
        }
        return updatedGroupAction(unfoldType: .unfolded, buttonsConfig: buttonsConfig)(typed)
    }
}
func eventsComparator(lhsTuple: (events: [ActionButtonEvent], local: Bool), rhsTuple: (events: [ActionButtonEvent], local: Bool)) -> Bool {
    if lhsTuple.events.elementsEqual(rhsTuple.events, by: { lhsElement, rhsElement -> Bool in
        lhsElement.buttonAction == rhsElement.buttonAction && lhsElement.pressType == rhsElement.pressType
    }) {
        return true
    }
    return false
}
