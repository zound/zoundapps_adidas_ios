//
//  SpotifySearchViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 19/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import ConnectionService
import RxSpotifyWrapper
import Reachability

private struct Const {
    static let gettingEntryNamesPercentageComplete: Float = 0.5
    static let gettingTracksListPercentagaComplete: Float = 1.0
    static let gettingEntryInfoPercentageComplete: Float = 1.0
    static let milisecondsPerSecond: Double = 1000.0
}
enum SpotifySearchError: Error {
    case cancelled
    case authentication
}
protocol SpotifySearchViewModelViewOutputDelegate {
    func viewDidLoad()
    func viewWillDisappear()
    func refresh()
    func update(searchQuery: String?)
    func searchButtonTapped()
    func cancelButtonTapped()
    func didSelect(indexPath: IndexPath)
    func reachabilityMonitor() -> Reachability
}
protocol SpotifySearchViewModelViewInputDelegate: class {
    func reloadData()
}
protocol SpotifySearchViewModelFlowOutputDelegate: class {
    func didSelectSpotifyAction()
}
protocol SpotifySearchViewModelType: BaseViewModel {
    var reloadTrigger: PublishSubject<Void> { get }
    var refreshFinished: PublishSubject<Void> { get }
    var progressTrigger: BehaviorSubject<Float> { get }
    var messageTrigger: PublishSubject<String> { get }
    func viewDidLoad()
}
final class SpotifyUISearchEntry: NSObject, Codable {
    let id: String
    @objc let name: String
    let imageURL: URL?
    let numberOfSongs: Int
    let duration: String
    let uri: String
    let type: SpotifySearchEntryType
    init(id: String, name: String, imageURL: URL?, numberOfSongs: Int, duration: String, uri: String, type: SpotifySearchEntryType) {
        self.id = id
        self.name = name
        self.imageURL = imageURL
        self.numberOfSongs = numberOfSongs
        self.duration = duration
        self.uri = uri
        self.type = type
        super.init()
    }
}
final class SpotifySearchViewModel: BaseViewModel, SpotifySearchViewModelType {
    static let formatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [.hour, .minute]
        return formatter
    }()
    let reloadTrigger = PublishSubject<Void>()
    let cancelTrigger = BehaviorSubject<Bool>(value: false)
    let progressTrigger = BehaviorSubject<Float>(value: .zero)
    let messageTrigger = PublishSubject<String>()
    var refreshFinished = PublishSubject<Void>()
    weak var flowOutputDelegate: SpotifySearchViewModelFlowOutputDelegate?
    weak var viewInputDelegate: SpotifySearchViewModelViewInputDelegate?
    private var searchDisposeBag = DisposeBag()
    private var currentSearchQuery = String()
    private let collation = UILocalizedIndexedCollation.current()
    let type: SpotifySearchEntryType
    let pressType: PressType
    private let services: ServicesType
    private var imageCache = [String: UIImage]()
    private var downloadTasks = [String: URLSessionDataTask]()
    private let downloadScheduler = SerialDispatchQueueScheduler(qos: .default)
    private var _sections = [[SpotifyUISearchEntry]]()
    private var _sectionTitles = [String]()
    private var _filteredEntries = [[SpotifyUISearchEntry]]()
    var sections: [[SpotifyUISearchEntry]] { return _sections }
    var sectionTitles: [String] { return _sectionTitles }
    var filteredEntries: [[SpotifyUISearchEntry]] { return _filteredEntries }
    private var entries = [SpotifyUISearchEntry]()
    private var disposeBag = DisposeBag()
    
    init(type: SpotifySearchEntryType, pressType: PressType, services: ServicesType) {
        self.type = type
        self.pressType = pressType
        self.services = services
    }
    func handler(for searchType: SpotifySearchEntryType) -> () -> Observable<([SpotifyUISearchEntry], Float)> {
        switch searchType {
        case .playlist: return playlistSearchEntries
        case .artist: return artistSearchEntries
        case .album: return albumSearchEntries
        }
    }
    func requestData(for searchType: SpotifySearchEntryType) {
        var remoteEntries = [SpotifyUISearchEntry]()
        handler(for: searchType)()
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] (entries, status) in
                    guard let strongSelf = self else { return }
                    remoteEntries.append(contentsOf: entries)
                    strongSelf.progressTrigger.onNext(status)
                    var changed = strongSelf.entries
                    changed.append(contentsOf: entries)
                    var filtered = [[SpotifyUISearchEntry]]()
                    var sectionTitles = [String]()
                    if strongSelf.currentSearchQuery.count > 0 {
                        (filtered, sectionTitles) = strongSelf.filter(entries: changed, query: strongSelf.currentSearchQuery)
                    } else {
                        let (divided, titles) = strongSelf.collation.partitionObjects(array: changed, collationStringSelector: #selector(getter: SpotifyUISearchEntry.name))
                        filtered = divided as! [[SpotifyUISearchEntry]]
                        sectionTitles = titles
                    }
                    strongSelf.entries = changed
                    strongSelf.commit(sections: filtered, sectionTitles: sectionTitles)
                },
                onError: { error in
                    // TODO
                },
                onCompleted: { [weak self] in
                    if remoteEntries.count == 0 {
                        self?.messageTrigger.onNext(searchType.notFoundTranslated())
                    }
                    self?.searchDisposeBag = DisposeBag()
                    self?.refreshFinished.onNext(())
                }
            )
            .disposed(by: searchDisposeBag)
    }
    private func commit(sections: [[SpotifyUISearchEntry]], sectionTitles: [String]) {
        _sections = sections
        _sectionTitles = sectionTitles
        _filteredEntries = sections
        viewInputDelegate?.reloadData()
    }
}
extension SpotifySearchViewModel: SpotifySearchViewModelViewOutputDelegate {
    func reachabilityMonitor() -> Reachability {
        return services.reachability
    }
    func didSelect(indexPath: IndexPath) {
        guard let uuid = services.rxConnectionService.reactiveDevice?.uuid else {
            return
        }
        let selected = _filteredEntries[indexPath.section][indexPath.row]
        let storedEntry = StoredSpotifyEntry(pressType: pressType, value: selected)
        guard let currentlyStoredConfiguration = DefaultStorage.shared.spotifyLocalData else {
            let actionButtonData = SpotifyActionButtonData(uuid: uuid, entries: [storedEntry])
            let toBeStored = StoredActionButtonData(perDeviceConfiguration: Set([actionButtonData]))
            DefaultStorage.shared.spotifyLocalData = toBeStored
            return
        }
        guard let existing = currentlyStoredConfiguration.perDeviceConfiguration
            .first(where: { ($0.uuid == uuid) }) else {
            let actionButtonData = SpotifyActionButtonData(uuid: uuid, entries: [storedEntry])
            var currentConfiguration = currentlyStoredConfiguration.perDeviceConfiguration
            currentConfiguration.insert(actionButtonData)
            let toBeStored = StoredActionButtonData(perDeviceConfiguration: currentConfiguration)
            DefaultStorage.shared.spotifyLocalData = toBeStored
            return
        }
        var perDeviceConfiguration = currentlyStoredConfiguration.perDeviceConfiguration
        perDeviceConfiguration.remove(existing)
        var filtered = existing.entries.filter { $0.pressType != pressType }
        filtered.append(storedEntry)
        let actionButtonData = SpotifyActionButtonData(uuid: uuid, entries: filtered)
        perDeviceConfiguration.insert(actionButtonData)
        let toBeStored = StoredActionButtonData(perDeviceConfiguration: perDeviceConfiguration)
        DefaultStorage.shared.spotifyLocalData = toBeStored
        services.actionButtonService.set(action: selected.type.action(), for: pressType)
        flowOutputDelegate?.didSelectSpotifyAction()
    }
    func viewDidLoad() {
        requestData(for: type)
    }
    func refresh() {
        searchDisposeBag = DisposeBag()
        entries.removeAll()
        imageCache.removeAll()
        let (filtered, sectionTitles) = filter(entries: entries, query: String.invisible)
        commit(sections: filtered, sectionTitles: sectionTitles)
        requestData(for: type)
    }
    func update(searchQuery: String?) {
        currentSearchQuery = searchQuery ?? ""
        let (filtered, sectionTitles) = filter(entries: entries, query: currentSearchQuery)
        commit(sections: filtered, sectionTitles: sectionTitles)
    }
    func searchButtonTapped() {
        print(#function)
    }
    func viewWillDisappear() {
        cancelTrigger.onNext(true)
    }
    func cancelButtonTapped() {
        currentSearchQuery = ""
        let (filtered, sectionTitles) = filter(entries: entries, query: currentSearchQuery)
        commit(sections: filtered, sectionTitles: sectionTitles)
    }
}
extension SpotifySearchViewModel {
    func remoteImage(for id: String) -> Observable<(id: String, image: UIImage)> {
        return downloadImage(for: id)
            .do(onNext: { [weak self] (id, image) in
                guard let _ = self?.imageCache[id] else {
                    self?.imageCache[id] = image
                    return
                }
            })
            .subscribeOn(downloadScheduler)
    }
    private func downloadImage(for id: String) -> Observable<(id:String, image: UIImage)> {
        return Observable.create { [weak self] subscriber in
            if let image = self?.imageCache[id] {
                subscriber.onNext((id: id,image: image))
                subscriber.onCompleted()
                return Disposables.create()
            }
            if let runningTask = self?.downloadTasks[id] {
                runningTask.cancel()
                self?.downloadTasks.removeValue(forKey: id)
            }
            guard let entry = self?.entries.filter({ $0.id == id }).first, let imageURL = entry.imageURL  else {
                subscriber.onCompleted()
                return Disposables.create()
            }
            let downloadTask = URLSession.shared.dataTask(with: imageURL) { (imageData, response, error) in
                guard let data = imageData,
                    let image = UIImage(data: data, scale: UIScreen.main.scale) else {
                        subscriber.onCompleted()
                        return
                }
                let scaled = image.scaled(
                    to: CGSize(width: Dimension.spotifySearchEntryDimension,
                               height: Dimension.spotifySearchEntryDimension))
                subscriber.onNext((id: id,image: scaled))
                subscriber.onCompleted()
            }
            self?.downloadTasks[id] = downloadTask
            downloadTask.resume()
            return Disposables.create()
        }
    }
}
extension SpotifySearchViewModel {
    private func combinedInitialHandler() -> () -> Single<String> {
        return {
            return Single.create { [weak self] event in
                var internalDisposeBag = DisposeBag()
                guard let strongSelf = self else {
                    event(.error(SpotifySearchError.cancelled))
                    return Disposables.create()
                }
                guard let cancelTriggerValue = try? strongSelf.cancelTrigger.value(), cancelTriggerValue == false else {
                    event(.error(SpotifySearchError.cancelled))
                    return Disposables.create()
                }
                strongSelf.services.rxSpotifyManager.setupAuthentication()
                    .subscribe(
                        onSuccess: { token in
                            event(.success(token))
                            internalDisposeBag = DisposeBag()
                    }, onError: { error in
                        event(.error(error))
                        internalDisposeBag = DisposeBag()
                    })
                    .disposed(by: internalDisposeBag)
                return Disposables.create()
            }
        }
    }
    func playlistSearchEntries() -> Observable<([SpotifyUISearchEntry], Float)> {
        return Observable.create { [weak self] subscriber -> Disposable in
            guard let strongSelf = self else {
                subscriber.onCompleted()
                return Disposables.create()
            }
            var internalDisposeBag = DisposeBag()
            let authenticationHandler = strongSelf.combinedInitialHandler()
            var playlistEntries = [SpotifyPlaylistEntry]()
            RxStreamingSpotifyManager.playlists(authenticationHandler: authenticationHandler)
                .subscribe(
                    onNext: { (playlists, status) in
                        switch status {
                        case .inProgress(_, let total):
                            if total > 0 {
                                let progress = (Float(playlists.count) / Float(total)) * Const.gettingEntryNamesPercentageComplete
                                subscriber.onNext(([], progress))
                            }
                        case .completed:
                            let progress = Const.gettingEntryNamesPercentageComplete
                            subscriber.onNext(([], progress))
                        default: break
                        }
                        playlistEntries.append(contentsOf: playlists)
                    },
                    onError: { error in
                        subscriber.onError(error)
                    },
                    onCompleted: {
                        let uiEntries = playlistEntries.map {
                            SpotifyUISearchEntry(id: $0.id,
                                                 name: $0.name,
                                                 imageURL: URL(string: $0.images.first?.url ?? String()),
                                                 numberOfSongs: $0.tracks.total,
                                                 duration: String(),
                                                 uri: $0.uri,
                                                 type: .playlist)
                        }
                        subscriber.onNext((uiEntries, Const.gettingTracksListPercentagaComplete))
                        internalDisposeBag = DisposeBag()
                        subscriber.onCompleted()
                    }
                )
                .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    func artistSearchEntries() -> Observable<([SpotifyUISearchEntry], Float)> {
        return Observable.create { [weak self] subscriber in
            guard let strongSelf = self else {
                subscriber.onCompleted()
                return Disposables.create()
            }
            let authenticationHandler = strongSelf.services.rxSpotifyManager.setupAuthentication
            var artistsDisposeBag = DisposeBag()
            var artistEntries = [SpotifyArtistEntry]()

            RxStreamingSpotifyManager.artists(authenticationHandler: authenticationHandler)
                .subscribe(
                    onNext: { (remoteEntries, status) in
                        artistEntries.append(contentsOf: remoteEntries)
                        switch status {
                        case .inProgress(_, let total):
                            if total > 0 {
                                let progress = Float(artistEntries.count) / Float(total)
                                subscriber.onNext(([], progress))
                            }
                        default: break
                        }
                    },
                    onError: { (error) in
                        subscriber.onError(error)
                    },
                    onCompleted: {
                        let uiEntries = artistEntries.map {
                            SpotifyUISearchEntry(id: $0.id,
                                                 name: $0.name,
                                                 imageURL: URL(string: $0.images.first?.url ?? ""),
                                                 numberOfSongs: 0,
                                                 duration: "",
                                                 uri: $0.uri,
                                                 type: .artist)
                        }
                        subscriber.onNext((uiEntries, Const.gettingEntryInfoPercentageComplete))
                        subscriber.onCompleted()
                        artistsDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: artistsDisposeBag)
            return Disposables.create()
        }
    }
    func albumSearchEntries() -> Observable<([SpotifyUISearchEntry], Float)> {
        return Observable.create { [weak self] subscriber -> Disposable in
            guard let strongSelf = self else {
                subscriber.onCompleted()
                return Disposables.create()
            }
            let authenticationHandler = strongSelf.services.rxSpotifyManager.setupAuthentication
            var internalDisposeBag = DisposeBag()
            var albumEntries = [SpotifyUserAlbumEntry]()
            
            RxStreamingSpotifyManager.albums(authenticationHandler: authenticationHandler)
                .subscribe(
                    onNext: { (entries, status) in
                        switch status {
                        case .inProgress(_, let total):
                            if total > 0 {
                                let progress = (Float(entries.count) / Float(total)) * Const.gettingEntryNamesPercentageComplete
                                subscriber.onNext(([], progress))
                            }
                        case .completed:
                            let progress = Const.gettingEntryNamesPercentageComplete
                            subscriber.onNext(([], progress))
                        default: break
                        }
                        albumEntries.append(contentsOf: entries)
                    },
                    onError: { error in
                        subscriber.onError(error)
                    },
                    onCompleted: {
                        let uiEntries = albumEntries.map {
                            SpotifyUISearchEntry(id: $0.album.id,
                                                 name: $0.album.name,
                                                 imageURL: URL(string: $0.album.images.first?.url ?? String()),
                                                 numberOfSongs: 0,
                                                 duration: String(),
                                                 uri: $0.album.uri,
                                                 type: .album)
                        }
                        subscriber.onNext((uiEntries, Const.gettingTracksListPercentagaComplete))
                        internalDisposeBag = DisposeBag()
                        subscriber.onCompleted()
                })
                .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    func filter(entries: [SpotifyUISearchEntry], query: String) -> ([[SpotifyUISearchEntry]], [String]) {
        let filtered: [SpotifyUISearchEntry]
        if query.count > 0 {
            filtered = entries.filter {
                $0.name.replacingOccurrences(of: " ", with: "").lowercased().contains(query.replacingOccurrences(of: " ", with: "").lowercased())
            }
        } else {
            filtered = entries
        }
        let (divided, titles) = collation.partitionObjects(array: filtered, collationStringSelector: #selector(getter: SpotifyUISearchEntry.name))
        return ((divided as! [[SpotifyUISearchEntry]], titles))
    }
}
