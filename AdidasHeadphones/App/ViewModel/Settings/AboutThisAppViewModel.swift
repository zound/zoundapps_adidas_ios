//
//  AboutThisAppViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 29/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


protocol AboutThisAppViewModelViewInputDelegate: class {
    func update(_ version: String)
}

protocol AboutThisAppViewModelViewOutputDelegate {
    func viewDidLoad()
    func selected(entry:AboutThisAppEntry)
}

protocol AboutThisAppViewModelFlowInputDelegate {

}

protocol AboutThisAppViewModelFlowOutputDelegate: class {
    func didRequestAboutThisAppEntry(_ entry: AboutThisAppEntry)
}

enum AboutThisAppEntry {
    case termsAndConditions
    case freeAndOpenSource
}

class AboutThisAppViewModel: BaseViewModel, AboutThisAppViewModelFlowInputDelegate, AboutThisAppViewModelViewOutputDelegate {

    private static let VERSION_KEY = "CFBundleShortVersionString"
    private static let CONFIGURATION = "Info"

    weak var flowOutputDelegate: AboutThisAppViewModelFlowOutputDelegate?
    weak var viewInputDelegate: AboutThisAppViewModelViewInputDelegate?

    init(flowOutputDelegate: AboutThisAppViewModelFlowOutputDelegate, viewInputDelegate: AboutThisAppViewModelViewInputDelegate) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        super.init()
    }

    func viewDidLoad() {
        viewInputDelegate?.update(BundleConfiguration.version)
    }

    func selected(entry: AboutThisAppEntry) {
        self.flowOutputDelegate?.didRequestAboutThisAppEntry(entry)
    }

}
