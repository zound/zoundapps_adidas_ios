//
//  GraphicalEqualizerViewModel.swift
//  AdidasHeadphones
//
//  Created by Wudarski Lukasz on 06/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService
import RxSwift

protocol GraphicalEqualizerViewModelViewInputDelegate: class {
    func initiate(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset, uiGraphicalEqualizerCustom: UIGraphicalEqualizer, uiGraphicalEqualizerMax: UInt8)
    func update(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset)
}
protocol GraphicalEqualizerViewModelViewOutputDelegate {
    func viewDidLoad()
    func viewWillDisappear()
    func didSelect(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset)
    func didUpdate(uiGraphicalEqualizer: UIGraphicalEqualizer)
}
protocol GraphicalEqualizerViewModelFlowInputDelegate {
}
protocol GraphicalEqualizerViewModelFlowOutputDelegate: class {
}

class GraphicalEqualizerViewModel: BaseViewModel, GraphicalEqualizerViewModelFlowInputDelegate, GraphicalEqualizerViewModelViewOutputDelegate {
    weak var flowOutputDelegate: GraphicalEqualizerViewModelFlowOutputDelegate?
    weak var viewInputDelegate: GraphicalEqualizerViewModelViewInputDelegate?
    private let services: ServicesType
    private var lastWrittenGraphicalEqualizer: GraphicalEqualizer?
    private let disposeBag = DisposeBag()
    init(flowOutputDelegate: GraphicalEqualizerViewModelFlowOutputDelegate,
         viewInputDelegate: GraphicalEqualizerViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
}

extension GraphicalEqualizerViewModel {
    func viewDidLoad() {
        services.rxConnectionService.reactiveDevice?.graphicalEqualizer.subscribe(onNext: { [weak self] resultGraphicalEqualizer in
            if case .success(let graphicalEqualizer) = resultGraphicalEqualizer {
                self?.viewInputDelegate?.initiate(
                    uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset(graphicalEqualizer),
                    uiGraphicalEqualizerCustom: DefaultStorage.shared.customDefaultUIGraphicalEqualizer ?? UIGraphicalEqualizer.customDefault,
                    uiGraphicalEqualizerMax: GraphicalEqualizer.max
                )
                self?.services.rxConnectionService.reactiveDevice?.monitoredGraphicalEqualizer.subscribe(onNext: { [weak self] monitoredGraphicalEqualizer in
                    self?.viewInputDelegate?.update(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset(monitoredGraphicalEqualizer))
                }).disposed(by: self!.disposeBag)
//                self?.services.rxConnectionService.reactiveDevice?.setGraphicalEqualizerMonitoring(enabled: true) // NOTE: Uncomment once needed
            }
        }).disposed(by: disposeBag)
        services.rxConnectionService.reactiveDevice?.read(deviceFeature: .graphicalEqualizer)
    }
    func viewWillDisappear() {
        if let graphicalEqualizer = lastWrittenGraphicalEqualizer {
            services.rxConnectionService.reactiveDevice?.monitoredGraphicalEqualizer.onNext(graphicalEqualizer)
        }
    }
    func didSelect(uiGraphicalEqualizerPreset: UIGraphicalEqualizerPreset) {
        services.rxConnectionService.reactiveDevice?.write(graphicalEqualizer: uiGraphicalEqualizerPreset.uiGraphicalEqualizer.graphicalEqualizer, completion: { [weak self] _ in
            self?.viewInputDelegate?.update(uiGraphicalEqualizerPreset: uiGraphicalEqualizerPreset)
            self?.lastWrittenGraphicalEqualizer = uiGraphicalEqualizerPreset.uiGraphicalEqualizer.graphicalEqualizer
        })
    }
    func didUpdate(uiGraphicalEqualizer: UIGraphicalEqualizer) {
        if uiGraphicalEqualizer.graphicalEqualizer.isCustom {
            DefaultStorage.shared.customDefaultUIGraphicalEqualizer = uiGraphicalEqualizer
        }
        services.rxConnectionService.reactiveDevice?.write(graphicalEqualizer: uiGraphicalEqualizer.graphicalEqualizer, completion: { [weak self] _ in
            self?.lastWrittenGraphicalEqualizer = uiGraphicalEqualizer.graphicalEqualizer
        })
    }
}


enum UIGraphicalEqualizerPreset: Equatable {
    static func == (lhs: UIGraphicalEqualizerPreset, rhs: UIGraphicalEqualizerPreset) -> Bool {
        switch (lhs, rhs) {
        case (.flat, .flat), (.rock, .rock), (.pop, .pop), (.hipHop, .hipHop), (.electronic, .electronic), (.vocalBooster, .vocalBooster), (.custom, .custom):
            return true
        default:
            return false
        }
    }
    static let all: [UIGraphicalEqualizerPreset] = [.flat, .rock, .pop, .hipHop, .electronic, .vocalBooster, .custom(UIGraphicalEqualizer.unknown)]
    case flat
    case rock
    case pop
    case hipHop
    case electronic
    case vocalBooster
    case custom(UIGraphicalEqualizer)
    var text: String {
        switch self {
        case .flat:
            return NSLocalizedString("sound_profile_flat_uc", value: "FLAT", comment: "Flat")
        case .rock:
            return NSLocalizedString("sound_profile_rock_uc", value: "ROCK", comment: "Rock")
        case .pop:
            return NSLocalizedString("sound_profile_pop_uc", value: "POP", comment: "Pop")
        case .hipHop:
            return NSLocalizedString("sound_profile_hiphop_uc", value: "HIP HOP", comment: "Hip Hop")
        case .electronic:
            return NSLocalizedString("sound_profile_electronic_uc", value: "ELECTRONIC", comment: "Electronic")
        case .vocalBooster:
            return NSLocalizedString("sound_profile_vocalbooster_uc", value: "VOCAL BOOSTER", comment: "Vocal Booster")
        case .custom:
            return NSLocalizedString("sound_profile_custom_uc", value: "CUSTOM", comment: "Custom")
        }
    }
}
extension UIGraphicalEqualizerPreset {
    init(_ graphicalEqualizer: GraphicalEqualizer) {
        switch graphicalEqualizer {
        case GraphicalEqualizerPreset.flat.graphicalEqualizer:
            self = .flat
        case GraphicalEqualizerPreset.rock.graphicalEqualizer:
            self = .rock
        case GraphicalEqualizerPreset.pop.graphicalEqualizer:
            self = .pop
        case GraphicalEqualizerPreset.hipHop.graphicalEqualizer:
            self = .hipHop
        case GraphicalEqualizerPreset.electronic.graphicalEqualizer:
            self = .electronic
        case GraphicalEqualizerPreset.vocalBooster.graphicalEqualizer:
            self = .vocalBooster
        default:
            self = .custom(UIGraphicalEqualizer(graphicalEqualizer))
        }
    }
    init(_ uiGraphicalEqualizer: UIGraphicalEqualizer) {
        self = UIGraphicalEqualizerPreset(uiGraphicalEqualizer.graphicalEqualizer)
    }
    var uiGraphicalEqualizer: UIGraphicalEqualizer {
        switch self {
        case .flat:
            return UIGraphicalEqualizer(GraphicalEqualizerPreset.flat.graphicalEqualizer)
        case .rock:
            return UIGraphicalEqualizer(GraphicalEqualizerPreset.rock.graphicalEqualizer)
        case .pop:
            return UIGraphicalEqualizer(GraphicalEqualizerPreset.pop.graphicalEqualizer)
        case .hipHop:
            return UIGraphicalEqualizer(GraphicalEqualizerPreset.hipHop.graphicalEqualizer)
        case .electronic:
            return UIGraphicalEqualizer(GraphicalEqualizerPreset.electronic.graphicalEqualizer)
        case .vocalBooster:
            return UIGraphicalEqualizer(GraphicalEqualizerPreset.vocalBooster.graphicalEqualizer)
        case .custom(let uiGraphicalEqualizer):
            return uiGraphicalEqualizer
        }
    }
}

class UIGraphicalEqualizer: Equatable, Codable {
    enum Band {
        case bass, low, mid, upper, high
    }
    static let customDefault: UIGraphicalEqualizer = UIGraphicalEqualizer(GraphicalEqualizerPreset.customDefault.graphicalEqualizer)
    static let unknown: UIGraphicalEqualizer = UIGraphicalEqualizer(GraphicalEqualizerPreset.unknown.graphicalEqualizer)
    static func == (lhs: UIGraphicalEqualizer, rhs: UIGraphicalEqualizer) -> Bool {
        return lhs.bass == rhs.bass && lhs.low == rhs.low && lhs.mid == rhs.mid && lhs.upper == rhs.upper && lhs.high == rhs.high
    }
    var bass: UInt8
    var low: UInt8
    var mid: UInt8
    var upper: UInt8
    var high: UInt8
    convenience init() {
        self.init(GraphicalEqualizer())
    }
    init(bass: UInt8, low: UInt8, mid: UInt8, upper: UInt8, high: UInt8) {
        (self.bass, self.low, self.mid, self.upper, self.high) = (bass, low, mid, upper, high)
    }
    init(_ graphicalEqualizer: GraphicalEqualizer) {
        (self.bass, self.low, self.mid, self.upper, self.high) =
        (graphicalEqualizer.bass, graphicalEqualizer.low, graphicalEqualizer.mid, graphicalEqualizer.upper, graphicalEqualizer.high)
    }
    var graphicalEqualizer: GraphicalEqualizer {
        return GraphicalEqualizer(bass: bass, low: low, mid: mid, upper: upper, high: high)
    }
    func value(for band: Band) -> UInt8 {
        switch band {
        case .bass:
            return bass
        case .low:
            return low
        case .mid:
            return mid
        case .upper:
            return upper
        case .high:
            return high
        }
    }
}
