//
//  HelpViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 02/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit


enum HelpScreenEntry {
    case selectDevice
    case website
    case contactSupport
}

protocol HelpViewModelViewInputDelegate: class {
    
}
protocol HelpViewModelViewOutputDelegate {
    func selected(entry:HelpScreenEntry)
}
protocol HelpViewModelFlowInputDelegate {

}
protocol HelpViewModelFlowOutputDelegate: class {
    func didRequestHelpScreenEntry(_ entry: HelpScreenEntry)
}

class HelpViewModel:  BaseViewModel, HelpViewModelFlowInputDelegate, HelpViewModelViewOutputDelegate {

    weak var flowOutputDelegate: HelpViewModelFlowOutputDelegate?
    weak var viewInputDelegate: HelpViewModelViewInputDelegate?

    init(flowOutputDelegate: HelpViewModelFlowOutputDelegate, viewInputDelegate: HelpViewModelViewInputDelegate) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        super.init()
    }

    func selected(entry: HelpScreenEntry) {
        flowOutputDelegate?.didRequestHelpScreenEntry(entry)
    }

}

