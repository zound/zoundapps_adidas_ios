//
//  SpotifyActionButtonConfigurationViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 06/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import ConnectionService
import RxSwift
import Reachability

protocol SpotifyActionButtonConfigurationViewModelViewOutputDelegate {
    func didSelect(action: ButtonAction)
    func viewWillAppear()
    func reachabilityMonitor() -> Reachability
}
protocol SpotifyActionButtonConfigurationViewModelFlowOutputDelegate: class {
    func didRequestSpotifySearch(type: SpotifySearchEntryType, pressType: PressType)
}
enum SpotifySearchEntryType: Int, Codable {
    case playlist
    case artist
    case album
    func notFoundTranslated() -> String {
        switch self {
        case .playlist:
            return NSLocalizedString("spotify_playlists_not_found_headline_uc", comment: "spotify_playlists_not_found_headline_uc")
        case .album, .artist:
            return NSLocalizedString("spotify_list_nothing_found_headline_default", comment: "spotify_list_nothing_found_headline_default")
        }
    }
}
extension SpotifySearchEntryType {
    func action() -> ButtonAction {
        switch self {
        case .playlist: return .playSpotifyPlaylist
        case .artist: return .playSpotifyArtist
        case .album: return .playSpotifyAlbum
        }
    }
}
extension ButtonAction {
    func spotifySearchType() -> SpotifySearchEntryType {
        switch self {
        case .playSpotifyPlaylist: return .playlist
        case .playSpotifyArtist: return .artist
        case .playSpotifyAlbum: return .album
        default: fatalError("No spotify search mapping found for \(self)")
        }
    }
}
struct SpotifyEntry {
    let actionName: String
    let type: SpotifySearchEntryType
    let name: String?
    let active: Bool
}
final class SpotifyActionButtonConfigurationViewModel: BaseViewModel {
    weak var flowOutputDelegate: SpotifyActionButtonConfigurationViewModelFlowOutputDelegate?
    var pressType: PressType
    var model: [ActionButtonEntry]
    var reloadTrigger = PublishSubject<Void>()
    private var services: ServicesType
    init(pressType: PressType, services: ServicesType) {
        self.pressType = pressType
        self.services = services
        self.model = [
            ActionButtonEntry(action: .playSpotifyPlaylist, subtitleDescription: nil, available: true, active: false),
            ActionButtonEntry(action: .playSpotifyArtist, subtitleDescription: nil, available: true, active: false),
            ActionButtonEntry(action: .playSpotifyAlbum, subtitleDescription: nil, available: true, active: false)
        ]
    }
    func updateModel() {
        guard let uuid = services.rxConnectionService.reactiveDevice?.uuid, let stored = DefaultStorage.shared.spotifyLocalData?.perDeviceConfiguration.filter({ $0.uuid == uuid }).first else {
            return
        }
        guard let toUpdate = stored.entries.filter({ $0.pressType == pressType }).first else { return }
        let updated = model.map { entry -> ActionButtonEntry in
            guard entry.action == toUpdate.value.type.action() else {
                return ActionButtonEntry(action: entry.action, subtitleDescription: nil, available: true, active: false)
            }
            return ActionButtonEntry(action: toUpdate.value.type.action(), subtitleDescription: toUpdate.value.name, available: true, active: true)
        }
        model = updated
        reloadTrigger.onNext(())
    }
}
extension SpotifyActionButtonConfigurationViewModel: SpotifyActionButtonConfigurationViewModelViewOutputDelegate {
    func viewWillAppear() {
        updateModel()
    }
    
    func didSelect(action: ButtonAction) {
        flowOutputDelegate?.didRequestSpotifySearch(type: action.spotifySearchType(), pressType: pressType)
    }
    func reachabilityMonitor() -> Reachability {
        return services.reachability
    }
}
