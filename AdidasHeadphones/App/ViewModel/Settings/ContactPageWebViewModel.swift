//
//  ContactPageWebViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 04/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class ContactPageWebViewModel: CommonWebViewModel {
    init(flowOutputDelegate: CommonWebViewModelFlowOutputDelegate, viewInputDelegate: CommonWebViewModelViewInputDelegate, services: ServicesType) {
        let title = NSLocalizedString("help_contact_title_uc", value: "CONTACT SUPPORT", comment: "CONTACT SUPPORT")
        let request = services.urlRequests.contactPageUrlRequest()
        super.init(flowOutputDelegate: flowOutputDelegate, viewInputDelegate: viewInputDelegate, request: request, title: title, services: services)
    }
}
