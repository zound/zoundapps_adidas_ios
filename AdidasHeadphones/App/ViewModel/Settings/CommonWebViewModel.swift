//
//  CommonWebViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 04/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import Reachability
import RxSwift
import UIKit


protocol CommonWebViewModelViewInputDelegate: class {
    func refreshRequest(_ request: URLRequest?)
    func refreshTitle(_ title: String)
    func setIndicator(hidden: Bool)
}
protocol CommonWebViewModelViewOutputDelegate {
    func viewDidLoad()
    func reachabilityMonitor() -> Reachability
    func webViewDidStartLoading()
    func webViewDidFinish()
    func webViewDidFail()
}
protocol CommonWebViewModelFlowInputDelegate {
}
protocol CommonWebViewModelFlowOutputDelegate: class {
}
class CommonWebViewModel: BaseViewModel, CommonWebViewModelFlowInputDelegate, CommonWebViewModelViewOutputDelegate {
    weak var flowOutputDelegate: CommonWebViewModelFlowOutputDelegate?
    weak var viewInputDelegate: CommonWebViewModelViewInputDelegate?
    private let request: URLRequest?
    private let title: String
    private let reachability: Reachability
    private var loaded = false
    private var disposeBag = DisposeBag()

    init(flowOutputDelegate: CommonWebViewModelFlowOutputDelegate,
         viewInputDelegate: CommonWebViewModelViewInputDelegate,
         request: URLRequest?, title: String, services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.title = title
        self.request = request
        self.reachability = services.reachability
        super.init()
    }
    func viewDidLoad() {
        viewInputDelegate?.refreshTitle(title)
        if reachability.connection != .unavailable {
            viewInputDelegate?.refreshRequest(request)
        }
        reachability.rx.reachabilityChanged.subscribe(onNext: { [weak self] reachability in
            self?.handleInternetStatus(status: reachability.connection)
        }).disposed(by: disposeBag)
    }
    func handleInternetStatus(status: Reachability.Connection){
        if status != .unavailable && loaded == false {
            viewInputDelegate?.refreshRequest(request)
        }
    }
    func reachabilityMonitor() -> Reachability {
        return self.reachability
    }
    func webViewDidStartLoading() {
        viewInputDelegate?.setIndicator(hidden: false)
    }
    func webViewDidFinish() {
        viewInputDelegate?.setIndicator(hidden: true)
        loaded = true
    }
    func webViewDidFail() {
        viewInputDelegate?.setIndicator(hidden: true)
    }
}

