//
//  SpotifyAppInfoViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 04/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSpotifyWrapper
import RxSwift
import SafariServices
import Reachability

public enum SpotifyAppState {
    case notConfigured
    case connecting
    case connected
}
protocol SpotifyAppInfoViewModelViewOutputDelegate {
    func connectToSpotify()
    func viewWillAppear()
    func didRequestSpotifyWebPage()
    func authorizationWebViewDismissed()
    func didRequestDeauthorizeAppWebPage()
    func reachabilityMonitor() -> Reachability
}
public protocol SpotifyAppInfoViewModelViewInputDelegate: class {
    func set(state: SpotifyAppState, for userId: String?)
    func present(authorizationViewController: UIViewController)
    func dismissAuthorization()
}
protocol SpotifyAppInfoViewModelFlowInputDelegate { }
protocol SpotifyAppInfoViewModelFlowOutputDelegate: class {
    func openSpotifyAccountPage()
    func popWhileAuthorizing()
}

final public class SpotifyAppInfoViewModel: NSObject, SpotifyAppInfoViewModelViewOutputDelegate, SpotifyAppInfoViewModelFlowInputDelegate {
    private let spotifyManager: RxSpotifyManagerType
    private let reachability: Reachability
    private let disposeBag = DisposeBag()
    public weak var viewInputDelegate: SpotifyAppInfoViewModelViewInputDelegate?
    weak var flowOutputDelegate: SpotifyAppInfoViewModelFlowOutputDelegate?
    init(services: ServicesType) {
        self.spotifyManager = services.rxSpotifyManager
        self.reachability = services.reachability
        super.init()
        self.spotifyManager.changed().subscribe(
            onNext: { [weak self] appStatus in
                self?.update(status: appStatus)
            }
        )
        .disposed(by: disposeBag)
        services.applicationModeService.appDidBecomeActive
            .withLatestFrom(self.spotifyManager.rxAuthStatus) { (_, connected) -> Bool in
                if case .connected = connected { return true }
                return false
            }
            .filter{ $0 == true }
            .do(onNext: { [weak self] _ in
                self?.viewInputDelegate?.set(state: .connected, for: nil)
            })
            .flatMap { [weak self] _ -> Observable<SpotifyUserInfo> in
                guard let strongSelf = self else { return Observable.empty() }
                return RxStreamingSpotifyManager.user(accessToken: strongSelf.spotifyManager.accessToken ?? "")
            }
            .subscribe(onNext: { [weak self] userInfo in
                self?.viewInputDelegate?.set(state: .connected, for: userInfo.name)
            })
            .disposed(by: disposeBag)
    }
    func viewWillAppear() {
        guard case .connected = self.spotifyManager.rxAuthStatus.value else {
            self.update(status: .notConnected)
            return
        }
        self.update(status: .connected)
        guard let accessToken = spotifyManager.accessToken else { return }
        var internalDisposeBag = DisposeBag()
        RxStreamingSpotifyManager.user(accessToken: accessToken).subscribe(
            onNext: { [weak self] info in
                self?.viewInputDelegate?.set(state: .connected, for: info.name)
            },
            onCompleted: {
                internalDisposeBag = DisposeBag()
            }
        )
        .disposed(by: internalDisposeBag)
    }
    func connectToSpotify() {
        let authorizationViewController = spotifyManager.authorizationViewController()
        authorizationViewController.modalPresentationStyle = .fullScreen
        authorizationViewController.delegate = self
        spotifyManager.startedAuthorization()
        viewInputDelegate?.present(authorizationViewController: authorizationViewController)
    }
    func didRequestSpotifyWebPage() {
        flowOutputDelegate?.openSpotifyAccountPage()
    }
    private func update(status: ConnectedApplicationStatus) {
        switch status {
        case .checking:
            self.viewInputDelegate?.set(state: .connecting, for: nil)
        case .connected:
            self.viewInputDelegate?.set(state: .connected, for: nil)
        case .notConnected:
            self.viewInputDelegate?.set(state: .notConfigured, for: nil)
        case .authorizationCallback:
            self.viewInputDelegate?.set(state: .connecting, for: nil)
            self.viewInputDelegate?.dismissAuthorization()
        default:
            self.viewInputDelegate?.set(state: .notConfigured, for: nil)
        }
    }
    func authorizationWebViewDismissed() {
        flowOutputDelegate?.popWhileAuthorizing()
    }
    func didRequestDeauthorizeAppWebPage() {
        flowOutputDelegate?.openSpotifyAccountPage()
    }
    func reachabilityMonitor() -> Reachability {
        return self.reachability
    }
}
extension SpotifyAppInfoViewModel: SFSafariViewControllerDelegate {
    public func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.spotifyManager.rxAuthStatus.accept(.notConnected)
    }
}

