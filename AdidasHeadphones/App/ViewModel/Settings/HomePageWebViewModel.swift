//
//  HomePageWebViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 04/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

class HomePageWebViewModel: CommonWebViewModel {
    init(flowOutputDelegate: CommonWebViewModelFlowOutputDelegate, viewInputDelegate: CommonWebViewModelViewInputDelegate, services: ServicesType) {
        let title = NSLocalizedString("help_website_title_uc", value: "ADIDASHEADPHONES.COM", comment: "ADIDASHEADPHONES.COM")
        let request = services.urlRequests.homePageUrlRequest()
        super.init(flowOutputDelegate: flowOutputDelegate, viewInputDelegate: viewInputDelegate, request: request, title: title, services: services)
    }
}
