//
//  NewsletterParentViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 10/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import Reachability
import RxSwift

protocol NewsletterParentViewModelFlowOutputDelegate: class {
    func didTapPrivacyPolicy()
}
protocol NewsletterParentViewModelViewInputDelegate: class {
    func concludeOKButton(hidden: Bool)
}

class NewsletterParentViewModel: BaseViewModel  {
    weak var flowOutputDelegate: NewsletterParentViewModelFlowOutputDelegate?
    weak var viewInputDelegate: NewsletterParentViewModelViewInputDelegate?
    private var services: ServicesType
    var newsletterViewModel: NewsletterViewModel
    private let reachability: Reachability
    private var disposeBag = DisposeBag()

    init(flowOutputDelegate: NewsletterParentViewModelFlowOutputDelegate, viewInputDelegate: NewsletterParentViewModelViewInputDelegate, services: ServicesType) {
        self.viewInputDelegate = viewInputDelegate
        self.flowOutputDelegate = flowOutputDelegate
        self.services = services
        self.newsletterViewModel = NewsletterViewModel(services: services)
        self.reachability = services.reachability
        super.init()
        self.newsletterViewModel.flowOutputDelegate = self
    }
    func okButtonTouched() {
        self.newsletterViewModel.subscribe()
    }
    func reachabilityMonitor() -> Reachability {
        return reachability
    }
    func viewDidLoad(){
        reachability.rx.reachabilityChanged.subscribe(onNext: { [weak self] reachability in
            self?.updateButton()
        }).disposed(by: disposeBag)
    }
}

extension NewsletterParentViewModel: NewsletterViewModelFlowOutputDelegate {
    func didTapPrivacyPolicy() {
       flowOutputDelegate?.didTapPrivacyPolicy()
    }
    func signUpFinishedWithResult(result: NewsletterResult) {
        updateButton()
    }
    func signOutFinishedWithResult(result: NewsletterResult) {
        updateButton()
    }
    func newsletterChangeInProgress() {
        updateButton()
    }
    private func updateButton(){
        let hidden = !newsletterViewModel.concludeOkButtonEnabled()
        viewInputDelegate?.concludeOKButton(hidden: hidden)
    }
    func keyboard(shown: Bool) {
    }
    func email(validated: Bool) {
        updateButton()
    }
}
