//
//  AboutThisDeviceViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 17/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

protocol AboutThisDeviceViewModelViewInputDelegate: class {
    func update(withDeviceData: [AboutItem])
}
protocol AboutThisDeviceViewModelViewOutputDelegate {
    func viewDidLoad()
}
protocol AboutThisDeviceViewModelFlowInputDelegate {}
protocol AboutThisDeviceViewModelFlowOutputDelegate: class {}

class AboutThisDeviceViewModel: BaseViewModel, AboutThisDeviceViewModelFlowInputDelegate, AboutThisDeviceViewModelViewOutputDelegate {
    weak var flowOutputDelegate: AboutThisDeviceViewModelFlowOutputDelegate?
    weak var viewInputDelegate: AboutThisDeviceViewModelViewInputDelegate?
    private let services: ServicesType
    private let disposeBag = DisposeBag()
    init(flowOutputDelegate: AboutThisDeviceViewModelFlowOutputDelegate, viewInputDelegate: AboutThisDeviceViewModelViewInputDelegate, services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
    
    func viewDidLoad() {
        let connectionService = services.rxConnectionService
        guard let device = connectionService.reactiveDevice else {
            return
        }
        Observable.combineLatest(device.serialNumber,
                                 device.modelNumber,
                                 device.firmwareRevision)
            .subscribe(
                onNext: { [weak self] serialNumberResult, modelNumberResult, firmwareRevisionResult in
                    switch (serialNumberResult, modelNumberResult, firmwareRevisionResult) {
                    case (.success(let serialNumber), .success(let modelNumber), .success(let firmwareRevision)):
                        let result = [
                            AboutItem(title: AboutFeature.name.title(), state: .value(device.name)),
                            AboutItem(title: AboutFeature.serial.title(), state: .value(serialNumber == String() ? String.invisible: serialNumber)),
                            AboutItem(title: AboutFeature.model.title(), state: .value(modelNumber == String() ? String.invisible:  modelNumber)),
                            AboutItem(title: AboutFeature.firmware.title(), state: .value(firmwareRevision == String() ? String.invisible: firmwareRevision))
                        ]
                        self?.viewInputDelegate?.update(withDeviceData: result)
                    default:
                        break
                    }
                }
            ).disposed(by: disposeBag)
        
        device.read(deviceFeature: .serialNumber)
        device.read(deviceFeature: .modelNumber)
        device.read(deviceFeature: .firmwareRevision)
    }
}
