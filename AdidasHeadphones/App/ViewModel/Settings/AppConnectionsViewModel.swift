//
//  AppConnectionsViewModel.swift
//  AppLibrary
//
//  Created by Grzegorz Kiel on 30/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxSpotifyWrapper
import UIKit
import Reachability

protocol AppConnectionsViewModelViewInputDelegate: class {
    func refresh()
}
protocol AppConnectionsViewModelViewOutputDelegate {
    func updateAppStates()
    func didSelectApp(id: ConnectedApplicationID)
    func reachabilityMonitor() -> Reachability
}
protocol SpotifyConnectedAppDelegate: class {
    func openSpotifyAppInfo()
}
protocol AppConnectionsViewModelFlowInputDelegate: class {
    func didFinishAppStoreRequest()
}
typealias AppConnectionsViewModelFlowOutputDelegate = SpotifyConnectedAppDelegate

enum ConnectedApplicationID: CaseIterable {
    case spotify
    func icon() -> UIImage {
        switch self {
        case .spotify:
            return Image.Icon.Service.spotify
        }
    }
    func title() -> String {
        switch self {
        case .spotify:
            return NSLocalizedString("appwide_spotify_uc", value: "SPOTIFY", comment: "Spotify")
        }
    }
}

protocol ConnectedApplicationInfoProvider {
    func info(delegate: AppConnectionsViewModelFlowOutputDelegate?) -> ConnectedApplicationInfo
    func changed() -> Observable<ConnectedApplicationStatus>
    func checkStatus()
}

enum ConnectedApplicationStatus {
    case checking
    case authorizationCallback
    case notInstalled
    case notConfigured
    case notConnected
    case connected
}

extension ConnectedApplicationStatus {
    func label() -> String {
        switch self {
        case .checking, .authorizationCallback: return String.invisible
        case .notInstalled: return NSLocalizedString("app_connections_app_not_installed", value: "Install", comment: "Install app")
        case .notConfigured: return NSLocalizedString("app_connections_app_not_connected", value: "Configure", comment: String.invisible)
        case .notConnected: return NSLocalizedString("app_connections_app_not_onnected", value: "Connect to Spotify", comment: "Connect to Spotify")
        case .connected: return NSLocalizedString("app_connections_app_connected", value: "Connected", comment: "Application connected")
        }
    }
}

struct ConnectedApplicationInfo {
    let id: ConnectedApplicationID
    let icon: UIImage
    let name: String
    let status: ConnectedApplicationStatus
    let didSelectRowHandler: (AppConnectionsViewModelFlowOutputDelegate?) -> Void
}

final class AppConnectionsViewModel: AppConnectionsViewModelViewOutputDelegate, AppConnectionsViewModelFlowInputDelegate {
    weak var viewInputDelegate: AppConnectionsViewModelViewInputDelegate?
    weak var flowOutputDelegate: AppConnectionsViewModelFlowOutputDelegate?
    private let disposeBag = DisposeBag()
    private let spotifyManger: RxSpotifyManagerType
    private let reachability: Reachability
    private var infoProviders:[ConnectedApplicationInfoProvider] = []
    var connectedAppsInfo:[ConnectedApplicationInfo] = []

    init(services: ServicesType) {
        self.spotifyManger = services.rxSpotifyManager
        self.reachability = services.reachability
        infoProviders.append(self.spotifyManger)
        connectedAppsInfo = infoProviders.map { $0.info(delegate: flowOutputDelegate) }
        
        Observable.combineLatest(infoProviders.map{ $0.changed() })
            .distinctUntilChanged()
            .subscribeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] status in
                    guard let strongSelf = self else { return }
                    strongSelf.connectedAppsInfo = strongSelf.infoProviders.map { $0.info(delegate: strongSelf.flowOutputDelegate) }
                    strongSelf.viewInputDelegate?.refresh()
                })
            .disposed(by: disposeBag)
        
        services.applicationModeService.appDidBecomeActive
            .subscribe(onNext: { [weak self] _ in
                self?.updateAppStates()
            })
            .disposed(by: disposeBag)
        }
    
    func updateAppStates() {
        infoProviders.forEach { $0.checkStatus() }
    }
    func didSelectApp(id: ConnectedApplicationID) {
        guard let handler = connectedAppsInfo.filter({ $0.id == id }).first?.didSelectRowHandler else {
            return
        }
        handler(flowOutputDelegate)
    }
    func reachabilityMonitor() -> Reachability {
        return self.reachability
    }
    func didFinishAppStoreRequest() {
        updateAppStates()
    }
}
