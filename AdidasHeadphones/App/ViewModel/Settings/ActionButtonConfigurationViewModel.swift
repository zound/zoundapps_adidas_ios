//
//  ActionButtonConfigurationViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 02/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import ConnectionService
import RxSwift

protocol ActionButtonConfigurationViewModelViewOutputDelegate {
    func didSelect(action: ButtonAction)
    func didSelect(application: ConnectedApplicationID)
    func viewDidLoad()
}
protocol ActionButtonConfigurationViewModelFlowOutputDelegate: class {
    func didFinishButtonConfiguration()
    func didRequestApplicationConfiguration(id: ConnectedApplicationID)
    func didRequestSpotifyButtonConfiguration(pressType: PressType)
}
enum GroupType: Int {
    case applicationActions
    case staticActions
}
extension ButtonAction {
    func configurationIcon() -> UIImage {
        switch self {
        case .noAction:
            return Image.Icon.Service.customized
        case .googleVoiceAssistantBisto:
            return Image.Icon.Service.googleAssistant
        case .defaultVoiceAssistant:
            return Image.Icon.Service.customized
        case .equalizer:
            return Image.Icon.Service.customized
        case .playSpotifyPlaylist, .playSpotifyArtist, .playSpotifyAlbum:
            return Image.Icon.Service.spotify
        }
    }
    func alternativeIcon() -> UIImage? {
        switch self {
        case .noAction:
            return Image.Icon.Black.notSet
        default:
            return nil
        }
    }
    func configurationTitle(for pressType: PressType) -> String {
        switch self {
        case .noAction:
            return NSLocalizedString("action_button_configuration_do_nothing_uc", value: "DO NOTHING", comment: "Do nothing")
        default:
            return self.title(for: pressType)
        }
    }
}
struct ActionButtonEntry {
    let action: ButtonAction
    let subtitleDescription: String?
    let available: Bool
    let active: Bool
}
struct GroupedActions {
    let type: GroupType
    let applicationID: ConnectedApplicationID?
    let entries: [ActionButtonEntry]
    let folded: Bool
}
final class ActionButtonConfigurationViewModel: BaseViewModel {
    weak var flowOutputDelegate: ActionButtonConfigurationViewModelFlowOutputDelegate?
    var model = [GroupedActions]()
    var pressType: PressType
    private let services: ServicesType
    private var disposeBag = DisposeBag()
    let modelUpdated = PublishSubject<Void>()
    init(pressType: PressType, value: ButtonAction, services: ServicesType) {
        self.pressType = pressType
        self.services = services
        self.model = [
            GroupedActions(type: .applicationActions,
                           applicationID: .spotify,
                           entries: [],
                           folded: true),
            GroupedActions(type: .staticActions,
                           applicationID: nil,
                           entries: [
                            ActionButtonEntry(action: .defaultVoiceAssistant, subtitleDescription: nil, available: true, active: value == .defaultVoiceAssistant),
                            /*ActionButtonEntry(action: .equalizer, subtitleDescription: nil, available: true, active: value == .equalizer),*/
                            ActionButtonEntry(action: .noAction, subtitleDescription: nil, available: true, active: value == .noAction)
                           ],
                           folded: false)
        ]
    }
}
extension ActionButtonConfigurationViewModel: ActionButtonConfigurationViewModelViewOutputDelegate {
    func didSelect(application: ConnectedApplicationID) {
        switch application {
        case .spotify:
            let currentStatus = self.services.rxSpotifyManager.rxAuthStatus.value
            switch currentStatus {
            case .connected:
                flowOutputDelegate?.didRequestSpotifyButtonConfiguration(pressType: pressType)
            default:
                flowOutputDelegate?.didRequestApplicationConfiguration(id: application)
                break
            }
            
        }
    }
    func didSelect(action: ButtonAction) {
        let newModel = model.map { actions -> GroupedActions in
            guard actions.type == .staticActions else {
                return actions
            }
            let newEntries = actions.entries.map { entry -> ActionButtonEntry in
                return ActionButtonEntry(action: entry.action, subtitleDescription: nil, available: entry.available, active: entry.action == action)
            }
            return GroupedActions(type: .staticActions, applicationID: nil, entries: newEntries, folded: false)
        }
        model = newModel
        modelUpdated.onNext(())
        services.actionButtonService.set(action: action, for: pressType)
        flowOutputDelegate?.didFinishButtonConfiguration()
    }
    func viewDidLoad() {
        self.services.actionButtonService.rxButtonConfiguration
            .skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] configuration in
                    guard configuration.local == false else { return }
                    guard let strongSelf = self else { return }
                    let newModel = strongSelf.model.map { actions -> GroupedActions in
                        guard actions.type == .staticActions else {
                            return actions
                        }
                        guard let currentlySelected = configuration.events
                            .filter({ $0.pressType == strongSelf.pressType }).first else {
                                return actions
                        }
                        let newEntries = actions.entries.map { entry -> ActionButtonEntry in
                            return ActionButtonEntry(action: entry.action, subtitleDescription: nil, available: entry.available, active: entry.action == currentlySelected.buttonAction)
                        }
                        return GroupedActions(type: .staticActions, applicationID: nil, entries: newEntries, folded: false)
                    }
                    strongSelf.model = newModel
                    strongSelf.modelUpdated.onNext(())
                }
            )
            .disposed(by: disposeBag)
        self.services.actionButtonService.requestConfiguration()
    }
}
