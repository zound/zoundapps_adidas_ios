//
//  SelectYourDeviceViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 03/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import ConnectionService

enum SelectYourDeviceScreenEntry {
    case arnold
    case freeman
    case desir
}
protocol SelectYourDeviceViewModelViewInputDelegate: class {
}
protocol SelectYourDeviceViewModelViewOutputDelegate {
    func selected(entry:SelectYourDeviceScreenEntry)
}
protocol SelectYourDeviceViewModelFlowInputDelegate {
}
protocol SelectYourDeviceViewModelFlowOutputDelegate: class {
    func didRequestAboutThisAppEntry(_ entry: SelectYourDeviceScreenEntry)
}
class SelectYourDeviceViewModel:  BaseViewModel, SelectYourDeviceViewModelFlowInputDelegate, SelectYourDeviceViewModelViewOutputDelegate {

    weak var flowOutputDelegate: SelectYourDeviceViewModelFlowOutputDelegate?
    weak var viewInputDelegate: SelectYourDeviceViewModelViewInputDelegate?

    init(flowOutputDelegate: SelectYourDeviceViewModelFlowOutputDelegate, viewInputDelegate: SelectYourDeviceViewModelViewInputDelegate) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        super.init()
    }
    func selected(entry: SelectYourDeviceScreenEntry) {
        flowOutputDelegate?.didRequestAboutThisAppEntry(entry)
    }
}

