//
//  GoogleAssistantHelpViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 21/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

protocol GoogleAssistantHelpViewModelViewInputDelegate: class {
}
protocol GoogleAssistantHelpViewModelViewOutputDelegate {
    func didRequestDownloadGoogleAssistant()
}
protocol GoogleAssistantHelpViewModelFlowInputDelegate {
    func viewDidLoad()
}
protocol GoogleAssistantHelpViewModelFlowOutputDelegate: class {
}
class GoogleAssistantHelpViewModel:  BaseViewModel, GoogleAssistantHelpViewModelFlowInputDelegate, GoogleAssistantHelpViewModelViewOutputDelegate {
    private let googleAssistanLink = "itms-apps://itunes.apple.com/app/id1220976145"
    weak var flowOutputDelegate: GoogleAssistantHelpViewModelFlowOutputDelegate?
    weak var viewInputDelegate: GoogleAssistantHelpViewModelViewInputDelegate?
    init(flowOutputDelegate: GoogleAssistantHelpViewModelFlowOutputDelegate, viewInputDelegate: GoogleAssistantHelpViewModelViewInputDelegate) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        super.init()
    }
    func didRequestDownloadGoogleAssistant() {
        if let url = URL(string: googleAssistanLink) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func viewDidLoad() {
        DefaultStorage.shared.googleAssistantHelpPresented = true
    }
}
