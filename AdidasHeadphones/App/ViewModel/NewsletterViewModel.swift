//
//  NewsletterViewModel.swift
//  AdidasHeadphones
//
//  Created by Grzegorz Kiel on 22/08/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import Reachability


enum NewsletterErrorStatus {
    case noError
    case emailInvalid
    case somethingWentWrong
}
private struct Config {
    static let charactersToActivateValidator = 3
}
private class EmailValidator: EmailValidating { }
protocol NewsletterViewModelFlowOutputDelegate: class {
    func keyboard(shown: Bool)
    func email(validated: Bool)
    func didTapPrivacyPolicy()
    func signUpFinishedWithResult(result: NewsletterResult)
    func signOutFinishedWithResult(result: NewsletterResult)
    func newsletterChangeInProgress()
}
protocol NewsletterViewModelViewInputDelegate: class {
    func toggle(subscriptionStatus: Bool, showKeyboard: Bool)
    func setup(subscribed: Bool, email: String?, error: NewsletterErrorStatus, indicator: Bool)
    func updateErrorStatus(status: NewsletterErrorStatus)
    func updateIndicator(animating: Bool)
    func updateCheckBox(isEnabled: Bool)
}
final class NewsletterViewModel:  NewsletterServiceResult {
    weak var viewInputDelegate: NewsletterViewModelViewInputDelegate?
    weak var flowOutputDelegate: NewsletterViewModelFlowOutputDelegate?
    private var toggled : Bool
    private let validator = EmailValidator()
    private var services: ServicesType
    private var currentEmail: String?
    private var currentErrorState = NewsletterErrorStatus.noError
    private var disposeBag = DisposeBag()
    private let reachability: Reachability

    init(services: ServicesType) {
        let newsletterService = services.newsletterService
        self.services = services
        self.toggled = newsletterService.newsletterSubscription
        if newsletterService.newsletterSubscription {
            self.currentEmail = newsletterService.subscriptionEmail
        } else {
            self.currentEmail = ""
        }
        self.reachability = services.reachability
        self.services.newsletterService.delegate = self
    }
    private func updateError(result: NewsletterResult, resetOnError: Bool){
        switch result {
        case .error:
            currentErrorState = .somethingWentWrong
            if resetOnError {
                toggled = services.newsletterService.newsletterSubscription
                viewInputDelegate?.toggle(subscriptionStatus: toggled, showKeyboard: false)
            }
        case .ok:
            currentErrorState = .noError
        }
        viewInputDelegate?.updateErrorStatus(status: currentErrorState)
    }
    func signUpFinishedWithResult(result: NewsletterResult) {
        updateError(result: result, resetOnError: false)
        viewInputDelegate?.updateIndicator(animating: workInProgress())
        flowOutputDelegate?.signUpFinishedWithResult(result: result)
    }
    func signOutFinishedWithResult(result: NewsletterResult) {
        updateError(result: result, resetOnError: true)
        viewInputDelegate?.updateIndicator(animating: workInProgress())
        flowOutputDelegate?.signOutFinishedWithResult(result: result)
    }
    func subscribe(){
        if let currentEmail = currentEmail{
            currentErrorState = .noError
            self.services.newsletterService.signUp(with: currentEmail)
            viewInputDelegate?.updateIndicator(animating: workInProgress())
            viewInputDelegate?.updateErrorStatus(status: currentErrorState)
            flowOutputDelegate?.newsletterChangeInProgress()
        }
    }
    func unSubscribe(){
        if services.newsletterService.newsletterSubscription {
            currentErrorState = .noError
            self.services.newsletterService.signOut()
            viewInputDelegate?.updateIndicator(animating: workInProgress())
            viewInputDelegate?.updateErrorStatus(status: currentErrorState)
            flowOutputDelegate?.newsletterChangeInProgress()
        }
    }
    func setup() {
        viewInputDelegate?.setup(
            subscribed: toggled,
            email: currentEmail,
            error: currentErrorState,
            indicator: workInProgress()
        )
        handleInternetStatus(status: reachability.connection)
        reachability.rx.reachabilityChanged.subscribe(onNext: { [weak self] reachability in
            self?.handleInternetStatus(status: reachability.connection)
        }).disposed(by: disposeBag)
        guard toggled == true else {
            flowOutputDelegate?.email(validated:true)
            return
        }
        flowOutputDelegate?.email(validated:toggled)
    }

    func handleInternetStatus(status: Reachability.Connection){
        viewInputDelegate?.updateCheckBox(isEnabled: status != .unavailable)
    }
    func keyboard(shown: Bool) {
        flowOutputDelegate?.keyboard(shown: shown)
    }
    func toggleNewsletter(email: String?) {
        guard workInProgress() == false else {
            viewInputDelegate?.toggle(subscriptionStatus: toggled, showKeyboard: false)
            return
        }
        currentEmail = email
        toggled.toggle()
        viewInputDelegate?.toggle(subscriptionStatus: toggled, showKeyboard: true)

        if toggled != self.services.newsletterService.newsletterSubscription {
            if !toggled {
                unSubscribe()
                return
            }
        } else {
            if !toggled {
                currentErrorState = .noError
                viewInputDelegate?.updateErrorStatus(status: currentErrorState)
            }
        }
        guard toggled == true else {
            flowOutputDelegate?.email(validated: true)
            return
        }
        guard let email = email, email.count >= Config.charactersToActivateValidator else {
            flowOutputDelegate?.email(validated: false)
            return
        }
        flowOutputDelegate?.email(validated: validator.validate(email: email))
    }
    func changed(email: String?) {
        currentEmail = email
        guard let email = email, email.count >= Config.charactersToActivateValidator else {
            flowOutputDelegate?.email(validated: false)
            return
        }
        let valid = validator.validate(email: email)
        let errorState: NewsletterErrorStatus = valid ? .noError : .emailInvalid
        if errorState != currentErrorState {
            currentErrorState = errorState
            viewInputDelegate?.updateErrorStatus(status: currentErrorState)
        }
        flowOutputDelegate?.email(validated: valid)
    }
    func isInternetConnection() -> Bool {
        return reachability.connection != .unavailable
    }
    func workInProgress() -> Bool {
        return self.services.newsletterService.workInProgress
    }
    private func isEmailChanged() -> Bool {
        let prev = self.services.newsletterService.subscriptionEmail
        let current = currentEmail ?? ""
        return prev != current
    }
    func concludeOkButtonEnabled() -> Bool {
        if workInProgress() {
            return false
        }
        if toggled == false {
            return false
        }
        if reachability.connection == .unavailable {
            return false
        }
        if !isEmailChanged() && self.services.newsletterService.newsletterSubscription {
            return false
        }
        guard let email = currentEmail, email.count >= Config.charactersToActivateValidator else {
            return false
        }
        return validator.validate(email: email)
    }
    func concludeButtonEnabled() -> Bool {
        if workInProgress() {
            return false
        }
        if reachability.connection == .unavailable {
            if isEmailChanged() && toggled{
                return false
            }
        }
        guard toggled == true else {
            return true
        }
        guard let email = currentEmail, email.count >= Config.charactersToActivateValidator else {
            return false
        }
        return validator.validate(email: email)
    }
    func didTapPrivacyPolicy() {
        flowOutputDelegate?.didTapPrivacyPolicy()
    }
    func reachabilityMonitor() -> Reachability{
        return reachability
    }
}
