//
//  GuideViewModel.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import Reachability
import RxSwift

enum GuideItemType {
    case actionButton_rpt01
    case equalizer_rpt01
    case stayUpdated_rpt01
    case actionButton_fwd01
    case ergo_fwd01
    case knob_fwd01
    case stayUpdated_fwd01
    case actionButton_rpd01
    case ergo_rpd01
    case knob_rpd01
    case stayUpdated_rpd01
}
protocol GuideViewModelViewInputDelegate: class {
    func add(guideItems: [GuideItem])
    func scrolling(enabled: Bool)
    func concludeGuideButton(enabled: Bool)
    func removeAll()
}
protocol GuideViewModelViewOutputDelegate {
    func viewDidAppear()
    func viewDidLoad()
    func didTapNextButtonAtLastCell()
    func reachabilityMonitor() -> Reachability
}
protocol GuideViewModelFlowOutputDelegate: class {
    func didFinishGuide()
    func didTapPrivacyPolicy()
}
class GuideViewModel: BaseViewModel, GuideViewModelViewOutputDelegate {

    weak var flowOutputDelegate: GuideViewModelFlowOutputDelegate?
    weak var viewInputDelegate: GuideViewModelViewInputDelegate?
    private var services: ServicesType
    private var shouldLoad = true
    var newsletterViewModel: NewsletterViewModel
    private let reachability: Reachability
    private var disposeBag = DisposeBag()
    public let isNewsletterScreenDisable: Bool

    init(flowOutputDelegate: GuideViewModelFlowOutputDelegate, viewInputDelegate: GuideViewModelViewInputDelegate, services: ServicesType) {
        self.viewInputDelegate = viewInputDelegate
        self.flowOutputDelegate = flowOutputDelegate
        self.services = services
        self.newsletterViewModel = NewsletterViewModel(services: services)
        self.reachability = services.reachability
        self.isNewsletterScreenDisable = services.newsletterService.newsletterSubscription
        super.init()
        self.newsletterViewModel.flowOutputDelegate = self
    }
}
extension GuideViewModel {
    func reachabilityMonitor() -> Reachability {
        return reachability
    }
    func viewDidLoad() {
        shouldLoad = true
        reachability.rx.reachabilityChanged.subscribe(onNext: { [weak self] reachability in
            self?.updateButton()
        }).disposed(by: disposeBag)
    }
    func viewDidAppear() {
        guard shouldLoad else{
            return
        }
        guard let knownDevice = services.rxConnectionService.knownDevice?.deviceTypeWithTraits else {
            return
        }
        shouldLoad = false
        var guideItems = knownDevice.guideItems
        if isNewsletterScreenDisable {
            guideItems = guideItems.filter({ item -> Bool in
                return !item.newsletterVisible
            })
        }
        viewInputDelegate?.add(guideItems: guideItems)
    }
    func didTapNextButtonAtLastCell() {
        if newsletterViewModel.concludeOkButtonEnabled() {
            newsletterViewModel.subscribe()
        } else {
            viewInputDelegate?.removeAll()
            flowOutputDelegate?.didFinishGuide()
        }
    }
    func checkNewsletterStatus() -> Bool {
        return newsletterViewModel.concludeButtonEnabled()
    }
}
extension GuideViewModel: NewsletterViewModelFlowOutputDelegate {

    func signUpFinishedWithResult(result: NewsletterResult){
        if result == .ok{
            flowOutputDelegate?.didFinishGuide()
        }
        updateButton()
    }
    func signOutFinishedWithResult(result: NewsletterResult){
        updateButton()
    }
    func newsletterChangeInProgress() {
        updateButton()
    }
    private func updateButton(){
        let validated = newsletterViewModel.concludeButtonEnabled()
        viewInputDelegate?.concludeGuideButton(enabled: validated)
    }
    func didTapPrivacyPolicy() {
        flowOutputDelegate?.didTapPrivacyPolicy()
    }
    func keyboard(shown: Bool) {
        viewInputDelegate?.scrolling(enabled: shown == false)
    }
    func email(validated: Bool) {
        viewInputDelegate?.concludeGuideButton(enabled: validated)
    }
}
