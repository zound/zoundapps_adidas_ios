//
//  BaseViewModel.swift
//  002
//
//  Created by Łukasz Wudarski on 27/03/2019.
//  Copyright © 2019 LWTeam. All rights reserved.
//

import Foundation

class BaseViewModel {
    deinit {
        print(#function, String(describing: self))
    }
}
