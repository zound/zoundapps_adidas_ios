//
//  QualityProgramViewModel.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 11/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

protocol QualityProgramViewModelViewInputDelegate: class {
    func setup(permissions : Bool)
}
protocol QualityProgramViewModelViewOutputDelegate {
    func viewDidLoad()
}
protocol QualityProgramViewModelFlowInputDelegate {
}
protocol QualityProgramViewModelFlowOutputDelegate: class {
}

class QualityProgramViewModel: BaseViewModel, QualityProgramViewModelFlowInputDelegate, QualityProgramViewModelViewOutputDelegate {
    weak var flowOutputDelegate: QualityProgramViewModelFlowOutputDelegate?
    weak var viewInputDelegate: QualityProgramViewModelViewInputDelegate?
    private let services: ServicesType

    init(flowOutputDelegate: QualityProgramViewModelFlowOutputDelegate,
         viewInputDelegate: QualityProgramViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
    public func setAnonymousDataPermissions(accepted: Bool) {
        DefaultStorage.shared.anonymousDataPermissions = accepted
    }
    func viewDidLoad(){
        viewInputDelegate?.setup(permissions: DefaultStorage.shared.anonymousDataPermissions)
    }
}

extension QualityProgramViewModel {

}
