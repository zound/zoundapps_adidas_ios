//
//  NotificationsViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 13/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService
import RxSwift

protocol NotificationsViewModelViewInputDelegate: class {

}
protocol NotificationsViewModelViewOutputDelegate {
    func didTapRegisterButton()
    func didTapCancelButton()
}
protocol NotificationsViewModelFlowInputDelegate {
}
protocol NotificationsViewModelFlowOutputDelegate: class {
    func notificationsDidRequestDismissal()
}
class NotificationsViewModel: BaseViewModel, NotificationsViewModelFlowInputDelegate, NotificationsViewModelViewOutputDelegate {
    weak var flowOutputDelegate: NotificationsViewModelFlowOutputDelegate?
    weak var viewInputDelegate: NotificationsViewModelViewInputDelegate?
    private let services: ServicesType
    init(flowOutputDelegate: NotificationsViewModelFlowOutputDelegate, viewInputDelegate: NotificationsViewModelViewInputDelegate, services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
    func didTapRegisterButton() {
        services.notificationsService.register() { [weak self] (granted, error) in
            DispatchQueue.main.async {
                DefaultStorage.shared.notificationSettingsPresented = true
                self?.flowOutputDelegate?.notificationsDidRequestDismissal()
            }
        }
    }
    func didTapCancelButton() {
        DefaultStorage.shared.notificationSettingsPresented = true
        flowOutputDelegate?.notificationsDidRequestDismissal()
    }

}
