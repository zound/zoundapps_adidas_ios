//
//  PairingViewModel.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 08/05/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import ConnectionService

protocol PairingViewModelViewInputDelegate: class {
    func update(deviceTypeImage: UIImage)
    func indicateDisconnectWill()
    func indicateLongPressConnectWill()
    func stopIndications()
    func showPairingCoverView(with deviceName: String)
    func hidePairingCoverView()
    func animateHeaderView()
    func animateSpinner()
    func hideAll()
}
protocol PairingViewModelViewOutputDelegate {
    func viewDidLoad()
    func viewDidAppear()
    func pairingCoverViewDidHide()
}
protocol PairingViewModelFlowInputDelegate {
}
protocol PairingViewModelFlowOutputDelegate: class {
    func didFinishPairingAccessory()
    func didFailToFinishPairingAccessory()
}

class PairingViewModel: BaseViewModel, PairingViewModelFlowInputDelegate, PairingViewModelViewOutputDelegate {
    weak var flowOutputDelegate: PairingViewModelFlowOutputDelegate?
    weak var viewInputDelegate: PairingViewModelViewInputDelegate?
    private let services: ServicesType
    private var pairingInfo: PairingInfo?
    private let disposeBag = DisposeBag()
    private var bluetoothClassicPairingModeSubscription: ()?
    init(flowOutputDelegate: PairingViewModelFlowOutputDelegate,
         viewInputDelegate: PairingViewModelViewInputDelegate,
         services: ServicesType,
         pairingInfo: PairingInfo) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        self.pairingInfo = pairingInfo
        super.init()
        self.services.rxConnectionService.reactiveDevice?.monitoredPeripheralConnectionState.subscribe(onNext: { [weak self] peripheralConnectionState in
            self?.handle(peripheralConnectionState: peripheralConnectionState)
        }).disposed(by: disposeBag)
        self.services.rxConnectionService.didCancelAccessoryPicker.subscribe(onNext: { [weak self] in
            self?.viewInputDelegate?.hidePairingCoverView()
        }).disposed(by: disposeBag)
        self.services.applicationModeService.appWillTerminate.subscribe(onNext: { [weak self] _ in
            self?.services.rxConnectionService.resetKnownDevice()
        }).disposed(by: disposeBag)
    }
}

private extension PairingViewModel {
    func handle(peripheralConnectionState: PeripheralConnectionState) {
        switch peripheralConnectionState {
        case .disconnected:
            viewInputDelegate?.indicateLongPressConnectWill()
        case .connecting:
            break
        case .connected:
            if let bluetoothClassicPairingMode = pairingInfo?.bluetoothClassicPairingMode {
                pairingInfo = nil
                handle(bluetoothClassicPairingMode: bluetoothClassicPairingMode)
            } else {
                if bluetoothClassicPairingModeSubscription == nil {
                    bluetoothClassicPairingModeSubscription = services.rxConnectionService.reactiveDevice?.bluetoothClassicPairingMode
                    .subscribe(onNext: { [weak self] resultBluetoothClassicPairingMode in
                        if case .success(let bluetoothClassicPairingMode) = resultBluetoothClassicPairingMode {
                            self?.handle(bluetoothClassicPairingMode: bluetoothClassicPairingMode)
                        } else {
                            self?.viewInputDelegate?.indicateDisconnectWill()
                        }
                    }).disposed(by: disposeBag)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) { [weak self] in
                    self?.services.rxConnectionService.reactiveDevice?.read(deviceFeature: .bluetoothClassicPairingMode)
                }
            }
        }
    }
    func handle(bluetoothClassicPairingMode: BluetoothClassicPairingMode) {
        if bluetoothClassicPairingMode == .inPairingMode, let name = services.rxConnectionService.reactiveDevice?.name {
            services.rxConnectionService.showAccessoryPicker()
            viewInputDelegate?.stopIndications()
            viewInputDelegate?.hideAll()
            viewInputDelegate?.showPairingCoverView(with: name)
        } else {
            viewInputDelegate?.indicateDisconnectWill()
        }
    }
    
}

extension PairingViewModel {
    func description1() -> String? {
        return services.rxConnectionService.knownDevice?.deviceTypeWithTraits.pairingDescription1Text
    }
    func description2() -> String? {
        return services.rxConnectionService.knownDevice?.deviceTypeWithTraits.pairingDescription2Text
    }
}

extension PairingViewModel {
    func viewDidLoad() {
        let deviceTypeImage = services.rxConnectionService.knownDevice?.deviceTypeWithTraits.discoveredDeviceImage ?? UIImage()
        viewInputDelegate?.update(deviceTypeImage: deviceTypeImage)
    }
    func viewDidAppear() {
        viewInputDelegate?.animateHeaderView()
        viewInputDelegate?.animateSpinner()
        if let peripheralConnectionState = services.rxConnectionService.reactiveDevice?.peripheralConnectionState {
            handle(peripheralConnectionState: peripheralConnectionState)
        } else {
            fatalError()
        }
    }
    func pairingCoverViewDidHide() {
        if let macAddress = services.rxConnectionService.reactiveDevice?.mac, services.rxConnectionService.isConnected(accessoryWith: macAddress) {
            flowOutputDelegate?.didFinishPairingAccessory()
        } else {
            flowOutputDelegate?.didFailToFinishPairingAccessory()
        }
    }
}
