//
//  PrivacyPolicyViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 06/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

protocol PrivacyPolicyViewModelViewInputDelegate: class {
}
protocol PrivacyPolicyViewModelViewOutputDelegate {
}
protocol PrivacyPolicyViewModelFlowInputDelegate {
}
protocol PrivacyPolicyViewModelFlowOutputDelegate: class {
}

class PrivacyPolicyViewModel:  BaseViewModel, PrivacyPolicyViewModelFlowInputDelegate, PrivacyPolicyViewModelViewOutputDelegate {
    weak var flowOutputDelegate: PrivacyPolicyViewModelFlowOutputDelegate?
    weak var viewInputDelegate: PrivacyPolicyViewModelViewInputDelegate?
    private let services: ServicesType
    init(flowOutputDelegate: PrivacyPolicyViewModelFlowOutputDelegate,
         viewInputDelegate: PrivacyPolicyViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
}

