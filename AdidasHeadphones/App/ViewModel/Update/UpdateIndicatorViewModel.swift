//
//  UpdateIndicatorViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 07/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol UpdateIndicatorViewModelViewInputDelegate: class {
    func updateDoneButtonState(isHidden: Bool)
    func updateIndicatorState(isHidden: Bool)
    func updateHeaderText(isFinished: Bool)
    func updateDescriptionText(isFinished: Bool)
}
protocol UpdateIndicatorViewModelViewOutputDelegate {
    func viewDidLoad()
    func doneButtonTouched()
}
protocol UpdateIndicatorViewModelFlowInputDelegate {}
protocol UpdateIndicatorViewModelFlowOutputDelegate: class {
    func didRequestFinishUpdate()
}
class UpdateIndicatorViewModel: BaseViewModel, UpdateIndicatorViewModelFlowInputDelegate, UpdateIndicatorViewModelViewOutputDelegate {
    weak var flowOutputDelegate: UpdateIndicatorViewModelFlowOutputDelegate?
    weak var viewInputDelegate: UpdateIndicatorViewModelViewInputDelegate?
    private let services: ServicesType
    private let disposeBag = DisposeBag()
    private var firmwareUpdateState: FirmwareUpdateState = .off {
        didSet {
            switch (oldValue, firmwareUpdateState) {
            case (.completed, .off):
                break
            case (_, .completed):
                update(isFinished: true)
            case (_, .failed), (_, .off):
                flowOutputDelegate?.didRequestFinishUpdate()
                break
            default:
                break
            }
        }
    }
    init(flowOutputDelegate: UpdateIndicatorViewModelFlowOutputDelegate,
         viewInputDelegate: UpdateIndicatorViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        self.services.firmwareUpdateService.didChangeFirmwareUpdateState.subscribe(onNext: {[weak self] firmwareUpdateState in
            self?.firmwareUpdateState = firmwareUpdateState
        }).disposed(by: disposeBag)
    }
    func viewDidLoad() {
        update(isFinished: false)
        services.firmwareUpdateService.install()
    }
    func doneButtonTouched() {
        flowOutputDelegate?.didRequestFinishUpdate()
    }
    private func update( isFinished: Bool) {
        viewInputDelegate?.updateHeaderText(isFinished: isFinished)
        viewInputDelegate?.updateDescriptionText(isFinished: isFinished)
        viewInputDelegate?.updateDoneButtonState(isHidden: !isFinished)
        viewInputDelegate?.updateIndicatorState(isHidden: isFinished)
    }

}
