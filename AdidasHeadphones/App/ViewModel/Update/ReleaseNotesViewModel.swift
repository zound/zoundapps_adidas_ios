//
//  ReleaseNotesViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 09/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

protocol ReleaseNotesViewModelViewInputDelegate: class {}
protocol ReleaseNotesViewModelViewOutputDelegate {}
protocol ReleaseNotesViewModelFlowInputDelegate {}
protocol ReleaseNotesViewModelFlowOutputDelegate: class {}

class ReleaseNotesViewModel: BaseViewModel, TermsAndCondViewModelFlowInputDelegate, TermsAndCondViewModelViewOutputDelegate {
    weak var flowOutputDelegate: ReleaseNotesViewModelFlowOutputDelegate?
    weak var viewInputDelegate: ReleaseNotesViewModelViewInputDelegate?
    private let services: ServicesType
    init(flowOutputDelegate: ReleaseNotesViewModelFlowOutputDelegate,
         viewInputDelegate: ReleaseNotesViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
    }
}
