//
//  UpdateViewModel.swift
//  AdidasHeadphones
//
//  Created by Paprota Przemyslaw on 02/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import ConnectionService

protocol UpdateViewModelViewInputDelegate: class {
    func updateInstallButtonState(enable: Bool)
    func updateInfoLabelState(isHidden: Bool)

}
protocol UpdateViewModelViewOutputDelegate {
    func viewDidLoad()
    func releaseNotesTouched()
    func installButtonTouched()
}
protocol UpdateViewModelFlowInputDelegate {}
protocol UpdateViewModelFlowOutputDelegate: class {
    func didRequestReleaseNotes()
    func didRequestInstallButton()
}
class UpdateViewModel:  BaseViewModel, UpdateViewModelFlowInputDelegate, UpdateViewModelViewOutputDelegate {

    private let minimumBaterryLevel = 50
    weak var flowOutputDelegate: UpdateViewModelFlowOutputDelegate?
    weak var viewInputDelegate: UpdateViewModelViewInputDelegate?
    private let services: ServicesType
    private var batteryLevel: Int = 0 {
        didSet{
            handleNewBatteryLevel()
        }
    }
    private var batteryLevelSubscription: Disposable?
    private var monitoredBatteryLevelSubscription: Disposable?
    init(flowOutputDelegate: UpdateViewModelFlowOutputDelegate,
         viewInputDelegate: UpdateViewModelViewInputDelegate,
         services: ServicesType) {
        self.flowOutputDelegate = flowOutputDelegate
        self.viewInputDelegate = viewInputDelegate
        self.services = services
        super.init()
        if let device = self.services.rxConnectionService.reactiveDevice {
            handleBatteryLevel(for: device)
        }
    }
    func releaseNotesTouched() {
        flowOutputDelegate?.didRequestReleaseNotes()
    }
    func installButtonTouched() {
        flowOutputDelegate?.didRequestInstallButton()
    }
    private func handleNewBatteryLevel(){
        let installEnable = batteryLevel >= minimumBaterryLevel
        viewInputDelegate?.updateInstallButtonState(enable: installEnable)
        viewInputDelegate?.updateInfoLabelState(isHidden: installEnable)
    }
    func viewDidLoad() {
        handleNewBatteryLevel()
    }
    func handleBatteryLevel(for reactiveDevice: ReactiveDevice) {
        batteryLevelSubscription = reactiveDevice.batteryLevel.subscribe(onNext: { [weak self] batteryLevelResult in
            switch batteryLevelResult {
            case .success(let batteryLevel):
                self?.batteryLevel = batteryLevel
                self?.monitoredBatteryLevelSubscription?.dispose()
                self?.monitoredBatteryLevelSubscription = reactiveDevice.monitoredBatteryLevel.subscribe(onNext: { [weak self] batteryLevel in
                    self?.batteryLevel = batteryLevel
                })
                self?.services.rxConnectionService.reactiveDevice?.setBatteryLevelMonitoring(enabled: true)
            default:
                break
            }
        })
        reactiveDevice.read(deviceFeature: .batteryLevel)
    }

}
