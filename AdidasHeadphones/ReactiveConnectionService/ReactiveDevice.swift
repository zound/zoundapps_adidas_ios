//
//  ReactiveDevice.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 27/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService
import RxSwift

public protocol ReactiveDeviceOutputType {
    // MARK: Monitor
    var monitoredPeripheralConnectionState: PublishSubject<PeripheralConnectionState> { get }
    var monitoredBatteryLevel: PublishSubject<Int> { get }
    var monitoredOverTheAirState: PublishSubject<OverTheAirState> { get }
    var monitoredBluetoothClassicPairingMode: PublishSubject<BluetoothClassicPairingMode> { get }
    var monitoredAudioSourceConnectionItem: PublishSubject<AudioSourceConnectionItem> { get }
    var monitoredActionButtonConfiguration: PublishSubject<ActionButtonConfiguration> { get }
    var monitoredActionButtonEvent: PublishSubject<ActionButtonEvent> { get }
    var monitoredAudioControlStatus: PublishSubject<AudioControlStatus> { get }
    var monitoredAudioNowPlaying: PublishSubject<AudioNowPlaying> { get }
    var monitoredGraphicalEqualizer: PublishSubject<GraphicalEqualizer> { get }
    //MARK: Read
    var batteryLevel: PublishSubject<ResultInt> { get }
    var manufacturerName: PublishSubject<ResultString> { get }
    var modelNumber: PublishSubject<ResultString> { get }
    var serialNumber: PublishSubject<ResultString> { get }
    var hardwareRevision: PublishSubject<ResultString> { get }
    var firmwareRevision: PublishSubject<ResultString> { get }
    var bluetoothClassicPairingStatus: PublishSubject<ResultBluetoothClassicPairingStatus> { get }
    var bluetoothClassicPairingMode: PublishSubject<ResultBluetoothClassicPairingMode> { get }
    var audioSourceConnections: PublishSubject<ResultAudioSourceConnections> { get }
    var actionButtonConfiguration: PublishSubject<ResultActionButtonConfiguration> { get }
    var audioControlStatus: PublishSubject<ResultAudioControlStatus> { get }
    var audioNowPlaying: PublishSubject<ResultAudioNowPlaying> { get }
    var graphicalEqualizer: PublishSubject<ResultGraphicalEqualizer> { get }
}

protocol ReactiveDeviceSupportingInterface {
    var knownFirmwareRevision: String { get }
    var isActionButtonGvaSupported: Bool { get }
}

public class ReactiveDevice: ReactiveDeviceOutputType {
    public var monitoredPeripheralConnectionState = PublishSubject<PeripheralConnectionState>()
    public var monitoredBatteryLevel = PublishSubject<Int>()
    public var monitoredOverTheAirState = PublishSubject<OverTheAirState>()
    public var monitoredBluetoothClassicPairingMode = PublishSubject<BluetoothClassicPairingMode>()
    public var monitoredAudioSourceConnectionItem = PublishSubject<AudioSourceConnectionItem>()
    public var monitoredActionButtonConfiguration = PublishSubject<ActionButtonConfiguration>()
    public var monitoredActionButtonEvent = PublishSubject<ActionButtonEvent>()
    public var monitoredAudioControlStatus = PublishSubject<AudioControlStatus>()
    public var monitoredAudioNowPlaying = PublishSubject<AudioNowPlaying>()
    public var monitoredGraphicalEqualizer = PublishSubject<GraphicalEqualizer>()
    public var batteryLevel = PublishSubject<ResultInt>()
    public var manufacturerName = PublishSubject<ResultString>()
    public var modelNumber = PublishSubject<ResultString>()
    public var serialNumber = PublishSubject<ResultString>()
    public var hardwareRevision = PublishSubject<ResultString>()
    public var firmwareRevision = PublishSubject<ResultString>()
    public var bluetoothClassicPairingStatus = PublishSubject<ResultBluetoothClassicPairingStatus>()
    public var bluetoothClassicPairingMode = PublishSubject<ResultBluetoothClassicPairingMode>()
    public var audioSourceConnections = PublishSubject<ResultAudioSourceConnections>()
    public var actionButtonConfiguration = PublishSubject<ResultActionButtonConfiguration>()
    public var audioControlStatus = PublishSubject<ResultAudioControlStatus>()
    public var audioNowPlaying = PublishSubject<ResultAudioNowPlaying>()
    public var graphicalEqualizer = PublishSubject<ResultGraphicalEqualizer>()
    private var device: Device
    private var internalKnownFirmwareRevision: String?
    init?(device: Device?) {
        guard let device = device else { return nil }
        self.device = device
        (self.device.delegate, self.device.notificationDelegate) = (self, self)
    }
}

extension ReactiveDevice: DeviceStaticInterface {
    public var name: String {
        return device.name
    }
    public var mac: String {
        return device.mac
    }
    public var uuid: String {
        return device.uuid
    }
    public var deviceTypeWithTraits: DeviceTypeWithTraits {
        return device.deviceTypeWithTraits
    }
    public var peripheralConnectionState: PeripheralConnectionState {
        return device.peripheralConnectionState
    }
}

extension ReactiveDevice: ReactiveDeviceSupportingInterface {
    var knownFirmwareRevision: String {
        return internalKnownFirmwareRevision ?? String()
    }
    var isActionButtonGvaSupported: Bool {
        guard let knownFreemanFirmwareRevision = FreemanFirmwareRevision(knownFirmwareRevision) else {
            return false
        }
        switch deviceTypeWithTraits {
        case .arnold, .desir:
            return false
        case .freeman:
            return knownFreemanFirmwareRevision >= FreemanFirmwareRevision.supportingGva
        }
    }
}

extension ReactiveDevice: DeviceDelegate {
    public func device(_ device: Device, didChange peripheralConnectionState: PeripheralConnectionState) {
        if peripheralConnectionState == .connected {
            device.data.readFirmwareRevision { [weak self] resultString in
                if case .success(let firmwareRevision) = resultString {
                    self?.internalKnownFirmwareRevision = firmwareRevision
                }
                self?.monitoredPeripheralConnectionState.onNext(peripheralConnectionState)
            }
        } else {
            monitoredPeripheralConnectionState.onNext(peripheralConnectionState)
        }
    }
}

extension ReactiveDevice: DataNotificationDelegate {
    public func didUpdate(batteryLevel: Int) {
        monitoredBatteryLevel.onNext(batteryLevel)
    }
    public func didUpdate(overTheAirState: OverTheAirState) {
        monitoredOverTheAirState.onNext(overTheAirState)
    }
    public func didUpdate(bluetoothClassicPairingMode: BluetoothClassicPairingMode) {
        monitoredBluetoothClassicPairingMode.onNext(bluetoothClassicPairingMode)
    }
    public func didUpdate(audioSourceConnectionItem: AudioSourceConnectionItem) {
        monitoredAudioSourceConnectionItem.onNext(audioSourceConnectionItem)
    }
    public func didUpdate(actionButtonConfiguration: ActionButtonConfiguration) {
        monitoredActionButtonConfiguration.onNext(actionButtonConfiguration)
    }
    public func didUpdate(actionButtonEvent: ActionButtonEvent) {
        monitoredActionButtonEvent.onNext(actionButtonEvent)
    }
    public func didUpdate(audioControlStatus: AudioControlStatus) {
        monitoredAudioControlStatus.onNext(audioControlStatus)
    }
    public func didUpdate(audioNowPlaying: AudioNowPlaying) {
        monitoredAudioNowPlaying.onNext(audioNowPlaying)
    }
    public func didUpdate(graphicalEqualizer: GraphicalEqualizer) {
        monitoredGraphicalEqualizer.onNext(graphicalEqualizer)
    }
}

extension ReactiveDevice: DataInputInterface {
    public var overTheAirInfo: OverTheAirInfo {
        return device.data.overTheAirInfo
    }
    public func completeFirmwareUpdate(with firmware: Firmware) {
        device.data.completeFirmwareUpdate(with: firmware)
    }
    public func startOverTheAirUpdate(with firmware: Firmware, firmwareVersion: String) {
        device.data.startOverTheAirUpdate(with: firmware, firmwareVersion: firmwareVersion)
    }
    public func reboot() {
        device.data.reboot()
    }
    public func abortOverTheAirUpdate() {
        device.data.abortOverTheAirUpdate()
    }
    public func setBatteryLevelMonitoring(enabled: Bool) {
        device.data.setBatteryLevelMonitoring(enabled: enabled)
    }
    public func setBluetoothClassicPairingMonitoring(enabled: Bool) {
        device.data.setBluetoothClassicPairingMonitoring(enabled: enabled)
    }
    public func setAudioSourceConnectionsMonitoring(enabled: Bool) {
        device.data.setAudioSourceConnectionsMonitoring(enabled: enabled)
    }
    public func setActionButtonConfigurationMonitoring(enabled: Bool) {
        device.data.setActionButtonConfigurationMonitoring(enabled: enabled)
    }
    public func setAudioControlStatusMonitoring(enabled: Bool) {
        device.data.setAudioControlStatusMonitoring(enabled: enabled)
    }
    public func setActionButtonEventMonitoring(enabled: Bool) {
        device.data.setActionButtonEventMonitoring(enabled: enabled)
    }
    public func setAudioNowPlayingMonitoring(enabled: Bool) {
        device.data.setAudioNowPlayingMonitoring(enabled: enabled)
    }
    public func setGraphicalEqualizerMonitoring(enabled: Bool) {
        device.data.setGraphicalEqualizerMonitoring(enabled: enabled)
    }
    public func write(audioSourceConnectionItem: WritableAudioSourceConnectionItem, completion: @escaping (Result) -> Void) {
        device.data.write(audioSourceConnectionItem: audioSourceConnectionItem, completion: completion)
    }
    public func write(actionButtonConfiguration: ActionButtonConfiguration, completion: @escaping (Result) -> Void) {
        device.data.write(actionButtonConfiguration: actionButtonConfiguration, completion: completion)
    }
    public func write(audioControlEvent: AudioControlEvent, completion: @escaping (Result) -> Void) {
        device.data.write(audioControlEvent: audioControlEvent, completion: completion)
    }
    public func write(graphicalEqualizer: GraphicalEqualizer, completion: @escaping (Result) -> Void) {
        device.data.write(graphicalEqualizer: graphicalEqualizer, completion: completion)
    }
    public func write(speedMode: SpeedMode, completion: @escaping (Result) -> Void) {
        device.data.write(speedMode: speedMode, completion: completion)
    }
}

extension ReactiveDevice {
    public func read(deviceFeature: DeviceFeature) {
        switch deviceFeature {
        case .batteryLevel:
            device.data.readBatteryLevel { resultInt in
                self.batteryLevel.onNext(resultInt)
            }
        case .manufacturerName:
            device.data.readManufacturerName { resultString in
                self.manufacturerName.onNext(resultString)
            }
        case .modelNumber:
            device.data.readModelNumber { resultString in
                self.modelNumber.onNext(resultString)
            }
        case .serialNumber:
            device.data.readSerialNumber { resultString in
                self.serialNumber.onNext(resultString)
            }
        case .hardwareRevision:
            device.data.readHardwareRevision { resultString in
                self.hardwareRevision.onNext(resultString)
            }
        case .firmwareRevision:
            device.data.readFirmwareRevision { resultString in
                self.firmwareRevision.onNext(resultString)
            }
        case .bluetoothClassicPairingStatus:
            device.data.readBluetoothClassicPairingStatus { resultBluetoothClassicPairingStatus in
                self.bluetoothClassicPairingStatus.onNext(resultBluetoothClassicPairingStatus)
            }
        case .bluetoothClassicPairingMode:
            device.data.readBluetoothClassicPairingMode { resultBluetoothClassicPairingMode in
                self.bluetoothClassicPairingMode.onNext(resultBluetoothClassicPairingMode)
            }
        case .audioSourceConnections:
            device.data.readAudioSourceConnections { resultAudioSourceConnections in
                self.audioSourceConnections.onNext(resultAudioSourceConnections)
            }
        case .actionButtonConfiguration:
            device.data.readActionButtonConfiguration { resultActionButtonConfiguration in
                self.actionButtonConfiguration.onNext(resultActionButtonConfiguration)
            }
        case .audioControl:
            device.data.readAudioControlStatus { resultAudioControlStatus in
                self.audioControlStatus.onNext(resultAudioControlStatus)
            }
        case .audioNowPlaying:
            device.data.readAudioNowPlaying { resultAudioNowPlaying in
                self.audioNowPlaying.onNext(resultAudioNowPlaying)
            }
        case .graphicalEqualizer:
            device.data.readGraphicalEqualizer { resultGraphicalEqualizer in
                self.graphicalEqualizer.onNext(resultGraphicalEqualizer)
            }
        case .actionButtonEvent, .speedMode:
            break
        }
    }
}

