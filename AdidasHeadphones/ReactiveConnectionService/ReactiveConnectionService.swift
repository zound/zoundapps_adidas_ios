//
//  ReactiveConnectionService.swift
//  HeadsetApp
//
//  Created by Wudarski Lukasz on 27/04/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ConnectionService
import RxSwift

public protocol ReactiveConnectionServiceOutputType {
    var isDiscoveryInProgress: Bool { get }
    var isReestablishingPeripheralConnection: Bool { get }
    var canStartDiscovering: Bool { get }
    var canStopDiscovering: Bool { get }
    var reactiveDevice: ReactiveDevice? { get }
    var inited: PublishSubject<Void> { get }
    var didFailToEstablishPeripheralConnection: PublishSubject<Void> { get }
    var didCancelPeripheralConnection: PublishSubject<Void> { get }
    var didUpdateBluetoothState: PublishSubject<BluetoothState> { get }
    var didDiscoverPeripheralAtIndex: PublishSubject<Int> { get }
    var didEstablishPeripheralConnectionToReactiveDevice: PublishSubject<ReactiveDevice> { get }
    var didCancelAccessoryPicker: PublishSubject<Void> { get }
    var didChangeAccessoryState: PublishSubject<AccessoryInfo> { get }
}

public typealias ReactiveConnectionServiceType = ConnectionServiceType & ReactiveConnectionServiceOutputType

public class ReactiveConnectionService: ReactiveConnectionServiceOutputType {
    private var connectionService: ConnectionService!
    public var canStartDiscovering: Bool {
        return bluetoothState == .available && !isDiscoveryInProgress
    }
    public var canStopDiscovering: Bool {
        return bluetoothState == .available && isDiscoveryInProgress
    }
    public var isDiscoveryInProgress = false
    public var isReestablishingPeripheralConnection = false
    public var inited = PublishSubject<Void>()
    public var didFailToEstablishPeripheralConnection = PublishSubject<Void>()
    public var didCancelPeripheralConnection = PublishSubject<Void>()
    public var didUpdateBluetoothState = PublishSubject<BluetoothState>()
    public var didDiscoverPeripheralAtIndex = PublishSubject<Int>()
    public var didEstablishPeripheralConnectionToReactiveDevice = PublishSubject<ReactiveDevice>()
    public var didCancelAccessoryPicker = PublishSubject<Void>()
    public var didChangeAccessoryState = PublishSubject<AccessoryInfo>()
    public var reactiveDevice: ReactiveDevice?
}

extension ReactiveConnectionService: ConnectionServiceType {
    public var bluetoothState: BluetoothState {
        return connectionService.bluetoothState
    }
    public var discoveredDevices: [DiscoveredDevice] {
        return connectionService.discoveredDevices
    }
    public var device: Device? {
        return connectionService.device
    }
    public var knownDevice: KnownDevice? {
        return connectionService.knownDevice
    }
    public var knownDevices: [KnownDevice] {
        return connectionService.knownDevices
    }
    public func resetKnownDevice() {
        connectionService.resetKnownDevice()
    }
    public func remove(knownDeviceWith UUID: String) {
        connectionService.remove(knownDeviceWith: UUID)
    }
    public func replace(knownDeviceWith UUID: String) {
        connectionService.replace(knownDeviceWith: UUID)
    }
    public func asNew() -> ConnectionServiceType {
        connectionService = ConnectionService(self)
        _ = connectionService.asNew()
        return self
    }
    public func asNewReactive() -> ReactiveConnectionServiceType {
        connectionService = ConnectionService(self)
        _ = connectionService.asNew()
        return self
    }
    public func asRestored(restorationId: String) -> ConnectionServiceType {
        connectionService = ConnectionService(self)
        _ = connectionService.asRestored(restorationId: restorationId)
        return self
    }
    public func asRestoredReactive(restorationId: String) -> ReactiveConnectionServiceType {
        connectionService = ConnectionService(self)
        _ = connectionService.asRestored(restorationId: restorationId)
        return self
    }
    public func startDiscovering() {
        isDiscoveryInProgress = true
        connectionService.startDiscovering()
    }
    public func stopDiscovering() {
        connectionService.stopDiscovering()
        isDiscoveryInProgress = false
    }
    public func stopDiscovering(selecting discoveredDevice: DiscoveredDevice) {
        connectionService.stopDiscovering(selecting: discoveredDevice)
        isDiscoveryInProgress = false
    }
    public func showAccessoryPicker() {
        connectionService.showAccessoryPicker()
    }
    public func isConnected(accessoryWith macAddress: String) -> Bool {
        return connectionService.isConnected(accessoryWith: macAddress)
    }
    public func setAccessoriesMonitoring(enabled: Bool) {
        connectionService.setAccessoriesMonitoring(enabled: enabled)
    }
    public func startTestEASession(withAccessoryThatUses macAddress: String) {
        connectionService.startTestEASession(withAccessoryThatUses: macAddress)
    }
    public func stopTestEASession() {
        connectionService.stopTestEASession()
    }
    public func establishPeripheralConnection() {
        connectionService.establishPeripheralConnection()
    }
    public func reestablishPeripheralConnection(to anyKnownDevice: KnownDevice) {
        isReestablishingPeripheralConnection = true
        connectionService.reestablishPeripheralConnection(to: anyKnownDevice)
    }
    public func cancelPeripheralConnection() {
        connectionService.cancelPeripheralConnection()
    }
}

extension ReactiveConnectionService: ConnectionServiceDelegate {
    public func connectionServiceInited(service: ConnectionServiceType) {
        inited.onCompleted()
    }
    public func connectionServiceDidFailToEstablishPeripheralConnection(service: ConnectionServiceType) {
        didFailToEstablishPeripheralConnection.onNext(())
    }
    public func connectionServiceDidCancelPeripheralConnection(service: ConnectionServiceType) {
        didCancelPeripheralConnection.onNext(())
    }
    public func connectionServiceDidCancelAccessoryPicker(service: ConnectionServiceType) {
        didCancelAccessoryPicker.onNext(())
    }
    public func connection(service: ConnectionServiceType, didUpdate bluetoothState: BluetoothState) {
        didUpdateBluetoothState.onNext(bluetoothState)
    }
    public func connection(service: ConnectionServiceType, didDiscoverPeripheralAt index: Int) {
        didDiscoverPeripheralAtIndex.onNext(index)
    }
    public func connection(service: ConnectionServiceType, didEstablishPeripheralConnectionTo device: Device) {
        isReestablishingPeripheralConnection = false
        reactiveDevice = ReactiveDevice(device: connectionService.device)
        didEstablishPeripheralConnectionToReactiveDevice.onNext(reactiveDevice!)
    }
    public func connection(service: ConnectionServiceType, didChangeAccessoryState accessoryInfo: AccessoryInfo) {
        didChangeAccessoryState.onNext(accessoryInfo)
    }
}
