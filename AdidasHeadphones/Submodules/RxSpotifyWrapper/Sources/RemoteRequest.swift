//
//  RemoteRequest.swift
//  RxSpotifyWrapper
//
//  Created by Grzegorz Kiel on 06/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import SwiftSpotifySDK
import RxSwift

private struct Config {
    static let headerLabelAuthorization = "Authorization"
    static let headerValueAuthorization = "Bearer"
    static let spaceString = " "
    static let spotifyRequestPlaylistsEntriesLimit = 50
    static let spotifyRequestTracksEntriesLimit = 100
    static let timeout: RxTimeInterval = RxTimeInterval.seconds(10)
    static let httpOKStatusLower = 200
    static let httpOKStatusUpper = 400
    static let httpErrorStatusTooManyRequests = 429
    static let maxRetries = 2
}
private enum SpotifyResponseError: Error {
    case tooManyRequests(Int)
    case unhandled
    case cancelled
}
private struct SpotifyRequestInfo {
    let url: URL
    let accessToken: String
    let query: String?
}
private struct SpotifyResponseInfo {
    let data: Data
    let status: SpotifyResponseStatus
}
private func decodeJSON<T: Decodable>(data: Data?) -> Result<T, Error> {
    do {
        let result: T = try JSONDecoder().decode(T.self, from: data ?? Data())
        return Result.success(result)
    } catch let error {
        return Result.failure(error)
    }
}
private func spotifyRequest(for url: URL, accesToken: String, usingQuery query: String?) -> URLRequest {
    var request = URLRequest(url: url)
    request.addValue(Config.headerValueAuthorization + " " + accesToken, forHTTPHeaderField: Config.headerLabelAuthorization)
    return request
}
enum SpotifyRemoteEntry {
    case user
    case playlist(userId: String)
    case playlistTracksInfo(playlistId: String)
    case artist
    case userAlbums
    case userAlbumTracks(albumId: String)
}
    
extension SpotifyRemoteEntry {
    func value<Value: Decodable>(token: String?, query: String?, decodable: Value.Type) -> Observable<Value> {
        return observable(token: token ?? "", query: query, decodable: decodable)
    }
    func apiEndpoint() -> URL {
        switch self {
        case .user: return URL(string: "https://api.spotify.com/v1/me")!
        case .playlist(let userId): return URL(string: String(format: "https://api.spotify.com/v1/users/%@/playlists?offset=0&limit=%d", userId, Config.spotifyRequestPlaylistsEntriesLimit))!
        case .artist: return URL(string: "https://api.spotify.com/v1/me/following?type=artist")!
        case .playlistTracksInfo(let playlistId): return URL(string: String(format: "https://api.spotify.com/v1/playlists/%@/tracks?fields=next,offset,total,items(track(duration_ms))", playlistId, Config.spotifyRequestTracksEntriesLimit))!
        case .userAlbums:
            return URL(string: "https://api.spotify.com/v1/me/albums")!
        case .userAlbumTracks(let albumId):
            return URL(string: String(format: "https://api.spotify.com/v1/albums/%@/tracks", albumId))!
        }
    }
    func rxRequest<Value, PagedResult: Chunked>(authenticationHandler: @escaping () -> Single<String>,
                                                partialObserver: AnyObserver<([Value], SpotifyResponseStatus)>,
                                                itemsKeyPath: KeyPath<PagedResult, [Value]>
                                                ) -> Observable<([Value], SpotifyResponseStatus)> {
        return Observable.create { subscriber in
            var internalDisposeBag = DisposeBag()
            chunk(url: self.apiEndpoint(),
                  authenticationHandler: authenticationHandler,
                  itemsKeypath: itemsKeyPath,
                  partialObserver: partialObserver,
                  lastChunkStatus: .inProgress(self.apiEndpoint(), .zero))
                .skipWhile { _, status in
                    if case .completed = status {
                        return false
                    }
                    return true
                }
                .take(1)
                .subscribe(
                    onError: { error in
                        subscriber.onError(error)
                    },
                    onCompleted: {
                        subscriber.onCompleted()
                        internalDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    private func observable<Value: Decodable>(token: String, query: String?, decodable: Value.Type) -> Observable<Value> {
        switch self {
        case .user, .playlist, .playlistTracksInfo, .artist, .userAlbums, .userAlbumTracks:
            return URLSession.shared.rx.data(request: spotifyRequest(for: self.apiEndpoint(), accesToken: token, usingQuery: query))
                .flatMap(mapToObservable(decoder: decodeJSON))
        }
    }
}
func id(authenticationHandler: () -> Single<String>) -> Observable<String> {
    return authenticationHandler()
        .asObservable()
        .flatMap {
            SpotifyRemoteEntry.user.value(token: $0, query: nil, decodable: SpotifyUserInfo.self)
        }
        .take(1)
        .flatMap { return Observable.just($0.id) }
        .observeOn(MainScheduler.instance)
}
private func chunk<Value, PagedResult: Chunked>(url: URL,
                                                authenticationHandler: @escaping () -> Single<String>,
                                                itemsKeypath: KeyPath<PagedResult, [Value]>,
                                                partialObserver: AnyObserver<([Value], SpotifyResponseStatus)>,
                                                lastChunkStatus: SpotifyResponseStatus
                                                ) -> Observable<([Value], SpotifyResponseStatus)> {
    let authenticatedPartialStep = authenticated(info: SpotifyRequestInfo(url: url, accessToken: String(), query: nil), lastStatus: lastChunkStatus)(authenticationHandler)
    let parsingStep = parse(keypath: itemsKeypath)
    let notifyExternalStep = notify(external: partialObserver)
    let nextChunkStep = nextChunk(authenticationHandler: authenticationHandler,
                                  itemsKeypath: itemsKeypath,
                                  partialObserver: partialObserver,
                                  retryURL: url)
    return authenticatedPartialStep
        .materialize()
        .flatMap(parsingStep)
        .map(notifyExternalStep)
        .flatMap(nextChunkStep)
}
private func authenticated(info: SpotifyRequestInfo, lastStatus: SpotifyResponseStatus) -> (() -> Single<String>) -> Observable<SpotifyResponseInfo> {
    return { authenticationHandler in
        authenticationHandler().asObservable()
            .map { (SpotifyRequestInfo(url: info.url, accessToken: $0, query: info.query), lastStatus) }
            .flatMap(partial)
    }
}
private func partial(info: SpotifyRequestInfo, lastStatus: SpotifyResponseStatus) -> Observable<SpotifyResponseInfo> {
    return URLSession.shared.rx.response(request: spotifyRequest(for: info.url,
                                                          accesToken: info.accessToken,
                                                          usingQuery: nil))
        .map { (response, data) in (response, data, lastStatus) }
        .flatMap(validateResponse)
}
private func validateResponse(response: HTTPURLResponse, data: Data, lastStatus: SpotifyResponseStatus) -> Observable<SpotifyResponseInfo> {
    switch response.statusCode {
    case Config.httpOKStatusLower..<Config.httpOKStatusUpper:
        return Observable.just(SpotifyResponseInfo(data: data, status: lastStatus))
    case Config.httpErrorStatusTooManyRequests:
        var numOfTries = 0
        if case .retry(let counter) = lastStatus { numOfTries = counter.advanced(by: 1) }
        else { numOfTries = 1 }
        return Observable.error(SpotifyResponseError.tooManyRequests(numOfTries))
    default:
        return Observable.error(SpotifyResponseError.unhandled)
    }
}
private func parse<Value, PagedResult: Chunked>(keypath: KeyPath<PagedResult, [Value]>) ->  (Event<SpotifyResponseInfo>) -> Observable<([Value], SpotifyResponseInfo)> {
    return {  event in
        switch event {
        case .next(let responseInfo):
            let pagingResult: Result<PagedResult, Error> = decodeJSON(data: responseInfo.data)
            switch pagingResult {
            case .success(let paging):
                let items = paging[keyPath: keypath]
                guard let next = paging.next, let url = URL(string: next) else {
                    return Observable.just((items, SpotifyResponseInfo(data: responseInfo.data, status: .completed)))
                }
                return Observable.just((items, SpotifyResponseInfo(data: responseInfo.data, status: .inProgress(url, paging.total))))
            case .failure:
                return Observable.error(SpotifyResponseError.unhandled)
            }
        case .error(let error):
            switch error {
            case SpotifyResponseError.tooManyRequests(let retriesCounter):
                guard retriesCounter <= Config.maxRetries else {
                    return Observable.error(SpotifyResponseError.cancelled)
                }
                return Observable.just(([], SpotifyResponseInfo(data: Data(), status: .retry(retriesCounter))))
            default:
                return Observable.error(error)
            }
        case .completed:
            return Observable.empty()
        }
    }
}
private func notify<Value>(external: AnyObserver<([Value], SpotifyResponseStatus)>) -> (([Value], SpotifyResponseInfo)) -> ([Value], SpotifyResponseInfo) {
    return { result in
        external.onNext((result.0, result.1.status))
        return result
    }
}
private func nextChunk<Value, PagedResult: Chunked>(authenticationHandler: @escaping () -> Single<String>,
                                                    itemsKeypath: KeyPath<PagedResult, [Value]>,
                                                    partialObserver: AnyObserver<([Value], SpotifyResponseStatus)>,
                                                    retryURL: URL)
    -> ([Value], SpotifyResponseInfo) -> Observable<([Value], SpotifyResponseStatus)> {
    return { (values, status) in
        switch status.status {
        case .inProgress(let url, _):
            return chunk(url: url,
                         authenticationHandler: authenticationHandler,
                         itemsKeypath: itemsKeypath,
                         partialObserver: partialObserver,
                         lastChunkStatus: status.status)
        case .retry:
            return chunk(url: retryURL,
                         authenticationHandler: authenticationHandler,
                         itemsKeypath: itemsKeypath,
                         partialObserver: partialObserver,
                         lastChunkStatus: status.status)
                .delaySubscription(Config.timeout, scheduler: MainScheduler.instance)
        default:
            return Observable.empty()
        }
    }
}
private func mapToObservable<Value: Decodable>(decoder: @escaping (Data?) -> Result<Value, Error>) -> (Data?) -> Observable<Value> {
    return { data in
        switch(decoder(data)) {
        case .success(let value): return Observable.just(value)
        case .failure(let error): return Observable.error(error)
        }
    }
}
