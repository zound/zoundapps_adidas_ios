//
//  RxStreamingSpotifyManagerType.swift
//  RxSpotifyWrapper
//
//  Created by Grzegorz Kiel on 11/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import SwiftSpotifySDK
import RxSwift
import RxCocoa
import SafariServices

public enum SpotifyPlayerStatus {
    case idle
    case preparingToPlay
    case playing
    case error(Error)
}
public enum NowPlayingPlayerStatus {
    case stopped
    case playing
    case paused
}
public enum AuthError: Error, Equatable {
    case missingTokenRefreshService
    case missingAccessToken
    case noPlayerInitialized
}
public struct SpotifyConfiguration {
    let clientId: String
    let redirectURL: URL
    let tokenSwapURL: URL
    let tokenRefreshURL: URL
    let userDefaultsKey: String

    public init(clientId: String,
                redirectURL: URL,
                tokenSwapURL: URL,
                tokenRefreshURL: URL,
                userDefaultsKey: String) {
        self.clientId = clientId
        self.redirectURL = redirectURL
        self.tokenSwapURL = tokenSwapURL
        self.tokenRefreshURL = tokenRefreshURL
        self.userDefaultsKey = userDefaultsKey
    }
}
public enum SpotifyAppStatus: Equatable {
    case idle
    case checking
    case waitingForAuthorizationCallback
    case notConnected
    case connected(accessToken: String?, refreshToken: String?)
    case error(AuthError)
}

public protocol RxStreamingSpotifyManagerInterface: class {
    var rxAuthStatus: BehaviorRelay<SpotifyAppStatus> { get }
    var rxPlayerStatus: BehaviorRelay<SpotifyPlayerStatus> { get }
    var accessToken: String? { get }
    
    func application(_ app: UIApplication, openURL url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool
    func spotifyStatus(initializePlayer: Bool)
    func authorizationViewController() -> SFSafariViewController
    func startedAuthorization()
    func playSpotifyEntry(uri: String) -> Single<Bool>
    func setupAuthentication() -> Single<String>
    
    static func user(accessToken: String) -> Observable<SpotifyUserInfo>
    static func playlists(authenticationHandler: @escaping () -> Single<String>) -> Observable<([SpotifyPlaylistEntry], SpotifyResponseStatus)>
    static func artists(authenticationHandler: @escaping () -> Single<String>) -> Observable<([SpotifyArtistEntry], SpotifyResponseStatus)>
    static func albums(authenticationHandler: @escaping () -> Single<String>) -> Observable<([SpotifyUserAlbumEntry], SpotifyResponseStatus)>
    static func playlistTracksInfo(authenticationHandler: @escaping () -> Single<String>, playlistId: String) -> Observable<([SpotifyTrackEntry], SpotifyResponseStatus)>
    static func albumTracksInfo(authenticationHandler: @escaping () -> Single<String>, albumId: String) -> Observable<([SpotifyUserAlbumTrackEntry], SpotifyResponseStatus)>
}
