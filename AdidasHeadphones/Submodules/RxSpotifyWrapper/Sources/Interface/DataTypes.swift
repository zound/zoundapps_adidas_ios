//
//  DataTypes.swift
//  RxSpotifyWrapper
//
//  Created by Grzegorz Kiel on 06/06/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

protocol Paged {
    var next: String? { get }
    var offset: Int { get }
    var total: Int { get }
}

typealias Chunked = Decodable & Paged

public enum SpotifyResponseStatus {
    case inProgress(URL, Int)
    case retry(Int)
    case completed
    case cancelled
}
public struct SpotifyUserInfo: Decodable {
    public var name: String
    public var id: String
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        id = try container.decode(String.self, forKey: .id)
    }
    enum DecodingKeys: String, CodingKey {
        case name = "display_name"
        case id
    }
}
public struct SpotifyUserPlaylistPaging: Chunked {
    let items: [SpotifyPlaylistEntry]
    let href: String
    let limit: Int
    let next: String?
    let offset: Int
    let total: Int
}
public struct SpotifyTracksPaging: Chunked {
    let items: [SpotifyTrackEntry]
    let next: String?
    let offset: Int
    let total: Int
}
public struct SpotifyPlaylistEntry: Decodable {
    public let id: String
    public let uri: String
    public let name: String
    public let images: [SpotifyImage]
    public let tracks: SpotifyPlaylistTracks
}
public struct SpotifyPlaylistTracks: Decodable {
    let href: String
    public let total: Int
}
public struct SpotifyArtistsRecordsPaging: Chunked {
    let artists: SpotifyArtistsPaging
    let next: String?
    var total: Int
    let limit: Int
    var offset: Int
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)
        artists = try container.decode(SpotifyArtistsPaging.self, forKey: .artists)
        next = artists.next
        total = artists.total
        limit = artists.limit
        offset = artists.items.count - limit == 0 ? limit : total
    }
    enum DecodingKeys: String, CodingKey {
        case artists
        case next
        case total
        case limit
    }
}
public struct SpotifyArtistsPaging: Decodable {
    let items:[SpotifyArtistEntry]
    let next: String?
    let offset: Int
    let total: Int
    let limit: Int
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)
        items = try container.decode([SpotifyArtistEntry].self, forKey: .items)
        next = try container.decode(String?.self, forKey: .next)
        total = try container.decode(Int.self, forKey: .total)
        limit = try container.decode(Int.self, forKey: .limit)
        offset = items.count - limit == 0 ? limit : total
    }
    enum DecodingKeys: String, CodingKey {
        case items
        case next
        case total
        case limit
    }
}
public struct SpotifyArtistEntry: Decodable {
    public let id: String
    public let uri: String
    public let name: String
    public let images: [SpotifyImage]
}
public struct SpotifyArtistTracks: Decodable {
    let tracks: ArtistTracksPaging
}
public struct SpotifyImage: Decodable {
    let height: Int?
    public let url: String?
    let width: Int?
}
public struct SpotifyTrackEntry: Decodable {
    public let track: DurationInfo?
}
public struct ArtistTracksPaging: Chunked {
    let items: [ArtistTrackEntry]
    let next: String?
    let offset: Int
    let total: Int
}
public struct ArtistTrackEntry: Decodable {
    public let duration_ms: Int
}
public struct DurationInfo: Decodable {
    public let duration_ms: Int
}
public struct SpotifyAlbumArtist: Decodable {
    public let name: String
}
public struct SpotifyAlbumEntry: Decodable {
    public let artists: [SpotifyAlbumArtist]
    public let id: String
    public let name: String
    public let uri: String
    public let images: [SpotifyImage]
}
public struct SpotifyUserAlbumEntry: Decodable {
    public let album: SpotifyAlbumEntry
}
public struct ArtistAlbumEntry: Decodable {
    public let id: String
}
public struct SpotifyArtistAlbumsPaging: Chunked {
    let items: [SpotifyAlbumEntry]
    let next: String?
    let offset: Int
    let total: Int
}
public struct SpotifyUserAlbumsPaging: Chunked {
    let items: [SpotifyUserAlbumEntry]
    let next: String?
    let offset: Int
    let total: Int
}
public struct SpotifyUserAlbumsTracksPaging: Chunked {
    let items: [SpotifyUserAlbumTrackEntry]
    let next: String?
    let offset: Int
    let total: Int
}
public struct SpotifyUserAlbumTrackEntry: Decodable {
    public let duration_ms: Int
}
