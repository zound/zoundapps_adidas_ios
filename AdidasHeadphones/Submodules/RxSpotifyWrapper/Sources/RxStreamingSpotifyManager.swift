//
//  RxStreamingSpotifyManager.swift
//  RxSpotifyWrapper
//
//  Created by Grzegorz Kiel on 06/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import SwiftSpotifySDK
import RxSwift
import RxCocoa
import SafariServices
import AVFoundation
import MediaPlayer

public final class RxStreamingSpotifyManager: NSObject, RxStreamingSpotifyManagerInterface {
    public static let shared = RxStreamingSpotifyManager()
    private var _configuration: SpotifyConfiguration!
    public var rxAuthStatus = BehaviorRelay<SpotifyAppStatus>.init(value: .idle)
    public var rxPlayerStatus = BehaviorRelay<SpotifyPlayerStatus>.init(value: .idle)
    var player: SPTAudioStreamingController? = nil
    let playerLoggedIn = PublishSubject<Bool>()
    let nowPlayableBehavior: NowPlayable
    var currentPosition: TimeInterval = 0
    var currentItemIdx: UInt = 0
    var requestedItemUri = String()
    var metadata = SPTPlaybackMetadata(prevTrack: nil,
                                       currentTrack: nil,
                                       nextTrack: nil)
    private var isInterrupted: Bool = false

    override private init() {
        self.nowPlayableBehavior = IOSNowPlayableBehavior()
        super.init()
        try? nowPlayableBehavior.handleNowPlayableConfiguration(commands: nowPlayableBehavior.defaultRegisteredCommands,
                                                               disabledCommands: nowPlayableBehavior.defaultDisabledCommands,
                                                               commandHandler: handleCommand(command:event:),
                                                               interruptionHandler: handleInterrupt(with:))
        nowPlayableBehavior.setupRouteChanged(handler: handleAudioRouteChanged)
    }
    public var accessToken: String? {
        return SPTAuth.defaultInstance().session?.accessToken
    }
    private var loginViewController: SFSafariViewController?
    public func configure(configuration: SpotifyConfiguration, requestedScopes: [String]) -> RxStreamingSpotifyManager {
        self._configuration = configuration
        let auth = SPTAuth.defaultInstance()
        auth.clientID = configuration.clientId
        auth.requestedScopes = requestedScopes
        auth.redirectURL = configuration.redirectURL
        auth.tokenSwapURL = configuration.tokenSwapURL
        auth.tokenRefreshURL = configuration.tokenRefreshURL
        auth.sessionUserDefaultsKey = configuration.userDefaultsKey
        spotifyStatus(initializePlayer: true)
        return self
    }
    public func application(_ app: UIApplication, openURL url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let auth = SPTAuth.defaultInstance()
        guard auth.canHandle(url) == true else {
            return false
        }
        self.rxAuthStatus.accept(.waitingForAuthorizationCallback)
        auth.handleAuthCallback(withTriggeredAuthURL: url) { [unowned self] (error, session) in
            guard error == nil else {
                self.rxAuthStatus.accept(.notConnected)
                auth.session = nil
                return
            }
            self.rxAuthStatus.accept(.connected(accessToken: session?.accessToken, refreshToken: session?.encryptedRefreshToken))
            self.prepareToPlay()
        }
        return true
    }
    public func spotifyStatus(initializePlayer: Bool = false) {
        switch rxAuthStatus.value {
        case .checking, .waitingForAuthorizationCallback:
            return
        default:
            rxAuthStatus.accept(.checking)
        }
        let auth = SPTAuth.defaultInstance()
        guard let session = auth.session else {
            rxAuthStatus.accept(.notConnected)
            return
        }
        guard session.isValid() == true else {
            guard auth.hasTokenRefreshService == true else {
                rxAuthStatus.accept(.error(.missingTokenRefreshService))
                return
            }
            auth.renewSession(session) { [unowned self] error, newSession in
                guard error == nil else {
                    self.rxAuthStatus.accept(.notConnected)
                    auth.session = nil
                    return
                }
                self.rxAuthStatus.accept(.connected(accessToken: newSession?.accessToken, refreshToken: newSession?.encryptedRefreshToken))
                self.prepareToPlay()
            }
            return
        }
        self.rxAuthStatus.accept(.connected(accessToken: session.accessToken, refreshToken: session.encryptedRefreshToken))
        if initializePlayer == true {
            self.prepareToPlay()
        }
    }
    public func authorizationViewController() -> SFSafariViewController {
        let viewController = SFSafariViewController(url: SPTAuth.defaultInstance().spotifyWebAuthenticationURL())
        return viewController
    }
    public func startedAuthorization() {
        rxAuthStatus.accept(.checking)
    }
    public func setupAuthentication() -> Single<String> {
        return Single<String>.create { [unowned self] subscriber in
            var localDisposeBag = DisposeBag()
            if case .connected = self.rxAuthStatus.value,
                let session = SPTAuth.defaultInstance().session,
                session.isValid() == true {
                subscriber(.success(session.accessToken))
                return Disposables.create()
            }
            self.rxAuthStatus.skipWhile {
                if case .connected = $0 { return false }
                return true
            }
            .take(1)
            .subscribe(
                onNext: { _ in
                    guard let accessToken = SPTAuth.defaultInstance().session?.accessToken else {
                        subscriber(.error(AuthError.missingAccessToken))
                        return
                    }
                    subscriber(.success(accessToken))
                },
                onError: { error in
                    subscriber(.error(error))
                },
                onCompleted: {
                    localDisposeBag = DisposeBag()
                }
            )
            .disposed(by: localDisposeBag)
            DispatchQueue.main.async { [unowned self] in
                self.spotifyStatus()
            }
            return Disposables.create()
        }
    }
}
extension RxStreamingSpotifyManager {
    private func setupPlayback(accessToken: String) -> Single<SPTAudioStreamingController> {
        return Single<SPTAudioStreamingController>.create { [unowned self] subscriber in
            var localDisposeBag = DisposeBag()
            
            guard let player = self.player else {
                self.player = SPTAudioStreamingController.sharedInstance()
                if SPTAudioStreamingController.sharedInstance().initialized == false {
                    do {
                        try self.player?.start(withClientId: self._configuration.clientId,
                                               audioController: nil,
                                               allowCaching: true)
                    } catch let error {
                        subscriber(.error(error))
                        self.player = nil
                    }
                }
                self.player?.delegate = self
                self.player?.playbackDelegate = self
                self.player?.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
                self.playerLoggedIn
                    .delay(RxTimeInterval.seconds(10), scheduler: MainScheduler.instance)
                    .subscribe(
                        onNext: { loggedIn in
                            guard loggedIn == true, let player = self.player else {
                                self.player = nil
                                subscriber(.error(AuthError.noPlayerInitialized))
                                return
                            }
                            subscriber(.success(player))
                        },
                        onError: { error in
                            dump(error)
                        },
                        onCompleted: {
                            localDisposeBag = DisposeBag()
                        }
                    )
                    .disposed(by: localDisposeBag)
                self.player?.login(withAccessToken: accessToken)
                return Disposables.create()
            }
            subscriber(.success(player))
            return Disposables.create()
        }
    }
    private func _play(uri: String, trackIdx: UInt, position: TimeInterval) -> (SPTAudioStreamingController) -> Single<Bool> {
        return { [unowned self] player in
            if case .preparingToPlay = self.rxPlayerStatus.value {
                return Single.just(false)
            }
            self.rxPlayerStatus.accept(.preparingToPlay)
            return Single<Bool>.create { subscriber in
                var localDisposeBag = DisposeBag()
                self.rxPlayerStatus.skipWhile {
                    if case .playing = $0 { return false }
                    return true
                }
                .timeout(RxTimeInterval.seconds(10), scheduler: MainScheduler.instance)
                .take(1)
                .subscribe(
                    onNext: { _ in
                        subscriber(.success(true))
                    },
                    onError: { error in
                        subscriber(.error(error))
                    },
                    onCompleted: {
                        localDisposeBag = DisposeBag()
                    })
                .disposed(by: localDisposeBag)
                DispatchQueue.main.async { [unowned self] in
                    player.playSpotifyURI(uri, startingWith: trackIdx, startingWithPosition: position) { [unowned self] error in
                        guard let error = error else {
                            self.rxPlayerStatus.accept(.playing)
                            return
                        }
                        self.rxPlayerStatus.accept(.error(error))
                    }
                }
                return Disposables.create()
            }
            
        }
    }
    private func setupPlayer() -> Single<SPTAudioStreamingController> {
        return setupAuthentication()
            .flatMap { [unowned self] token in
                return self.setupPlayback(accessToken: token)
            }
            .flatMap { configured in
                return Single.just(configured)
            }
    }
    private func prepareToPlay() {
        guard let session = SPTAuth.defaultInstance().session else {
            rxAuthStatus.accept(.notConnected)
            return
        }
        var localDisposeBag = DisposeBag()
        self.setupPlayback(accessToken: session.accessToken)
            .subscribe(
                onSuccess: { [unowned self] _ in
                    let auth = SPTAuth.defaultInstance()
                    self.rxAuthStatus.accept(.connected(accessToken: session.accessToken, refreshToken: session.encryptedRefreshToken))
                    auth.session = session
                    localDisposeBag = DisposeBag()
                },
                onError: { error in
                    self.rxAuthStatus.accept(.notConnected)
                    localDisposeBag = DisposeBag()
                })
            .disposed(by: localDisposeBag)
    }
}
public extension RxStreamingSpotifyManager {
    func playSpotifyEntry(uri: String) -> Single<Bool> {
        return setupPlayer().flatMap { [unowned self] player in
            self.requestedItemUri = uri
            return self._play(uri: uri, trackIdx: 0, position: 0)(player)
        }
    }
}
extension RxStreamingSpotifyManager {
    public static func user(accessToken: String) -> Observable<SpotifyUserInfo> {
        return SpotifyRemoteEntry.user.value(token: accessToken, query: nil, decodable: SpotifyUserInfo.self).take(1).observeOn(MainScheduler.instance)
    }
    public static func playlists(authenticationHandler: @escaping () -> Single<String>) -> Observable<([SpotifyPlaylistEntry], SpotifyResponseStatus)> {
        return Observable.create {  subscriber -> Disposable in
            var internalDisposeBag = DisposeBag()
            id(authenticationHandler: authenticationHandler)
                .flatMap { id -> Observable<([SpotifyPlaylistEntry], SpotifyResponseStatus)> in
                    return SpotifyRemoteEntry.playlist(userId: id).rxRequest(authenticationHandler: authenticationHandler,
                                                                      partialObserver: subscriber,
                                                                      itemsKeyPath: \SpotifyUserPlaylistPaging.items)
                }
                .subscribe(
                    onCompleted: {
                        subscriber.onCompleted()
                        internalDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    public static func artists(authenticationHandler: @escaping () -> Single<String>) -> Observable<([SpotifyArtistEntry], SpotifyResponseStatus)> {
        return Observable.create { subscriber -> Disposable in
            var internalDisposeBag = DisposeBag()
            SpotifyRemoteEntry.artist.rxRequest(authenticationHandler: authenticationHandler,
                                                partialObserver: subscriber,
                                                itemsKeyPath: \SpotifyArtistsRecordsPaging.artists.items)
                .subscribe(
                    onCompleted: {
                        subscriber.onCompleted()
                        internalDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    public static func albums(authenticationHandler: @escaping () -> Single<String>) -> Observable<([SpotifyUserAlbumEntry], SpotifyResponseStatus)> {
        return Observable.create { subscriber in
            var internalDisposeBag = DisposeBag()
            SpotifyRemoteEntry.userAlbums.rxRequest(authenticationHandler: authenticationHandler,
                                                    partialObserver: subscriber,
                                                    itemsKeyPath: \SpotifyUserAlbumsPaging.items)
                .subscribe(
                    onCompleted: {
                        subscriber.onCompleted()
                        internalDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    public static func playlistTracksInfo(authenticationHandler: @escaping () -> Single<String>, playlistId: String) -> Observable<([SpotifyTrackEntry], SpotifyResponseStatus)> {
        return Observable.create { subscriber -> Disposable in
            var internalDisposeBag = DisposeBag()
            SpotifyRemoteEntry.playlistTracksInfo(playlistId: playlistId).rxRequest(authenticationHandler: authenticationHandler,
                                                                                    partialObserver: subscriber,
                                                                                    itemsKeyPath: \SpotifyTracksPaging.items)
            .subscribe(
                onCompleted: {
                    subscriber.onCompleted()
                    internalDisposeBag = DisposeBag()
                }
            )
            .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
    public static func albumTracksInfo(authenticationHandler: @escaping () -> Single<String>, albumId: String) -> Observable<([SpotifyUserAlbumTrackEntry], SpotifyResponseStatus)> {
        return Observable.create { subscriber -> Disposable in
            var internalDisposeBag = DisposeBag()
            SpotifyRemoteEntry.userAlbumTracks(albumId: albumId).rxRequest(authenticationHandler: authenticationHandler,
                                                                           partialObserver: subscriber,
                                                                           itemsKeyPath: \SpotifyUserAlbumsTracksPaging.items)
            .subscribe(
                onCompleted: {
                    subscriber.onCompleted()
                    internalDisposeBag = DisposeBag()
                }
            )
            .disposed(by: internalDisposeBag)
            return Disposables.create()
        }
    }
}
extension SPTPlaybackTrack {
    func nowPlayingInfo(position: TimeInterval?) -> NowPlayableMetadata {
        return NowPlayableMetadata(
                assetURL: URL(fileURLWithPath: ""),
                mediaType: MPNowPlayingInfoMediaType.audio,
                isLiveStream: false,
                title: name,
                artist: artistName,
                artwork: nil,
                albumArtist: artistName,
                albumTitle: albumName,
                duration: duration,
                position: position ?? 0)
    }
}
extension SPTPlaybackMetadata {
    func trackInfo(for uri: String) -> NowPlayableMetadata? {
        let current = [prevTrack, currentTrack, nextTrack]
            .compactMap { $0 }
            .filter { $0.uri == uri }.first
        guard let toBePlayed = current else { return nil }
        return toBePlayed.nowPlayingInfo(position: nil)
    }
}
private extension RxStreamingSpotifyManager {
    private func handleCommand(command: NowPlayableCommand, event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
        switch command {
        case .pause:
            player?.setIsPlaying(false, callback: nil)
        case .play:
            player?.setIsPlaying(true, callback: nil)
        case .nextTrack:
            player?.skipNext { error in
                guard let error = error else { return }
                dump(error)
            }
        case .previousTrack:
            player?.skipPrevious { error in
                guard let error = error else { return }
                dump(error)
            }
        case .changePlaybackPosition:
            guard let event = event as? MPChangePlaybackPositionCommandEvent else { return .commandFailed }
            player?.seek(to: event.positionTime, callback: { error in
                guard let error = error else { return }
                dump(error)
            })
        default:
            break
        }
        
        return .success
    }
    private func handleInterrupt(with interruption: NowPlayableInterruption) {
        switch interruption {
        case .began:
            isInterrupted = true
            guard let player = player, player.playbackState.isPlaying == true else { return }
            player.setIsPlaying(false, callback: { error in
                guard let error = error else { return }
                dump(error)
            })
        case .ended(let shouldPlay):
            isInterrupted = false
            guard let player = player, player.playbackState.isPlaying == false, shouldPlay == true else { return }
            player.setIsPlaying(true, callback: { error in
                guard let error = error else { return }
                dump(error)
            })
        case .failed(let error):
            dump(error)
        }
    }
    private func handleAudioRouteChanged(reason: AVAudioSession.RouteChangeReason) {
        switch reason {
        case .oldDeviceUnavailable:
            player?.setIsPlaying(false, callback: nil)
        default:
                break
        }
    }
}

