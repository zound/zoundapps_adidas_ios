//
//  RxStreamingSpotifyManager+SPTAudioStreamingDelegate.swift
//  RxSpotifyWrapper
//
//  Created by Grzegorz Kiel on 10/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import SwiftSpotifySDK

extension RxStreamingSpotifyManager: SPTAudioStreamingDelegate {
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didReceiveError error: Error) {}
    public func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController) {
        playerLoggedIn.onNext(true)
    }
    public func audioStreamingDidLogout(_ audioStreaming: SPTAudioStreamingController) {
        playerLoggedIn.onNext(false)
    }
    public func audioStreamingDidEncounterTemporaryConnectionError(_ audioStreaming: SPTAudioStreamingController) {}
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didReceiveMessage message: String) {}
    public func audioStreamingDidDisconnect(_ audioStreaming: SPTAudioStreamingController) {}
    public func audioStreamingDidReconnect(_ audioStreaming: SPTAudioStreamingController) {}
}
