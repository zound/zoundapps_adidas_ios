//
//  RxStreamingSpotifyManager+SPTAudioStreamingPlaybackDelegate.swift
//  RxSpotifyWrapper
//
//  Created by Grzegorz Kiel on 10/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import SwiftSpotifySDK
import MediaPlayer

extension RxStreamingSpotifyManager: SPTAudioStreamingPlaybackDelegate {
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didReceive event: SpPlaybackEvent) {
        handle(event: event)
    }
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didChangePosition position: TimeInterval) {
        guard let currentTrack = metadata?.currentTrack else { return }
        nowPlayableBehavior.handleNowPlayableItemChange(metadata: currentTrack.nowPlayingInfo(position: position))
        currentPosition = position
    }
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didChangePlaybackStatus isPlaying: Bool) {
        guard isPlaying == true else {
            return
        }
        guard let currentTrack = metadata?.currentTrack else { return }
        nowPlayableBehavior.handleNowPlayableItemChange(metadata: currentTrack.nowPlayingInfo(position: currentPosition))
    }
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didSeekToPosition position: TimeInterval) { }
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didChange metadata: SPTPlaybackMetadata) {
        self.metadata = metadata
        guard let currentTrack = metadata.currentTrack else {
            return
        }
        nowPlayableBehavior.handleNowPlayableItemChange(metadata: currentTrack.nowPlayingInfo(position: nil))
    }
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didStartPlayingTrack trackUri: String) {
        guard let trackInfo = metadata?.trackInfo(for: trackUri) else { return }
        nowPlayableBehavior.handleNowPlayableItemChange(metadata: trackInfo)
        player?.setIsPlaying(true, callback: { error in
            guard let error = error else { return }
            dump(error)
        })
    }
    public func audioStreaming(_ audioStreaming: SPTAudioStreamingController, didStopPlayingTrack trackUri: String) {}
    public func audioStreamingDidSkip(toNextTrack audioStreaming: SPTAudioStreamingController) {
        currentItemIdx = currentItemIdx.advanced(by: 1)
    }
    public func audioStreamingDidSkip(toPreviousTrack audioStreaming: SPTAudioStreamingController) {
        guard currentItemIdx > 0 else { return }
        _ = currentItemIdx.subtractingReportingOverflow(1)
    }
    public func audioStreamingDidBecomeActivePlaybackDevice(_ audioStreaming: SPTAudioStreamingController) {
    }
    public func audioStreamingDidBecomeInactivePlaybackDevice(_ audioStreaming: SPTAudioStreamingController) {
    }
    public func audioStreamingDidLosePermission(forPlayback audioStreaming: SPTAudioStreamingController) {
    }
    public func audioStreamingDidPopQueue(_ audioStreaming: SPTAudioStreamingController)  {
    }
    private func handle(event: SpPlaybackEvent) {}
}
