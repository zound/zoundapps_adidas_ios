//
//  FreemanFirmwareRevisionTests.swift
//  AdidasHeadphonesTests
//
//  Created by Wudarski Lukasz on 27/09/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import XCTest
@testable import AdidasHeadphones

class FreemanFirmwareRevisionTests: XCTestCase {
    func testInitNil() {
        XCTAssertNil(FreemanFirmwareRevision(""))
        XCTAssertNil(FreemanFirmwareRevision("0.0.0"))
        XCTAssertNil(FreemanFirmwareRevision("0.0.0.0.0"))
        XCTAssertNil(FreemanFirmwareRevision("."))
        XCTAssertNil(FreemanFirmwareRevision(".0."))
    }
    func testInitNotNil() {
        XCTAssertNotNil(FreemanFirmwareRevision("0.0.0.0"))
    }
    func testGreater() {
        XCTAssertGreaterThan(FreemanFirmwareRevision("0.0.0.1")!, FreemanFirmwareRevision("0.0.0.0")!)
        XCTAssertGreaterThan(FreemanFirmwareRevision("0.0.1.0")!, FreemanFirmwareRevision("0.0.0.0")!)
        XCTAssertGreaterThan(FreemanFirmwareRevision("0.1.0.0")!, FreemanFirmwareRevision("0.0.0.0")!)
        XCTAssertGreaterThan(FreemanFirmwareRevision("1.0.0.0")!, FreemanFirmwareRevision("0.0.0.0")!)
        XCTAssertGreaterThan(FreemanFirmwareRevision("0.0.1.0")!, FreemanFirmwareRevision("0.0.0.1")!)
        XCTAssertGreaterThan(FreemanFirmwareRevision("0.1.0.0")!, FreemanFirmwareRevision("0.0.1.1")!)
        XCTAssertGreaterThan(FreemanFirmwareRevision("1.0.0.0")!, FreemanFirmwareRevision("0.1.1.1")!)
    }
    func testLess() {
        XCTAssertLessThan(FreemanFirmwareRevision("0.0.0.0")!, FreemanFirmwareRevision("0.0.0.1")!)
        XCTAssertLessThan(FreemanFirmwareRevision("0.0.0.0")!, FreemanFirmwareRevision("0.0.1.0")!)
        XCTAssertLessThan(FreemanFirmwareRevision("0.0.0.0")!, FreemanFirmwareRevision("0.1.0.0")!)
        XCTAssertLessThan(FreemanFirmwareRevision("0.0.0.0")!, FreemanFirmwareRevision("1.0.0.0")!)
        XCTAssertLessThan(FreemanFirmwareRevision("0.0.0.1")!, FreemanFirmwareRevision("0.0.1.0")!)
        XCTAssertLessThan(FreemanFirmwareRevision("0.0.1.1")!, FreemanFirmwareRevision("0.1.0.0")!)
        XCTAssertLessThan(FreemanFirmwareRevision("0.1.1.1")!, FreemanFirmwareRevision("1.0.0.0")!)
    }
    func testEqual() {
        XCTAssertEqual(FreemanFirmwareRevision("0.0.0.0"), FreemanFirmwareRevision("0.0.0.0"))
    }
    func testNotEqual() {
        XCTAssertNotEqual(FreemanFirmwareRevision("0.0.0.0"), FreemanFirmwareRevision("1.1.1.1"))
    }
}
