submodules:
	git submodule sync --recursive || true
	git submodule update --init --recursive || true

strings:
	swiftc bin/main.swift InternalFrameworks/AppLibrary/Locales/GenerateStrings.swift -o bin/prepareTranslations
	bin/prepareTranslations
	rm bin/prepareTranslations

